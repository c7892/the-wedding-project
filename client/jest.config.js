// eslint-disable-next-line no-undef
module.exports = {
    roots: ["<rootDir>/src"],
    transform: {
        "^.+\\.tsx?$": "ts-jest",
        "^.+\\.jsx?$": "babel-jest",
        "^.+\\.js$": "babel-jest",
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
            "<rootDir>/fileTransformer.js",
    },
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|tsx?)$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    snapshotSerializers: ["enzyme-to-json/serializer"],
    collectCoverage: true,
    collectCoverageFrom: [
        "**/*.{ts,tsx}",
        "!**/__tests__",
        "!src/reportWebVitals.ts",
        "!src/service-worker.ts",
        "!src/setupTests.ts",
        "!src/serviceWorkerRegistration.ts",
        "!src/decs.d.ts",
        "!src/react-app-env.d.ts",
        "!src/jest-setup-file.ts",
        "!**/node_modules/**",
    ],
    coverageDirectory: "./jest-coverage",
    setupFilesAfterEnv: ["./src/jest-setup-file.ts"],
    moduleDirectories: ["node_modules"],
    moduleNameMapper: {
        "\\.(css|less)$": "identity-obj-proxy",
    },
};
