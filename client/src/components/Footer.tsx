import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const footerLinks = [
    {
        link: "https://codinghouse.dev",
        text: "Contact",
    },
    {
        link: "https://projects.codinghouse.dev/project/yorel56-the-house-project/kanban",
        text: "Backlog",
    },
    {
        link: "https://gitlab.com/c7892/the-wedding-project",
        text: "Source Code",
    },
];
export const Footer = () => {
    return (
        <footer className="footer mt-auto py-3 bg-dark" id="weddingFooter">
            <Container fluid>
                <Row>
                    {footerLinks.map((link, index) => (
                        <Col
                            key={index}
                            xs={12 / footerLinks.length}
                            style={{ justifyContent: "center", textAlign: "center" }}>
                            <a href={link.link}>{link.text}</a>
                        </Col>
                    ))}
                </Row>
            </Container>
        </footer>
    );
};
