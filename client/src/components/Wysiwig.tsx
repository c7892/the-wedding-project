import React, { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import { QuillDeltaToHtmlConverter } from "quill-delta-to-html";
import { DeltaOperation } from "quill";
import "react-quill/dist/quill.snow.css";

const toolbarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "link"],
    [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }], // text direction

    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ script: "sub" }, { script: "super" }],
    [{ align: [] }],
    ["image", "video"],
    ["clean"],
];
const formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
];

interface IOwnProps {
    readonly?: boolean;
    defaultValue?: string;
    onTextChange?: (newText: string) => void;
}

// eslint-disable-next-line react/display-name
export const Wysiwig = React.forwardRef<ReactQuill, IOwnProps>(({ defaultValue, ...props }: IOwnProps, ref) => {
    const [value, setValue] = useState(defaultValue ?? "");
    useEffect(() => {
        setValue(defaultValue ?? "");
    }, [defaultValue]);
    return (
        <ReactQuill
            ref={ref}
            theme="snow"
            modules={{ toolbar: toolbarOptions }}
            formats={formats}
            readOnly={props.readonly}
            onChange={(content) => {
                setValue(content);
            }}
            defaultValue={defaultValue ?? ""}
            value={value}
        />
    );
});

const plainToolbarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "link"],
    [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }], // text direction

    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ script: "sub" }, { script: "super" }],
    [{ align: [] }],
    ["clean"],
];
const plainFormats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
];

interface NoMultiMediaWysiwigProps extends IOwnProps {
    shouldUpdateDefaultValue?: boolean;
}

// eslint-disable-next-line react/display-name
export const NoMultiMediaWysiwig = React.forwardRef<ReactQuill, NoMultiMediaWysiwigProps>(
    ({ defaultValue, shouldUpdateDefaultValue = false, ...props }: NoMultiMediaWysiwigProps, ref) => {
        const [value, setValue] = useState(defaultValue ?? "");
        useEffect(() => {
            if (shouldUpdateDefaultValue === true) {
                setValue(defaultValue ?? "");
            }
        }, [defaultValue, shouldUpdateDefaultValue]);
        return (
            <ReactQuill
                ref={ref}
                theme="snow"
                modules={{ toolbar: plainToolbarOptions }}
                formats={plainFormats}
                readOnly={props.readonly}
                onChange={(content) => {
                    setValue(content);
                    if (props.onTextChange) {
                        props.onTextChange(content);
                    }
                }}
                defaultValue={defaultValue ?? ""}
                value={value}
            />
        );
    },
);

export const convertWysiwigTextToString = (ops: DeltaOperation[]) => {
    const cfg = {
        encodeHtml: true,
        inlineStyles: true,
    };
    const converter = new QuillDeltaToHtmlConverter(ops, cfg);
    return converter.convert();
};
