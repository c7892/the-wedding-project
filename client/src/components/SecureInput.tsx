/* eslint-disable react/display-name */
import React, { SyntheticEvent, useState } from "react";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import zxcvbn from "zxcvbn";

interface IOwnProps {
    fieldTitle: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    renderAs?: any;
    minimumLength: number;
    minimumLengthErrorText: string;
    weakErrorText: string;
    disabled?: boolean;
}

export const SecureInput = React.forwardRef<HTMLInputElement, IOwnProps>((props: IOwnProps, ref) => {
    if (props.minimumLength <= 0) {
        throw Error("invalid minimum length");
    }
    const [showPassword, setShowPassword] = useState(false);
    const [isInvalid, setIsInvalid] = useState(false);
    const [invalidText, setInvalidText] = useState("");
    const checkPassword = (e: SyntheticEvent) => {
        const value = (e.target as HTMLInputElement).value;
        if (!value || value.length === 0) {
            setIsInvalid(false);
            setInvalidText("");
            return;
        }
        if (value.length < props.minimumLength) {
            setIsInvalid(true);
            setInvalidText(props.minimumLengthErrorText);
            return;
        }
        const results = zxcvbn(value);
        if (results.score < 3) {
            setIsInvalid(true);
            setInvalidText(props.weakErrorText);
            return;
        }
        setIsInvalid(false);
        setInvalidText("");
    };
    return (
        <Form.Group as={props.renderAs}>
            <Form.Label>{props.fieldTitle}</Form.Label>
            <InputGroup>
                <Form.Control
                    type={showPassword ? "text" : "password"}
                    placeholder="Password"
                    // eslint-disable-next-line react/prop-types
                    ref={ref}
                    required
                    isInvalid={isInvalid}
                    onChange={checkPassword}
                    disabled={props.disabled}
                />
                <InputGroup.Append>
                    <InputGroup.Text
                        id={"passwordInput"}
                        onClick={() => setShowPassword(!showPassword)}
                        style={showPassword ? { color: "orange" } : undefined}>
                        <FontAwesomeIcon icon={showPassword ? faEye : faEyeSlash} />
                    </InputGroup.Text>
                </InputGroup.Append>
                <Form.Control.Feedback type="invalid">{invalidText}</Form.Control.Feedback>
            </InputGroup>
        </Form.Group>
    );
});
