import React from "react";
import { useStore } from "react-redux";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { container } from "tsyringe";
import { RouteKey } from "../../routing";
import { AuthenticationSelector } from "../../store/selectors";

/**
 * A route HOC that redirects the user to the login screen if they aren't authenticated
 * @category Higher Order Components
 * @component
 * @param {Object} props - Route Properties
 */
export const PrivateRoute = (props: RouteProps) => {
    const { component, ...rest } = props;
    const state = useStore().getState();
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const isAuthenticated = authenticationSelector.selectIsAuthenticated(state);
    return (
        <Route
            {...rest}
            render={({ location }) => {
                return isAuthenticated ? (
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    React.createElement(component as any, rest)
                ) : (
                    <Redirect
                        to={{
                            pathname: RouteKey.Login,
                            state: { from: location },
                        }}
                    />
                );
            }}
        />
    );
};
