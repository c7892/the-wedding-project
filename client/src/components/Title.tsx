import React from "react";
import { Helmet } from "react-helmet";
import { container } from "tsyringe";
import { IConfig } from "../utilities";

interface IOwnProps {
    title?: string;
}

export const Title = ({ title }: IOwnProps) => {
    const config = container.resolve<IConfig>("Config");
    return (
        <Helmet>
            <title>
                {config.AppName}
                {title && title.length > 0 ? ` - ${title}` : ""}
            </title>
        </Helmet>
    );
};
