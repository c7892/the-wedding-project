import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import ToolTip from "react-bootstrap/Tooltip";
import Button from "react-bootstrap/Button";
import { guestSideNavRoutes, RouteKey, spouseSideNavRoutes } from "../../routing";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { container } from "tsyringe";
import { AuthenticationSelector } from "../../store/selectors";

export const SideNavigationBar = () => {
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const history = useHistory();
    const weddingCode = useSelector(authenticationSelector.selectWeddingCode);
    const userId = useSelector(authenticationSelector.selectUserId);
    const isGuest = useSelector(authenticationSelector.selectIsGuest);
    const navigate = (path: string) => {
        if (isGuest) {
            history.push(`${RouteKey.GuestBase}${weddingCode}/${userId}${path}`);
        } else {
            history.push(`${RouteKey.SpouseBase}${weddingCode}${path}`);
        }
    };
    const routes = isGuest ? guestSideNavRoutes : spouseSideNavRoutes;
    return (
        <div className="sidebar d-none d-md-block">
            <Container className="sticky">
                {routes.map((route, index) => {
                    return (
                        <Row key={index}>
                            <Col className="sidenav-icon-container">
                                <Button
                                    variant="secondary"
                                    style={{
                                        minWidth: "69px",
                                    }}>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={(props) => {
                                            return (
                                                <ToolTip id={`sidenav-${index}`} {...props}>
                                                    {route.label}
                                                </ToolTip>
                                            );
                                        }}>
                                        <FontAwesomeIcon
                                            icon={route.icon}
                                            size={"2x"}
                                            className="icon-btn-primary"
                                            onClick={() => {
                                                navigate(route.to);
                                            }}
                                        />
                                    </OverlayTrigger>
                                </Button>
                            </Col>
                        </Row>
                    );
                })}
            </Container>
        </div>
    );
};
