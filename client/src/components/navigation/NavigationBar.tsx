import React, { PureComponent, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { guestSideNavRoutes, RouteKey, spouseSideNavRoutes } from "../../routing";
import { ApplicationReduxState, AuthenticationActionCreator, LogoutAction, ReauthTokenAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector } from "../../store/selectors";
import { connect } from "react-redux";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Guest, User } from "../../models";

interface IStateProps {
    isAuthenticated: boolean;
    user?: User | Guest;
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
}

interface IDispatchProps {
    logout: () => LogoutAction;
    reauth: () => ReauthTokenAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class NavigationBarComponent extends PureComponent<Props> {
    private authTimer: NodeJS.Timeout | undefined;
    public componentDidMount() {
        this.setupAuthTimer();
    }
    public componentWillUnmount() {
        if (this.authTimer !== undefined) {
            clearInterval(this.authTimer);
        }
    }
    public componentDidUpdate(prevProps: Props) {
        if ((prevProps.isAuthenticated && !this.props.isAuthenticated) || (prevProps.user && !this.props.user)) {
            this.props.logout();
            this.props.history.push(RouteKey.LandingPage);
            if (this.authTimer !== undefined) {
                clearInterval(this.authTimer);
            }
        } else if (!prevProps.isAuthenticated && this.props.isAuthenticated) {
            this.setupAuthTimer();
        }
    }
    public render() {
        const location = this.props.location;
        const routes = this.props.isGuest ? guestSideNavRoutes : spouseSideNavRoutes;
        return location.pathname.includes(RouteKey.ViewArInvitationBase) ? null : (
            <Navbar bg="primary" expand="lg" variant={"dark"}>
                <Container>
                    <Navbar.Brand href={RouteKey.LandingPage}>
                        <img
                            src="/android-chrome-192x192.png"
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                            alt="Coding House LLC"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" className="d-md-none " />
                    <Navbar.Collapse id="responsive-navbar-nav" className="d-md-none">
                        <Nav className="mr-auto d-md-none">
                            {this.props.isAuthenticated
                                ? routes.map((route, index) => {
                                      return (
                                          <Nav.Link
                                              key={index}
                                              onClick={(e: SyntheticEvent) => {
                                                  e.preventDefault();
                                                  this.navigate(route.to);
                                              }}>
                                              <FontAwesomeIcon icon={route.icon} />
                                              &nbsp;{route.label}
                                          </Nav.Link>
                                      );
                                  })
                                : null}
                            <hr />
                            {this.props.isAuthenticated ? (
                                <Nav.Link
                                    as={Button}
                                    className="btn-secondary"
                                    onClick={(e: SyntheticEvent) => {
                                        e.preventDefault();
                                        this.props.logout();
                                    }}>
                                    Logout
                                </Nav.Link>
                            ) : null}
                        </Nav>
                    </Navbar.Collapse>
                    <Nav className="d-none d-md-block">
                        {this.props.isAuthenticated ? (
                            <Nav.Link
                                as={Button}
                                className="btn-secondary float-right"
                                onClick={(e: SyntheticEvent) => {
                                    e.preventDefault();
                                    this.props.logout();
                                    this.props.history.push(RouteKey.LandingPage);
                                }}>
                                Logout
                            </Nav.Link>
                        ) : null}
                    </Nav>
                </Container>
            </Navbar>
        );
    }
    private setupAuthTimer = () => {
        if (this.authTimer !== undefined) {
            clearInterval(this.authTimer);
        }
        this.authTimer = setInterval(this.props.reauth, 5 * 60 * 1000);
    };
    private navigate = (path: string) => {
        if (this.props.isGuest) {
            this.props.history.push(`${RouteKey.GuestBase}${this.props.weddingCode}/${this.props.userId}${path}`);
        } else {
            this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}${path}`);
        }
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isAuthenticated: authenticationSelector.selectIsAuthenticated(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        user: authenticationSelector.selectUser(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    logout: authenticationActionCreator.logout,
    reauth: authenticationActionCreator.reauth,
};

export const NavigationBar = connect(mapStateToProps, mapDispatchToProps)(withRouter(NavigationBarComponent));
