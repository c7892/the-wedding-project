import React, { PureComponent } from "react";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import { container } from "tsyringe";
import { AddressValidator as AV, Address } from "../utilities";
import { isEmpty } from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationArrow } from "@fortawesome/free-solid-svg-icons";

interface IOwnState {
    typingTimeout?: NodeJS.Timeout;
    altAddresses: Address[];
}

interface IOwnProps {
    disabled?: boolean;
    defaultValue?: string;
    label?: string;
    required?: boolean;
}

type Props = IOwnProps;

export class AddressValidator extends PureComponent<Props, IOwnState> {
    private readonly address = React.createRef<HTMLInputElement>();
    private readonly validation = container.resolve(AV);

    public constructor(props: Props) {
        super(props);
        this.state = {
            typingTimeout: undefined,
            altAddresses: [],
        };
    }

    public render() {
        return (
            <Form.Group>
                <Form.Label>{this.props.label ?? "Address"}</Form.Label>
                <Form.Control
                    type="text"
                    placeholder={!this.props.required ? "Optional" : "Required"}
                    required={this.props.required}
                    ref={this.address}
                    onChange={this.onChange}
                    disabled={this.props.disabled}
                    defaultValue={this.props.defaultValue}
                />
                {isEmpty(this.state.altAddresses) ? null : (
                    <Row>
                        <Col>
                            <em>Did you mean?</em>
                            <ListGroup id={"altAddresses"}>
                                {this.state.altAddresses.map((address, index) => {
                                    return (
                                        <ListGroup.Item key={index} onClick={() => this.setAddress(address)}>
                                            <FontAwesomeIcon icon={faLocationArrow} />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            {address.toString()}
                                        </ListGroup.Item>
                                    );
                                })}
                            </ListGroup>
                        </Col>
                    </Row>
                )}
            </Form.Group>
        );
    }

    public getAddress = () => this.address.current?.value;

    private setAddress = (address: Address) => {
        if (this.props.disabled) {
            return;
        }
        if (this.address.current) {
            this.address.current.value = address.toString() ?? "";
        }
    };

    private onChange = () => {
        if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
        }
        this.setState({
            typingTimeout: setTimeout(this.checkAddressAsync, 1500),
        });
    };
    private checkAddressAsync = async () => {
        const address = this.address.current?.value;
        if (!address) {
            this.setState({ altAddresses: [] });
            return;
        }
        try {
            const results = await this.validation.validate(address);
            const alts = (results?.exact ?? []).concat(results?.inexact ?? []);
            this.setState({ altAddresses: alts });
        } catch (err) {
            this.setState({ altAddresses: [] });
            return;
        }
    };
}
