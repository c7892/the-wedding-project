import { Guest, SpouseRegistrationInformation, User } from "../../models";
import { IAction } from "./IAction";

export enum AuthenticationReducerKeys {
    ReducerName = "Authentication",
    IsLoggingIn = "IsLoggingIn",
    IsRegistering = "IsRegistering",
    LoginFailedMessage = "LoginFailedMessage",
    RegistrationFailedMessage = "RegistrationFailedMessage",
    RegistrationSucceeded = "RegistrationSucceeded",
    LoginSucceeded = "LoginSucceeded",
    User = "User",
    Spouse = "Spouse",
    IsSavingUser = "IsSavingUser",
    SavingError = "SavingError",
}

export interface AuthenticationStore {
    [AuthenticationReducerKeys.IsLoggingIn]: boolean | undefined;
    [AuthenticationReducerKeys.IsRegistering]: boolean | undefined;
    [AuthenticationReducerKeys.LoginFailedMessage]: string | undefined;
    [AuthenticationReducerKeys.RegistrationFailedMessage]: string | undefined;
    [AuthenticationReducerKeys.RegistrationSucceeded]: boolean | undefined;
    [AuthenticationReducerKeys.LoginSucceeded]: boolean | undefined;
    [AuthenticationReducerKeys.User]: Guest | User | undefined;
    [AuthenticationReducerKeys.Spouse]: User[] | undefined;
    [AuthenticationReducerKeys.SavingError]: string | undefined;
    [AuthenticationReducerKeys.IsSavingUser]: boolean;
}

export interface AuthenticationState {
    [AuthenticationReducerKeys.ReducerName]: AuthenticationStore;
}

export enum AuthenticationActionTypes {
    SET_IS_LOGGING_IN = "SET_IS_LOGGING_IN",
    SET_IS_REGISTERING = "SET_IS_REGISTERING",
    SET_LOGIN_FAILED_MESSAGE = "SET_LOGIN_FAILED_MESSAGE",
    SET_REGISTRATION_FAILED_MESSAGE = "SET_REGISTRATION_FAILED_MESSAGE",
    SET_REGISTRATION_SUCCEEDED = "SET_REGISTRATION_SUCCEEDED",
    SET_LOGIN_SUCCEEDED = "SET_LOGIN_SUCCEEDED",
    LOGOUT = "LOGOUT",
    LOGIN = "LOGIN",
    GUEST_LOGIN = "GUEST_LOGIN",
    REGISTER = "REGISTER",
    SET_USER = "SET_USER",
    SET_SPOUSE = "SET_SPOUSE",
    FETCH_USER = "FETCH_USER",
    CLEAR_AUTH_PROCESS_DATA = "CLEAR_AUTH_PROCESS_DATA",
    REAUTH_TOKEN = "REAUTH_TOKEN",
    UPDATE_USER = "UPDATE_USER",
    SET_IS_SAVING_USER = "SET_IS_SAVING_USER",
    SET_SAVING_ERROR = "SET_SAVING_ERROR",
    CHANGE_PASSWORD = "CHANGE_PASSWORD",
    DELETE_ACCOUNT = "DELETE_ACCOUNT",
}

export type SetIsLoggingInAction = IAction<boolean | undefined, AuthenticationActionTypes.SET_IS_LOGGING_IN>;
export type SetIsRegisteringAction = IAction<boolean | undefined, AuthenticationActionTypes.SET_IS_REGISTERING>;
export type SetLoginFailedMessageAction = IAction<
    string | undefined,
    AuthenticationActionTypes.SET_LOGIN_FAILED_MESSAGE
>;
export type SetRegistrationFailedMessageAction = IAction<
    string | undefined,
    AuthenticationActionTypes.SET_REGISTRATION_FAILED_MESSAGE
>;
export type SetRegistrationSucceededAction = IAction<
    boolean | undefined,
    AuthenticationActionTypes.SET_REGISTRATION_SUCCEEDED
>;
export type SetLoginSucceededAction = IAction<boolean | undefined, AuthenticationActionTypes.SET_LOGIN_SUCCEEDED>;
export type SetUserAction = IAction<Guest | User | undefined, AuthenticationActionTypes.SET_USER>;
export type SetSpouseAction = IAction<User[] | undefined, AuthenticationActionTypes.SET_SPOUSE>;
export type LogoutAction = IAction<undefined, AuthenticationActionTypes.LOGOUT>;
export type LoginAction = IAction<{ email: string; password: string }, AuthenticationActionTypes.LOGIN>;
export type GuestLoginAction = IAction<{ email: string; inviteCode: string }, AuthenticationActionTypes.GUEST_LOGIN>;
export type RegisterAction = IAction<SpouseRegistrationInformation[], AuthenticationActionTypes.REGISTER>;
export type ClearAuthProcessDataAction = IAction<undefined, AuthenticationActionTypes.CLEAR_AUTH_PROCESS_DATA>;
export type ReauthTokenAction = IAction<undefined, AuthenticationActionTypes.REAUTH_TOKEN>;
export type FetchUserAction = IAction<undefined, AuthenticationActionTypes.FETCH_USER>;
export type UpdateUserAction = IAction<{ updates: Guest | User; image?: File }, AuthenticationActionTypes.UPDATE_USER>;
export type SetIsSavingUserAction = IAction<boolean, AuthenticationActionTypes.SET_IS_SAVING_USER>;
export type SetSavingUserErrorAction = IAction<string | undefined, AuthenticationActionTypes.SET_SAVING_ERROR>;
export type ChangePasswordAction = IAction<
    { oldPassword: string; newPassword: string },
    AuthenticationActionTypes.CHANGE_PASSWORD
>;
export type DeleteAccountAction = IAction<undefined, AuthenticationActionTypes.DELETE_ACCOUNT>;

export type UpdateAuthenticationReducerAction =
    | SetIsLoggingInAction
    | SetIsRegisteringAction
    | SetLoginFailedMessageAction
    | SetRegistrationFailedMessageAction
    | SetRegistrationSucceededAction
    | SetLoginSucceededAction
    | SetUserAction
    | SetSpouseAction
    | SetSavingUserErrorAction
    | SetIsSavingUserAction;
