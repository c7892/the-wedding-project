import { Event } from "../../models";
import { IAction } from "./IAction";

export enum EventReducerKeys {
    ReducerName = "Events",
    IsSavingEvent = "IsSavingEvent",
    UserEvents = "UserEvents",
    EventBeingSaved = "EventBeingSaved",
    ArEvent = "ArEvent",
    WeddingDay = "WeddingDay",
    AllEvents = "AllEvents",
}

export interface EventStore {
    [EventReducerKeys.UserEvents]: Event[];
    [EventReducerKeys.IsSavingEvent]: boolean;
    [EventReducerKeys.EventBeingSaved]: Event | undefined;
    [EventReducerKeys.ArEvent]: Event | undefined;
    [EventReducerKeys.WeddingDay]: Event | undefined;
    [EventReducerKeys.AllEvents]: Event[];
}

export interface EventState {
    [EventReducerKeys.ReducerName]: EventStore;
}

export enum EventActionTypes {
    FETCH_EVENTS = "FETCH_EVENTS",
    FETCH_AR_EVENT = "FETCH_AR_EVENT",
    SET_AR_EVENT = "SET_AR_EVENT",
    SET_EVENTS = "SET_EVENTS",
    SET_IS_SAVING_EVENT = "SET_IS_SAVING_EVENT",
    SET_EVENT_BEING_SAVED = "SET_EVENT_BEING_SAVED",
    SAVE_EVENT = "SAVE_EVENT",
    DELETE_EVENT = "DELETE_EVENT",
    RSVP = "RSVP",
    UNRSVP = "UNRSVP",
    FETCH_WEDDING_DAY = "FETCH_WEDDING_DAY",
    SET_WEDDING_DAY = "SET_WEDDING_DAY",
    FETCH_ALL_EVENTS = "FETCH_ALL_ALL_EVENTS",
    SET_ALL_EVENTS = "SET_ALL_EVENTS",
}

export type FetchEventsAction = IAction<Date | undefined, EventActionTypes.FETCH_EVENTS>;
export type SetEventsAction = IAction<Event[], EventActionTypes.SET_EVENTS>;
export type SetArEventAction = IAction<Event | undefined, EventActionTypes.SET_AR_EVENT>;
export type FetchArEventAction = IAction<{ id: string; guest: string }, EventActionTypes.FETCH_AR_EVENT>;
export type SetIsSavingEventAction = IAction<boolean, EventActionTypes.SET_IS_SAVING_EVENT>;
export type SetEventBeingSavedAction = IAction<Event | undefined, EventActionTypes.SET_EVENT_BEING_SAVED>;
export type SaveEventAction = IAction<Event, EventActionTypes.SAVE_EVENT>;
export type DeleteEventAction = IAction<string, EventActionTypes.DELETE_EVENT>;
export type RsvpAction = IAction<{ eventId: string; guests: number }, EventActionTypes.RSVP>;
export type UnRsvpAction = IAction<string, EventActionTypes.UNRSVP>;
export type FetchWeddingDayAction = IAction<undefined, EventActionTypes.FETCH_WEDDING_DAY>;
export type SetWeddingDayAction = IAction<Event | undefined, EventActionTypes.SET_WEDDING_DAY>;
export type FetchAllEventsAction = IAction<undefined, EventActionTypes.FETCH_ALL_EVENTS>;
export type SetAllEventsAction = IAction<Event[], EventActionTypes.SET_ALL_EVENTS>;

export type UpdateEventReducerAction =
    | SetEventBeingSavedAction
    | SetIsSavingEventAction
    | SetEventsAction
    | SetArEventAction
    | SetAllEventsAction
    | SetWeddingDayAction;
