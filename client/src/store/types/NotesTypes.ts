import { Notes } from "../../models";
import { IAction } from "./IAction";

export enum NotesReducerKeys {
    ReducerName = "Notes",
    IsSavingNotes = "IsSavingNotes",
    AllNotes = "AllNotes",
    Note = "Note",
    NoteBeingSaved = "NoteBeingSaved",
}

export interface NotesStore {
    [NotesReducerKeys.IsSavingNotes]: boolean | undefined;
    [NotesReducerKeys.Note]: Notes | undefined;
    [NotesReducerKeys.NoteBeingSaved]: Notes | undefined;
    [NotesReducerKeys.AllNotes]: Notes[];
}

export interface NotesState {
    [NotesReducerKeys.ReducerName]: NotesStore;
}

export enum NotesActionTypes {
    FETCH_ALL_NOTES = "FETCH_ALL_NOTES",
    SET_ALL_NOTES = "SAVE_ALL_NOTES",
    SAVE_NOTES = "SAVE_NOTES",
    SET_IS_SAVING_NOTES = "SET_IS_SAVING_NOTES",
    SET_NOTES_BEING_SAVED = "SET_NOTES_BEING_SAVED",
    FETCH_NOTES = "FETCH_NOTES",
    SET_NOTES = "SET_NOTES",
    DELETE_NOTES = "DELETE_NOTES",
}

export type FetchAllNotesAction = IAction<undefined, NotesActionTypes.FETCH_ALL_NOTES>;
export type SetAllNotesAction = IAction<Notes[], NotesActionTypes.SET_ALL_NOTES>;
export type SaveNotesAction = IAction<Notes, NotesActionTypes.SAVE_NOTES>;
export type SetIsSavingNotesAction = IAction<boolean | undefined, NotesActionTypes.SET_IS_SAVING_NOTES>;
export type SetNotesBeingSavedAction = IAction<Notes | undefined, NotesActionTypes.SET_NOTES_BEING_SAVED>;
export type FetchNotesAction = IAction<string, NotesActionTypes.FETCH_NOTES>;
export type SetNotesAction = IAction<Notes | undefined, NotesActionTypes.SET_NOTES>;
export type DeleteNotesAction = IAction<string, NotesActionTypes.DELETE_NOTES>;

export type UpdateNotesReducerAction =
    | SetAllNotesAction
    | SetIsSavingNotesAction
    | SetNotesBeingSavedAction
    | SetNotesAction;
