import { Registry } from "../../models";
import { IAction } from "./IAction";

export enum RegistryReducerKeys {
    ReducerName = "Registry",
    WeddingRegistry = "WeddingRegistry",
    IsSavingRegistry = "IsSavingRegistry",
    RegistryMessage = "RegistryMessage",
}

export interface RegistryStore {
    [RegistryReducerKeys.RegistryMessage]: string | undefined;
    [RegistryReducerKeys.IsSavingRegistry]: boolean;
    [RegistryReducerKeys.WeddingRegistry]: Registry | undefined;
}

export interface RegistryState {
    [RegistryReducerKeys.ReducerName]: RegistryStore;
}

export enum RegistryActionTypes {
    FETCH_REGISTRY = "FETCH_REGISTRY",
    UPDATE_REGISTRY = "UPDATE_REGISTRY",
    SET_REGISTRY = "SET_REGISTRY",
    SET_IS_SAVING_REGISTRY = "SET_IS_SAVING_REGISTRY",
    SET_REGISTRY_MESSAGE = "SET_REGISTRY_MESSAGE",
}

export type FetchRegistryAction = IAction<undefined, RegistryActionTypes.FETCH_REGISTRY>;
export type UpdateRegistryAction = IAction<Registry, RegistryActionTypes.UPDATE_REGISTRY>;
export type SetRegistryAction = IAction<Registry | undefined, RegistryActionTypes.SET_REGISTRY>;
export type SetIsSavingRegistryAction = IAction<boolean, RegistryActionTypes.SET_IS_SAVING_REGISTRY>;
export type SetRegistryMessageAction = IAction<string | undefined, RegistryActionTypes.SET_REGISTRY_MESSAGE>;

export type UpdateRegistryReducerAction = SetRegistryMessageAction | SetIsSavingRegistryAction | SetRegistryAction;
