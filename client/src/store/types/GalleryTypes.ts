import { Gallery } from "../../models";
import { IAction } from "./IAction";

export enum GalleryReducerKeys {
    ReducerName = "Gallery",
    Galleries = "Galleries",
    GalleryActionMessage = "GalleryActionMessage",
    GalleryDownloadLink = "GalleryDownloadLink",
}

export interface GalleryStore {
    [GalleryReducerKeys.Galleries]: Gallery[];
    [GalleryReducerKeys.GalleryActionMessage]: string | undefined;
    [GalleryReducerKeys.GalleryDownloadLink]: string | undefined;
}

export interface GalleryState {
    [GalleryReducerKeys.ReducerName]: GalleryStore;
}

export enum GalleryActionTypes {
    FETCH_GALLERIES = "FETCH_GALLERIES",
    FETCH_GALLERY = "FETCH_GALLERY",
    SET_GALLERIES = "SET_GALLERIES",
    CREATE_GALLERY = "CREATE_GALLERIES",
    UPDATE_GALLERY = "UPDATE_GALLERY",
    DELETE_GALLERY = "DELETE_GALLERY",
    REMOVE_IMAGE_FROM_GALLERY = "REMOVE_IMAGE_FROM_GALLERY",
    ADD_IMAGE_TO_GALLERY = "ADD_IMAGE_TO_GALLERY",
    SET_GALLERY_ACTION_MESSAGE = "SET_GALLERY_ACTION_MESSAGE",
    DOWNLOAD_GALLERY = "DOWNLOAD_GALLERY",
    SET_DOWNLOAD_GALLERY_LINK = "SET_DOWNLOAD_GALLERY_LINK",
}

export type FetchGalleriesAction = IAction<undefined, GalleryActionTypes.FETCH_GALLERIES>;
export type FetchGalleryAction = IAction<string, GalleryActionTypes.FETCH_GALLERY>;
export type CreateGalleryAction = IAction<Gallery, GalleryActionTypes.CREATE_GALLERY>;
export type UpdateGalleryAction = IAction<Gallery, GalleryActionTypes.UPDATE_GALLERY>;
export type DeleteGalleryAction = IAction<string, GalleryActionTypes.DELETE_GALLERY>;
export type RemoveGalleryImageAction = IAction<
    { galleryId: string; src: string },
    GalleryActionTypes.REMOVE_IMAGE_FROM_GALLERY
>;
export type AddGalleryImageAction = IAction<
    { galleryId?: string; eventId?: string; image: File }[],
    GalleryActionTypes.ADD_IMAGE_TO_GALLERY
>;
export type SetGalleriesAction = IAction<Gallery[], GalleryActionTypes.SET_GALLERIES>;
export type SetGalleryActionMessageAction = IAction<string | undefined, GalleryActionTypes.SET_GALLERY_ACTION_MESSAGE>;
export type DownloadGalleryAction = IAction<string, GalleryActionTypes.DOWNLOAD_GALLERY>;
export type SetDownloadGalleryLinkAction = IAction<string | undefined, GalleryActionTypes.SET_DOWNLOAD_GALLERY_LINK>;

export type UpdateGalleryReducerAction =
    | SetGalleriesAction
    | SetGalleryActionMessageAction
    | SetDownloadGalleryLinkAction;
