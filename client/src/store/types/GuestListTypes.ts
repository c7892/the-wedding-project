import { Guest, GuestList } from "../../models";
import { IAction } from "./IAction";

export enum GuestListReducerKeys {
    ReducerName = "GuestList",
    GuestLists = "GuestLists",
    Guests = "Guests",
    IsSavingGuestList = "IsSavingGuestList",
    GuestListError = "GuestListError",
}

export interface GuestListStore {
    [GuestListReducerKeys.IsSavingGuestList]: boolean;
    [GuestListReducerKeys.GuestLists]: GuestList[];
    [GuestListReducerKeys.Guests]: Guest[];
    [GuestListReducerKeys.GuestListError]: string | undefined;
}

export interface GuestListState {
    [GuestListReducerKeys.ReducerName]: GuestListStore;
}

export enum GuestListActionTypes {
    FETCH_GUEST_LISTS = "FETCH_GUEST_LISTS",
    FETCH_GUESTS = "FETCH_GUESTS",
    SAVE_GUESTS = "SAVE_GUESTS",
    SAVE_GUEST_LISTS = "SAVE_GUESTS_LISTS",
    UPDATE_GUEST_LISTS = "UPDATE_GUEST_LISTS",
    SET_IS_SAVING_GUEST_LIST = "SET_IS_SAVING_GUEST_LIST",
    SET_GUEST_LIST_ERROR = "SET_GUEST_LIST_ERROR",
    UPDATE_GUEST_INFO = "UPDATE_GUEST_INFO",
    DELETE_GUEST = "DELETE_GUEST",
    DELETE_GUEST_LIST = "DELETE_GUEST_LIST",
}

export type FetchGuestListsAction = IAction<undefined, GuestListActionTypes.FETCH_GUEST_LISTS>;
export type FetchGuestsAction = IAction<undefined, GuestListActionTypes.FETCH_GUESTS>;
export type SaveGuestsAction = IAction<Guest[], GuestListActionTypes.SAVE_GUESTS>;
export type SaveGuestListAction = IAction<{ list: GuestList; guests?: Guest[] }, GuestListActionTypes.SAVE_GUEST_LISTS>;
export type UpdateGuestListsAction = IAction<GuestList[], GuestListActionTypes.UPDATE_GUEST_LISTS>;
export type SetIsSavingGuestListAction = IAction<boolean, GuestListActionTypes.SET_IS_SAVING_GUEST_LIST>;
export type SetGuestListErrorAction = IAction<string | undefined, GuestListActionTypes.SET_GUEST_LIST_ERROR>;
export type UpdateGuestInfoAction = IAction<Guest, GuestListActionTypes.UPDATE_GUEST_INFO>;
export type DeleteGuestAction = IAction<Guest, GuestListActionTypes.DELETE_GUEST>;
export type DeleteGuestListAction = IAction<GuestList, GuestListActionTypes.DELETE_GUEST_LIST>;

export type UpdateGuestListReducerAction =
    | SetIsSavingGuestListAction
    | UpdateGuestListsAction
    | SaveGuestsAction
    | SetGuestListErrorAction;
