import { PaymentIntent } from "@stripe/stripe-js";
import { Guest, Invite } from "../../models";
import { IAction } from "./IAction";

export interface SendInviteParams {
    front: string;
    back: string;
    inviteId: string;
    guest: Guest;
    sendMethods: string[];
    paymentPayload?: PaymentIntent;
}

export enum InviteReducerKeys {
    ReducerName = "Invitations",
    IsSavingInvite = "IsSavingInvite",
    IsSendingInvites = "IsSendingInvites",
    Invites = "Invites",
    InviteBeingSaved = "InviteBeingSaved",
}

export interface InviteStore {
    [InviteReducerKeys.IsSavingInvite]: boolean;
    [InviteReducerKeys.IsSendingInvites]: boolean;
    [InviteReducerKeys.Invites]: Invite[];
    [InviteReducerKeys.InviteBeingSaved]: Invite | undefined;
}

export interface InviteState {
    [InviteReducerKeys.ReducerName]: InviteStore;
}

export enum InviteActionTypes {
    FETCH_INVITES = "FETCH_INVITES",
    SET_INVITES = "SET_INVITES",
    SET_IS_SAVING_INVITE = "SET_IS_SAVING_INVITE",
    DELETE_INVITE = "DELETE_INVITE",
    SAVE_INVITE = "SAVE_INVITE",
    SET_IS_SENDING_INVITES = "SET_IS_SENDING_INVITES",
    SET_INVITE_BEING_SAVED = "SET_INVITE_BEING_SAVED",
    SEND_INVITE = "SEND_INVITE",
    SEND_INVITE_CONFIRMATION = "SEND_INVITE_CONFIRMATION",
}

export type FetchInvitesAction = IAction<undefined, InviteActionTypes.FETCH_INVITES>;
export type SetInvitesAction = IAction<Invite[], InviteActionTypes.SET_INVITES>;
export type SetIsSavingInviteAction = IAction<boolean, InviteActionTypes.SET_IS_SAVING_INVITE>;
export type SetIsSendingInviteAction = IAction<boolean, InviteActionTypes.SET_IS_SENDING_INVITES>;
export type DeleteInviteAction = IAction<string, InviteActionTypes.DELETE_INVITE>;
export type SaveInviteAction = IAction<Invite, InviteActionTypes.SAVE_INVITE>;
export type SetInviteBeingSavedAction = IAction<Invite | undefined, InviteActionTypes.SET_INVITE_BEING_SAVED>;
export type SendInviteAction = IAction<SendInviteParams, InviteActionTypes.SEND_INVITE>;
export type SendInviteConfirmationAction = IAction<
    { guestIds: string[]; inviteId: string },
    InviteActionTypes.SEND_INVITE_CONFIRMATION
>;

export type UpdateInviteReducerAction =
    | SetInvitesAction
    | SetIsSavingInviteAction
    | SetIsSendingInviteAction
    | SetInviteBeingSavedAction;
