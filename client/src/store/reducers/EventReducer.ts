import { extend } from "lodash";
import { singleton } from "tsyringe";
import { EventActionTypes, EventReducerKeys, EventState, EventStore, UpdateEventReducerAction } from "../types";
import { IReducer } from "./IReducer";
import { Event } from "../../models";

@singleton()
export class EventReducer implements IReducer<EventState, EventStore> {
    public readonly initialState: EventState;
    private readonly initialValues: EventStore = {
        [EventReducerKeys.EventBeingSaved]: undefined,
        [EventReducerKeys.IsSavingEvent]: false,
        [EventReducerKeys.UserEvents]: [],
        [EventReducerKeys.ArEvent]: undefined,
        [EventReducerKeys.WeddingDay]: undefined,
        [EventReducerKeys.AllEvents]: [],
    };

    public constructor() {
        this.initialState = { [EventReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: EventStore | undefined, action: UpdateEventReducerAction): EventStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case EventActionTypes.SET_IS_SAVING_EVENT:
                return this.setIsSavingEvent(incomingStore, action.payload);
            case EventActionTypes.SET_EVENTS:
                return this.setUserEvents(incomingStore, action.payload);
            case EventActionTypes.SET_EVENT_BEING_SAVED:
                return this.setEventBeingSaved(incomingStore, action.payload);
            case EventActionTypes.SET_AR_EVENT:
                return this.setArEvent(incomingStore, action.payload);
            case EventActionTypes.SET_WEDDING_DAY:
                return this.setWeddingDay(incomingStore, action.payload);
            case EventActionTypes.SET_ALL_EVENTS:
                return this.setAllEvents(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setIsSavingEvent = (store: EventStore, payload: boolean) => {
        return extend({}, store, { [EventReducerKeys.IsSavingEvent]: payload });
    };
    private setUserEvents = (store: EventStore, payload: Event[]) => {
        return extend({}, store, { [EventReducerKeys.UserEvents]: payload });
    };
    private setArEvent = (store: EventStore, payload?: Event) => {
        return extend({}, store, { [EventReducerKeys.ArEvent]: payload });
    };
    private setEventBeingSaved = (store: EventStore, payload: Event | undefined) => {
        return extend({}, store, { [EventReducerKeys.EventBeingSaved]: payload });
    };
    private setWeddingDay = (store: EventStore, payload: Event | undefined) => {
        return extend({}, store, { [EventReducerKeys.WeddingDay]: payload });
    };
    private setAllEvents = (store: EventStore, payload: Event[]) => {
        return extend({}, store, { [EventReducerKeys.AllEvents]: payload });
    };
}
