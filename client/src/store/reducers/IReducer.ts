/*
 * Created on Wed Mar 31 2021
 *
 * Copyright (c) 2021 Coding House LLC
 */
import { IAction } from "../types";

export interface IReducer<State, Store> {
    initialState: State;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    updateState: <T extends IAction<any, any>>(store: Store | undefined, action: T) => Store;
}
