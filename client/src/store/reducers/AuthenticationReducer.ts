import { extend } from "lodash";
import { singleton } from "tsyringe";
import { Guest, User } from "../../models";
import {
    AuthenticationActionTypes,
    AuthenticationReducerKeys,
    AuthenticationState,
    AuthenticationStore,
    UpdateAuthenticationReducerAction,
} from "../types";
import { IReducer } from "./IReducer";

@singleton()
export class AuthenticationReducer implements IReducer<AuthenticationState, AuthenticationStore> {
    public readonly initialState: AuthenticationState;
    private readonly initialValues: AuthenticationStore = {
        [AuthenticationReducerKeys.IsLoggingIn]: undefined,
        [AuthenticationReducerKeys.IsRegistering]: undefined,
        [AuthenticationReducerKeys.LoginFailedMessage]: undefined,
        [AuthenticationReducerKeys.RegistrationFailedMessage]: undefined,
        [AuthenticationReducerKeys.RegistrationSucceeded]: undefined,
        [AuthenticationReducerKeys.LoginSucceeded]: undefined,
        [AuthenticationReducerKeys.User]: undefined,
        [AuthenticationReducerKeys.Spouse]: undefined,
        [AuthenticationReducerKeys.SavingError]: undefined,
        [AuthenticationReducerKeys.IsSavingUser]: false,
    };

    public constructor() {
        this.initialState = { [AuthenticationReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (
        store: AuthenticationStore | undefined,
        action: UpdateAuthenticationReducerAction,
    ): AuthenticationStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case AuthenticationActionTypes.SET_LOGIN_SUCCEEDED:
                return this.setLoginSucceeded(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_REGISTRATION_SUCCEEDED:
                return this.setRegisteringSucceeded(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_IS_LOGGING_IN:
                return this.setIsLoggingIn(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_IS_REGISTERING:
                return this.setIsRegistering(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_LOGIN_FAILED_MESSAGE:
                return this.setLoginFailedMessage(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_REGISTRATION_FAILED_MESSAGE:
                return this.setRegistrationFailedMessage(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_USER:
                return this.setUser(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_SPOUSE:
                return this.setSpouse(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_IS_SAVING_USER:
                return this.setIsSavingUser(incomingStore, action.payload);
            case AuthenticationActionTypes.SET_SAVING_ERROR:
                return this.setSavingError(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setLoginSucceeded = (store: AuthenticationStore, payload?: boolean): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.LoginSucceeded]: payload });
    };
    private setRegisteringSucceeded = (store: AuthenticationStore, payload?: boolean): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.RegistrationSucceeded]: payload });
    };
    private setIsLoggingIn = (store: AuthenticationStore, payload?: boolean): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.IsLoggingIn]: payload });
    };
    private setIsRegistering = (store: AuthenticationStore, payload?: boolean): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.IsRegistering]: payload });
    };
    private setRegistrationFailedMessage = (store: AuthenticationStore, payload?: string): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.RegistrationFailedMessage]: payload });
    };
    private setLoginFailedMessage = (store: AuthenticationStore, payload?: string): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.LoginFailedMessage]: payload });
    };
    private setUser = (store: AuthenticationStore, payload?: Guest | User): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.User]: payload });
    };
    private setSpouse = (store: AuthenticationStore, payload?: User[]): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.Spouse]: payload });
    };
    private setIsSavingUser = (store: AuthenticationStore, payload: boolean): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.IsSavingUser]: payload });
    };
    private setSavingError = (store: AuthenticationStore, payload?: string): AuthenticationStore => {
        return extend({}, store, { [AuthenticationReducerKeys.SavingError]: payload });
    };
}
