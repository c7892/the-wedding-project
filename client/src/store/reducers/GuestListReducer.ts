import { extend } from "lodash";
import { singleton } from "tsyringe";
import { Guest, GuestList } from "../../models";
import {
    GuestListActionTypes,
    GuestListReducerKeys,
    GuestListState,
    GuestListStore,
    UpdateGuestListReducerAction,
} from "../types";
import { IReducer } from "./IReducer";

@singleton()
export class GuestListReducer implements IReducer<GuestListState, GuestListStore> {
    public readonly initialState: GuestListState;
    public readonly initialValues: GuestListStore = {
        [GuestListReducerKeys.GuestLists]: [],
        [GuestListReducerKeys.Guests]: [],
        [GuestListReducerKeys.IsSavingGuestList]: false,
        [GuestListReducerKeys.GuestListError]: undefined,
    };

    public constructor() {
        this.initialState = { [GuestListReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: GuestListStore | undefined, action: UpdateGuestListReducerAction): GuestListStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case GuestListActionTypes.SET_IS_SAVING_GUEST_LIST:
                return this.setIsSavingGuestList(incomingStore, action.payload);
            case GuestListActionTypes.SAVE_GUESTS:
                return this.setGuests(incomingStore, action.payload);
            case GuestListActionTypes.UPDATE_GUEST_LISTS:
                return this.setGuestList(incomingStore, action.payload);
            case GuestListActionTypes.SET_GUEST_LIST_ERROR:
                return this.setGuestListError(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setIsSavingGuestList = (store: GuestListStore, payload: boolean) => {
        return extend({}, store, { [GuestListReducerKeys.IsSavingGuestList]: payload });
    };
    private setGuestList = (store: GuestListStore, payload: GuestList[]) => {
        return extend({}, store, { [GuestListReducerKeys.GuestLists]: payload });
    };
    private setGuests = (store: GuestListStore, payload: Guest[]) => {
        return extend({}, store, { [GuestListReducerKeys.Guests]: payload });
    };
    private setGuestListError = (store: GuestListStore, payload?: string) => {
        return extend({}, store, { [GuestListReducerKeys.GuestListError]: payload });
    };
}
