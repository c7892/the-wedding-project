import { extend } from "lodash";
import { singleton } from "tsyringe";
import { Notes } from "../../models";
import { NotesActionTypes, NotesReducerKeys, NotesState, NotesStore, UpdateNotesReducerAction } from "../types";
import { IReducer } from "./IReducer";

@singleton()
export class NotesReducer implements IReducer<NotesState, NotesStore> {
    public readonly initialState: NotesState;
    private readonly initialValues: NotesStore = {
        [NotesReducerKeys.AllNotes]: [],
        [NotesReducerKeys.IsSavingNotes]: undefined,
        [NotesReducerKeys.Note]: undefined,
        [NotesReducerKeys.NoteBeingSaved]: undefined,
    };

    public constructor() {
        this.initialState = { [NotesReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: NotesStore | undefined, action: UpdateNotesReducerAction): NotesStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case NotesActionTypes.SET_ALL_NOTES:
                return this.setAllNotes(incomingStore, action.payload);
            case NotesActionTypes.SET_NOTES_BEING_SAVED:
                return this.setNotesBeingSaved(incomingStore, action.payload);
            case NotesActionTypes.SET_NOTES:
                return this.setNotes(incomingStore, action.payload);
            case NotesActionTypes.SET_IS_SAVING_NOTES:
                return this.setIsSavingNotes(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };
    private setIsSavingNotes = (store: NotesStore, payload?: boolean) => {
        return extend({}, store, { [NotesReducerKeys.IsSavingNotes]: payload });
    };
    private setNotes = (store: NotesStore, payload?: Notes) => {
        return extend({}, store, { [NotesReducerKeys.Note]: payload });
    };
    private setNotesBeingSaved = (store: NotesStore, payload: Notes | undefined) => {
        return extend({}, store, { [NotesReducerKeys.NoteBeingSaved]: payload });
    };
    private setAllNotes = (store: NotesStore, payload: Notes[]) => {
        return extend({}, store, { [NotesReducerKeys.AllNotes]: payload });
    };
}
