import { extend } from "lodash";
import { singleton } from "tsyringe";
import {
    RegistryState,
    RegistryStore,
    RegistryReducerKeys,
    UpdateRegistryReducerAction,
    RegistryActionTypes,
} from "../types";
import { IReducer } from "./IReducer";
import { Registry } from "../../models";

@singleton()
export class RegistryReducer implements IReducer<RegistryState, RegistryStore> {
    public readonly initialState: RegistryState;
    private readonly initialValues: RegistryStore = {
        [RegistryReducerKeys.RegistryMessage]: undefined,
        [RegistryReducerKeys.IsSavingRegistry]: false,
        [RegistryReducerKeys.WeddingRegistry]: undefined,
    };

    public constructor() {
        this.initialState = { [RegistryReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: RegistryStore | undefined, action: UpdateRegistryReducerAction): RegistryStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case RegistryActionTypes.SET_REGISTRY:
                return this.setRegistry(incomingStore, action.payload);
            case RegistryActionTypes.SET_REGISTRY_MESSAGE:
                return this.setRegistryActionMessage(incomingStore, action.payload);
            case RegistryActionTypes.SET_IS_SAVING_REGISTRY:
                return this.setIsSavingRegistry(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setRegistry = (store: RegistryStore, payload?: Registry) => {
        return extend({}, store, { [RegistryReducerKeys.WeddingRegistry]: payload });
    };

    private setRegistryActionMessage = (store: RegistryStore, payload?: string) => {
        return extend({}, store, { [RegistryReducerKeys.RegistryMessage]: payload });
    };
    private setIsSavingRegistry = (store: RegistryStore, payload: boolean) => {
        return extend({}, store, { [RegistryReducerKeys.IsSavingRegistry]: payload });
    };
}
