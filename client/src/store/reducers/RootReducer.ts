import { combineReducers } from "redux";
import { container } from "tsyringe";
import {
    AuthenticationReducerKeys,
    AuthenticationStore,
    EventReducerKeys,
    EventStore,
    GalleryReducerKeys,
    GalleryStore,
    InviteReducerKeys,
    InviteStore,
    NotesReducerKeys,
    NotesStore,
    RegistryReducerKeys,
    RegistryStore,
} from "../types";
import { GuestListReducerKeys, GuestListStore } from "../types/GuestListTypes";
import { AuthenticationReducer } from "./AuthenticationReducer";
import { EventReducer } from "./EventReducer";
import { GuestListReducer } from "./GuestListReducer";
import { InviteReducer } from "./InviteReducer";
import { GalleryReducer } from "./GalleryReducer";
import { RegistryReducer } from "./RegistryReducer";
import { NotesReducer } from "./NotesReducer";

const authenticationReducer = container.resolve(AuthenticationReducer);
const guestListReducer = container.resolve(GuestListReducer);
const eventReducer = container.resolve(EventReducer);
const inviteReducer = container.resolve(InviteReducer);
const galleryReducer = container.resolve(GalleryReducer);
const registryReducer = container.resolve(RegistryReducer);
const notesReducer = container.resolve(NotesReducer);

export const rootReducer = combineReducers({
    [AuthenticationReducerKeys.ReducerName]: authenticationReducer.updateState,
    [GuestListReducerKeys.ReducerName]: guestListReducer.updateState,
    [EventReducerKeys.ReducerName]: eventReducer.updateState,
    [InviteReducerKeys.ReducerName]: inviteReducer.updateState,
    [GalleryReducerKeys.ReducerName]: galleryReducer.updateState,
    [RegistryReducerKeys.ReducerName]: registryReducer.updateState,
    [NotesReducerKeys.ReducerName]: notesReducer.updateState,
});

export const initialState = {
    ...authenticationReducer.initialState,
    ...guestListReducer.initialState,
    ...eventReducer.initialState,
    ...inviteReducer.initialState,
    ...galleryReducer.initialState,
    ...registryReducer.initialState,
    ...notesReducer.initialState,
};

export interface ApplicationReduxState {
    [AuthenticationReducerKeys.ReducerName]: AuthenticationStore;
    [GuestListReducerKeys.ReducerName]: GuestListStore;
    [EventReducerKeys.ReducerName]: EventStore;
    [InviteReducerKeys.ReducerName]: InviteStore;
    [GalleryReducerKeys.ReducerName]: GalleryStore;
    [RegistryReducerKeys.ReducerName]: RegistryStore;
    [NotesReducerKeys.ReducerName]: NotesStore;
}
