import { extend, uniqBy } from "lodash";
import { singleton } from "tsyringe";
import {
    GalleryActionTypes,
    GalleryReducerKeys,
    GalleryState,
    GalleryStore,
    UpdateGalleryReducerAction,
} from "../types";
import { IReducer } from "./IReducer";
import { Gallery } from "../../models";

@singleton()
export class GalleryReducer implements IReducer<GalleryState, GalleryStore> {
    public readonly initialState: GalleryState;
    private readonly initialValues: GalleryStore = {
        [GalleryReducerKeys.GalleryActionMessage]: undefined,
        [GalleryReducerKeys.Galleries]: [],
        [GalleryReducerKeys.GalleryDownloadLink]: undefined,
    };

    public constructor() {
        this.initialState = { [GalleryReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: GalleryStore | undefined, action: UpdateGalleryReducerAction): GalleryStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case GalleryActionTypes.SET_GALLERIES:
                return this.setGalleries(incomingStore, action.payload);
            case GalleryActionTypes.SET_GALLERY_ACTION_MESSAGE:
                return this.setGalleryActionMessage(incomingStore, action.payload);
            case GalleryActionTypes.SET_DOWNLOAD_GALLERY_LINK:
                return this.setGalleryDownloadLink(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setGalleries = (store: GalleryStore, payload: Gallery[]) => {
        return extend({}, store, { [GalleryReducerKeys.Galleries]: uniqBy(payload, "_id") });
    };

    private setGalleryActionMessage = (store: GalleryStore, payload?: string) => {
        return extend({}, store, { [GalleryReducerKeys.GalleryActionMessage]: payload });
    };
    private setGalleryDownloadLink = (store: GalleryStore, payload?: string) => {
        return extend({}, store, { [GalleryReducerKeys.GalleryDownloadLink]: payload });
    };
}
