import { extend } from "lodash";
import { singleton } from "tsyringe";
import { Invite } from "../../models";
import { InviteActionTypes, InviteReducerKeys, InviteState, InviteStore, UpdateInviteReducerAction } from "../types";
import { IReducer } from "./IReducer";

@singleton()
export class InviteReducer implements IReducer<InviteState, InviteStore> {
    public readonly initialState: InviteState;
    private readonly initialValues: InviteStore = {
        [InviteReducerKeys.Invites]: [],
        [InviteReducerKeys.IsSavingInvite]: false,
        [InviteReducerKeys.IsSendingInvites]: false,
        [InviteReducerKeys.InviteBeingSaved]: undefined,
    };

    public constructor() {
        this.initialState = { [InviteReducerKeys.ReducerName]: this.initialValues };
    }

    public updateState = (store: InviteStore | undefined, action: UpdateInviteReducerAction): InviteStore => {
        const incomingStore = store ? store : this.initialValues;
        switch (action.type) {
            case InviteActionTypes.SET_INVITES:
                return this.setInvites(incomingStore, action.payload);
            case InviteActionTypes.SET_IS_SAVING_INVITE:
                return this.setIsSavingInvite(incomingStore, action.payload);
            case InviteActionTypes.SET_IS_SENDING_INVITES:
                return this.setIsSendingInvite(incomingStore, action.payload);
            case InviteActionTypes.SET_INVITE_BEING_SAVED:
                return this.setInviteBeingSaved(incomingStore, action.payload);
            default:
                return incomingStore;
        }
    };

    private setInvites = (store: InviteStore, payload: Invite[]): InviteStore => {
        return extend({}, store, { [InviteReducerKeys.Invites]: payload });
    };
    private setIsSavingInvite = (store: InviteStore, payload: boolean): InviteStore => {
        return extend({}, store, { [InviteReducerKeys.IsSavingInvite]: payload });
    };
    private setIsSendingInvite = (store: InviteStore, payload: boolean): InviteStore => {
        return extend({}, store, { [InviteReducerKeys.IsSendingInvites]: payload });
    };
    private setInviteBeingSaved = (store: InviteStore, payload?: Invite): InviteStore => {
        return extend({}, store, { [InviteReducerKeys.InviteBeingSaved]: payload });
    };
}
