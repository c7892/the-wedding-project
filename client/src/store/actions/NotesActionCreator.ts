import { singleton } from "tsyringe";
import { Notes } from "../../models";
import {
    NotesActionTypes,
    FetchAllNotesAction,
    SetAllNotesAction,
    SetNotesAction,
    SetNotesBeingSavedAction,
    SetIsSavingNotesAction,
    FetchNotesAction,
    SaveNotesAction,
    DeleteNotesAction,
} from "../types";

@singleton()
export class NotesActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchAllNotes = (): FetchAllNotesAction => ({
        payload: undefined,
        type: NotesActionTypes.FETCH_ALL_NOTES,
    });
    public setAllNotes = (payload: Notes[]): SetAllNotesAction => ({
        payload,
        type: NotesActionTypes.SET_ALL_NOTES,
    });
    public setNotes = (payload?: Notes): SetNotesAction => ({
        payload,
        type: NotesActionTypes.SET_NOTES,
    });
    public setNotesBeingSaved = (payload?: Notes): SetNotesBeingSavedAction => ({
        payload,
        type: NotesActionTypes.SET_NOTES_BEING_SAVED,
    });
    public setIsSavingNotes = (payload?: boolean): SetIsSavingNotesAction => ({
        payload,
        type: NotesActionTypes.SET_IS_SAVING_NOTES,
    });
    public fetchNotes = (payload: string): FetchNotesAction => ({
        payload,
        type: NotesActionTypes.FETCH_NOTES,
    });
    public saveNotes = (payload: Notes): SaveNotesAction => ({
        payload,
        type: NotesActionTypes.SAVE_NOTES,
    });
    public deleteNotes = (payload: string): DeleteNotesAction => ({
        payload,
        type: NotesActionTypes.DELETE_NOTES,
    });
}
