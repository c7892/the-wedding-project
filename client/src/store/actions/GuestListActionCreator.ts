import { singleton } from "tsyringe";
import { Guest, GuestList } from "../../models";
import {
    DeleteGuestAction,
    DeleteGuestListAction,
    FetchGuestListsAction,
    FetchGuestsAction,
    GuestListActionTypes,
    SaveGuestListAction,
    SaveGuestsAction,
    SetGuestListErrorAction,
    SetIsSavingGuestListAction,
    UpdateGuestInfoAction,
    UpdateGuestListsAction,
} from "../types";

@singleton()
export class GuestListActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchGuests = (): FetchGuestsAction => ({
        payload: undefined,
        type: GuestListActionTypes.FETCH_GUESTS,
    });

    public fetchGuestLists = (): FetchGuestListsAction => ({
        payload: undefined,
        type: GuestListActionTypes.FETCH_GUEST_LISTS,
    });

    public saveGuests = (payload: Guest[]): SaveGuestsAction => ({
        payload,
        type: GuestListActionTypes.SAVE_GUESTS,
    });

    public updateGuestInfo = (payload: Guest): UpdateGuestInfoAction => ({
        payload,
        type: GuestListActionTypes.UPDATE_GUEST_INFO,
    });
    public deleteGuest = (payload: Guest): DeleteGuestAction => ({
        payload,
        type: GuestListActionTypes.DELETE_GUEST,
    });
    public deleteGuestList = (payload: GuestList): DeleteGuestListAction => ({
        payload,
        type: GuestListActionTypes.DELETE_GUEST_LIST,
    });

    public updateGuestLists = (payload: GuestList[]): UpdateGuestListsAction => ({
        payload,
        type: GuestListActionTypes.UPDATE_GUEST_LISTS,
    });
    public saveGuestList = (payload: { list: GuestList; guests?: Guest[] }): SaveGuestListAction => ({
        payload,
        type: GuestListActionTypes.SAVE_GUEST_LISTS,
    });

    public setIsSavingGuestList = (payload: boolean): SetIsSavingGuestListAction => ({
        payload,
        type: GuestListActionTypes.SET_IS_SAVING_GUEST_LIST,
    });

    public setGuestListError = (payload?: string): SetGuestListErrorAction => ({
        payload,
        type: GuestListActionTypes.SET_GUEST_LIST_ERROR,
    });
}
