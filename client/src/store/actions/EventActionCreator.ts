import { singleton } from "tsyringe";
import { Event } from "../../models";
import {
    DeleteEventAction,
    EventActionTypes,
    FetchAllEventsAction,
    FetchArEventAction,
    FetchEventsAction,
    FetchWeddingDayAction,
    RsvpAction,
    SaveEventAction,
    SetAllEventsAction,
    SetArEventAction,
    SetEventBeingSavedAction,
    SetEventsAction,
    SetIsSavingEventAction,
    SetWeddingDayAction,
    UnRsvpAction,
} from "../types";

@singleton()
export class EventActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchEvents = (payload?: Date): FetchEventsAction => ({
        payload,
        type: EventActionTypes.FETCH_EVENTS,
    });
    public fetchAllEvents = (): FetchAllEventsAction => ({
        payload: undefined,
        type: EventActionTypes.FETCH_ALL_EVENTS,
    });
    public setAllEvents = (payload: Event[]): SetAllEventsAction => ({
        payload,
        type: EventActionTypes.SET_ALL_EVENTS,
    });

    public setEvents = (payload: Event[]): SetEventsAction => ({
        payload,
        type: EventActionTypes.SET_EVENTS,
    });
    public setArEvent = (payload?: Event): SetArEventAction => ({
        payload,
        type: EventActionTypes.SET_AR_EVENT,
    });
    public setWeddingDay = (payload?: Event): SetWeddingDayAction => ({
        payload,
        type: EventActionTypes.SET_WEDDING_DAY,
    });

    public fetchWeddingDay = (): FetchWeddingDayAction => ({
        payload: undefined,
        type: EventActionTypes.FETCH_WEDDING_DAY,
    });
    public fetchArEvent = (payload: { id: string; guest: string }): FetchArEventAction => ({
        payload,
        type: EventActionTypes.FETCH_AR_EVENT,
    });

    public setIsSavingEvent = (payload: boolean): SetIsSavingEventAction => ({
        payload,
        type: EventActionTypes.SET_IS_SAVING_EVENT,
    });

    public setEventBeingSaving = (payload?: Event): SetEventBeingSavedAction => ({
        payload,
        type: EventActionTypes.SET_EVENT_BEING_SAVED,
    });
    public saveEvent = (payload: Event): SaveEventAction => ({
        payload,
        type: EventActionTypes.SAVE_EVENT,
    });
    public deleteEvent = (payload: string): DeleteEventAction => ({
        payload,
        type: EventActionTypes.DELETE_EVENT,
    });
    public rsvp = (payload: { eventId: string; guests: number }): RsvpAction => ({
        payload,
        type: EventActionTypes.RSVP,
    });
    public unrsvp = (payload: string): UnRsvpAction => ({
        payload,
        type: EventActionTypes.UNRSVP,
    });
}
