import { singleton } from "tsyringe";
import {
    AddGalleryImageAction,
    CreateGalleryAction,
    DeleteGalleryAction,
    DownloadGalleryAction,
    FetchGalleriesAction,
    FetchGalleryAction,
    GalleryActionTypes,
    RemoveGalleryImageAction,
    SetDownloadGalleryLinkAction,
    SetGalleriesAction,
    SetGalleryActionMessageAction,
    UpdateGalleryAction,
} from "../types";
import { Gallery } from "../../models";

@singleton()
export class GalleryActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchGalleries = (): FetchGalleriesAction => ({
        payload: undefined,
        type: GalleryActionTypes.FETCH_GALLERIES,
    });

    public fetchGallery = (payload: string): FetchGalleryAction => ({
        payload,
        type: GalleryActionTypes.FETCH_GALLERY,
    });

    public createGallery = (payload: Gallery): CreateGalleryAction => ({
        payload,
        type: GalleryActionTypes.CREATE_GALLERY,
    });

    public updateGallery = (payload: Gallery): UpdateGalleryAction => ({
        payload,
        type: GalleryActionTypes.UPDATE_GALLERY,
    });

    public deleteGallery = (payload: string): DeleteGalleryAction => ({
        payload,
        type: GalleryActionTypes.DELETE_GALLERY,
    });

    public removeImageFromGallery = (payload: { galleryId: string; src: string }): RemoveGalleryImageAction => ({
        payload,
        type: GalleryActionTypes.REMOVE_IMAGE_FROM_GALLERY,
    });

    public addImageToGallery = (
        payload: {
            galleryId?: string;
            eventId?: string;
            image: File;
        }[],
    ): AddGalleryImageAction => ({
        payload,
        type: GalleryActionTypes.ADD_IMAGE_TO_GALLERY,
    });

    public setGalleries = (payload: Gallery[]): SetGalleriesAction => ({
        payload,
        type: GalleryActionTypes.SET_GALLERIES,
    });
    public setGalleryActionMessage = (payload?: string): SetGalleryActionMessageAction => ({
        payload,
        type: GalleryActionTypes.SET_GALLERY_ACTION_MESSAGE,
    });
    public setDownloadGalleryLink = (payload?: string): SetDownloadGalleryLinkAction => ({
        payload,
        type: GalleryActionTypes.SET_DOWNLOAD_GALLERY_LINK,
    });
    public downloadGallery = (payload: string): DownloadGalleryAction => ({
        payload,
        type: GalleryActionTypes.DOWNLOAD_GALLERY,
    });
}
