import { singleton } from "tsyringe";
import { Guest, SpouseRegistrationInformation, User } from "../../models";
import {
    AuthenticationActionTypes,
    ChangePasswordAction,
    ClearAuthProcessDataAction,
    DeleteAccountAction,
    FetchUserAction,
    GuestLoginAction,
    LoginAction,
    LogoutAction,
    ReauthTokenAction,
    RegisterAction,
    SetIsLoggingInAction,
    SetIsRegisteringAction,
    SetIsSavingUserAction,
    SetLoginFailedMessageAction,
    SetLoginSucceededAction,
    SetRegistrationFailedMessageAction,
    SetRegistrationSucceededAction,
    SetSavingUserErrorAction,
    SetSpouseAction,
    SetUserAction,
    UpdateUserAction,
} from "../types";

@singleton()
export class AuthenticationActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public logout = (): LogoutAction => ({
        payload: undefined,
        type: AuthenticationActionTypes.LOGOUT,
    });
    public login = (payload: { email: string; password: string }): LoginAction => ({
        payload,
        type: AuthenticationActionTypes.LOGIN,
    });
    public guestLogin = (payload: { email: string; inviteCode: string }): GuestLoginAction => ({
        payload,
        type: AuthenticationActionTypes.GUEST_LOGIN,
    });
    public register = (payload: SpouseRegistrationInformation[]): RegisterAction => ({
        payload,
        type: AuthenticationActionTypes.REGISTER,
    });
    public setIsLoggingIn = (payload?: boolean): SetIsLoggingInAction => ({
        payload,
        type: AuthenticationActionTypes.SET_IS_LOGGING_IN,
    });
    public setIsRegistering = (payload?: boolean): SetIsRegisteringAction => ({
        payload,
        type: AuthenticationActionTypes.SET_IS_REGISTERING,
    });
    public setLogInSucceeded = (payload?: boolean): SetLoginSucceededAction => ({
        payload,
        type: AuthenticationActionTypes.SET_LOGIN_SUCCEEDED,
    });
    public setRegistrationSucceeded = (payload?: boolean): SetRegistrationSucceededAction => ({
        payload,
        type: AuthenticationActionTypes.SET_REGISTRATION_SUCCEEDED,
    });
    public setLoginFailedMessage = (payload?: string): SetLoginFailedMessageAction => ({
        payload,
        type: AuthenticationActionTypes.SET_LOGIN_FAILED_MESSAGE,
    });
    public setRegistrationFailedMessage = (payload?: string): SetRegistrationFailedMessageAction => ({
        payload,
        type: AuthenticationActionTypes.SET_REGISTRATION_FAILED_MESSAGE,
    });
    public setUser = (payload?: Guest | User): SetUserAction => ({ payload, type: AuthenticationActionTypes.SET_USER });
    public setSpouse = (payload?: User[]): SetSpouseAction => ({ payload, type: AuthenticationActionTypes.SET_SPOUSE });
    public updateUser = (payload: { updates: Guest | User; image?: File }): UpdateUserAction => ({
        payload,
        type: AuthenticationActionTypes.UPDATE_USER,
    });
    public setIsSavingUser = (payload: boolean): SetIsSavingUserAction => ({
        payload,
        type: AuthenticationActionTypes.SET_IS_SAVING_USER,
    });
    public setIsSavingUserError = (payload?: string): SetSavingUserErrorAction => ({
        payload,
        type: AuthenticationActionTypes.SET_SAVING_ERROR,
    });
    public clear = (): ClearAuthProcessDataAction => ({
        payload: undefined,
        type: AuthenticationActionTypes.CLEAR_AUTH_PROCESS_DATA,
    });
    public fetchUser = (): FetchUserAction => ({
        payload: undefined,
        type: AuthenticationActionTypes.FETCH_USER,
    });
    public reauth = (): ReauthTokenAction => ({
        payload: undefined,
        type: AuthenticationActionTypes.REAUTH_TOKEN,
    });
    public changePassword = (payload: { oldPassword: string; newPassword: string }): ChangePasswordAction => ({
        payload,
        type: AuthenticationActionTypes.CHANGE_PASSWORD,
    });
    public deleteAccount = (): DeleteAccountAction => ({
        payload: undefined,
        type: AuthenticationActionTypes.DELETE_ACCOUNT,
    });
}
