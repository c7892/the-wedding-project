import { singleton } from "tsyringe";
import { Invite } from "../../models";
import {
    DeleteInviteAction,
    FetchInvitesAction,
    InviteActionTypes,
    SaveInviteAction,
    SendInviteAction,
    SendInviteConfirmationAction,
    SendInviteParams,
    SetInviteBeingSavedAction,
    SetInvitesAction,
    SetIsSavingInviteAction,
    SetIsSendingInviteAction,
} from "../types";

@singleton()
export class InviteActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchInvites = (): FetchInvitesAction => ({
        payload: undefined,
        type: InviteActionTypes.FETCH_INVITES,
    });

    public saveInvite = (payload: Invite): SaveInviteAction => ({
        payload,
        type: InviteActionTypes.SAVE_INVITE,
    });

    public setInvites = (payload: Invite[]): SetInvitesAction => ({
        payload,
        type: InviteActionTypes.SET_INVITES,
    });

    public setIsSavingInvite = (payload: boolean): SetIsSavingInviteAction => ({
        payload,
        type: InviteActionTypes.SET_IS_SAVING_INVITE,
    });

    public setIsSendingInvite = (payload: boolean): SetIsSendingInviteAction => ({
        payload,
        type: InviteActionTypes.SET_IS_SENDING_INVITES,
    });

    public deleteInvite = (payload: string): DeleteInviteAction => ({
        payload,
        type: InviteActionTypes.DELETE_INVITE,
    });

    public setInviteBeingSaved = (payload?: Invite): SetInviteBeingSavedAction => ({
        payload,
        type: InviteActionTypes.SET_INVITE_BEING_SAVED,
    });

    public sendInvite = (payload: SendInviteParams): SendInviteAction => ({
        payload,
        type: InviteActionTypes.SEND_INVITE,
    });
    public sendInviteConfirmation = (payload: {
        guestIds: string[];
        inviteId: string;
    }): SendInviteConfirmationAction => ({
        payload,
        type: InviteActionTypes.SEND_INVITE_CONFIRMATION,
    });
}
