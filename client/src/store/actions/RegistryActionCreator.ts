import { singleton } from "tsyringe";
import {
    FetchRegistryAction,
    RegistryActionTypes,
    SetIsSavingRegistryAction,
    SetRegistryAction,
    SetRegistryMessageAction,
    UpdateRegistryAction,
} from "../types";
import { Registry } from "../../models";

@singleton()
export class RegistryActionCreator {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public fetchRegistry = (): FetchRegistryAction => ({
        payload: undefined,
        type: RegistryActionTypes.FETCH_REGISTRY,
    });
    public updateRegistry = (payload: Registry): UpdateRegistryAction => ({
        payload,
        type: RegistryActionTypes.UPDATE_REGISTRY,
    });
    public setRegistryMessage = (payload?: string): SetRegistryMessageAction => ({
        payload,
        type: RegistryActionTypes.SET_REGISTRY_MESSAGE,
    });
    public setRegistry = (payload?: Registry): SetRegistryAction => ({
        payload,
        type: RegistryActionTypes.SET_REGISTRY,
    });
    public setIsSavingRegistry = (payload: boolean): SetIsSavingRegistryAction => ({
        payload,
        type: RegistryActionTypes.SET_IS_SAVING_REGISTRY,
    });
}
