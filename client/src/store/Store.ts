import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { isDev } from "../utilities";
import { initialState, rootReducer } from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
export const sagaMiddleWare = createSagaMiddleware();
const middleWares = [sagaMiddleWare];
export const store = isDev
    ? createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleWares)))
    : createStore(rootReducer, initialState, applyMiddleware(...middleWares));
