import { createSelector, ParametricSelector, Selector } from "reselect";
import { singleton } from "tsyringe";
import { Gallery } from "../../models";
import { ApplicationReduxState } from "../reducers";
import { GalleryReducerKeys } from "../types";

@singleton()
export class GallerySelector {
    public readonly selectGalleries: Selector<ApplicationReduxState, Gallery[]>;
    public readonly selectGallery: ParametricSelector<ApplicationReduxState, string, Gallery | undefined>;
    public readonly selectGalleryActionMessage: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectGalleryDownloadLink: Selector<ApplicationReduxState, string | undefined>;

    public constructor() {
        this.selectGalleries = createSelector(this.getGalleryStore, (store) => store[GalleryReducerKeys.Galleries]);
        this.selectGallery = createSelector(
            [this.selectGalleries, (store: ApplicationReduxState, id: string) => id],
            (galleries, id) => galleries.find((gallery) => gallery._id === id),
        );
        this.selectGalleryActionMessage = createSelector(
            this.getGalleryStore,
            (store) => store[GalleryReducerKeys.GalleryActionMessage],
        );
        this.selectGalleryDownloadLink = createSelector(
            this.getGalleryStore,
            (store) => store[GalleryReducerKeys.GalleryDownloadLink],
        );
    }

    private getGalleryStore = (state: ApplicationReduxState) => state[GalleryReducerKeys.ReducerName];
}
