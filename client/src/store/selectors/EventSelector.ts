import { isEmpty } from "lodash";
import { createSelector, ParametricSelector, Selector } from "reselect";
import { singleton } from "tsyringe";
import { Event } from "../../models";
import { ApplicationReduxState } from "../reducers";
import { EventReducerKeys } from "../types";
import moment from "moment";

@singleton()
export class EventSelector {
    public readonly selectUserEvents: Selector<ApplicationReduxState, Event[]>;
    public readonly selectUpcomingEvents: Selector<ApplicationReduxState, Event[]>;
    public readonly selectAllEvents: Selector<ApplicationReduxState, Event[]>;
    public readonly selectEventBeingSaved: Selector<ApplicationReduxState, Event | undefined>;
    public readonly selectArEvent: Selector<ApplicationReduxState, Event | undefined>;
    public readonly selectWeddingDay: Selector<ApplicationReduxState, Event | undefined>;
    public readonly selectIsSavingEvent: Selector<ApplicationReduxState, boolean>;
    public readonly selectEventsForDate: ParametricSelector<ApplicationReduxState, Date, Event[]>;
    public readonly selectEventById: ParametricSelector<ApplicationReduxState, string, Event | undefined>;
    public readonly selectIsRsvpdForEvent: ParametricSelector<
        ApplicationReduxState,
        { eventId: string; userId: string },
        boolean
    >;

    public constructor() {
        this.selectUserEvents = createSelector(this.getEventStore, (store) => store[EventReducerKeys.UserEvents]);
        this.selectEventBeingSaved = createSelector(
            this.getEventStore,
            (store) => store[EventReducerKeys.EventBeingSaved],
        );
        this.selectArEvent = createSelector(this.getEventStore, (store) => store[EventReducerKeys.ArEvent]);
        this.selectAllEvents = createSelector(this.getEventStore, (store) => store[EventReducerKeys.AllEvents]);
        this.selectWeddingDay = createSelector(this.getEventStore, (store) => store[EventReducerKeys.WeddingDay]);
        this.selectIsSavingEvent = createSelector(this.getEventStore, (store) => store[EventReducerKeys.IsSavingEvent]);
        this.selectEventsForDate = createSelector(
            [this.selectUserEvents, (state: ApplicationReduxState, date: Date) => date],
            (events, date) => {
                if (isEmpty(events)) {
                    return [];
                }
                return events.filter((event) => {
                    const start = moment(event.start);
                    const end = moment(event.end);
                    return moment(date).isBetween(start, end, "day", "[]");
                });
            },
        );
        this.selectUpcomingEvents = createSelector([this.selectUserEvents], (events) => {
            if (isEmpty(events)) {
                return [];
            }
            return events.filter((event) => {
                const start = moment(event.start);
                const end = moment(event.end);
                return moment().isBetween(start, end, "day", "[]");
            });
        });
        this.selectEventById = createSelector(
            [this.selectUserEvents, (state: ApplicationReduxState, id: string) => id],
            (events, id) => {
                if (isEmpty(events)) {
                    return undefined;
                }
                return events.find((event) => event._id === id);
            },
        );
        this.selectIsRsvpdForEvent = createSelector(
            [
                this.selectUserEvents,
                (state: ApplicationReduxState, props: { eventId: string; userId: string }) => props,
            ],
            (events, { eventId, userId }) => {
                if (isEmpty(events)) {
                    return false;
                }
                const event = events.find((event) => event._id === eventId);
                if (!event) {
                    return false;
                }
                const rsvpList = event.rsvpList;
                const item = rsvpList.find((entry) => entry.hostingGuest === userId);
                return item !== undefined;
            },
        );
    }

    private getEventStore = (state: ApplicationReduxState) => state[EventReducerKeys.ReducerName];
}
