import { Selector } from "react-redux";
import { singleton } from "tsyringe";
import { ApplicationReduxState } from "../reducers";
import { AuthenticationReducerKeys } from "../types";
import { createSelector } from "reselect";
import { getConfig, IConfig } from "../../utilities";
import Cookies from "universal-cookie";
import { Guest, User } from "../../models";

export interface TokenBody {
    _id: string;
    email: string;
    firstName: string;
    surname: string;
    newFamilyName?: string;
    profilePic?: string;
    uniqueWeddingCode: string;
    exp: number;
}

@singleton()
export class AuthenticationSelector {
    public readonly selectIsLoggingIn: Selector<ApplicationReduxState, boolean | undefined>;
    public readonly selectIsRegistering: Selector<ApplicationReduxState, boolean | undefined>;
    public readonly selectLoginFailedMessage: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectRegistrationFailedMessage: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectLoginSucceeded: Selector<ApplicationReduxState, boolean | undefined>;
    public readonly selectRegistrationSucceeded: Selector<ApplicationReduxState, boolean | undefined>;
    public readonly selectIsAuthenticated: Selector<ApplicationReduxState, boolean>;
    public readonly selectTimeUntilTokenExp: Selector<ApplicationReduxState, number>;
    public readonly selectWeddingCode: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectUserId: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectUserEmail: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectIsGuest: Selector<ApplicationReduxState, boolean>;
    public readonly selectTokenBody: Selector<ApplicationReduxState, TokenBody | undefined>;
    public readonly selectUser: Selector<ApplicationReduxState, User | Guest | undefined>;
    public readonly selectSpouse: Selector<ApplicationReduxState, User[] | undefined>;
    public readonly selectIsSavingUser: Selector<ApplicationReduxState, boolean>;
    public readonly selectIsSavingUserError: Selector<ApplicationReduxState, string | undefined>;

    public constructor() {
        this.selectIsSavingUserError = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.SavingError],
        );
        this.selectIsSavingUser = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.IsSavingUser],
        );
        this.selectIsLoggingIn = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.IsLoggingIn],
        );
        this.selectIsRegistering = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.IsRegistering],
        );
        this.selectLoginFailedMessage = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.LoginFailedMessage],
        );
        this.selectUser = createSelector(this.getAuthenticationStore, (store) => store[AuthenticationReducerKeys.User]);
        this.selectSpouse = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.Spouse],
        );
        this.selectRegistrationFailedMessage = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.RegistrationFailedMessage],
        );
        this.selectLoginSucceeded = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.LoginSucceeded],
        );
        this.selectRegistrationSucceeded = createSelector(
            this.getAuthenticationStore,
            (store) => store[AuthenticationReducerKeys.RegistrationSucceeded],
        );
        this.selectIsAuthenticated = createSelector([this.getParsedToken, this.selectUser], (token) => {
            if (!token) {
                return false;
            }
            return token.exp > Date.now();
        });
        this.selectTimeUntilTokenExp = createSelector(this.getParsedToken, (token) => {
            if (!token) {
                return -1;
            }
            return parseInt(token.exp + "") - Date.now();
        });
        this.selectIsGuest = createSelector(this.getParsedToken, (token) => {
            if (!token) {
                return false;
            }
            return token.newFamilyName === undefined;
        });
        this.selectUserEmail = createSelector(this.getParsedToken, (token) => {
            if (!token) {
                return undefined;
            }
            return token.email;
        });
        this.selectUserId = createSelector(this.getParsedToken, (token) => {
            return token?._id;
        });
        this.selectWeddingCode = createSelector(this.getParsedToken, (token) => {
            return token?.uniqueWeddingCode;
        });
        this.selectTokenBody = createSelector(this.getParsedToken, (token) => token);
    }

    private getParsedToken = (): TokenBody | undefined => {
        const config: IConfig = getConfig();
        const token: string | undefined = new Cookies(document.cookie).get(`${config.JwtTokenName}-jwt`);
        if (!token) {
            return undefined;
        }
        const tokenParts = token.split(".");
        const body: string | undefined = tokenParts[1];
        if (!body) {
            return undefined;
        }
        return JSON.parse(window.atob(body));
    };

    private getAuthenticationStore = (state: ApplicationReduxState) => state[AuthenticationReducerKeys.ReducerName];
}
