import { isEmpty } from "lodash";
import { ParametricSelector, Selector, createSelector } from "reselect";
import { singleton } from "tsyringe";
import { Guest, GuestList } from "../../models";
import { ApplicationReduxState } from "../reducers";
import { GuestListReducerKeys } from "../types";

@singleton()
export class GuestListSelector {
    public readonly selectGuestLists: Selector<ApplicationReduxState, GuestList[]>;
    public readonly selectGuests: Selector<ApplicationReduxState, Guest[]>;
    public readonly selectGuestsInList: ParametricSelector<ApplicationReduxState, string, Guest[]>;
    public readonly selectGuestListError: Selector<ApplicationReduxState, string | undefined>;
    public readonly selectIsSavingGuestList: Selector<ApplicationReduxState, boolean>;
    public readonly selectGuestsByIds: ParametricSelector<ApplicationReduxState, string[], Guest[]>;

    public constructor() {
        this.selectGuestListError = createSelector(
            this.getGuestListStore,
            (store) => store[GuestListReducerKeys.GuestListError],
        );
        this.selectGuestLists = createSelector(
            this.getGuestListStore,
            (store) => store[GuestListReducerKeys.GuestLists],
        );
        this.selectGuests = createSelector(this.getGuestListStore, (store) => store[GuestListReducerKeys.Guests]);
        this.selectIsSavingGuestList = createSelector(
            this.getGuestListStore,
            (store) => store[GuestListReducerKeys.IsSavingGuestList],
        );
        this.selectGuestsInList = createSelector(
            [this.selectGuestLists, this.selectGuests, (state: ApplicationReduxState, id: string) => id],
            (guestLists, guests, id) => {
                const list = guestLists.find((guestList) => guestList._id === id);
                if (!list) {
                    return [];
                }
                return guests.filter((guest) => list.guests.indexOf(guest._id ?? "") >= 0);
            },
        );
        this.selectGuestsByIds = createSelector(
            [this.selectGuests, (state: ApplicationReduxState, ids: string[]) => ids],
            (guests, ids) => {
                if (isEmpty(guests)) {
                    return [];
                }
                if (isEmpty(ids)) {
                    return guests;
                }
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                return guests.filter((guest) => ids.indexOf(guest._id!) >= 0);
            },
        );
    }

    private getGuestListStore = (state: ApplicationReduxState) => state[GuestListReducerKeys.ReducerName];
}
