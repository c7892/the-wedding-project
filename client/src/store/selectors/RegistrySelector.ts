import { createSelector, Selector } from "reselect";
import { singleton } from "tsyringe";
import { RegistryReducerKeys } from "../types";
import { Registry } from "../../models";
import { ApplicationReduxState } from "../reducers";

@singleton()
export class RegistrySelector {
    public readonly selectRegistry: Selector<ApplicationReduxState, Registry | undefined>;
    public readonly selectIsSavingRegistry: Selector<ApplicationReduxState, boolean>;
    public readonly selectRegistryMessage: Selector<ApplicationReduxState, string | undefined>;

    public constructor() {
        this.selectRegistry = createSelector(
            this.getRegistryStore,
            (store) => store[RegistryReducerKeys.WeddingRegistry],
        );
        this.selectIsSavingRegistry = createSelector(
            this.getRegistryStore,
            (store) => store[RegistryReducerKeys.IsSavingRegistry],
        );
        this.selectRegistryMessage = createSelector(
            this.getRegistryStore,
            (store) => store[RegistryReducerKeys.RegistryMessage],
        );
    }

    private getRegistryStore = (state: ApplicationReduxState) => state[RegistryReducerKeys.ReducerName];
}
