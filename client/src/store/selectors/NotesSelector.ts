import { createSelector, Selector } from "reselect";
import { singleton } from "tsyringe";
import { Notes } from "../../models";
import { ApplicationReduxState } from "../reducers";
import { NotesReducerKeys, NotesStore } from "../types";

@singleton()
export class NotesSelector {
    public readonly selectIsSavingNotes: Selector<ApplicationReduxState, boolean | undefined>;
    public readonly selectAllNotes: Selector<ApplicationReduxState, Notes[]>;
    public readonly selectNotesBeingSaved: Selector<ApplicationReduxState, Notes | undefined>;
    public readonly selectNotes: Selector<ApplicationReduxState, Notes | undefined>;

    public constructor() {
        this.selectAllNotes = createSelector(this.getStore, (store) => store[NotesReducerKeys.AllNotes]);
        this.selectIsSavingNotes = createSelector(this.getStore, (store) => store[NotesReducerKeys.IsSavingNotes]);
        this.selectNotes = createSelector(this.getStore, (store) => store[NotesReducerKeys.Note]);
        this.selectNotesBeingSaved = createSelector(this.getStore, (store) => store[NotesReducerKeys.NoteBeingSaved]);
    }

    private getStore = (state: ApplicationReduxState): NotesStore => state[NotesReducerKeys.ReducerName];
}
