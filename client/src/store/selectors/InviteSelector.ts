import { createSelector, Selector } from "reselect";
import { singleton } from "tsyringe";
import { Invite } from "../../models";
import { ApplicationReduxState } from "../reducers";
import { InviteReducerKeys } from "../types";

@singleton()
export class InviteSelector {
    public readonly selectInvites: Selector<ApplicationReduxState, Invite[]>;
    public readonly selectInviteBeingSaved: Selector<ApplicationReduxState, Invite | undefined>;
    public readonly selectIsSavingInvite: Selector<ApplicationReduxState, boolean>;
    public readonly selectIsSendingInvite: Selector<ApplicationReduxState, boolean>;

    public constructor() {
        this.selectInvites = createSelector(this.getInviteStore, (store) => store[InviteReducerKeys.Invites]);
        this.selectInviteBeingSaved = createSelector(
            this.getInviteStore,
            (store) => store[InviteReducerKeys.InviteBeingSaved],
        );
        this.selectIsSavingInvite = createSelector(
            this.getInviteStore,
            (store) => store[InviteReducerKeys.IsSavingInvite],
        );
        this.selectIsSendingInvite = createSelector(
            this.getInviteStore,
            (store) => store[InviteReducerKeys.IsSendingInvites],
        );
    }

    private getInviteStore = (state: ApplicationReduxState) => state[InviteReducerKeys.ReducerName];
}
