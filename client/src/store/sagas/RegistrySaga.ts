import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { container } from "tsyringe";
import { RegistryActionCreator } from "../actions";
import { Registry } from "../../models";
import { RegistryService } from "../../services";
import { RegistryActionTypes, UpdateRegistryAction } from "../types";
import { ISaga } from "./ISaga";

export class RegistrySaga implements ISaga {
    private readonly registryActionCreator: RegistryActionCreator;
    private readonly registryService: RegistryService;

    public constructor(inviteActionCreator: RegistryActionCreator, inviteService: RegistryService) {
        this.registryActionCreator = inviteActionCreator;
        this.registryService = inviteService;
        this.watcher = this.watcher.bind(this);
        this.fetchRegistry = this.fetchRegistry.bind(this);
        this.updateRegistry = this.updateRegistry.bind(this);
    }

    public *fetchRegistry() {
        const registry: Registry | undefined = yield call(this.registryService.fetchRegistry);
        if (!registry) {
            yield put(this.registryActionCreator.setRegistryMessage("Failed to find a registry"));
        }
        yield put(this.registryActionCreator.setRegistry(registry));
    }
    public *updateRegistry({ payload }: UpdateRegistryAction) {
        yield put(this.registryActionCreator.setIsSavingRegistry(true));
        const registry: Registry | undefined = yield call(this.registryService.updateRegistry, payload);
        if (!registry) {
            yield put(this.registryActionCreator.setRegistryMessage("Failed to update the registry"));
        } else {
            yield put(this.registryActionCreator.setRegistryMessage("Successfully updated the registry"));
        }
        yield put(this.registryActionCreator.setRegistry(registry));
        yield put(this.registryActionCreator.setIsSavingRegistry(false));
    }

    public *watcher() {
        yield all([
            takeEvery(RegistryActionTypes.FETCH_REGISTRY, this.fetchRegistry),
            takeLatest(RegistryActionTypes.UPDATE_REGISTRY, this.updateRegistry),
        ]);
    }

    public static getSaga = () => {
        return new RegistrySaga(
            container.resolve(RegistryActionCreator),
            new RegistryService(container.resolve("Config")),
        );
    };
}
