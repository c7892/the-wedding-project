import { all, call, put, select, takeEvery, takeLatest } from "redux-saga/effects";
import { container } from "tsyringe";
import { Event } from "../../models";
import { GuestService } from "../../services";
import { EventService } from "../../services/EventService";
import { sleep } from "../../utilities";
import { EventActionCreator } from "../actions/EventActionCreator";
import { AuthenticationSelector, EventSelector } from "../selectors";
import {
    DeleteEventAction,
    EventActionTypes,
    FetchArEventAction,
    FetchEventsAction,
    RsvpAction,
    SaveEventAction,
    UnRsvpAction,
} from "../types";
import { ISaga } from "./ISaga";

export class EventSaga implements ISaga {
    private readonly eventActionCreator: EventActionCreator;
    private readonly eventService: EventService;
    private readonly guestService: GuestService;
    private readonly eventSelector: EventSelector;
    private readonly authSelector: AuthenticationSelector;

    public constructor(
        eventActionCreator: EventActionCreator,
        eventService: EventService,
        eventSelector: EventSelector,
        guestService: GuestService,
        authSelector: AuthenticationSelector,
    ) {
        this.eventActionCreator = eventActionCreator;
        this.eventService = eventService;
        this.eventSelector = eventSelector;
        this.guestService = guestService;
        this.authSelector = authSelector;
        this.watcher = this.watcher.bind(this);
        this.fetchEvents = this.fetchEvents.bind(this);
        this.saveEvent = this.saveEvent.bind(this);
        this.fetchArEvent = this.fetchArEvent.bind(this);
        this.deleteEvent = this.deleteEvent.bind(this);
        this.rsvp = this.rsvp.bind(this);
        this.unrsvp = this.unrsvp.bind(this);
        this.fetchWeddingDay = this.fetchWeddingDay.bind(this);
        this.fetchAllEvents = this.fetchAllEvents.bind(this);
    }

    public *fetchAllEvents() {
        const events: Event[] = yield call(this.eventService.fetchAllEvents);
        yield put(this.eventActionCreator.setAllEvents(events));
    }

    public *fetchEvents({ payload }: FetchEventsAction) {
        const isGuest: boolean = yield select(this.authSelector.selectIsGuest);
        const events: Event[] = yield isGuest
            ? call(this.guestService.fetchEvents)
            : call(this.eventService.fetchEvents, payload);
        yield put(this.eventActionCreator.setEvents(events));
    }

    public *saveEvent({ payload }: SaveEventAction) {
        yield put(this.eventActionCreator.setIsSavingEvent(true));
        if (!this.isValidEventForSaving(payload)) {
            yield call(sleep, 500);
            yield put(this.eventActionCreator.setEventBeingSaving(undefined));
            yield put(this.eventActionCreator.setIsSavingEvent(false));
            return;
        }
        const event: Event | undefined = yield call(this.eventService.saveEvent, payload);
        yield put(this.eventActionCreator.setEventBeingSaving(event));
        yield put(this.eventActionCreator.setIsSavingEvent(false));
    }

    public *fetchArEvent({ payload }: FetchArEventAction) {
        const events: Event | undefined = yield call(this.eventService.fetchArEvent, payload);
        yield put(this.eventActionCreator.setArEvent(events));
    }
    public *fetchWeddingDay() {
        const weddingDay: Event | undefined = yield call(this.eventService.fetchWeddingEvent);
        yield put(this.eventActionCreator.setWeddingDay(weddingDay));
    }

    public *deleteEvent({ payload }: DeleteEventAction) {
        const event: Event | undefined = yield call(this.eventService.deleteEvent, payload);
        if (!event) {
            return;
        }
        const events: Event[] = yield select(this.eventSelector.selectUserEvents);
        const filtered = events.filter((event) => event._id !== event._id);
        yield put(this.eventActionCreator.setEvents(filtered));
    }

    public *rsvp({ payload }: RsvpAction) {
        yield call(this.guestService.rsvp, payload.eventId, payload.guests);
        yield put(this.eventActionCreator.fetchEvents());
    }
    public *unrsvp({ payload }: UnRsvpAction) {
        yield call(this.guestService.unrsvp, payload);
        yield put(this.eventActionCreator.fetchEvents());
    }

    private isValidEventForSaving(event: Event) {
        if (!event.title || event.title.trim().length === 0) {
            return false;
        }
        if (!event.description || event.description.trim().length === 0) {
            return false;
        }
        if (event.start >= event.end) {
            return false;
        }
        if (event.rsvpByDate >= event.start) {
            return false;
        }
        return true;
    }

    public *watcher() {
        yield all([
            takeEvery(EventActionTypes.FETCH_EVENTS, this.fetchEvents),
            takeLatest(EventActionTypes.SAVE_EVENT, this.saveEvent),
            takeEvery(EventActionTypes.FETCH_AR_EVENT, this.fetchArEvent),
            takeEvery(EventActionTypes.DELETE_EVENT, this.deleteEvent),
            takeLatest(EventActionTypes.RSVP, this.rsvp),
            takeLatest(EventActionTypes.UNRSVP, this.unrsvp),
            takeLatest(EventActionTypes.FETCH_WEDDING_DAY, this.fetchWeddingDay),
            takeLatest(EventActionTypes.FETCH_ALL_EVENTS, this.fetchAllEvents),
        ]);
    }

    public static getSaga = () => {
        return new EventSaga(
            container.resolve(EventActionCreator),
            new EventService(container.resolve("Config")),
            container.resolve(EventSelector),
            new GuestService(container.resolve("Config")),
            container.resolve(AuthenticationSelector),
        );
    };
}
