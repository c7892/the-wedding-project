import { all, call, put, select, takeLatest } from "redux-saga/effects";
import { container } from "tsyringe";
import { Notes } from "../../models";
import { NotesService } from "../../services";
import { NotesActionCreator } from "../actions";
import { AuthenticationSelector } from "../selectors";
import { DeleteNotesAction, FetchNotesAction, NotesActionTypes, SaveNotesAction } from "../types";
import { ISaga } from "./ISaga";

export class NotesSaga implements ISaga {
    private readonly notesActionCreator: NotesActionCreator;
    private readonly notesService: NotesService;
    private readonly authSelector: AuthenticationSelector;

    public constructor(
        notesActionCreator: NotesActionCreator,
        notesService: NotesService,
        authSelector: AuthenticationSelector,
    ) {
        this.notesActionCreator = notesActionCreator;
        this.notesService = notesService;
        this.authSelector = authSelector;
        this.watcher = this.watcher.bind(this);
        this.fetchAllNotes = this.fetchAllNotes.bind(this);
        this.saveNotes = this.saveNotes.bind(this);
        this.deleteNotes = this.deleteNotes.bind(this);
        this.fetchNotes = this.fetchNotes.bind(this);
    }

    public *fetchAllNotes() {
        const isGuest: boolean = yield select(this.authSelector.selectIsGuest);
        const notes: Notes[] = yield call(this.notesService.fetchAllNotes, isGuest ? true : undefined);
        yield put(this.notesActionCreator.setAllNotes(notes));
    }

    public *saveNotes({ payload }: SaveNotesAction) {
        yield put(this.notesActionCreator.setIsSavingNotes(true));
        yield put(this.notesActionCreator.setNotesBeingSaved(undefined));
        const notes: Notes | undefined = yield call(this.notesService.saveNotes, payload);
        yield put(this.notesActionCreator.setNotesBeingSaved(notes));
        yield put(this.notesActionCreator.setIsSavingNotes(false));
    }
    public *deleteNotes({ payload }: DeleteNotesAction) {
        yield call(this.notesService.deleteNotes, payload);
        yield put(this.notesActionCreator.fetchAllNotes());
    }
    public *fetchNotes({ payload }: FetchNotesAction) {
        const notes: Notes | undefined = yield call(this.notesService.fetchNotes, payload);
        yield put(this.notesActionCreator.setNotes(notes));
    }

    public *watcher() {
        yield all([
            takeLatest(NotesActionTypes.FETCH_ALL_NOTES, this.fetchAllNotes),
            takeLatest(NotesActionTypes.SAVE_NOTES, this.saveNotes),
            takeLatest(NotesActionTypes.DELETE_NOTES, this.deleteNotes),
            takeLatest(NotesActionTypes.FETCH_NOTES, this.fetchNotes),
        ]);
    }
    public static getSaga = () => {
        return new NotesSaga(
            container.resolve(NotesActionCreator),
            new NotesService(container.resolve("Config")),
            container.resolve(AuthenticationSelector),
        );
    };
}
