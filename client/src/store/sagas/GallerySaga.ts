import { isEmpty } from "lodash";
import { all, call, put, select, spawn, takeEvery, takeLatest } from "redux-saga/effects";
import { container } from "tsyringe";
import {
    AddGalleryImageAction,
    CreateGalleryAction,
    DeleteGalleryAction,
    DownloadGalleryAction,
    FetchGalleryAction,
    GalleryActionTypes,
    RemoveGalleryImageAction,
    UpdateGalleryAction,
} from "..";
import { Gallery } from "../../models";
import { GalleryService } from "../../services";
import { GalleryActionCreator } from "../actions";
import { GallerySelector } from "../selectors";
import { ISaga } from "./ISaga";

export class GallerySaga implements ISaga {
    private readonly galleryActionCreator: GalleryActionCreator;
    private readonly galleryService: GalleryService;
    private readonly gallerySelector: GallerySelector;

    public constructor(
        galleryActionCreator: GalleryActionCreator,
        galleryService: GalleryService,
        gallerySelector: GallerySelector,
    ) {
        this.galleryActionCreator = galleryActionCreator;
        this.galleryService = galleryService;
        this.gallerySelector = gallerySelector;
        this.watcher = this.watcher.bind(this);
        this.fetchAllGalleries = this.fetchAllGalleries.bind(this);
        this.fetchGallery = this.fetchGallery.bind(this);
        this.createGallery = this.createGallery.bind(this);
        this.updateGallery = this.updateGallery.bind(this);
        this.deleteGallery = this.deleteGallery.bind(this);
        this.removeImageFromGallery = this.removeImageFromGallery.bind(this);
        this.addImageToGallery = this.addImageToGallery.bind(this);
        this.addImageToGalleryThread = this.addImageToGalleryThread.bind(this);
        this.downloadGallery = this.downloadGallery.bind(this);
    }

    public *fetchAllGalleries() {
        const galleries: Gallery[] = yield call(this.galleryService.fetchGalleries);
        if (isEmpty(galleries)) {
            return;
        }
        yield put(this.galleryActionCreator.setGalleries(galleries));
    }
    public *fetchGallery({ payload }: FetchGalleryAction) {
        const gallery: Gallery | undefined = yield call(this.galleryService.fetchGallery, payload);
        if (!gallery) {
            yield put(this.galleryActionCreator.setGalleryActionMessage("Failed to find gallery"));
            return;
        }
        const galleries: Gallery[] = yield select(this.gallerySelector.selectGalleries);
        const index = galleries.findIndex((gall) => gall._id === gallery._id);
        if (index >= 0) {
            galleries.splice(index, 1);
        }
        galleries.push(gallery);
        yield put(this.galleryActionCreator.setGalleries(galleries));
    }
    public *downloadGallery({ payload }: DownloadGalleryAction) {
        yield put(this.galleryActionCreator.setGalleryActionMessage("Downloading gallery, please wait"));
        const gallery: string | undefined = yield call(this.galleryService.downloadGallery, payload);
        if (!gallery) {
            yield put(
                this.galleryActionCreator.setGalleryActionMessage(
                    "Failed to download gallery, please try again or download individual photos",
                ),
            );
            return;
        }
        yield put(this.galleryActionCreator.setDownloadGalleryLink(gallery));
        yield put(this.galleryActionCreator.setGalleryActionMessage("Download successful"));
    }
    public *createGallery({ payload }: CreateGalleryAction) {
        const gallery: Gallery | undefined = yield call(this.galleryService.createGallery, payload);
        if (!gallery) {
            yield put(this.galleryActionCreator.setGalleryActionMessage("Failed to create gallery, please try again"));
            return;
        }
        const galleries: Gallery[] = yield select(this.gallerySelector.selectGalleries);
        const index = galleries.findIndex((gall) => gall._id === gallery._id);
        if (index >= 0) {
            galleries.splice(index, 1);
        }
        galleries.push(gallery);
        yield put(this.galleryActionCreator.setGalleries(galleries));
    }
    public *updateGallery({ payload }: UpdateGalleryAction) {
        const gallery: Gallery | undefined = yield call(this.galleryService.updateGallery, payload);
        if (!gallery) {
            yield put(this.galleryActionCreator.setGalleryActionMessage("Failed to update gallery, please try again"));
            return;
        }
        const galleries: Gallery[] = yield select(this.gallerySelector.selectGalleries);
        const index = galleries.findIndex((gall) => gall._id === gallery._id);
        if (index >= 0) {
            galleries.splice(index, 1);
        }
        galleries.push(gallery);
        yield put(this.galleryActionCreator.setGalleries(galleries));
    }
    public *deleteGallery({ payload }: DeleteGalleryAction) {
        yield call(this.galleryService.deleteGallery, payload);
        yield put(this.galleryActionCreator.setGalleryActionMessage("Deleted gallery"));
        yield put(this.galleryActionCreator.fetchGalleries());
    }
    public *removeImageFromGallery({ payload }: RemoveGalleryImageAction) {
        const gallery: Gallery | undefined = yield call(
            this.galleryService.removeImageFromGallery,
            payload.src,
            payload.galleryId,
        );
        if (!gallery) {
            yield put(this.galleryActionCreator.setGalleryActionMessage("Failed to update gallery, please try again"));
            return;
        }
        const galleries: Gallery[] = yield select(this.gallerySelector.selectGalleries);
        const index = galleries.findIndex((gall) => gall._id === gallery._id);
        if (index >= 0) {
            galleries.splice(index, 1);
        }
        galleries.push(gallery);
        yield put(this.galleryActionCreator.setGalleries(galleries));
        yield put(this.galleryActionCreator.setGalleryActionMessage("Successfully to updated gallery"));
    }
    public *addImageToGallery(action: AddGalleryImageAction) {
        for (const payload of action.payload) {
            yield spawn(this.addImageToGalleryThread, payload);
        }
    }

    private *addImageToGalleryThread(payload: { galleryId?: string; eventId?: string; image: File }) {
        yield put(
            this.galleryActionCreator.setGalleryActionMessage(`Adding ${payload.image.name} to gallery, please wait`),
        );
        const gallery: Gallery | undefined = yield call(
            this.galleryService.addImageGallery,
            payload.image,
            payload.galleryId,
            payload.eventId,
        );
        if (!gallery) {
            yield put(
                this.galleryActionCreator.setGalleryActionMessage(
                    `Failed to add ${payload.image.name} to gallery, please try again`,
                ),
            );
            return;
        }
        yield put(
            this.galleryActionCreator.setGalleryActionMessage(`Successfully to added ${payload.image.name} to gallery`),
        );
        const galleries: Gallery[] = yield select(this.gallerySelector.selectGalleries);
        const index = galleries.findIndex((gall) => gall._id === gallery._id);
        if (index >= 0) {
            galleries.splice(index, 1);
        }
        galleries.push(gallery);
        yield put(this.galleryActionCreator.setGalleries(galleries));
    }

    public *watcher() {
        yield all([
            takeEvery(GalleryActionTypes.FETCH_GALLERIES, this.fetchAllGalleries),
            takeEvery(GalleryActionTypes.FETCH_GALLERY, this.fetchGallery),
            takeLatest(GalleryActionTypes.CREATE_GALLERY, this.createGallery),
            takeLatest(GalleryActionTypes.UPDATE_GALLERY, this.updateGallery),
            takeLatest(GalleryActionTypes.DELETE_GALLERY, this.deleteGallery),
            takeLatest(GalleryActionTypes.REMOVE_IMAGE_FROM_GALLERY, this.removeImageFromGallery),
            takeLatest(GalleryActionTypes.DOWNLOAD_GALLERY, this.downloadGallery),
            takeEvery(GalleryActionTypes.ADD_IMAGE_TO_GALLERY, this.addImageToGallery),
        ]);
    }

    public static getSaga = () => {
        return new GallerySaga(
            container.resolve(GalleryActionCreator),
            new GalleryService(container.resolve("Config")),
            container.resolve(GallerySelector),
        );
    };
}
