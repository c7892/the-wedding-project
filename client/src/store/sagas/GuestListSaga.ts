import { container } from "tsyringe";
import { all, takeLatest } from "@redux-saga/core/effects";
import { GuestListActionCreator } from "../actions";
import { ISaga } from "./ISaga";
import { GuestListService } from "../../services";
import { call, put, takeEvery, takeLeading } from "redux-saga/effects";
import {
    DeleteGuestAction,
    DeleteGuestListAction,
    GuestListActionTypes,
    SaveGuestListAction,
    UpdateGuestInfoAction,
} from "../types";
import { Guest, GuestList } from "../../models";

export class GuestListSaga implements ISaga {
    private readonly guestListActionCreator: GuestListActionCreator;
    private readonly guestListService: GuestListService;
    public constructor(guestListActionCreator: GuestListActionCreator, guestListService: GuestListService) {
        this.guestListActionCreator = guestListActionCreator;
        this.guestListService = guestListService;
        this.watcher = this.watcher.bind(this);
        this.saveList = this.saveList.bind(this);
        this.fetchGuestLists = this.fetchGuestLists.bind(this);
        this.fetchGuests = this.fetchGuests.bind(this);
        this.updateGuestInfo = this.updateGuestInfo.bind(this);
        this.deleteGuest = this.deleteGuest.bind(this);
        this.deleteGuestList = this.deleteGuestList.bind(this);
    }

    public *saveList({ payload }: SaveGuestListAction) {
        yield put(this.guestListActionCreator.setIsSavingGuestList(true));
        try {
            const { list, guests } = payload;
            if (!list.name.trim()) {
                yield put(this.guestListActionCreator.setGuestListError("Please provide a name for your guest list"));
                yield put(this.guestListActionCreator.setIsSavingGuestList(false));
                return;
            }
            if (guests && guests.length > 0) {
                for (const guest of guests) {
                    const { firstName, surname, mobile } = guest;
                    if (!firstName || !surname || !mobile) {
                        yield put(
                            this.guestListActionCreator.setGuestListError(
                                "Please provide a name and phone for each guest on your list",
                            ),
                        );
                        yield put(this.guestListActionCreator.setIsSavingGuestList(false));
                        return;
                    }
                }
            }
            const savedList: GuestList | null | undefined = yield call(
                this.guestListService.saveGuestListAsync,
                list,
                guests,
            );
            if (!savedList) {
                throw new Error();
            }
            yield put(this.guestListActionCreator.setGuestListError(undefined));
            yield put(this.guestListActionCreator.fetchGuestLists());
            yield put(this.guestListActionCreator.fetchGuests());
        } catch (err) {
            yield put(this.guestListActionCreator.setGuestListError("Failed to save guest list, try again"));
        }
        yield put(this.guestListActionCreator.setIsSavingGuestList(false));
    }

    public *fetchGuestLists() {
        try {
            const lists: GuestList[] | undefined = yield call(this.guestListService.getGuestListsAsync);
            if (!lists) {
                return;
            }
            yield put(this.guestListActionCreator.updateGuestLists(lists));
        } catch (err) {
            // do nothing
        }
    }
    public *fetchGuests() {
        try {
            const guests: Guest[] | undefined = yield call(this.guestListService.getGuestsAsync);
            if (!guests) {
                return;
            }
            yield put(this.guestListActionCreator.saveGuests(guests));
        } catch (err) {
            // do nothing
        }
    }
    public *updateGuestInfo({ payload }: UpdateGuestInfoAction) {
        try {
            yield put(this.guestListActionCreator.setIsSavingGuestList(true));
            const { firstName, surname, mobile } = payload;
            if (!firstName || !surname || !mobile) {
                yield put(
                    this.guestListActionCreator.setGuestListError("Please provide a name and phone for your guest"),
                );
                return;
            }
            yield call(this.guestListService.updateGuestInfo, payload);
            yield put(this.guestListActionCreator.setGuestListError(undefined));
            yield put(this.guestListActionCreator.fetchGuestLists());
            yield put(this.guestListActionCreator.fetchGuests());
        } catch (err) {
            yield put(this.guestListActionCreator.setGuestListError("Failed to save guest info, try again"));
        }
        yield put(this.guestListActionCreator.setIsSavingGuestList(false));
    }
    public *deleteGuest({ payload }: DeleteGuestAction) {
        try {
            if (!payload._id) {
                return;
            }
            yield put(this.guestListActionCreator.setIsSavingGuestList(true));
            yield call(this.guestListService.deleteGuest, payload._id);
            yield put(this.guestListActionCreator.setGuestListError(undefined));
            yield put(this.guestListActionCreator.fetchGuestLists());
            yield put(this.guestListActionCreator.fetchGuests());
        } catch (err) {
            yield put(this.guestListActionCreator.setGuestListError("Failed to delete guest info, try again"));
        }
        yield put(this.guestListActionCreator.setIsSavingGuestList(false));
    }
    public *deleteGuestList({ payload }: DeleteGuestListAction) {
        try {
            if (!payload._id) {
                return;
            }
            yield put(this.guestListActionCreator.setIsSavingGuestList(true));
            yield call(this.guestListService.deleteGuestList, payload._id);
            yield put(this.guestListActionCreator.setGuestListError(undefined));
            yield put(this.guestListActionCreator.fetchGuestLists());
            yield put(this.guestListActionCreator.fetchGuests());
        } catch (err) {
            yield put(this.guestListActionCreator.setGuestListError("Failed to delete guest info, try again"));
        }
        yield put(this.guestListActionCreator.setIsSavingGuestList(false));
    }

    public *watcher() {
        yield all([
            takeLatest(GuestListActionTypes.SAVE_GUEST_LISTS, this.saveList),
            takeEvery(GuestListActionTypes.FETCH_GUEST_LISTS, this.fetchGuestLists),
            takeEvery(GuestListActionTypes.FETCH_GUESTS, this.fetchGuests),
            takeLeading(GuestListActionTypes.UPDATE_GUEST_INFO, this.updateGuestInfo),
            takeEvery(GuestListActionTypes.DELETE_GUEST, this.deleteGuest),
            takeEvery(GuestListActionTypes.DELETE_GUEST_LIST, this.deleteGuestList),
        ]);
    }

    public static getSaga = (): GuestListSaga =>
        new GuestListSaga(container.resolve(GuestListActionCreator), new GuestListService(container.resolve("Config")));
}
