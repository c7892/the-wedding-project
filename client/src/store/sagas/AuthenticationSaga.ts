import { container } from "tsyringe";
import { all, put, takeLatest } from "@redux-saga/core/effects";
import { AuthenticationActionCreator } from "../actions";
import {
    AuthenticationActionTypes,
    ChangePasswordAction,
    LoginAction,
    RegisterAction,
    UpdateUserAction,
} from "../types";
import { ISaga } from "./ISaga";
import { AuthenticationService } from "../../services";
import { call, select, takeEvery, takeLeading } from "redux-saga/effects";
import Cookies from "universal-cookie";
import { IConfig } from "../../utilities";
import { AuthenticationSelector } from "../selectors";
import { Guest, User } from "../../models";
import { has } from "lodash";

export class AuthenticationSaga implements ISaga {
    private readonly authenticationActionCreator: AuthenticationActionCreator;
    private readonly authenticationService: AuthenticationService;
    private readonly authenticationSelector: AuthenticationSelector;
    private readonly config: IConfig;
    public constructor(
        authenticationActionCreator: AuthenticationActionCreator,
        authenticationService: AuthenticationService,
        authenticationSelector: AuthenticationSelector,
        config: IConfig,
    ) {
        this.config = config;
        this.authenticationSelector = authenticationSelector;
        this.authenticationActionCreator = authenticationActionCreator;
        this.authenticationService = authenticationService;
        this.watcher = this.watcher.bind(this);
        this.login = this.login.bind(this);
        this.register = this.register.bind(this);
        this.clear = this.clear.bind(this);
        this.logout = this.logout.bind(this);
        this.refreshToken = this.refreshToken.bind(this);
        this.fetchUser = this.fetchUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.deleteAccount = this.deleteAccount.bind(this);
    }

    public *login({ payload }: LoginAction) {
        try {
            yield put(this.authenticationActionCreator.setIsLoggingIn(true));
            yield call(this.authenticationService.loginAsync, payload.email, payload.password);
            yield put(this.authenticationActionCreator.setLogInSucceeded(true));
            yield put(this.authenticationActionCreator.setLoginFailedMessage(undefined));
        } catch (ex) {
            yield put(this.authenticationActionCreator.setLoginFailedMessage("Failed to login, please try again."));
            yield put(this.authenticationActionCreator.setLogInSucceeded(false));
        } finally {
            yield put(this.authenticationActionCreator.setIsLoggingIn(false));
        }
    }

    public *fetchUser() {
        try {
            const user: Guest | User | undefined = yield call(this.authenticationService.fetchUserAsync);
            yield put(this.authenticationActionCreator.setUser(user));
            if (has(user, "newFamilyName")) {
                const spouse: undefined | User[] = yield call(this.authenticationService.fetchSpouseAsync);
                yield put(this.authenticationActionCreator.setSpouse(spouse));
            }
        } catch (ex) {
            // do nothing
        }
    }

    public *updateUser({ payload }: UpdateUserAction) {
        try {
            yield put(this.authenticationActionCreator.setIsSavingUser(true));
            const user: Guest | User | undefined = yield call(
                this.authenticationService.updateUser,
                payload.updates,
                payload.image,
            );
            if (!user) {
                throw Error();
            }
            yield put(this.authenticationActionCreator.setUser(user));
        } catch (ex) {
            // do nothing
            yield put(
                this.authenticationActionCreator.setIsSavingUserError(
                    "Failed to make account updates, please try again",
                ),
            );
        } finally {
            yield put(this.authenticationActionCreator.setIsSavingUser(false));
        }
    }

    public *changePassword({ payload }: ChangePasswordAction) {
        try {
            yield put(this.authenticationActionCreator.setIsSavingUser(true));
            const user: Guest | User | undefined = yield call(
                this.authenticationService.changePassword,
                payload.oldPassword,
                payload.newPassword,
            );
            if (!user) {
                throw Error();
            }
            yield put(this.authenticationActionCreator.setUser(user));
        } catch (ex) {
            // do nothing
            yield put(
                this.authenticationActionCreator.setIsSavingUserError("Failed to change password, please try again"),
            );
        } finally {
            yield put(this.authenticationActionCreator.setIsSavingUser(false));
        }
    }
    public *deleteAccount() {
        try {
            const isGuest: boolean = yield select(this.authenticationSelector.selectIsGuest);
            const didDelete: boolean = yield call(this.authenticationService.deleteAccount, isGuest);
            if (!didDelete) {
                throw new Error();
            }
            yield put(this.authenticationActionCreator.setIsSavingUserError("Successfully deleted account"));
            yield put(this.authenticationActionCreator.logout());
        } catch (ex) {
            yield put(
                this.authenticationActionCreator.setIsSavingUserError("Failed to delete account, please try again"),
            );
        }
    }

    public *register({ payload }: RegisterAction) {
        try {
            yield put(this.authenticationActionCreator.setIsRegistering(true));
            yield call(this.authenticationService.registerAsync, payload);
            yield put(this.authenticationActionCreator.setRegistrationSucceeded(true));
            yield put(this.authenticationActionCreator.setRegistrationFailedMessage(undefined));
        } catch (ex) {
            yield put(
                this.authenticationActionCreator.setRegistrationFailedMessage("Failed to login, please try again."),
            );
            yield put(this.authenticationActionCreator.setRegistrationSucceeded(false));
        } finally {
            yield put(this.authenticationActionCreator.setIsRegistering(false));
        }
    }
    public *clear() {
        yield put(this.authenticationActionCreator.setIsLoggingIn(undefined));
        yield put(this.authenticationActionCreator.setLogInSucceeded(undefined));
        yield put(this.authenticationActionCreator.setLoginFailedMessage(undefined));
        yield put(this.authenticationActionCreator.setIsRegistering(undefined));
        yield put(this.authenticationActionCreator.setRegistrationSucceeded(undefined));
        yield put(this.authenticationActionCreator.setRegistrationFailedMessage(undefined));
        yield put(this.authenticationActionCreator.setUser(undefined));
    }
    public *logout() {
        new Cookies(document.cookie).remove(`${this.config.JwtTokenName}-jwt`);
        yield put(this.authenticationActionCreator.clear());
    }
    public *refreshToken() {
        try {
            const timeTilExp = this.getTimeUntilExp();
            if (timeTilExp <= 0) {
                yield put(this.authenticationActionCreator.logout());
                return;
            }
            if (timeTilExp > 12 * 60 * 1000) {
                return;
            }
            yield call(this.authenticationService.refreshToken);
        } catch (ex) {
            yield put(this.authenticationActionCreator.logout());
        }
    }

    private getTimeUntilExp() {
        const config = container.resolve<IConfig>("Config");
        const token: string | undefined = new Cookies(document.cookie).get(`${config.JwtTokenName}-jwt`);
        if (!token) {
            return -1;
        }
        const tokenParts = token.split(".");
        const body: string | undefined = tokenParts[1];
        if (!body) {
            return -1;
        }
        const decodedToken: { exp: number } = JSON.parse(window.atob(body));
        return decodedToken.exp - Date.now();
    }

    public *watcher() {
        yield all([
            takeLatest(AuthenticationActionTypes.LOGIN, this.login),
            takeLatest(AuthenticationActionTypes.FETCH_USER, this.fetchUser),
            takeLeading(AuthenticationActionTypes.REGISTER, this.register),
            takeLatest(AuthenticationActionTypes.CLEAR_AUTH_PROCESS_DATA, this.clear),
            takeLatest(AuthenticationActionTypes.UPDATE_USER, this.updateUser),
            takeLatest(AuthenticationActionTypes.DELETE_ACCOUNT, this.deleteAccount),
            takeEvery(AuthenticationActionTypes.LOGOUT, this.logout),
            takeEvery(AuthenticationActionTypes.REAUTH_TOKEN, this.refreshToken),
            takeEvery(AuthenticationActionTypes.CHANGE_PASSWORD, this.changePassword),
        ]);
    }

    public static getSaga = (): AuthenticationSaga =>
        new AuthenticationSaga(
            container.resolve(AuthenticationActionCreator),
            new AuthenticationService(container.resolve("Config")),
            container.resolve(AuthenticationSelector),
            container.resolve("Config"),
        );
}
