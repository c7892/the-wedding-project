import { all, call, put, select, takeEvery, takeLatest } from "redux-saga/effects";
import { container } from "tsyringe";
import { Invite } from "../../models";
import { InviteService } from "../../services";
import { sleep } from "../../utilities";
import { InviteActionCreator } from "../actions/InviteActionCreator";
import { InviteSelector } from "../selectors";
import {
    DeleteInviteAction,
    InviteActionTypes,
    SaveInviteAction,
    SendInviteAction,
    SendInviteConfirmationAction,
} from "../types";
import { ISaga } from "./ISaga";

export class InviteSaga implements ISaga {
    private readonly inviteActionCreator: InviteActionCreator;
    private readonly inviteService: InviteService;
    private readonly inviteSelector: InviteSelector;

    public constructor(
        inviteActionCreator: InviteActionCreator,
        inviteService: InviteService,
        inviteSelector: InviteSelector,
    ) {
        this.inviteActionCreator = inviteActionCreator;
        this.inviteService = inviteService;
        this.inviteSelector = inviteSelector;
        this.watcher = this.watcher.bind(this);
        this.fetchInvites = this.fetchInvites.bind(this);
        this.saveInvite = this.saveInvite.bind(this);
        this.deleteEvent = this.deleteEvent.bind(this);
        this.sendInvite = this.sendInvite.bind(this);
        this.sendInviteConfirmation = this.sendInviteConfirmation.bind(this);
    }

    public *fetchInvites() {
        const invites: Invite[] = yield call(this.inviteService.fetchInvites);
        yield put(this.inviteActionCreator.setInvites(invites));
    }

    public *saveInvite({ payload }: SaveInviteAction) {
        yield put(this.inviteActionCreator.setIsSavingInvite(true));
        if (!this.isValidEventForSaving(payload)) {
            yield call(sleep, 500);
            yield put(this.inviteActionCreator.setInviteBeingSaved(undefined));
            yield put(this.inviteActionCreator.setIsSavingInvite(false));
            return;
        }
        const invite: Invite | undefined = yield call(this.inviteService.saveInvite, payload);
        yield put(this.inviteActionCreator.setInviteBeingSaved(invite));
        yield put(this.inviteActionCreator.setIsSavingInvite(false));
    }

    public *deleteEvent({ payload }: DeleteInviteAction) {
        const invite: Invite | undefined = yield call(this.inviteService.deleteInvite, payload);
        if (!invite) {
            return;
        }
        const invites: Invite[] = yield select(this.inviteSelector.selectInvites);
        const filtered = invites.filter((item) => item._id !== invite._id);
        yield put(this.inviteActionCreator.setInvites(filtered));
    }

    private isValidEventForSaving(invite: Invite) {
        if (!invite.title || invite.title.trim().length === 0) {
            return false;
        }
        if (!invite.inviteTitle || invite.inviteTitle.trim().length === 0) {
            return false;
        }
        return true;
    }

    public *sendInvite({ payload }: SendInviteAction) {
        yield call(this.inviteService.sendInvite, payload);
    }
    public *sendInviteConfirmation({ payload }: SendInviteConfirmationAction) {
        yield call(this.inviteService.sendInviteConfirmation, payload);
    }

    public *watcher() {
        yield all([
            takeEvery(InviteActionTypes.FETCH_INVITES, this.fetchInvites),
            takeLatest(InviteActionTypes.SAVE_INVITE, this.saveInvite),
            takeEvery(InviteActionTypes.DELETE_INVITE, this.deleteEvent),
            takeEvery(InviteActionTypes.SEND_INVITE, this.sendInvite),
            takeLatest(InviteActionTypes.SEND_INVITE_CONFIRMATION, this.sendInviteConfirmation),
        ]);
    }

    public static getSaga = () => {
        return new InviteSaga(
            container.resolve(InviteActionCreator),
            new InviteService(container.resolve("Config")),
            container.resolve(InviteSelector),
        );
    };
}
