import { spawn } from "redux-saga/effects";
import { GallerySaga } from "./GallerySaga";
import { AuthenticationSaga } from "./AuthenticationSaga";
import { EventSaga } from "./EventSaga";
import { GuestListSaga } from "./GuestListSaga";
import { InviteSaga } from "./InviteSaga";
import { RegistrySaga } from "./RegistrySaga";
import { NotesSaga } from "./NotesSaga";

export const rootSaga = function* rootSaga() {
    yield spawn(AuthenticationSaga.getSaga().watcher);
    yield spawn(GuestListSaga.getSaga().watcher);
    yield spawn(EventSaga.getSaga().watcher);
    yield spawn(InviteSaga.getSaga().watcher);
    yield spawn(GallerySaga.getSaga().watcher);
    yield spawn(RegistrySaga.getSaga().watcher);
    yield spawn(NotesSaga.getSaga().watcher);
};
