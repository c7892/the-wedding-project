import { IConfig, getConfig } from "./IConfig";
import { container } from "tsyringe";
import { CommunicationService, PaymentService } from "../services";

export const bindDependencies = (done?: () => void) => {
    const config = getConfig();
    container.register<IConfig>("Config", { useValue: config });
    container.register<PaymentService>("PaymentService", { useValue: new PaymentService(config) });
    container.register<CommunicationService>("CommunicationService", { useValue: new CommunicationService(config) });
    if (done) {
        done();
    }
};
