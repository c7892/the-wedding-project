/* eslint-disable @typescript-eslint/no-explicit-any */
import { difference, find, forEach, forIn, indexOf, isEmpty } from "lodash";

interface IMatchType {
    location_type: string;
    types: string[];
    exact: boolean;
}

export const addressMatch: { [key: string]: IMatchType[] } = {
    streetAddress: [
        { location_type: "ROOFTOP", types: ["street_address"], exact: true },
        { location_type: "RANGE_INTERPOLATED", types: ["street_address"], exact: false },
    ],
    route: [{ location_type: "GEOMETRIC_CENTER", types: ["route"], exact: true }],
    city: [{ location_type: "APPROXIMATE", types: ["locality", "political"], exact: true }],
    state: [{ location_type: "APPROXIMATE", types: ["administrative_area_level_1", "political"], exact: true }],
    country: [{ location_type: "APPROXIMATE", types: ["country", "political"], exact: true }],
    unknown: [{ location_type: "unknown", types: ["unknown"], exact: true }], //wont match anything in the response.
};

export interface IAddress {
    streetNumber: string;
    street: string;
    streetAbbr: string;
    city: string;
    state: string;
    stateAbbr: string;
    country: string;
    countryAbbr: string;
    postalCode: string;
    colloquialArea?: string;
    postalCodePrefix?: string;
    sublocality?: string;
    subpremise?: string;
    location: { lat: number; lon: number };
    toString: () => string;
}
export const defaultMatchType = addressMatch.streetAddress;
interface IComponent {
    long_name: string;
    short_name: string;
    types: string[];
}
export class Address {
    public matchType: IMatchType[] = [];
    public exactMatch = false;
    public addressStr = "";
    public formatted_address = "";
    public address_components: IComponent[] = [];
    public readonly geometry?: { location?: { lat: number; lng: number }; location_type: string };
    public readonly partial_match: boolean = false;
    public streetNumber = "";
    public street = "";
    public streetAbbr = "";
    public city = "";
    public state = "";
    public stateAbbr = "";
    public country = "";
    public countryAbbr = "";
    public postalCode = "";
    public colloquialArea?: string;
    public sublocality?: string;
    public subpremise?: string;
    public postalCodePrefix = "";
    public types: string[] = [];
    public location: { lat: number; lon: number } | undefined = undefined;

    constructor(address: string | Address, public isObject?: boolean, public generated?: boolean) {
        if (isObject == null) {
            isObject = false;
        }
        this.isObject = isObject;
        if (generated == null) {
            generated = false;
        }
        this.generated = generated;
        if (typeof address === "object") {
            //this gives you higher accuracy because we can compare resulting address parts to the input's address parts and see if its they are the same or not
            this.isObject = true;
            const addy = address as Address;
            let addressWithData: IAddress | undefined = undefined;
            if (addy.address_components) {
                this.generated = true;
                const location = {
                    lat: addy.geometry?.location?.lat ?? 0,
                    lon: addy.geometry?.location?.lng ?? 0,
                };

                this.exactMatch = !address.partial_match;
                forIn(addressMatch, (list) => {
                    return forEach(list, (obj) => {
                        if (
                            obj.location_type === addy.geometry?.location_type &&
                            difference(obj.types, addy.types).length === 0
                        ) {
                            this.matchType = list;
                            if (!obj.exact) {
                                return (this.exactMatch = false);
                            }
                        }
                    });
                });
                if (!this.matchType || isEmpty(this.matchType)) {
                    this.exactMatch = false;
                    this.matchType = addressMatch.unknown;
                }

                const getComponent = this.componentFinder(address.address_components);
                const [, subpremise] = Array.from(getComponent("subpremise"));
                const [, streetNum] = Array.from(getComponent("street_number"));
                const [streetAbbr, street] = Array.from(getComponent("route"));
                const [, city] = Array.from(getComponent("locality"));
                const [stateAbbr, state] = Array.from(getComponent("administrative_area_level_1"));
                const [countryAbbr, country] = Array.from(getComponent("country"));
                const [postalCode] = Array.from(getComponent("postal_code"));
                const [postalCodePrefix] = Array.from(getComponent("postal_code_prefix"));
                const [colloquialArea] = Array.from(getComponent("colloquial_area"));
                const [sublocality] = Array.from(getComponent("sublocality"));
                addressWithData = {
                    streetNumber: streetNum,
                    street,
                    streetAbbr,
                    city,
                    state,
                    stateAbbr,
                    country,
                    countryAbbr,
                    postalCode,
                    postalCodePrefix,
                    colloquialArea,
                    sublocality,
                    location,
                    subpremise,
                };
            }
            if (addressWithData) {
                forIn(addressWithData, (val, key) => {
                    (this as any)[key] = val;
                });
            }
        } else {
            this.addressStr = address as string;
        }
    }

    private componentFinder = (components: IComponent[]) => {
        return (type: string) => {
            const it = find(components, (c) => {
                return indexOf(c.types, type) >= 0;
            });
            return [it !== undefined ? it.short_name : "", it !== undefined ? it.long_name : ""];
        };
    };

    public toString = (useCountryAbbr = true, useStateAbbr = true, useStreetAbbr = false) => {
        if (useCountryAbbr == null) {
            useCountryAbbr = true;
        }
        if (useStateAbbr == null) {
            useStateAbbr = true;
        }
        if (useStreetAbbr == null) {
            useStreetAbbr = false;
        }
        if (!this.isObject) {
            return this.addressStr;
        }
        const arr = [];
        const stateVal = useStateAbbr && this.generated ? "stateAbbr" : "state";
        const countryVal = useCountryAbbr && this.generated ? "countryAbbr" : "country";
        const streetVal = useStreetAbbr && this.generated ? "streetAbbr" : "street";
        const props = this.subpremise
            ? [streetVal, "subpremise", "city", stateVal, countryVal, "postalCode"]
            : [streetVal, "city", stateVal, countryVal, "postalCode"];
        for (const prop of props) {
            const addy = this as any;
            if (addy[prop] !== undefined) {
                arr.push(addy[prop]);
            }
        }
        let str = arr.join(", ");
        if (this.streetNumber) {
            str = `${this.streetNumber} ${str}`;
        }
        return str;
    };
}
