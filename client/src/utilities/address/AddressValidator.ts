/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from "axios";
import { forEach } from "lodash";
import { container, singleton } from "tsyringe";
import { IConfig } from "../IConfig";
import { Address, addressMatch, defaultMatchType } from "./Address";

@singleton()
export class AddressValidator {
    private readonly config: IConfig;
    public constructor() {
        this.config = container.resolve("Config");
    }

    public validate = async (
        address: string | Address,
        addressType = defaultMatchType,
    ): Promise<{ exact: Address[]; inexact: Address[] } | undefined> => {
        const inputAddress = address instanceof Address ? address : new Address(address);
        const qs = {
            sensor: false,
            address: inputAddress.toString(),
            region: "us",
            language: "en",
            key: this.config.GoogleGeoEncodeApiKey,
        };
        const protocol = "https";

        const opts = {
            json: true,
            url: `${protocol}://maps.googleapis.com/maps/api/geocode/json`,
            method: "GET",
            qs,
        };
        const response = await axios.get(opts.url, {
            params: opts.qs,
        });
        const results: any[] = response.data.results;
        if (!results) {
            return undefined;
        }
        const validAddresses: Address[] = [];
        const inexactMatches: Address[] = [];
        forEach(results, (result) => {
            const resAddress = new Address(result);
            if (typeof address === "string" && !resAddress.addressStr) {
                resAddress.addressStr = address;
            }
            if (addressType === addressMatch.unknown) {
                if (this.matchUnknownType(resAddress, inputAddress)) {
                    return validAddresses.push(resAddress);
                } else {
                    return inexactMatches.push(resAddress);
                }
            } else if (addressType === resAddress.matchType) {
                if (resAddress.exactMatch) {
                    return validAddresses.push(resAddress);
                } else {
                    return inexactMatches.push(resAddress);
                }
            } else {
                if (resAddress.exactMatch) {
                    return validAddresses.push(resAddress);
                } else {
                    return inexactMatches.push(resAddress);
                }
            }
        });
        return { exact: validAddresses, inexact: inexactMatches };
    };

    private matchUnknownType = (known: Address, unknown: Address) => {
        const compare = (prop: string) => {
            if ((known as any)[prop] && (unknown as any)[prop]) {
                if ((known as any)[prop].toLowerCase() === (unknown as any)[prop].toLowerCase()) {
                    return true;
                }
                if (unknown.generated && (unknown as any)[prop + "Abbr"]) {
                    return (known as any)[prop].toLowerCase() === (unknown as any)[prop + "Abbr"].toLowerCase();
                } else if (known.generated && (known as any)[prop + "Abbr"]) {
                    return (known as any)[prop + "Abbr"].toLowerCase() === (unknown as any)[prop].toLowerCase();
                } else {
                    return false;
                }
            }
            return !(known as any)[prop] && !(unknown as any)[prop];
        };
        if (known.isObject && unknown.isObject) {
            return compare("city") && compare("state") && compare("country");
        } else if (known.isObject && !unknown.isObject) {
            //unknown was provided as a string, and now we must check if the provided address is indeed one of the ones returned
            const props = ["streetNumber", "street", "city", "state", "country", "postalCode"];
            let otherAddress = (unknown as any).toString().toLowerCase();
            if (known.toString() === otherAddress) {
                return true;
            }

            let foundProps = 0;
            let haveProps = 0;
            const find = (val: string) => {
                val = val.toLowerCase();
                const oldlen = otherAddress.length;
                otherAddress = otherAddress.replace(new RegExp("\\b" + val + "\\b", "i"), "");
                if (oldlen !== otherAddress.length) {
                    foundProps++;
                    return true;
                }
                return false;
            };

            for (const prop of Array.from(props)) {
                let value = (known as any)[prop];
                if (value !== undefined) {
                    let found = find(value);
                    if (
                        !found &&
                        ["state", "country", "street"].includes(prop) &&
                        (known as any)[prop + "Abbr"] !== undefined
                    ) {
                        found = find((known as any)[prop + "Abbr"]);
                    }
                    if (!found && prop === "country" && value.toLowerCase() === "united states") {
                        found = find("usa");
                    }
                    if (!found && prop === "street") {
                        value = value.replace(/( street)/i, " st");
                        found = find(value);
                        if (!found) {
                            value = value.replace(/( road)/i, " rd");
                            find(value);
                        }
                    }
                    if (!found && prop === "postalCode") {
                        haveProps--; //these arent always specified. if the rest of the address matches we dont care about this
                    }
                    haveProps++;
                }
            }

            otherAddress = otherAddress.replace(/[ ,]/g, "");
            return foundProps === haveProps && otherAddress.length === 0;
        } else {
            return (known as any).toString().toLowerCase() === (unknown as any).toString().toLowerCase();
        }
    };
}
