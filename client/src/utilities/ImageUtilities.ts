import imageCompression from "browser-image-compression";

export const getBase64ImageDataString = async (forFile?: File): Promise<string> => {
    if (!forFile) {
        return "";
    }
    const compressedFile = await imageCompression(forFile, {
        maxSizeMB: 5,
        useWebWorker: true,
    });
    return imageCompression.getDataUrlFromFile(compressedFile);
};

export const download = (url: string, ext: string, name?: string) => {
    fetch(url, {
        method: "GET",
        headers: {},
    })
        .then((response) => {
            response.arrayBuffer().then(function (buffer) {
                const url = window.URL.createObjectURL(new Blob([buffer]));
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", `${name ?? "image"}.${ext}`); //or any other extension
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });
        })
        .catch((err) => {
            console.log(err);
        });
};
