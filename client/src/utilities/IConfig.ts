export interface IConfig {
    AppName: string;
    ApiBaseUri: string;
    GoogleGeoEncodeApiKey: string;
    JwtTokenName: string;
    ClientEndpoint: string;
    StripeKey: string;
}

export const getConfig = () => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    return require("../env.json") as IConfig;
};
