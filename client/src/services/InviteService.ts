import { injectable } from "tsyringe";
import { Service } from ".";
import { Invite } from "../models";
import { SendInviteParams } from "../store";
import { IConfig } from "../utilities";

@injectable()
export class InviteService extends Service {
    private readonly endpoint = "invite/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchInvites = async (): Promise<Invite[]> => {
        try {
            return (await this.get<Invite[]>(this.endpoint)).data;
        } catch (ex) {
            return [];
        }
    };

    public saveInvite = async (invite: Invite): Promise<Invite | undefined> => {
        try {
            return (await this.post<Invite>(this.endpoint, { invite })).data;
        } catch (ex) {
            return undefined;
        }
    };

    public deleteInvite = async (id: string): Promise<Invite | undefined> => {
        try {
            return (await this.delete<Invite>(`${this.endpoint}${id}`)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public sendInviteConfirmation = async (data: { guestIds: string[]; inviteId: string }): Promise<void> => {
        try {
            await this.post(`${this.endpoint}send/confirmation`, data);
        } catch (ex) {
            // do nothing
        }
    };
    public sendInvite = async (data: SendInviteParams): Promise<void> => {
        try {
            await this.post(`${this.endpoint}send`, data);
        } catch (ex) {
            // do nothing
        }
    };
}
