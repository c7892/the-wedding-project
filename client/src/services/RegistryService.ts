import { injectable } from "tsyringe";
import { Service } from ".";
import { Registry } from "../models";
import { IConfig } from "../utilities";

@injectable()
export class RegistryService extends Service {
    private readonly endpoint = "registry/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchRegistry = async (): Promise<Registry | undefined> => {
        try {
            return (await this.get<Registry>(`${this.endpoint}`)).data;
        } catch (ex) {
            return undefined;
        }
    };
    public updateRegistry = async (updates: Registry): Promise<Registry | undefined> => {
        try {
            return (await this.put<Registry>(`${this.endpoint}`, { updates })).data;
        } catch (ex) {
            return undefined;
        }
    };
}
