import { injectable } from "tsyringe";
import { Gallery } from "../models";
import { IConfig } from "../utilities";
import { Service } from "./Service";

@injectable()
export class GalleryService extends Service {
    private readonly endpoint = "gallery/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchGalleries = async (): Promise<Gallery[]> => {
        try {
            return (await this.get<Gallery[]>(`${this.endpoint}all`)).data;
        } catch (ex) {
            return [];
        }
    };

    public fetchGallery = async (id: string): Promise<Gallery | undefined> => {
        try {
            return (await this.get<Gallery>(`${this.endpoint}${id}`)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public downloadGallery = async (id: string): Promise<string | undefined> => {
        try {
            return (await this.get<string>(`${this.endpoint}download/${id}`)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public deleteGallery = async (id: string): Promise<void> => {
        try {
            await this.delete(`${this.endpoint}${id}`);
        } catch (ex) {
            // do nothing
        }
    };
    public createGallery = async (gallery: Gallery): Promise<Gallery | undefined> => {
        try {
            return (await this.post<Gallery>(`${this.endpoint}`, { gallery })).data;
        } catch (ex) {
            return undefined;
        }
    };
    public updateGallery = async (gallery: Gallery): Promise<Gallery | undefined> => {
        try {
            return (await this.put<Gallery>(`${this.endpoint}`, { gallery })).data;
        } catch (ex) {
            return undefined;
        }
    };
    public removeImageFromGallery = async (src: string, galleryId: string): Promise<Gallery | undefined> => {
        try {
            return (await this.put<Gallery>(`${this.endpoint}image/${galleryId}`, { src })).data;
        } catch (ex) {
            return undefined;
        }
    };
    public addImageGallery = async (src: File, galleryId?: string, eventId?: string): Promise<Gallery | undefined> => {
        try {
            const formData = new FormData();
            formData.append("image", src);
            formData.append("galleryId", galleryId ?? "");
            formData.append("eventId", eventId ?? "");
            return (await this.put<Gallery>(`${this.endpoint}image`, formData)).data;
        } catch (ex) {
            return undefined;
        }
    };
}
