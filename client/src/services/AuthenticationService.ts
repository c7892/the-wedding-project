import { injectable } from "tsyringe";
import { Guest, SpouseRegistrationInformation, User } from "../models";
import { IConfig } from "../utilities";
import { Service } from "./Service";
import Cookies from "universal-cookie";
import { keys } from "lodash";

@injectable()
export class AuthenticationService extends Service {
    private readonly endpoint = "authentication/";
    public constructor(config: IConfig) {
        super(config);
    }

    public loginAsync = async (email: string, password: string): Promise<unknown> => {
        const loginResponse = await this.postNoAuth<string>(`${this.endpoint}login`, { email, password });
        new Cookies(document.cookie).set(`${this.config.JwtTokenName}-jwt`, loginResponse.data);
        return loginResponse;
    };
    public fetchUserAsync = async (): Promise<Guest | User> => {
        const loginResponse = await this.get<Guest | User>(`${this.endpoint}user`);
        return loginResponse.data;
    };
    public fetchSpouseAsync = async (): Promise<User[]> => {
        const loginResponse = await this.get<User[]>(`${this.endpoint}spouse`);
        return loginResponse.data;
    };
    public refreshToken = async (): Promise<unknown> => {
        const refreshResponse = await this.post<string>(`${this.endpoint}refreshtoken`);
        new Cookies(document.cookie).set(`${this.config.JwtTokenName}-jwt`, refreshResponse.data);
        return refreshResponse;
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public registerAsync = async (spouses: SpouseRegistrationInformation[]): Promise<any> => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return await this.postNoAuth<any>(`${this.endpoint}register`, spouses);
    };
    public updateUser = async (updates: Guest | User, image?: File): Promise<User | Guest | undefined> => {
        const formData = new FormData();
        if (image) {
            formData.append("image", image);
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const item = updates as { [key: string]: any };
        keys(updates).forEach((key: string) => {
            if (item[key]) {
                formData.append(key, item[key] + "");
            }
        });
        return (await this.put<User | Guest>(`${this.endpoint}user`, formData)).data;
    };
    public changePassword = async (oldPassword: string, newPassword: string): Promise<User | undefined> => {
        return (await this.put<User>(`${this.endpoint}password`, { oldPassword, newPassword })).data;
    };
    public deleteAccount = async (isGuest: boolean): Promise<boolean> => {
        return (await this.delete<boolean>(`${this.endpoint}${isGuest ? "guest" : "user"}`)).data;
    };
}
