import { injectable } from "tsyringe";
import { Guest, GuestList } from "../models";
import { IConfig } from "../utilities";
import { Service } from "./Service";

@injectable()
export class GuestListService extends Service {
    private readonly endpoint = "guestlist/";
    public constructor(config: IConfig) {
        super(config);
    }

    public saveGuestListAsync = async (list: GuestList, guests?: Guest[]) => {
        return (
            await this.post<GuestList>(this.endpoint, {
                list,
                guests,
            })
        ).data;
    };

    public getGuestsAsync = async () => {
        return (await this.get<Guest[]>(`${this.endpoint}guests`)).data;
    };

    public getGuestListsAsync = async (id: string | undefined = undefined) => {
        return (await this.get<GuestList[]>(`${this.endpoint}${id ? `${id}` : ""}`)).data;
    };

    public deleteGuestList = async (id: string) => {
        return await this.delete(`${this.endpoint}${id}`);
    };

    public removeGuestFromList = async (id: string, guestId: string) => {
        return await this.put(`${this.endpoint}${id}/removeguest`, { guestId });
    };
    public deleteGuest = async (id: string) => {
        return await this.delete(`${this.endpoint}guest/${id}`);
    };
    public updateGuestInfo = async (guest: Guest) => {
        return await this.put(`${this.endpoint}guest`, { guest });
    };
}
