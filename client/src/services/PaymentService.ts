import { injectable } from "tsyringe";
import { IConfig } from "../utilities";
import { Service } from "./Service";

@injectable()
export class PaymentService extends Service {
    private readonly endpoint = "payment/";
    public constructor(config: IConfig) {
        super(config);
    }

    public requestCostAsync = async (
        postCards: number,
        texts: number,
    ): Promise<
        { clientSecret: string | null; cost: number; error?: string; postageCost: number; smsCost: number } | undefined
    > => {
        if (isNaN(postCards)) {
            return undefined;
        }
        try {
            const response = (
                await this.get<{
                    clientSecret: string | null;
                    cost: number;
                    error?: string;
                    postageCost: number;
                    smsCost: number;
                }>(`${this.endpoint}request_cost`, {
                    postCards,
                    texts,
                })
            ).data;
            return response;
        } catch (ex) {
            return undefined;
        }
    };
}
