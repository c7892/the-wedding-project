import { PaymentIntent } from "@stripe/stripe-js";
import { injectable } from "tsyringe";
import { Guest } from "../models";
import { IConfig } from "../utilities";
import { Service } from "./Service";

@injectable()
export class CommunicationService extends Service {
    private readonly endpoint = "message/";
    public constructor(config: IConfig) {
        super(config);
    }

    public sendMessage = async (data: {
        message: string;
        guest: Guest;
        sendMethods: string[];
        paymentPayload?: PaymentIntent;
    }): Promise<unknown> => {
        return await this.post(`${this.endpoint}`, data);
    };
}
