import { injectable } from "tsyringe";
import { Service } from ".";
import { Event } from "../models";
import { IConfig } from "../utilities";

@injectable()
export class GuestService extends Service {
    private readonly endpoint = "guest/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchEvents = async (): Promise<Event[]> => {
        try {
            return (await this.get<Event[]>(`${this.endpoint}events`)).data;
        } catch (ex) {
            return [];
        }
    };

    public rsvp = async (eventId: string, guests: number): Promise<Event | undefined> => {
        try {
            return (await this.post<Event>(`${this.endpoint}events/rsvp/${eventId}`, { guests })).data;
        } catch (ex) {
            return undefined;
        }
    };

    public unrsvp = async (eventId: string): Promise<Event | undefined> => {
        try {
            return (await this.post<Event>(`${this.endpoint}events/unrsvp/${eventId}`)).data;
        } catch (ex) {
            return undefined;
        }
    };
}
