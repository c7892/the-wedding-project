import { IConfig } from "../utilities";
import axios from "axios";
import Cookies from "universal-cookie";

export abstract class Service {
    constructor(protected readonly config: IConfig) {}

    public getAccessToken = () => {
        let token: string | undefined = new Cookies(document.cookie).get(`${this.config.JwtTokenName}-jwt`);
        if (token && token.indexOf(":") >= 0) {
            token = token.substring(token.indexOf(":") + 1);
        }
        if (token) {
            const tokenParts = token.split(".");
            token = "";
            for (let i = 0; i < 3 && i < tokenParts.length; i++) {
                token += tokenParts[i];
                if (i < 2) {
                    token += ".";
                }
            }
        }
        return token ?? "";
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected get<T>(endpoint: string, params?: any) {
        return axios
            .get<T>(`${this.config.ApiBaseUri}/${endpoint}`, {
                headers: {
                    authorization: `Bearer ${this.getAccessToken()}`,
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                },
                params,
            })
            .catch((error) => {
                throw error;
            });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected getNoAuth<T>(endpoint: string, params?: any) {
        return axios
            .get<T>(`${this.config.ApiBaseUri}/${endpoint}`, {
                headers: {
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                },
                params,
            })
            .catch((error) => {
                throw error;
            });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected post<T>(endpoint: string, body?: any, additionalHeaders: any = {}) {
        return axios
            .post<T>(`${this.config.ApiBaseUri}/${endpoint}`, body, {
                headers: {
                    authorization: `Bearer ${this.getAccessToken()}`,
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                    ...additionalHeaders,
                },
            })
            .catch((error) => {
                throw error;
            });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected put<T>(endpoint: string, body?: any, additionalHeaders: any = {}) {
        return axios
            .put<T>(`${this.config.ApiBaseUri}/${endpoint}`, body, {
                headers: {
                    authorization: `Bearer ${this.getAccessToken()}`,
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                    ...additionalHeaders,
                },
            })
            .catch((error) => {
                throw error;
            });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected delete<T>(endpoint: string, params?: any) {
        return axios
            .delete<T>(`${this.config.ApiBaseUri}/${endpoint}`, {
                headers: {
                    authorization: `Bearer ${this.getAccessToken()}`,
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                },
                params,
            })
            .catch((error) => {
                throw error;
            });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected postNoAuth<T>(endpoint: string, body?: any, additionalHeaders: any = {}) {
        return axios
            .post<T>(`${this.config.ApiBaseUri}/${endpoint}`, body, {
                headers: {
                    accept: "*/*",
                    "cache-Control": "no-cache",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,OPTIONS",
                    ...additionalHeaders,
                },
                withCredentials: true,
            })
            .catch((error) => {
                throw error;
            });
    }
}
