import { injectable } from "tsyringe";
import { Service } from ".";
import { Notes } from "../models";
import { IConfig } from "../utilities";

@injectable()
export class NotesService extends Service {
    private readonly endpoint = "notes/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchAllNotes = async (isPublished?: boolean): Promise<Notes[]> => {
        try {
            return (await this.get<Notes[]>(this.endpoint, { isPublished })).data;
        } catch (ex) {
            return [];
        }
    };

    public saveNotes = async (notes: Notes): Promise<Notes | undefined> => {
        try {
            return (await this.post<Notes>(this.endpoint, { notes })).data;
        } catch (ex) {
            return undefined;
        }
    };

    public fetchNotes = async (id: string): Promise<Notes | undefined> => {
        try {
            return (await this.get<Notes>(this.endpoint + id)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public deleteNotes = async (id: string): Promise<Notes | undefined> => {
        try {
            return (await this.delete<Notes>(`${this.endpoint}${id}`)).data;
        } catch (ex) {
            return undefined;
        }
    };
}
