import { injectable } from "tsyringe";
import { Service } from ".";
import { Event } from "../models";
import { IConfig } from "../utilities";

@injectable()
export class EventService extends Service {
    private readonly endpoint = "events/";
    public constructor(config: IConfig) {
        super(config);
    }

    public fetchEvents = async (date?: Date): Promise<Event[]> => {
        try {
            return (await this.get<Event[]>(this.endpoint, { date: date })).data;
        } catch (ex) {
            return [];
        }
    };
    public fetchAllEvents = async (): Promise<Event[]> => {
        try {
            return (await this.get<Event[]>(this.endpoint + "all")).data;
        } catch (ex) {
            return [];
        }
    };

    public saveEvent = async (event: Event): Promise<Event | undefined> => {
        try {
            return (await this.post<Event>(this.endpoint, { event })).data;
        } catch (ex) {
            console.log(ex);
            return undefined;
        }
    };

    public fetchArEvent = async (q: { id: string; guest: string }): Promise<Event | undefined> => {
        try {
            return (await this.getNoAuth<Event>(`${this.endpoint}ar`, q)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public fetchWeddingEvent = async (): Promise<Event | undefined> => {
        try {
            return (await this.get<Event>(`${this.endpoint}wedding`)).data;
        } catch (ex) {
            return undefined;
        }
    };

    public deleteEvent = async (id: string): Promise<Event | undefined> => {
        try {
            return (await this.delete<Event>(`${this.endpoint}${id}`)).data;
        } catch (ex) {
            return undefined;
        }
    };
}
