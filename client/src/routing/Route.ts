import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import {
    faCalendar,
    faCogs,
    faEnvelope,
    faGifts,
    faHome,
    faImages,
    faSms,
    faUserFriends,
} from "@fortawesome/free-solid-svg-icons";
import { faNoteSticky } from "@fortawesome/free-regular-svg-icons";
import React from "react";
import { RouteChildrenProps, RouteComponentProps } from "react-router-dom";
import {
    ArInvitationScreen,
    EditEventScreen,
    EditInvitationScreen,
    EventsScreen,
    GuestListScreen,
    HomeScreen,
    InvitationsScreen,
    LandingPage,
    LoginScreen,
    ViewInvitationScreen,
    GuestViewEventScreen,
    SpouseViewEventScreen,
    SettingsScreen,
    GalleriesScreen,
    GalleryScreen,
    RegistryScreen,
    PurchaseInvitationServicesScreen,
    NotesScreen,
    UpdateNotesScreen,
    ViewNotesScreen,
} from "../screens";
import { SendAMessageScreen } from "../screens/sendamessage/SendAMessageScreen";

export enum RouteKey {
    LandingPage = "/",
    Login = "/login",
    LoginRoot = "login",
    Home = "/home",
    Spouse = "/spouse/:weddingCode",
    Guest = "/guest/:weddingCode/:guestId",
    GuestBase = "/guest/",
    SpouseBase = "/spouse/",
    GuestList = "/guestlist",
    Notes = "/notes",
    NotesBase = "/notes/",
    ViewNotes = "/notes/:id/view",
    EditNotes = "/notes/edit",
    Events = "/events",
    ViewEvent = "/events/view",
    EditEvent = "/events/edit",
    Invitations = "/invitations",
    EditInvitation = "/invitations/edit",
    ViewArInvitation = "/invitation/ar/:eventId/:guestId",
    ViewArInvitationBase = "/invitation/ar/",
    ViewInvitation = "/invitation/view",
    PurchaseInvitation = "/invitation/purchase",
    Gallery = "/gallery",
    GalleryPage = "/gallery/:galleryId",
    Registry = "/registry",
    Settings = "/settings",
    SendAMessage = "/message",
}

export interface Route {
    path: RouteKey;
    exact?: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    children?: ((props: RouteChildrenProps<any>) => React.ReactNode) | React.ReactNode;
}
export interface PrivateRoute {
    path: string;
    exact?: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    children?: ((props: RouteChildrenProps<any>) => React.ReactNode) | React.ReactNode;
}

export const routes: Route[] = [
    {
        path: RouteKey.LandingPage,
        exact: true,
        component: LandingPage,
    },
    {
        path: RouteKey.Login,
        exact: true,
        component: LoginScreen,
    },
    {
        path: RouteKey.ViewArInvitation,
        exact: true,
        component: ArInvitationScreen,
    },
];

export const sideNavRoute = [];

export const spousePrivateRoutes: PrivateRoute[] = [
    {
        path: RouteKey.Spouse + RouteKey.Home,
        exact: true,
        component: HomeScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.GuestList,
        exact: true,
        component: GuestListScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Events,
        exact: true,
        component: EventsScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.EditEvent,
        exact: true,
        component: EditEventScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.ViewEvent,
        exact: true,
        component: SpouseViewEventScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Invitations,
        exact: true,
        component: InvitationsScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.EditInvitation,
        exact: true,
        component: EditInvitationScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.ViewInvitation,
        exact: true,
        component: ViewInvitationScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.PurchaseInvitation,
        exact: true,
        component: PurchaseInvitationServicesScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Gallery,
        exact: true,
        component: GalleriesScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.GalleryPage,
        exact: true,
        component: GalleryScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Registry,
        exact: true,
        component: RegistryScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Settings,
        exact: true,
        component: SettingsScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.Notes,
        exact: true,
        component: NotesScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.EditNotes,
        exact: true,
        component: UpdateNotesScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.ViewNotes,
        exact: true,
        component: ViewNotesScreen,
    },
    {
        path: RouteKey.Spouse + RouteKey.SendAMessage,
        exact: true,
        component: SendAMessageScreen,
    },
];

export const guestPrivateRoutes: PrivateRoute[] = [
    {
        path: RouteKey.Guest + RouteKey.Home,
        exact: true,
        component: HomeScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.ViewEvent,
        exact: true,
        component: GuestViewEventScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.Events,
        exact: true,
        component: EventsScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.Gallery,
        exact: true,
        component: GalleriesScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.GalleryPage,
        exact: true,
        component: GalleryScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.Registry,
        exact: true,
        component: RegistryScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.Settings,
        exact: true,
        component: SettingsScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.Notes,
        exact: true,
        component: NotesScreen,
    },
    {
        path: RouteKey.Guest + RouteKey.ViewNotes,
        exact: true,
        component: ViewNotesScreen,
    },
];

export interface SideNavRoute {
    label: string;
    to: RouteKey;
    icon: IconDefinition;
}

export const spouseSideNavRoutes: SideNavRoute[] = [
    {
        label: "Home",
        to: RouteKey.Home,
        icon: faHome,
    },
    {
        label: "Events",
        to: RouteKey.Events,
        icon: faCalendar,
    },
    {
        label: "Invitations",
        to: RouteKey.Invitations,
        icon: faEnvelope,
    },
    {
        label: "Guest Lists",
        to: RouteKey.GuestList,
        icon: faUserFriends,
    },
    {
        label: "Galleries",
        to: RouteKey.Gallery,
        icon: faImages,
    },
    {
        label: "Registry",
        to: RouteKey.Registry,
        icon: faGifts,
    },
    {
        label: "Notes",
        to: RouteKey.Notes,
        icon: faNoteSticky,
    },
    {
        label: "Send A Message",
        to: RouteKey.SendAMessage,
        icon: faSms,
    },
    {
        label: "Settings",
        to: RouteKey.Settings,
        icon: faCogs,
    },
];

export const guestSideNavRoutes: SideNavRoute[] = [
    {
        label: "Home",
        to: RouteKey.Home,
        icon: faHome,
    },
    {
        label: "Events",
        to: RouteKey.Events,
        icon: faCalendar,
    },
    {
        label: "Galleries",
        to: RouteKey.Gallery,
        icon: faImages,
    },
    {
        label: "Registry",
        to: RouteKey.Registry,
        icon: faGifts,
    },
    {
        label: "Notes",
        to: RouteKey.Notes,
        icon: faNoteSticky,
    },
    {
        label: "Settings",
        to: RouteKey.Settings,
        icon: faCogs,
    },
];
