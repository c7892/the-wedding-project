export interface Notes {
    _id?: string;
    title: string;
    details: string;
    isPublished: boolean;
    ownerWeddingCode: string;
    dateCreated: number;
    modifiedDttm: number;
}
