export interface Gallery {
    _id?: string;
    title: string;
    event?: string;
    details: string;
    images: Image[];
    ownerWeddingCode: string;
    owners: string;
    dateCreated: number;
    modifiedDttm: number;
}

export interface Image {
    _id?: string;
    src: string;
    dateCreated: number;
    uploadedBy: string;
}
