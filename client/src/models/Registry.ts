export interface RegistryItem {
    name: string;
    link: string;
    purchasedBy?: string[];
    quantityPurchased: number;
    quantityWanted: number;
}

export interface Registry {
    _id?: string;
    note: string;
    miscLinks: string[];
    items: RegistryItem[];
    ownerWeddingCode: string;
    dateCreated: number;
    modifiedDttm: number;
}
