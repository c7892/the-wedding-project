export interface GuestList {
    ownerWeddingCode: string;
    name: string;
    description?: string;
    guests: string[];
    guestObjs?: Guest[];
    _id?: string;
    dateCreated: number;
    modifiedDttm: number;
}

export interface Guest {
    _id?: string;
    firstName: string;
    surname: string;
    email?: string;
    mobile: string;
    profilePic?: string;
    address?: string;
    relation?: {
        [id: string]: string;
        _spouse: string;
    };
    uniqueWeddingCode: string;
    events: string[];
    dateCreated: number;
    modifiedDttm: number;
}
