export interface User {
    firstName: string;
    surname: string;
    email: string;
    newFamilyName: string;
    mobile?: string;
    profilePic?: string;
    address?: string;
}

export interface SpouseRegistrationInformation extends User {
    password: string;
}
