import React, { PureComponent } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { NavigationBar, PrivateRoute, Title } from "./components";
import { RouteKey, spousePrivateRoutes, routes, guestPrivateRoutes, PrivateRoute as PR } from "./routing/Route";
import { LandingPage } from "./screens";
import { bindDependencies } from "./utilities";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { MainScreen } from "./screens/MainScreen";
import { connect } from "react-redux";
import { container } from "tsyringe";
import { ApplicationReduxState } from "./store";
import { AuthenticationSelector } from "./store/selectors";
import { Footer } from "./components/Footer";
import Container from "react-bootstrap/Container";

interface IStateProps {
    isGuest: boolean;
}

interface IOwnState {
    routes: PR[];
}

type Props = IStateProps;

export class AppComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            routes: [],
        };
        bindDependencies();
    }
    public componentDidMount() {
        const routes = this.props.isGuest ? guestPrivateRoutes : spousePrivateRoutes;
        this.setState({ routes });
    }
    public componentDidUpdate(prevProps: Props) {
        if (prevProps.isGuest !== this.props.isGuest) {
            const routes = this.props.isGuest ? guestPrivateRoutes : spousePrivateRoutes;
            this.setState({ routes });
        }
    }
    public render() {
        return (
            <BrowserRouter>
                <Title />
                <div className={"App"}>
                    <NavigationBar />
                    <Container id="content-container" fluid>
                        <Switch>
                            {routes.map((route, index) => (
                                <Route key={index} {...route} />
                            ))}
                            {this.state.routes.map((route, index) => (
                                <PrivateRoute
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    component={MainScreen}
                                />
                            ))}
                            <PrivateRoute path={RouteKey.Spouse} exact component={MainScreen} />
                            <PrivateRoute path={RouteKey.Guest} exact component={MainScreen} />
                            <Route component={LandingPage} />
                        </Switch>
                    </Container>
                    <Footer />
                    <ToastContainer />
                </div>
            </BrowserRouter>
        );
    }
}

const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isGuest: authenticationSelector.selectIsGuest(state),
    };
};

export const App = connect(mapStateToProps)(AppComponent);
