import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { Guest, Registry, User } from "../../models";
import {
    ApplicationReduxState,
    AuthenticationActionCreator,
    FetchRegistryAction,
    FetchUserAction,
    RegistryActionCreator,
    SetRegistryMessageAction,
    UpdateRegistryAction,
} from "../../store";
import { connect } from "react-redux";
import { AuthenticationSelector, RegistrySelector } from "../../store/selectors";
import { container } from "tsyringe";
import { toast } from "react-toastify";
import { EditRegistryForm } from "./components/EditRegistryForm";
import { ViewRegistryForm } from "./components/ViewRegistryForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBan, faPencilAlt } from "@fortawesome/free-solid-svg-icons";

interface IStateProps {
    registry?: Registry;
    isGuest: boolean;
    isSavingRegistry: boolean;
    registryMessage?: string;
    userId?: string;
    user?: User | Guest;
    spouses?: User[];
}

interface IDispatchProps {
    fetchUser: () => FetchUserAction;
    fetchRegistry: () => FetchRegistryAction;
    updateRegistry: (payload: Registry) => UpdateRegistryAction;
    setRegistryMessage: (payload?: string) => SetRegistryMessageAction;
}

interface IOwnState {
    inEditingMode: boolean;
}

type Props = IStateProps & IDispatchProps;

export class RegistryScreenComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            inEditingMode: false,
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            this.props.fetchRegistry();
            if (!this.props.isGuest) {
                this.props.fetchUser();
            }
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (prevProps.isSavingRegistry && !this.props.isSavingRegistry) {
            this.setState({ inEditingMode: false });
        }
        if (prevProps.registryMessage !== this.props.registryMessage && this.props.registryMessage) {
            toast(this.props.registryMessage);
            this.props.setRegistryMessage(undefined);
        }
    }
    public render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <h1>
                            {this.props.isGuest ? "The" : "Your"} Registry&nbsp;&nbsp;&nbsp;&nbsp;
                            {this.props.isGuest ? null : (
                                <Button
                                    variant={this.state.inEditingMode ? "danger" : "warning"}
                                    onClick={() => this.setState({ inEditingMode: !this.state.inEditingMode })}>
                                    <FontAwesomeIcon icon={this.state.inEditingMode ? faBan : faPencilAlt} />
                                    &nbsp;{this.state.inEditingMode ? "Cancel" : "Edit"}
                                </Button>
                            )}
                        </h1>
                    </Col>
                    <hr />
                </Row>
                <Row>
                    <Col>
                        {this.props.registry ? (
                            this.props.isGuest || !this.state.inEditingMode ? (
                                <ViewRegistryForm
                                    registry={this.props.registry}
                                    spouses={this.props.spouses ?? []}
                                    user={this.props.user as User}
                                    isGuest={this.props.isGuest}
                                    currentUserId={this.props.userId ?? ""}
                                    onPurchaseMade={this.props.updateRegistry}
                                />
                            ) : (
                                <EditRegistryForm
                                    registry={this.props.registry}
                                    disable={this.props.isSavingRegistry}
                                    onFormSubmitted={this.props.updateRegistry}
                                />
                            )
                        ) : null}
                    </Col>
                </Row>
            </Container>
        );
    }
}
const registrySelector = container.resolve(RegistrySelector);
const authenticationSelector = container.resolve(AuthenticationSelector);
const registryActionCreator = container.resolve(RegistryActionCreator);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSavingRegistry: registrySelector.selectIsSavingRegistry(state),
        registry: registrySelector.selectRegistry(state),
        registryMessage: registrySelector.selectRegistryMessage(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        userId: authenticationSelector.selectUserId(state),
        user: authenticationSelector.selectUser(state),
        spouses: authenticationSelector.selectSpouse(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchUser: authenticationActionCreator.fetchUser,
    fetchRegistry: registryActionCreator.fetchRegistry,
    updateRegistry: registryActionCreator.updateRegistry,
    setRegistryMessage: registryActionCreator.setRegistryMessage,
};

export const RegistryScreen = connect(mapStateToProps, mapDispatchToProps)(RegistryScreenComponent);
