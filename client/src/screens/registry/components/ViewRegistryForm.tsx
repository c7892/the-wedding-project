import React, { PureComponent } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Registry, RegistryItem, User } from "../../../models";
import parse from "html-react-parser";
import { LinkPreview, placeholderImg } from "@dhaiwat10/react-link-preview";
import { extend, first, isEmpty, shuffle } from "lodash";
import axios from "axios";
import { container } from "tsyringe";
import { IConfig } from "../../../utilities";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import ButtonGroup from "react-bootstrap/ButtonGroup";

interface IOwnProps {
    onPurchaseMade: (updates: Registry) => void;
    registry: Registry;
    spouses: User[];
    user?: User;
    isGuest: boolean;
    currentUserId: string;
}

export class ViewRegistryForm extends PureComponent<IOwnProps> {
    public render() {
        const { registry, spouses, isGuest, currentUserId } = this.props;
        return (
            <>
                <Row style={{ marginBottom: 25 }}>
                    <Col>
                        <h4>
                            A note from {spouses[0]?.firstName ?? ""}&nbsp;&amp;&nbsp;{spouses[1]?.firstName ?? ""}
                        </h4>
                        {parse(registry.note)}
                    </Col>
                </Row>
                <Row style={{ marginBottom: 25 }}>
                    {registry.miscLinks.map((link, index) => {
                        return (
                            <Col xs={12} md={6} lg={4} key={index}>
                                <LinkPreview
                                    url={link}
                                    openInNewTab
                                    fallback={this.renderFallbackLink(link)}
                                    fetcher={this.customFetcher}
                                />
                            </Col>
                        );
                    })}
                </Row>
                <Row style={{ marginBottom: 25 }}>
                    <h5>Gifts for the new family</h5>
                    <hr />
                    {registry.items
                        .filter((item) => {
                            if (!isGuest) {
                                return true;
                            }
                            if (!isEmpty(item.purchasedBy) && item.purchasedBy?.includes(currentUserId)) {
                                return true;
                            }
                            return item.quantityWanted !== item.quantityPurchased;
                        })
                        .map((item, index) => {
                            return (
                                <Col xs={12} md={6} lg={4} key={index}>
                                    <em>
                                        {item.quantityWanted - item.quantityPurchased}&nbsp;{item.name}(s) wanted
                                    </em>
                                    <LinkPreview
                                        url={item.link}
                                        openInNewTab
                                        fallback={this.renderFallback(item)}
                                        fetcher={this.customFetcher}
                                    />
                                    <ButtonGroup>
                                        <OverlayTrigger
                                            placement={"top"}
                                            overlay={
                                                <Tooltip id="tooltip-top">
                                                    Clicking this button will mark this item as purchased by you
                                                </Tooltip>
                                            }>
                                            <Button
                                                style={{ marginTop: 5 }}
                                                variant="warning"
                                                disabled={
                                                    isGuest &&
                                                    item.purchasedBy &&
                                                    item.purchasedBy.includes(currentUserId)
                                                }
                                                onClick={() => {
                                                    const items = registry.items;
                                                    items[index].quantityPurchased++;
                                                    if (!items[index].purchasedBy) {
                                                        items[index].purchasedBy = [];
                                                    }
                                                    items[index].purchasedBy?.push(currentUserId);
                                                    const updates = extend({}, registry, {
                                                        items,
                                                    });
                                                    this.props.onPurchaseMade(updates);
                                                }}>
                                                Mark as purchased
                                            </Button>
                                        </OverlayTrigger>
                                        <OverlayTrigger
                                            placement={"top"}
                                            overlay={
                                                <Tooltip id="tooltip-top">
                                                    Clicking this button will remove this item as being purchased by
                                                    yourself
                                                </Tooltip>
                                            }>
                                            <Button
                                                style={{ marginTop: 5 }}
                                                variant="danger"
                                                disabled={
                                                    isGuest &&
                                                    (isEmpty(item.purchasedBy) ||
                                                        !item.purchasedBy?.includes(currentUserId))
                                                }
                                                onClick={() => {
                                                    const items = registry.items;
                                                    items[index].quantityPurchased--;
                                                    if (items[index].quantityPurchased < 0) {
                                                        items[index].quantityPurchased = 0;
                                                    }
                                                    let purchasedBy = items[index].purchasedBy ?? [];
                                                    purchasedBy = purchasedBy.filter(
                                                        (purchaser) => purchaser !== currentUserId,
                                                    );
                                                    items[index].purchasedBy = purchasedBy;
                                                    const updates = extend({}, registry, {
                                                        items,
                                                    });
                                                    this.props.onPurchaseMade(updates);
                                                }}>
                                                Undo purchase
                                            </Button>
                                        </OverlayTrigger>
                                    </ButtonGroup>
                                    <br />
                                    <em>
                                        {item.quantityWanted - item.quantityPurchased}&nbsp;{item.name}(s) wanted
                                    </em>
                                </Col>
                            );
                        })}
                </Row>
            </>
        );
    }

    private customFetcher = async (url: string) => {
        try {
            const config = container.resolve<IConfig>("Config");
            const response = await axios.get(`${config.ApiBaseUri}/meta`, { params: { url } });
            const metadata = response.data.metadata;
            if (metadata && !isEmpty(Object.keys(metadata.og))) {
                return { siteName: metadata.og.site_name, hostname: metadata.og.url, ...metadata.og };
            }
            const image = first(shuffle(metadata.images).filter((image) => image.src.includes("https://")));
            const meta = {
                image: typeof image === "string" ? image : (image as { src: string })?.src,
                siteName: metadata.meta.title,
                hostname: url,
                ...metadata.meta,
            };
            if (meta.description) {
                meta.description = (meta.description as string).substring(0, 255);
            }
            if (meta.title) {
                meta.title = (meta.title as string).substring(0, 50);
            }
            if (meta.siteName) {
                meta.siteName = (meta.siteName as string).substring(0, 50);
            }
            if (meta.hostname) {
                meta.hostname = (meta.hostname as string).substring(0, 50);
            }
            return meta;
        } catch (ex) {
            return null;
        }
    };

    private shortenUrl = (url: string) => {
        const lastSlash = url.lastIndexOf("/");
        if (lastSlash < 0) {
            return url;
        }
        const firstTry = url.substring(0, lastSlash);
        if (firstTry.indexOf(".") < 0) {
            return url;
        }
        return firstTry;
    };

    private renderFallback = (item: RegistryItem) => {
        return (
            <Card
                id="activeCard"
                onClick={() => {
                    window.open(item.link, "_blank");
                }}>
                <Card.Img variant="top" src={placeholderImg} />
                <Card.Body>
                    <Card.Title>{item.name}</Card.Title>
                </Card.Body>
            </Card>
        );
    };
    private renderFallbackLink = (link: string) => {
        return (
            <Card
                id="activeCard"
                onClick={() => {
                    window.open(link, "_blank");
                }}>
                <Card.Img variant="top" src={placeholderImg} />
                <Card.Body>
                    <Card.Title>{"We couldn't find any metadata for the given link"}</Card.Title>
                    <Card.Body>{this.shortenUrl(link)} Click to open in new tab</Card.Body>
                </Card.Body>
            </Card>
        );
    };
}
