import React, { PureComponent, SyntheticEvent } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Form from "react-bootstrap/Form";
import ListGroup from "react-bootstrap/ListGroup";
import { Registry, RegistryItem } from "../../../models";
import ReactQuill from "react-quill";
import { Wysiwig } from "../../../components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { extend, isEqual } from "lodash";
import { QuillDeltaToHtmlConverter } from "quill-delta-to-html";

interface IOwnProps {
    onFormSubmitted: (updates: Registry) => void;
    disable: boolean;
    registry: Registry;
}

interface IOwnState {
    miscLinks: string[];
    items: RegistryItem[];
}

export class EditRegistryForm extends PureComponent<IOwnProps, IOwnState> {
    private readonly note = React.createRef<ReactQuill>();

    public constructor(props: IOwnProps) {
        super(props);
        this.state = {
            miscLinks: props.registry.miscLinks,
            items: props.registry.items,
        };
    }

    public componentDidUpdate(prevProps: IOwnProps) {
        if (!isEqual(prevProps.registry, this.props.registry)) {
            this.setState({ miscLinks: this.props.registry.miscLinks, items: this.props.registry.items });
        }
    }

    public render() {
        return (
            <Form onSubmit={this.submit} id="registryForm">
                <Row>
                    <Col>
                        <Form.Label>Note</Form.Label>
                        <Wysiwig
                            ref={this.note}
                            defaultValue={this.props.registry.note}
                            readonly={this.props.disable}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label>Misc. Links</Form.Label>
                        <br />
                        <Row>
                            <Col>
                                <ListGroup>
                                    <ListGroup.Item>
                                        <Button
                                            variant="info"
                                            style={{ width: "100%" }}
                                            disabled={this.props.disable}
                                            onClick={() => {
                                                const links = this.state.miscLinks;
                                                this.setState({ miscLinks: links.concat([""]) });
                                            }}>
                                            <FontAwesomeIcon icon={faPlus} size="lg" />
                                        </Button>
                                    </ListGroup.Item>
                                    {this.state.miscLinks.map((link, index) => {
                                        return (
                                            <ListGroup.Item key={index}>
                                                <Row>
                                                    <Col xs={10}>
                                                        <Form.Control
                                                            type="text"
                                                            placeholder="A link to some other resource (i.e. cashapp, zelle, vemo etc.)"
                                                            defaultValue={link}
                                                            value={link}
                                                            required
                                                            disabled={this.props.disable}
                                                            onChange={(
                                                                e: SyntheticEvent<
                                                                    | HTMLInputElement
                                                                    | HTMLSelectElement
                                                                    | HTMLTextAreaElement
                                                                >,
                                                            ) => {
                                                                const links = ([] as string[]).concat(
                                                                    this.state.miscLinks,
                                                                );
                                                                links[index] = e.currentTarget.value;
                                                                this.setState({ miscLinks: links });
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col>
                                                        <Button
                                                            variant="danger"
                                                            disabled={this.props.disable}
                                                            onClick={() => {
                                                                const links = ([] as string[]).concat(
                                                                    this.state.miscLinks,
                                                                );
                                                                links.splice(index, 1);
                                                                this.setState({ miscLinks: links });
                                                            }}>
                                                            <FontAwesomeIcon icon={faTrashAlt} size="sm" />
                                                        </Button>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>
                                        );
                                    })}
                                </ListGroup>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label>Gift&apos;s you&apos;d like</Form.Label>
                        <br />
                        <Row>
                            <Col>
                                <ListGroup>
                                    <ListGroup.Item>
                                        <Button
                                            variant="info"
                                            style={{ width: "100%" }}
                                            disabled={this.props.disable}
                                            onClick={() => {
                                                const items = this.state.items;
                                                this.setState({
                                                    items: items.concat([
                                                        {
                                                            name: "",
                                                            link: "",
                                                            purchasedBy: undefined,
                                                            quantityPurchased: 0,
                                                            quantityWanted: 1,
                                                        },
                                                    ]),
                                                });
                                            }}>
                                            <FontAwesomeIcon icon={faPlus} size="lg" />
                                        </Button>
                                    </ListGroup.Item>
                                    {this.state.items.map((item, index) => {
                                        return (
                                            <ListGroup.Item key={index}>
                                                <Row>
                                                    <Col xs={10}>
                                                        <Row>
                                                            <Col>
                                                                <Form.Label>Item Name</Form.Label>
                                                                <Form.Control
                                                                    type="text"
                                                                    required
                                                                    placeholder="Item Name (i.e. HomeCollections Bath Set)"
                                                                    defaultValue={item.name}
                                                                    value={item.name}
                                                                    disabled={this.props.disable}
                                                                    onChange={(
                                                                        e: SyntheticEvent<
                                                                            | HTMLInputElement
                                                                            | HTMLSelectElement
                                                                            | HTMLTextAreaElement
                                                                        >,
                                                                    ) => {
                                                                        const items = ([] as RegistryItem[]).concat(
                                                                            this.state.items,
                                                                        );
                                                                        const item: RegistryItem = items[index] ?? {
                                                                            name: "",
                                                                            link: "",
                                                                            purchasedBy: undefined,
                                                                            quantityPurchased: 0,
                                                                            quantityWanted: 1,
                                                                        };
                                                                        item.name = e.currentTarget.value;
                                                                        items[index] = item;
                                                                        this.setState({ items });
                                                                    }}
                                                                />
                                                            </Col>
                                                            <Col>
                                                                <Form.Label>Item Link</Form.Label>
                                                                <Form.Control
                                                                    type="url"
                                                                    required
                                                                    placeholder="Item Link (i.e. amazon.com)"
                                                                    defaultValue={item.link}
                                                                    value={item.link}
                                                                    disabled={this.props.disable}
                                                                    onChange={(
                                                                        e: SyntheticEvent<
                                                                            | HTMLInputElement
                                                                            | HTMLSelectElement
                                                                            | HTMLTextAreaElement
                                                                        >,
                                                                    ) => {
                                                                        const items = ([] as RegistryItem[]).concat(
                                                                            this.state.items,
                                                                        );
                                                                        const item: RegistryItem = items[index] ?? {
                                                                            name: "",
                                                                            link: "",
                                                                            purchasedBy: undefined,
                                                                            quantityPurchased: 0,
                                                                            quantityWanted: 1,
                                                                        };
                                                                        item.link = e.currentTarget.value;
                                                                        items[index] = item;
                                                                        this.setState({ items });
                                                                    }}
                                                                />
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <Form.Label>Quantity Wanted</Form.Label>
                                                                <Form.Control
                                                                    type="number"
                                                                    required
                                                                    min={1}
                                                                    max={99}
                                                                    defaultValue={item.quantityWanted}
                                                                    value={item.quantityWanted}
                                                                    disabled={this.props.disable}
                                                                    onChange={(
                                                                        e: SyntheticEvent<
                                                                            | HTMLInputElement
                                                                            | HTMLSelectElement
                                                                            | HTMLTextAreaElement
                                                                        >,
                                                                    ) => {
                                                                        const items = ([] as RegistryItem[]).concat(
                                                                            this.state.items,
                                                                        );
                                                                        const item: RegistryItem = items[index] ?? {
                                                                            name: "",
                                                                            link: "",
                                                                            purchasedBy: undefined,
                                                                            quantityPurchased: 0,
                                                                            quantityWanted: 1,
                                                                        };
                                                                        item.quantityWanted = parseInt(
                                                                            e.currentTarget.value,
                                                                        );
                                                                        items[index] = item;
                                                                        this.setState({ items });
                                                                    }}
                                                                />
                                                            </Col>
                                                            <Col>
                                                                <Form.Label>
                                                                    Quantity Purchased Already {item.quantityPurchased}
                                                                </Form.Label>
                                                                <br />
                                                                <ButtonGroup>
                                                                    <Button
                                                                        variant="primary"
                                                                        onClick={() => {
                                                                            const items = ([] as RegistryItem[]).concat(
                                                                                this.state.items,
                                                                            );
                                                                            const item: RegistryItem = items[index] ?? {
                                                                                name: "",
                                                                                link: "",
                                                                                purchasedBy: undefined,
                                                                                quantityPurchased: 0,
                                                                                quantityWanted: 1,
                                                                            };
                                                                            item.quantityPurchased++;
                                                                            items[index] = item;
                                                                            this.setState({ items });
                                                                        }}>
                                                                        Increase number of items purchased
                                                                    </Button>
                                                                    <Button
                                                                        variant="warning"
                                                                        onClick={() => {
                                                                            const items = ([] as RegistryItem[]).concat(
                                                                                this.state.items,
                                                                            );
                                                                            const item: RegistryItem = items[index] ?? {
                                                                                name: "",
                                                                                link: "",
                                                                                purchasedBy: undefined,
                                                                                quantityPurchased: 0,
                                                                                quantityWanted: 1,
                                                                            };
                                                                            item.quantityPurchased--;
                                                                            if (item.quantityPurchased < 0) {
                                                                                item.quantityPurchased = 0;
                                                                            }
                                                                            items[index] = item;
                                                                            this.setState({ items });
                                                                        }}>
                                                                        Decrease number of items purchased
                                                                    </Button>
                                                                    <Button
                                                                        variant="secondary"
                                                                        onClick={() => {
                                                                            const items = ([] as RegistryItem[]).concat(
                                                                                this.state.items,
                                                                            );
                                                                            const item: RegistryItem = items[index] ?? {
                                                                                name: "",
                                                                                link: "",
                                                                                purchasedBy: undefined,
                                                                                quantityPurchased: 0,
                                                                                quantityWanted: 1,
                                                                            };
                                                                            item.quantityWanted =
                                                                                item.quantityPurchased;
                                                                            items[index] = item;
                                                                            this.setState({ items });
                                                                        }}>
                                                                        Mark item completely bought
                                                                    </Button>
                                                                </ButtonGroup>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col style={{ textAlign: "center" }}>
                                                        <Button
                                                            className="vertical-center"
                                                            variant="danger"
                                                            disabled={this.props.disable}
                                                            onClick={() => {
                                                                const items = ([] as RegistryItem[]).concat(
                                                                    this.state.items,
                                                                );
                                                                items.splice(index, 1);
                                                                this.setState({ items });
                                                            }}>
                                                            <FontAwesomeIcon icon={faTrashAlt} size="sm" />
                                                        </Button>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>
                                        );
                                    })}
                                </ListGroup>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button variant={"success"} type="submit" onClick={this.submit} disabled={this.props.disable}>
                            Update Registry
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
    private submit = (e: SyntheticEvent) => {
        e.preventDefault();
        const cfg = {
            encodeHtml: true,
            inlineStyles: true,
        };
        const converter = new QuillDeltaToHtmlConverter(this.note.current?.getEditor()?.getContents()?.ops ?? [], cfg);
        const updates = extend({}, this.props.registry, {
            ...this.state,
            note: converter.convert(),
        });
        this.props.onFormSubmitted(updates);
    };
}
