import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import { DefaultLoginForm, GuestLoginForm, RegistrationForm } from "./components";
import "../../styles/screens/authentication.css";
import { useSelector } from "react-redux";
import { AuthenticationSelector } from "../../store/selectors";
import { container } from "tsyringe";
import { useHistory } from "react-router-dom";
import { RouteKey } from "../../routing";

export const LoginScreen = () => {
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const [authForm, setAuthForm] = useState("default");
    const isAuthenticated = useSelector(authenticationSelector.selectIsAuthenticated);
    const weddingCode = useSelector(authenticationSelector.selectWeddingCode);
    const userId = useSelector(authenticationSelector.selectUserId);
    const isGuest = useSelector(authenticationSelector.selectIsGuest);
    const history = useHistory();
    useEffect(() => {
        if (isAuthenticated && weddingCode && !isGuest) {
            history.push(`${RouteKey.SpouseBase}${weddingCode}`);
        }
        if (isAuthenticated && weddingCode && isGuest) {
            history.push(`${RouteKey.GuestBase}${weddingCode}/${userId}`);
        }
    });
    return (
        <Container fluid style={{ padding: 12 }}>
            {authForm === "default" ? (
                <DefaultLoginForm
                    onGuestLoginClicked={() => setAuthForm("guest")}
                    onRegisterClicked={() => setAuthForm("register")}
                />
            ) : null}
            {authForm === "guest" ? (
                <GuestLoginForm
                    onSpouseLoginClicked={() => setAuthForm("default")}
                    onRegisterClicked={() => setAuthForm("register")}
                />
            ) : null}
            {authForm === "register" ? (
                <RegistrationForm
                    onSpouseLoginClicked={() => setAuthForm("default")}
                    onGuestLoginClicked={() => setAuthForm("guest")}
                />
            ) : null}
        </Container>
    );
};
