import React, { PureComponent, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Jumbotron from "react-bootstrap/Jumbotron";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Title } from "../../../components";
import { toast } from "react-toastify";
import { container } from "tsyringe";
import {
    ApplicationReduxState,
    AuthenticationActionCreator,
    ClearAuthProcessDataAction,
    LoginAction,
} from "../../../store";
import { AuthenticationSelector } from "../../../store/selectors";
import { connect } from "react-redux";

interface IOwnProps {
    onRegisterClicked: () => void;
    onSpouseLoginClicked: () => void;
}

interface IStateProps {
    isLoggingIn?: boolean;
    loginSucceeded?: boolean;
    loginFailureMessage?: string;
}

interface IDispatchProps {
    login: (payload: { email: string; password: string }) => LoginAction;
    clear: () => ClearAuthProcessDataAction;
}

type Props = RouteComponentProps & IOwnProps & IDispatchProps & IStateProps;

export class GuestLoginFormComponent extends PureComponent<Props> {
    private readonly emailRef = React.createRef<HTMLInputElement>();
    private readonly codeRef = React.createRef<HTMLInputElement>();

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isLoggingIn && this.props.isLoggingIn) {
            toast.warning("Logging in, please wait");
            return;
        }
        if (!prevProps.loginSucceeded && this.props.loginSucceeded) {
            toast.success("Successfully logged in!");
            return;
        }
        if (prevProps.loginSucceeded === false && this.props.loginFailureMessage) {
            toast.error(this.props.loginFailureMessage);
            return;
        }
    }

    public componentWillUnmount() {
        this.props.clear();
    }

    public render() {
        return (
            <>
                <Title title="Guest Login" />
                <Container>
                    <Row>
                        <Col>
                            <Jumbotron>
                                <Container fluid>
                                    <Row>
                                        <h3>Wedding Guest Login</h3>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form onSubmit={this.login}>
                                                <Form.Group>
                                                    <Form.Label>Phone number</Form.Label>
                                                    <Form.Control
                                                        type="text"
                                                        placeholder="Enter phone number"
                                                        ref={this.emailRef}
                                                    />
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Label>Invite Code (found on your invitation)</Form.Label>
                                                    <Form.Control
                                                        type={"text"}
                                                        maxLength={12}
                                                        placeholder="Code"
                                                        ref={this.codeRef}
                                                    />
                                                </Form.Group>
                                                <Button variant="secondary" type="submit" className="mt-2">
                                                    Login
                                                </Button>
                                            </Form>
                                        </Col>
                                    </Row>
                                    <Row className="mt-5">
                                        <Col xs={{ span: 4, order: "first" }} onClick={this.props.onSpouseLoginClicked}>
                                            <Button variant="info">Spouses Click Here</Button>
                                        </Col>
                                        <Col xs={{ order: "last", span: 4, offset: 4 }}>
                                            <Button
                                                variant="warning"
                                                className={"float-right"}
                                                onClick={this.props.onRegisterClicked}>
                                                Register
                                            </Button>
                                        </Col>
                                    </Row>
                                </Container>
                            </Jumbotron>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
    private login = (e: SyntheticEvent) => {
        e.preventDefault();
        const email = this.emailRef.current?.value ?? "";
        const password = this.codeRef.current?.value ?? "";
        this.props.login({ email, password });
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isLoggingIn: authenticationSelector.selectIsLoggingIn(state),
        loginFailureMessage: authenticationSelector.selectLoginFailedMessage(state),
        loginSucceeded: authenticationSelector.selectLoginSucceeded(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    login: authenticationActionCreator.login,
    clear: authenticationActionCreator.clear,
};
export const GuestLoginForm = connect(mapStateToProps, mapDispatchToProps)(withRouter(GuestLoginFormComponent));
