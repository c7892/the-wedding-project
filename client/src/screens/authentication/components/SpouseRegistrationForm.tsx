import React, { PureComponent } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { SecureInput } from "../../../components";

interface IOwnProps {
    disabled?: boolean;
}

type Props = IOwnProps;

export class SpouseRegistrationForm extends PureComponent<Props> {
    private readonly firstName = React.createRef<HTMLInputElement>();
    private readonly surName = React.createRef<HTMLInputElement>();
    private readonly emailRef = React.createRef<HTMLInputElement>();
    private readonly passwordRef = React.createRef<HTMLInputElement>();
    public render() {
        return (
            <>
                <Form.Row className="mb-2">
                    <Form.Group as={Col} className="mb-2">
                        <Form.Label>First name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="First name"
                            ref={this.firstName}
                            required
                            disabled={this.props.disabled}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Surname</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Surname"
                            ref={this.surName}
                            required
                            disabled={this.props.disabled}
                        />
                    </Form.Group>
                </Form.Row>
                <Form.Row className="mb-2">
                    <Form.Group as={Col} className="mb-2">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            ref={this.emailRef}
                            required
                            disabled={this.props.disabled}
                        />
                    </Form.Group>
                    <SecureInput
                        renderAs={Col}
                        fieldTitle={"Password"}
                        ref={this.passwordRef}
                        minimumLength={8}
                        minimumLengthErrorText={"Password must be at least 8 characters"}
                        weakErrorText={"Password is too weak, please supply stronger password"}
                        disabled={this.props.disabled}
                    />
                </Form.Row>
            </>
        );
    }

    public getSpouseInformation = () => {
        return {
            firstName: this.firstName.current?.value ?? "",
            surname: this.surName.current?.value ?? "",
            email: this.emailRef.current?.value ?? "",
            password: this.passwordRef.current?.value ?? "",
        };
    };
}
