import React, { PureComponent, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Jumbotron from "react-bootstrap/Jumbotron";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { Title } from "../../../components";
import {
    ApplicationReduxState,
    AuthenticationActionCreator,
    ClearAuthProcessDataAction,
    LoginAction,
} from "../../../store";
import { container } from "tsyringe";
import { toast } from "react-toastify";
import { AuthenticationSelector } from "../../../store/selectors";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import { RouteKey } from "../../../routing";

interface IOwnState {
    showPassword: boolean;
}

interface IOwnProps {
    onRegisterClicked: () => void;
    onGuestLoginClicked: () => void;
}
interface IStateProps {
    isLoggingIn?: boolean;
    loginSucceeded?: boolean;
    loginFailureMessage?: string;
    weddingCode?: string;
    isGuest: boolean;
    userId?: string;
}

interface IDispatchProps {
    login: (payload: { email: string; password: string }) => LoginAction;
    clear: () => ClearAuthProcessDataAction;
}

type Props = RouteComponentProps & IOwnProps & IDispatchProps & IStateProps;

export class DefaultLoginFormComponent extends PureComponent<Props, IOwnState> {
    private readonly emailRef = React.createRef<HTMLInputElement>();
    private readonly passwordRef = React.createRef<HTMLInputElement>();

    public constructor(props: Props) {
        super(props);
        this.state = {
            showPassword: false,
        };
    }
    public componentDidUpdate(prevProps: Props, prevState: IOwnState) {
        if (!isEqual(prevState, this.state)) {
            return;
        }
        if (!prevProps.isLoggingIn && this.props.isLoggingIn) {
            toast.warning("Logging in, please wait");
            return;
        }
        if (!prevProps.loginSucceeded && this.props.loginSucceeded) {
            toast.success("Successfully logged in!");
            if (!prevProps.weddingCode && this.props.weddingCode) {
                if (this.props.isGuest) {
                    this.props.history.push(`${RouteKey.GuestBase}${this.props.weddingCode}/${this.props.userId}`);
                } else {
                    this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}`);
                }
            }
            return;
        }
        if (!prevProps.weddingCode && this.props.weddingCode) {
            if (this.props.isGuest) {
                this.props.history.push(`${RouteKey.GuestBase}${this.props.weddingCode}/${this.props.userId}`);
            } else {
                this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}`);
            }
            return;
        }
        if (prevProps.loginSucceeded === false && this.props.loginFailureMessage) {
            toast.error(this.props.loginFailureMessage);
            return;
        }
    }

    public componentWillUnmount() {
        this.props.clear();
    }

    public render() {
        return (
            <>
                <Title title="Spouse Login" />
                <Container>
                    <Row>
                        <Col>
                            <Jumbotron>
                                <Container fluid>
                                    <Row>
                                        <h3>Login</h3>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form onSubmit={this.login}>
                                                <Form.Group>
                                                    <Form.Label>Email address</Form.Label>
                                                    <Form.Control
                                                        type="email"
                                                        placeholder="Enter email"
                                                        ref={this.emailRef}
                                                        required
                                                    />
                                                </Form.Group>

                                                <Form.Group>
                                                    <Form.Label>Password</Form.Label>
                                                    <InputGroup className="mb-2">
                                                        <Form.Control
                                                            type={this.state.showPassword ? "text" : "password"}
                                                            placeholder="Password"
                                                            ref={this.passwordRef}
                                                            required
                                                        />
                                                        <InputGroup.Append>
                                                            <InputGroup.Text
                                                                id={"passwordInput"}
                                                                onClick={() =>
                                                                    this.setState({
                                                                        showPassword: !this.state.showPassword,
                                                                    })
                                                                }
                                                                style={
                                                                    this.state.showPassword
                                                                        ? { color: "orange" }
                                                                        : undefined
                                                                }>
                                                                <FontAwesomeIcon
                                                                    icon={this.state.showPassword ? faEye : faEyeSlash}
                                                                />
                                                            </InputGroup.Text>
                                                        </InputGroup.Append>
                                                    </InputGroup>
                                                </Form.Group>
                                                <Button variant="secondary" type="submit">
                                                    Login
                                                </Button>
                                            </Form>
                                        </Col>
                                    </Row>
                                    <Row className="mt-5">
                                        <Col xs={{ span: 4, order: "first" }} onClick={this.props.onGuestLoginClicked}>
                                            <Button variant="info">Wedding Guest Click Here</Button>
                                        </Col>
                                        <Col xs={{ order: "last", span: 4, offset: 4 }}>
                                            <Button
                                                variant="warning"
                                                className={"float-right"}
                                                onClick={this.props.onRegisterClicked}>
                                                Register
                                            </Button>
                                        </Col>
                                    </Row>
                                </Container>
                            </Jumbotron>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
    private login = (e: SyntheticEvent) => {
        e.preventDefault();
        const email = this.emailRef.current?.value ?? "";
        const password = this.passwordRef.current?.value ?? "";
        this.props.login({ email, password });
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isLoggingIn: authenticationSelector.selectIsLoggingIn(state),
        loginFailureMessage: authenticationSelector.selectLoginFailedMessage(state),
        loginSucceeded: authenticationSelector.selectLoginSucceeded(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    login: authenticationActionCreator.login,
    clear: authenticationActionCreator.clear,
};
export const DefaultLoginForm = connect(mapStateToProps, mapDispatchToProps)(withRouter(DefaultLoginFormComponent));
