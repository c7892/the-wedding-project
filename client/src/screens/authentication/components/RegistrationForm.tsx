import React, { PureComponent, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Jumbotron from "react-bootstrap/Jumbotron";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { SpouseRegistrationForm } from "./SpouseRegistrationForm";
import { Title } from "../../../components";
import { SpouseRegistrationInformation } from "../../../models";
import {
    ApplicationReduxState,
    AuthenticationActionCreator,
    ClearAuthProcessDataAction,
    RegisterAction,
} from "../../../store";
import { container } from "tsyringe";
import { AuthenticationSelector } from "../../../store/selectors";
import { connect } from "react-redux";
import { extend } from "lodash";
import { toast } from "react-toastify";

interface IOwnProps {
    onGuestLoginClicked: () => void;
    onSpouseLoginClicked: () => void;
}

interface IStateProps {
    isRegistering?: boolean;
    registrationSucceeded?: boolean;
    registrationFailureMessage?: string;
}

interface IDispatchProps {
    register: (payload: SpouseRegistrationInformation[]) => RegisterAction;
    clear: () => ClearAuthProcessDataAction;
}

type Props = RouteComponentProps & IOwnProps & IStateProps & IDispatchProps;

export class RegistrationFormComponent extends PureComponent<Props> {
    private readonly familyName = React.createRef<HTMLInputElement>();
    private spouse1: SpouseRegistrationForm | null = null;
    private spouse2: SpouseRegistrationForm | null = null;

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isRegistering && this.props.isRegistering) {
            toast.warning("Registering spouses, please wait");
            return;
        }
        if (!prevProps.registrationSucceeded && this.props.registrationSucceeded) {
            toast.success("Successfully registered spouses, please login in");
            setTimeout(this.props.onSpouseLoginClicked, 1000);
            return;
        }
        if (prevProps.registrationSucceeded === false && this.props.registrationFailureMessage) {
            toast.error(this.props.registrationFailureMessage);
            return;
        }
    }

    public componentWillUnmount() {
        this.props.clear();
    }

    public render() {
        return (
            <>
                <Title title="Registration" />
                <Container>
                    <Row>
                        <Col>
                            <Jumbotron>
                                <Container fluid>
                                    <Row>
                                        <h3>Register</h3>
                                        <hr />
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form onSubmit={this.register}>
                                                <Row>
                                                    <Col xs={6}>
                                                        <h5>You</h5>
                                                        <SpouseRegistrationForm
                                                            ref={(ref) => {
                                                                this.spouse1 = ref;
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <h5>Your Spouse</h5>
                                                        <SpouseRegistrationForm
                                                            ref={(ref) => {
                                                                this.spouse2 = ref;
                                                            }}
                                                            disabled={this.props.isRegistering}
                                                        />
                                                    </Col>
                                                </Row>
                                                <Form.Row>
                                                    <Form.Group as={Col} className="mb-2">
                                                        <Form.Label>Your new family name</Form.Label>
                                                        <Form.Control
                                                            type="text"
                                                            placeholder="Your new family name"
                                                            ref={this.familyName}
                                                            required
                                                            disabled={this.props.isRegistering}
                                                        />
                                                    </Form.Group>
                                                </Form.Row>
                                                <Row>
                                                    <Col>
                                                        <Button
                                                            variant="secondary"
                                                            type="submit"
                                                            className="mt-2"
                                                            disabled={this.props.isRegistering}>
                                                            Register
                                                        </Button>
                                                    </Col>
                                                </Row>
                                            </Form>
                                        </Col>
                                    </Row>
                                    <Row className="mt-5">
                                        <Col xs={{ span: 4, order: "first" }} onClick={this.props.onSpouseLoginClicked}>
                                            <Button variant="info" disabled={this.props.isRegistering}>
                                                Spouses Login
                                            </Button>
                                        </Col>
                                        <Col xs={{ order: "last", span: 4, offset: 4 }}>
                                            <Button
                                                variant="warning"
                                                className={"float-right"}
                                                onClick={this.props.onGuestLoginClicked}
                                                disabled={this.props.isRegistering}>
                                                Guest Login
                                            </Button>
                                        </Col>
                                    </Row>
                                </Container>
                            </Jumbotron>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
    private register = (e: SyntheticEvent) => {
        e.preventDefault();
        const spouse1 = this.spouse1?.getSpouseInformation();
        const spouse2 = this.spouse2?.getSpouseInformation();
        if (!spouse1 || !spouse2) {
            return;
        }
        const newFamilyName = this.familyName.current?.value ?? "";
        const spouses: SpouseRegistrationInformation[] = [
            extend({}, spouse1, { newFamilyName: newFamilyName }),
            extend({}, spouse2, { newFamilyName: newFamilyName }),
        ];
        this.props.register(spouses);
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isRegistering: authenticationSelector.selectIsRegistering(state),
        registrationFailureMessage: authenticationSelector.selectRegistrationFailedMessage(state),
        registrationSucceeded: authenticationSelector.selectRegistrationSucceeded(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    register: authenticationActionCreator.register,
    clear: authenticationActionCreator.clear,
};

export const RegistrationForm = connect(mapStateToProps, mapDispatchToProps)(withRouter(RegistrationFormComponent));
