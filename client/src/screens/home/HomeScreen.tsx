import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { AllEventsList, UpcomingEventsScreen } from "../events";
import { CountdownToBigDay } from "./components/CountdownToBigDay";

export class HomeScreen extends PureComponent {
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <Row>
                            <CountdownToBigDay />
                        </Row>
                        <Row>
                            <UpcomingEventsScreen />
                        </Row>
                    </Col>
                    <Col className="d-none d-sm-none d-md-block" md={4}>
                        <AllEventsList />
                    </Col>
                </Row>
            </Container>
        );
    }
}
