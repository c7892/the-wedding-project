import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { RouteComponentProps, withRouter } from "react-router-dom";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { container } from "tsyringe";
import { connect } from "react-redux";
import { Event } from "../../../models";
import { ApplicationReduxState, EventActionCreator, FetchWeddingDayAction } from "../../../store";
import { EventSelector } from "../../../store/selectors";
import moment from "moment-timezone";
import parse from "html-react-parser";

interface IOwnState {
    startsIn: string | JSX.Element;
}

interface IStateProps {
    wedding?: Event;
}

interface IDispatchProps {
    fetchWeddingDay: () => FetchWeddingDayAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class CountdownToBigDayComponent extends PureComponent<Props, IOwnState> {
    private countDown?: NodeJS.Timeout;
    public constructor(props: Props) {
        super(props);
        this.state = {
            startsIn: "",
        };
    }
    public componentDidMount() {
        setTimeout(this.props.fetchWeddingDay, 500);
        this.countDown = setInterval(this.updateCountDown, 1000);
    }
    public componentDidUpdate(prevProps: Props) {
        if (this.props.wedding !== prevProps.wedding) {
            if (this.countDown) {
                clearInterval(this.countDown);
            }
            this.countDown = setInterval(this.updateCountDown, 1000);
        }
    }
    public componentWillUnmount() {
        if (this.countDown) {
            clearInterval(this.countDown);
        }
    }
    public render() {
        const event = this.props.wedding;
        return !event ? null : (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>The Big Day</h1>
                        <hr />
                    </Col>
                </Row>
                <Row>
                    {event.location ? (
                        <Col xs={6} md={4}>
                            <strong>
                                At:&nbsp;
                                <a
                                    href={`https://maps.google.com/?q=${event.location}`}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    {event.location}
                                </a>
                            </strong>
                        </Col>
                    ) : null}
                    <Col>
                        {this.state.startsIn}
                        <strong>
                            {moment.tz(event.start, event.timezone).format("ddd, MMM Do YYYY, h:mm a z")}
                            &nbsp;-&nbsp;
                            {moment.tz(event.end, event.timezone).format("ddd, MMM Do YYYY, h:mm a z")}
                        </strong>
                    </Col>
                </Row>
                <Row style={{ marginTop: 8 }}>
                    <Col>
                        <h4>Details</h4>
                    </Col>
                </Row>
                <Row>
                    <Col>{parse(event?.description ?? "")}</Col>
                </Row>
            </Container>
        );
    }

    private updateCountDown = () => {
        if (!this.props.wedding) {
            return;
        }
        const event = this.props.wedding;
        const start = moment.tz(event.start, event.timezone);
        const now = moment.tz(new Date(), event.timezone);
        const diff = start.toDate().getTime() - now.toDate().getTime();
        if (diff <= 0) {
            this.setState({ startsIn: <h2>The celebration has begun!!</h2> }, () => {
                if (this.countDown) {
                    clearInterval(this.countDown);
                }
            });
            return;
        }
        const days = moment(start).diff(now, "days");
        const hours = moment(start).diff(now, "hours") % 24;
        const minutes = (moment(start).diff(now, "minutes") % 60) % 24;
        const seconds = ((moment(start).diff(now, "seconds") % 60) % 60) % 60;
        const startsIn = (
            <>
                <h4>The party starts in: </h4>
                <h2>{`${days > 9 ? "" : "0"}${days} day${days != 1 ? "s" : ""} ${hours > 9 ? "" : "0"}${hours} hour${
                    hours != 1 ? "s" : ""
                } ${minutes > 9 ? "" : "0"}${minutes} minute${minutes != 1 ? "s" : ""} ${
                    seconds > 9 ? "" : "0"
                }${seconds} second${minutes != 1 ? "s" : ""}`}</h2>
            </>
        );
        this.setState({ startsIn });
    };
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        wedding: eventSelector.selectWeddingDay(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchWeddingDay: eventActionCreator.fetchWeddingDay,
};

export const CountdownToBigDay = connect(mapStateToProps, mapDispatchToProps)(withRouter(CountdownToBigDayComponent));
