import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { container } from "tsyringe";
import { ApplicationReduxState, FetchArEventAction, SetArEventAction } from "../../store";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { EventSelector } from "../../store/selectors";
import { Event } from "../../models";
import { IConfig } from "../../utilities";
import { Helmet } from "react-helmet";
import { isEqual } from "lodash";
import Spinner from "react-bootstrap/Spinner";
import moment from "moment-timezone";
import { RouteKey } from "../../routing";
declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace JSX {
        interface IntrinsicElements {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-marker": { [key: string]: any };
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-plane": { [key: string]: any };
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-text": { [key: string]: any };
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-assets": any;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-scene": any;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            "a-entity": any;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            [key: string]: any;
        }
    }
}

interface IStateProps {
    event?: Event;
}

interface IDispatchProps {
    fetchArEvent: (payload: { id: string; guest: string }) => FetchArEventAction;
    clear: () => SetArEventAction;
}

interface IOwnState {
    isRegistered: boolean;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class ArInvitationScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly clockTextRef = React.createRef();
    private clockInterval: NodeJS.Timeout | undefined = undefined;
    public constructor(props: Props) {
        super(props);
        this.state = {
            isRegistered: false,
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            const params = this.props.match.params as { eventId: string; guestId: string };
            this.props.fetchArEvent({ id: params.eventId, guest: params.guestId });
            this.registerAframe();
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (isEqual(prevProps.event, this.props.event)) {
            return;
        }
        this.setState({ isRegistered: false }, this.registerAframe);
    }
    public componentWillUnmount() {
        this.setState({ isRegistered: false });
        this.props.clear();
        clearInterval(this.clockInterval as unknown as number);
    }
    public render() {
        const config = container.resolve<IConfig>("Config");
        return !this.state.isRegistered ? (
            <Spinner
                animation="grow"
                variant="primary"
                style={{
                    width: "10rem",
                    height: "10rem",
                    position: "absolute",
                    left: "50%",
                    right: "50%",
                    top: "40%",
                    bottom: "40%",
                }}
            />
        ) : (
            <>
                <Helmet>
                    <meta name="apple-mobile-web-app-capable" content="yes"></meta>
                </Helmet>
                <a-scene
                    id="scene"
                    arjs="trackingMethod: best; debugUIEnabled: false; detectionMode: mono_and_matrix; matrixCodeType: 3x3;"
                    vr-mode-ui="enabled: false"
                    ar-template>
                    <a-assets>
                        <img id="invitation" src={config.ClientEndpoint + "/assets/rsvp.png"} crossOrigin="anonymous" />
                        <img
                            id="map-point"
                            src={config.ClientEndpoint + "/assets/location.png"}
                            crossOrigin="anonymous"
                        />

                        <img id="texture" src={config.ClientEndpoint + "/assets/clock.png"} crossOrigin="anonymous" />
                    </a-assets>

                    <a-marker id="marker1" type="barcode" value="2" video-vidhandler>
                        <a-plane
                            scale="1.5 1.5"
                            position="0 0 0"
                            rotation="-90 0 0"
                            material="transparent:true;src:#invitation"
                            controls></a-plane>
                    </a-marker>
                    <a-marker id="marker2" type="barcode" value="1">
                        <a-plane
                            scale="1.5 1.5"
                            position="0 0 0"
                            rotation="-90 0 0"
                            material="transparent:true;src:#texture">
                            <a-text
                                ref={this.clockTextRef}
                                id="timer"
                                clock-text
                                value="00:00:00:00"
                                width="1.5"
                                height="2"
                                position="-0.451 0.041 0.000"
                                color="#000000"></a-text>
                        </a-plane>
                    </a-marker>

                    <a-marker
                        id="marker3"
                        type="barcode"
                        value="3"
                        emitevents="true"
                        cursor="rayOrigin: mouse"
                        mappoint-handler>
                        <a-plane
                            id="img-map"
                            scale="1.5 1.5"
                            position="0 0 0"
                            rotation="-90 0 0"
                            material="transparent:true;src:#map-point"></a-plane>
                    </a-marker>
                    <a-entity camera>
                        <a-cursor></a-cursor>
                    </a-entity>
                </a-scene>
            </>
        );
    }

    private registerAframe = () => {
        if (!this.props.event) {
            return;
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const aFrame: any = (window as unknown as { AFRAME: any }).AFRAME;
        if (!aFrame) {
            return;
        }
        if (this.state.isRegistered) {
            return;
        }
        aFrame.registerComponent("video-vidhandler", {
            init: () => {
                console.log("video init entered");
                // this.toggle = false;
                // this.vid = document.querySelector("#invitation");
                // this.vid.pause();
            },
            tick: () => {
                // if (this.el.object3D.visible == true) {
                //     if (!this.toggle) {
                //         this.toggle = true;
                //         this.vid.play();
                //     }
                // } else {
                //     this.toggle = false;
                //     this.vid.pause();
                // }
            },
        });

        aFrame.registerComponent("clock-text", {
            init: () => {
                if (!this.props.event) {
                    return;
                }
                const endTime = moment.tz(this.props.event.start, this.props.event.timezone);
                let timeRem = "";
                clearInterval(this.clockInterval as unknown as number);
                this.clockInterval = setInterval(() => {
                    const currTime = moment.tz(new Date(), moment.tz.guess(true));

                    const diff = endTime.diff(currTime);
                    if (diff > 0) {
                        const days = Math.floor(diff / (1000 * 60 * 60 * 24));
                        const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        const mins = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
                        const secs = Math.floor((diff % (1000 * 60)) / 1000);
                        timeRem = days + " days " + hours + " hrs " + mins + " mins " + secs + " secs ";
                    } else {
                        timeRem = "Finally! The day has come.";
                    }
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    (this.clockTextRef.current as any)?.setAttribute("value", timeRem);
                }, 1500);
            },
            tick: () => {
                // do nothing
            },
        });

        aFrame.registerComponent("mappoint-handler", {
            init: () => {
                const marker3 = document.querySelector("#marker3");
                const imgMap = document.querySelector("#img-map");
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                marker3?.addEventListener("click", (ev: any) => {
                    const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
                    if (marker3 && intersectedElement === marker3 && this.props.event?.location) {
                        document.location.href = `https://maps.google.com/?q=${this.props.event.location}`;
                    }
                });
                imgMap?.addEventListener("click", () => {
                    if (this.props.event?.location) {
                        document.location.href = `https://maps.google.com/?q=${this.props.event.location}`;
                    }
                });
                const marker1 = document.querySelector("#marker1");
                const invitationIcon = document.querySelector("#map-point");
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                marker1?.addEventListener("click", (ev: any) => {
                    const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
                    if (marker1 && intersectedElement === marker1) {
                        this.props.history.push(RouteKey.Login);
                    }
                });
                invitationIcon?.addEventListener("click", () => {
                    this.props.history.push(RouteKey.Login);
                });
            },
        });
        this.setState({
            isRegistered: true,
        });
    };
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        event: eventSelector.selectArEvent(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchArEvent: eventActionCreator.fetchArEvent,
    clear: eventActionCreator.setArEvent,
};

export const ArInvitationScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(ArInvitationScreenComponent));
