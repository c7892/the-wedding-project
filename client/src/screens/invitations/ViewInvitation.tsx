import React, { PureComponent, SyntheticEvent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { container } from "tsyringe";
import { Guest, Event, Invite } from "../../models";
import {
    ApplicationReduxState,
    FetchEventsAction,
    FetchGuestListsAction,
    FetchGuestsAction,
    GuestListActionCreator,
    EventActionCreator,
    SetIsSendingInviteAction,
    SendInviteParams,
    SendInviteAction,
    InviteActionCreator,
    SendInviteConfirmationAction,
} from "../../store";
import { EventSelector, AuthenticationSelector, GuestListSelector, InviteSelector } from "../../store/selectors";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { InvitationPreview } from "./components";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { isEmpty, values } from "lodash";
import { toast } from "react-toastify";
import { RouteKey } from "../../routing";

interface IOwnState {
    invite: Invite;
    guestId: string;
}

interface IStateProps {
    guests: Guest[];
    event?: Event;
    weddingCode?: string;
    userId?: string;
    isSendingInvite: boolean;
}

interface IDispatchProps {
    getGuestLists: () => FetchGuestListsAction;
    getGuests: () => FetchGuestsAction;
    getEvents: (payload?: Date) => FetchEventsAction;
    setIsSendingInvite: (payload: boolean) => SetIsSendingInviteAction;
    sendInvite: (payload: SendInviteParams) => SendInviteAction;
    sendInviteConfirmation: (payload: { guestIds: string[]; inviteId: string }) => SendInviteConfirmationAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class ViewInvitationScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly guestReferences: { [id: string]: HTMLInputElement[] } = {};
    private readonly invitePreview = React.createRef<InvitationPreview>();
    public constructor(props: Props) {
        super(props);
        const locationState = props.location.state as { invite: Invite };
        this.state = {
            invite: locationState.invite,
            guestId: props.userId ?? "",
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            this.props.getEvents();
            this.props.getGuestLists();
            this.props.getGuests();
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSendingInvite && this.props.isSendingInvite) {
            toast.warning("Sending invites, please do not leave this page, it can take some time send each invite");
        }
        if (prevProps.isSendingInvite && !this.props.isSendingInvite) {
            toast.success("Successfully sent invites!");
        }
    }

    public render() {
        return (
            <Container fluid style={{ marginBottom: 16 }}>
                <Row>
                    <Col>
                        <h1>{this.state.invite.inviteTitle}</h1>
                    </Col>
                </Row>
                <Row>
                    <Col
                        style={{
                            borderRight: "1px solid black",
                        }}>
                        <InvitationPreview
                            ref={this.invitePreview}
                            guestId={this.state.guestId}
                            event={this.props.event}
                            frontImage={this.state.invite.frontImage}
                            backImage={this.state.invite.backImage}
                            notes={this.state.invite.notes}
                            titleColor={this.state.invite.titleColor}
                            bodyColor={this.state.invite.bodyColor}
                            title2Color={this.state.invite.title2Color}
                            scanColor={this.state.invite.scanColor}
                            weddingCode={this.props.weddingCode}
                            title={this.state.invite.title}
                        />
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                <h3>Guests</h3>
                                <p>(De)Select each method you want to use to send your invite to your guests.</p>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 8 }}>
                            <Col>
                                <Button variant="secondary" onClick={this.selectAllTexts}>
                                    Check All Texts
                                </Button>
                            </Col>
                            <Col>
                                <Button variant="secondary" onClick={this.selectAllEmails}>
                                    Check All Emails
                                </Button>
                            </Col>
                            <Col>
                                <Button variant="secondary" onClick={this.selectAllPostal}>
                                    Check All Postal
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form onSubmit={this.send}>
                                    {this.props.guests.map(this.renderGuestRow)}
                                    <Row>
                                        <Col>
                                            {isEmpty(this.props.guests) ? null : (
                                                <Button
                                                    type="submit"
                                                    variant="success"
                                                    disabled={this.props.isSendingInvite}>
                                                    Send
                                                </Button>
                                            )}
                                        </Col>
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }

    /* eslint-disable @typescript-eslint/no-non-null-assertion */
    private renderGuestRow = (guest: Guest) => {
        if (isEmpty(this.guestReferences[guest._id!])) {
            this.guestReferences[guest._id!] = [];
        }
        return (
            <Form.Group as={Row} key={guest._id}>
                <Form.Label column sm={4}>
                    {guest.firstName}&nbsp;{guest.surname}&nbsp;
                    {this.getRelationShipToYou(guest) ? `(${this.getRelationShipToYou(guest)})` : ""}
                </Form.Label>
                <Col>
                    <Form.Check
                        inline
                        label="SMS"
                        name="SMS"
                        type={"checkbox"}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex((r) => r.name === "SMS");
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                    <Form.Check
                        inline
                        disabled={!guest.email ?? isEmpty(guest.email)}
                        label="Email"
                        name="Email"
                        type={"checkbox"}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex((r) => r.name === "Email");
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                    <Form.Check
                        inline
                        disabled={!guest.address ?? isEmpty(guest.address)}
                        label="Postal Mail"
                        name="Postal Mail"
                        type={"checkbox"}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex(
                                    (r) => r.name === "Postal Mail",
                                );
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                </Col>
            </Form.Group>
        );
    };
    /* eslint-enable @typescript-eslint/no-non-null-assertion */
    private getRelationShipToYou = (guest: Guest) => {
        return guest.relation && this.props.userId ? guest.relation[this.props.userId] : "";
    };

    private selectAllTexts = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "SMS" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
    };
    private selectAllEmails = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "Email" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
    };
    private selectAllPostal = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "Postal Mail" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
    };

    private send = async (e: SyntheticEvent) => {
        e.preventDefault();
        this.props.setIsSendingInvite(true);
        const back = await this.invitePreview.current?.captureBack();
        const seekingPaidServices: string[] = [];
        const allGuests: string[] = [];
        for (const guest of this.props.guests) {
            allGuests.push(guest._id ?? "");
            const id = await this.sendGuestInvite(guest, back ?? "");
            if (id) {
                seekingPaidServices.push(id);
            }
        }
        this.props.sendInviteConfirmation({ inviteId: this.state.invite?._id ?? "", guestIds: allGuests });
        this.props.setIsSendingInvite(false);
        if (!isEmpty(seekingPaidServices)) {
            setTimeout(() => {
                this.props.history.push(
                    `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.PurchaseInvitation}`,
                    {
                        invite: this.state.invite,
                        guestIds: seekingPaidServices,
                    },
                );
            }, 1000);
        }
    };

    private sendGuestInvite = async (guest: Guest, back: string): Promise<string | undefined> => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const refs = this.guestReferences[guest._id!];
        const checked = refs.filter((ref) => ref.checked);
        if (isEmpty(checked)) {
            return undefined;
        }
        const sendMethods = checked.map((ref) => ref.name);
        await new Promise((resolve) => {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            this.setState({ guestId: guest._id! }, () => {
                resolve(0);
            });
        });
        let wantsToSendViaPostOrSms = false;
        let index = sendMethods.indexOf("Postal Mail");
        if (index >= 0) {
            wantsToSendViaPostOrSms = true;
            sendMethods.splice(index, 1);
        }
        index = sendMethods.indexOf("SMS");
        if (index >= 0) {
            wantsToSendViaPostOrSms = true;
            sendMethods.splice(index, 1);
        }
        if (sendMethods.length <= 0 && index < 0) {
            return undefined;
        } else if (sendMethods.length <= 0 && index >= 0) {
            return guest._id;
        }
        const front = await this.invitePreview.current?.captureFront();
        this.props.sendInvite({
            sendMethods,
            front: front ?? "",
            back,
            inviteId: this.state.invite?._id ?? "",
            guest,
        });
        if (wantsToSendViaPostOrSms) {
            return guest._id;
        }
        return undefined;
    };
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const guestListActionCreator = container.resolve(GuestListActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const guestListSelector = container.resolve(GuestListSelector);
const inviteActionCreator = container.resolve(InviteActionCreator);
const inviteSelector = container.resolve(InviteSelector);

const mapStateToProps = (state: ApplicationReduxState, props: RouteComponentProps): IStateProps => {
    const locationState = props.location.state as { invite: Invite };

    const event = eventSelector.selectEventById(state, locationState.invite.event ?? "");
    const weddingCode = authenticationSelector.selectWeddingCode(state);
    return {
        event,
        weddingCode,
        guests: guestListSelector.selectGuestsByIds(state, event?.guests ?? []),
        userId: authenticationSelector.selectUserId(state),
        isSendingInvite: inviteSelector.selectIsSendingInvite(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
    getGuestLists: guestListActionCreator.fetchGuestLists,
    getGuests: guestListActionCreator.fetchGuests,
    sendInvite: inviteActionCreator.sendInvite,
    setIsSendingInvite: inviteActionCreator.setIsSendingInvite,
    sendInviteConfirmation: inviteActionCreator.sendInviteConfirmation,
};

export const ViewInvitationScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(ViewInvitationScreenComponent));
