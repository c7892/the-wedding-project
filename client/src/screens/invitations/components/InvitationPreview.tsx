import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Qrcode from "qrcode.react";
import Image from "react-bootstrap/Image";
import { container } from "tsyringe";
import { getBase64ImageDataString, IConfig } from "../../../utilities";
import { RouteKey } from "../../../routing";
import { Event } from "../../../models";
import { isEqual } from "lodash";
import moment from "moment-timezone";
import { toPng } from "html-to-image";
import parse from "html-react-parser";
import Color from "color";

interface IOwnProps {
    event?: Event;
    guestId?: string;
    frontImage?: File | string;
    backImage?: File | string;
    notes?: string;
    title?: string;
    titleColor?: string;
    bodyColor?: string;
    title2Color?: string;
    scanColor?: string;
    weddingCode?: string;
}

interface IOwnState {
    frontImageStyle?: React.CSSProperties;
    backImageStyle?: React.CSSProperties;
}

type Props = IOwnProps;

export class InvitationPreview extends PureComponent<Props, IOwnState> {
    private readonly config = container.resolve<IConfig>("Config");
    public constructor(props: Props) {
        super(props);
        this.state = {
            frontImageStyle: undefined,
            backImageStyle: undefined,
        };
    }

    public componentDidMount() {
        this.setupImageStyles();
    }

    public componentDidUpdate(prevProps: Props) {
        if (
            isEqual(prevProps, this.props) ||
            (isEqual(prevProps.backImage, this.props.backImage) && isEqual(prevProps.frontImage, this.props.frontImage))
        ) {
            return;
        }
        this.setupImageStyles();
    }

    public render() {
        const regex = new RegExp("<.*?>", "gmi");
        const notesIsEmpty = !this.props.notes || this.props.notes.replace(regex, "").trim().length === 0;
        return (
            <>
                <h3>Front</h3>
                <Container
                    className="invitation-preview"
                    style={{ ...this.state.frontImageStyle, position: "relative" }}
                    fluid
                    id="front">
                    <Row>
                        <Col>
                            <h1 style={{ color: this.props.titleColor }}>{this.props.title}</h1>
                        </Col>
                        {this.props.event ? (
                            <Col>
                                <Row>
                                    <Col>
                                        <p style={{ color: this.props.scanColor }}>
                                            <Qrcode
                                                id="qrcode"
                                                value={`${this.config.ClientEndpoint}${RouteKey.ViewArInvitationBase}${this.props.event?._id}/${this.props.guestId}`}
                                                includeMargin
                                                size={64}
                                                renderAs={"svg"}
                                                className="float-right"
                                            />
                                            *Scan QR Code to launch AR invite (then scan each tile for a surprise)
                                        </p>
                                    </Col>
                                </Row>
                            </Col>
                        ) : null}
                    </Row>
                    {this.props.event ? (
                        <Row
                            style={{
                                position: "absolute",
                                left: 20,
                                right: 0,
                                bottom: 70,
                                marginLeft: "auto",
                                marginRight: "auto",
                            }}>
                            <Col>
                                <Image src="/assets/01-clock.png" style={{ width: 100 }} />
                            </Col>
                            <Col>
                                <Image src="/assets/02-rsvp.png" style={{ width: 100 }} />
                            </Col>
                            <Col>
                                <Image src="/assets/03-location.png" style={{ width: 100 }} />
                            </Col>
                        </Row>
                    ) : null}
                </Container>
                <br />
                <h3>Back</h3>
                <Container className="invitation-preview" style={this.state.backImageStyle} id="back" fluid>
                    {this.props.event ? (
                        <>
                            <Row>
                                <Col>
                                    <h2 style={{ color: this.props.title2Color }}>{this.props.event.title}</h2>
                                </Col>
                            </Row>
                            {this.props.notes ? (
                                <Row
                                    style={
                                        !notesIsEmpty
                                            ? {
                                                  color: this.props.bodyColor,
                                                  backgroundColor: this.generateBackgroundColor(this.props.bodyColor),
                                              }
                                            : undefined
                                    }>
                                    <Col>{parse(this.props.notes ?? "")}</Col>
                                </Row>
                            ) : null}
                            <Row
                                style={{
                                    color: this.props.bodyColor,
                                    backgroundColor: this.generateBackgroundColor(this.props.bodyColor),
                                }}>
                                <Col>
                                    {this.props.event.location ? <p>Location: {this.props.event.location}</p> : null}
                                    <p>
                                        When:{" "}
                                        {moment
                                            .tz(this.props.event.start, this.props.event.timezone)
                                            .format("ddd, MMM Do YYYY, h:mm a z")}
                                    </p>
                                    <p>
                                        Til:{" "}
                                        {moment
                                            .tz(this.props.event.end, this.props.event.timezone)
                                            .format("ddd, MMM Do YYYY, h:mm a z")}
                                    </p>
                                    <p>
                                        RSVP at{" "}
                                        <a
                                            href={`${this.config.ClientEndpoint}${RouteKey.LoginRoot}`}
                                            style={{
                                                color: this.props.bodyColor,
                                            }}>{`${this.config.ClientEndpoint}`}</a>{" "}
                                    </p>
                                    <p>Invite code:&nbsp;{this.props.weddingCode}</p>
                                </Col>
                            </Row>
                        </>
                    ) : (
                        <Row
                            style={
                                !notesIsEmpty
                                    ? {
                                          color: this.props.bodyColor,
                                          backgroundColor: this.generateBackgroundColor(this.props.bodyColor),
                                      }
                                    : undefined
                            }>
                            <Col>{parse(this.props.notes ?? "")}</Col>
                        </Row>
                    )}
                </Container>
            </>
        );
    }

    private generateBackgroundColor = (textColor?: string): string => {
        if (!textColor) {
            return "";
        }
        const foregroundColor = Color(textColor);
        return foregroundColor.negate().fade(0.4).string();
    };

    public captureFront = async () => {
        const frontNode = document.getElementById("front");
        if (!frontNode) {
            return "";
        }
        return await toPng(frontNode);
    };
    public captureBack = async () => {
        const backNode = document.getElementById("back");
        if (!backNode) {
            return "";
        }
        return await toPng(backNode);
    };

    private setupImageStyles = async () => {
        const { backImage, frontImage } = this.props;
        if (backImage) {
            if (typeof backImage === "string") {
                this.setState({
                    backImageStyle: {
                        backgroundImage: `url('${backImage}')`,
                    },
                });
            } else {
                const base64 = await getBase64ImageDataString(backImage);
                this.setState({
                    backImageStyle: {
                        backgroundImage: `url('${base64}')`,
                    },
                });
            }
        }
        if (frontImage) {
            if (typeof frontImage === "string") {
                this.setState({
                    frontImageStyle: {
                        backgroundImage: `url('${frontImage}')`,
                    },
                });
            } else {
                const base64 = await getBase64ImageDataString(frontImage);
                this.setState({
                    frontImageStyle: {
                        backgroundImage: `url('${base64}')`,
                    },
                });
            }
        }
    };
}
