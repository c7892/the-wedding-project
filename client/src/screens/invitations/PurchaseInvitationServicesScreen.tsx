import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { container } from "tsyringe";
import { Guest, Event, Invite } from "../../models";
import {
    ApplicationReduxState,
    FetchEventsAction,
    FetchGuestListsAction,
    FetchGuestsAction,
    GuestListActionCreator,
    EventActionCreator,
    SetIsSendingInviteAction,
    SendInviteParams,
    SendInviteAction,
    InviteActionCreator,
    SendInviteConfirmationAction,
} from "../../store";
import { EventSelector, AuthenticationSelector, GuestListSelector, InviteSelector } from "../../store/selectors";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { InvitationPreview, PurchasePostcardCheckoutForm } from "./components";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { isEmpty, values } from "lodash";
import { toast } from "react-toastify";
import { loadStripe, PaymentIntent } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { PaymentService } from "../../services";
import { IConfig } from "../../utilities";
import { Stripe } from "@stripe/stripe-js";
import { RouteKey } from "../../routing";

interface IOwnState {
    invite: Invite;
    guestId: string;
    guestIds: string[];
    numberOfSms: number;
    numberOfPostage: number;
    clientSecret?: string;
    cost?: number;
    postageCost?: number;
    smsCost?: number;
    paymentPayload?: PaymentIntent;
}

interface IStateProps {
    guests: Guest[];
    event?: Event;
    weddingCode?: string;
    userId?: string;
    email?: string;
    isSendingInvite: boolean;
}

interface IDispatchProps {
    getGuestLists: () => FetchGuestListsAction;
    getGuests: () => FetchGuestsAction;
    getEvents: (payload?: Date) => FetchEventsAction;
    setIsSendingInvite: (payload: boolean) => SetIsSendingInviteAction;
    sendInvite: (payload: SendInviteParams) => SendInviteAction;
    sendInviteConfirmation: (payload: { guestIds: string[]; inviteId: string }) => SendInviteConfirmationAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class PurchaseInvitationServicesScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly guestReferences: { [id: string]: HTMLInputElement[] } = {};
    private readonly invitePreview = React.createRef<InvitationPreview>();
    private readonly paymentService = container.resolve<PaymentService>("PaymentService");
    private readonly stripePromise: Promise<Stripe | null>;

    public constructor(props: Props) {
        super(props);
        const config = container.resolve<IConfig>("Config");
        this.stripePromise = loadStripe(config.StripeKey);
        const locationState = props.location.state as { invite: Invite; guestIds: string[] };
        this.state = {
            invite: locationState.invite,
            guestIds: locationState.guestIds,
            numberOfSms: 0,
            numberOfPostage: 0,
            guestId: props.userId ?? "",
            clientSecret: undefined,
            smsCost: undefined,
            postageCost: undefined,
            cost: undefined,
            paymentPayload: undefined,
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            this.props.getEvents();
            this.props.getGuestLists();
            this.props.getGuests();
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSendingInvite && this.props.isSendingInvite) {
            toast.warning("Sending invites, please do not leave this page, it can take some time send each invite");
        }
        if (prevProps.isSendingInvite && !this.props.isSendingInvite) {
            toast.success("Successfully sent invites!");
            toast("We will now navigate you back home");
            setTimeout(() => {
                this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}`);
            }, 750);
        }
    }

    public render() {
        return (
            <Container fluid style={{ marginBottom: 16 }}>
                <Row>
                    <Col>
                        <h1>Purchase postage for invitations</h1>
                        <p>
                            You are here on this page, because you requested to send invitations for&nbsp;
                            {this.state.invite.inviteTitle} via US Postal Services and/or SMS. This is not a free
                            service, and so before&nbsp; the invites are sent out, we will must ask you pay to cover our
                            overhead. We ask that before&nbsp; you proceed, you verify all addresses and phone numbers
                            are correct, as we cannot guarantee delivery for invalid addresses or phone numbers. Thank
                            you!
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col
                        style={{
                            borderRight: "1px solid black",
                        }}>
                        <InvitationPreview
                            ref={this.invitePreview}
                            guestId={this.state.guestId}
                            event={this.props.event}
                            frontImage={this.state.invite.frontImage}
                            backImage={this.state.invite.backImage}
                            notes={this.state.invite.notes}
                            titleColor={this.state.invite.titleColor}
                            bodyColor={this.state.invite.bodyColor}
                            title2Color={this.state.invite.title2Color}
                            scanColor={this.state.invite.scanColor}
                            weddingCode={this.props.weddingCode}
                            title={this.state.invite.title}
                        />
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                <h3>Guests</h3>
                                <p>(De)Select each guest you wish to send an invitation to.</p>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 8 }}>
                            <Col>
                                <Button variant="secondary" onClick={this.selectAllTexts}>
                                    Check All Texts
                                </Button>
                            </Col>
                            <Col>
                                <Button variant="secondary" onClick={this.selectAllPostal}>
                                    Check All Postal
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form onSubmit={(e) => e.preventDefault()}>
                                    {this.props.guests.map(this.renderGuestRow)}
                                </Form>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                {this.state.clientSecret && !this.props.isSendingInvite ? (
                                    <Elements
                                        options={{
                                            appearance: { theme: "stripe" },
                                            clientSecret: this.state.clientSecret,
                                        }}
                                        stripe={this.stripePromise}>
                                        <PurchasePostcardCheckoutForm
                                            clientSecret={this.state.clientSecret}
                                            totalCost={this.state.cost ?? 0}
                                            email={this.props.email ?? ""}
                                            numberOfPostage={this.state.numberOfPostage}
                                            numberOfSms={this.state.numberOfSms}
                                            onPaymentSuccessful={this.send}
                                            smsCost={this.state.smsCost ?? 0}
                                            postageCost={this.state.postageCost ?? 0}
                                        />
                                    </Elements>
                                ) : null}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }

    /* eslint-disable @typescript-eslint/no-non-null-assertion */
    private renderGuestRow = (guest: Guest) => {
        if (isEmpty(this.guestReferences[guest._id!])) {
            this.guestReferences[guest._id!] = [];
        }
        return (
            <Form.Group as={Row} key={guest._id}>
                <Form.Label column sm={6}>
                    {guest.firstName}&nbsp;{guest.surname}&nbsp;
                    {this.getRelationShipToYou(guest) ? `(${this.getRelationShipToYou(guest)})` : ""}
                </Form.Label>
                <Col>
                    <Form.Check
                        inline
                        label="SMS"
                        name="SMS"
                        type={"checkbox"}
                        onChange={() => this.requestNewCost()}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex((r) => r.name === "SMS");
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                    <Form.Check
                        inline
                        disabled={!guest.address ?? isEmpty(guest.address)}
                        label="Postal Mail"
                        name="Postal Mail"
                        type={"checkbox"}
                        onChange={() => this.requestNewCost()}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex(
                                    (r) => r.name === "Postal Mail",
                                );
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                </Col>
            </Form.Group>
        );
    };
    /* eslint-enable @typescript-eslint/no-non-null-assertion */
    private getRelationShipToYou = (guest: Guest) => {
        return guest.relation && this.props.userId ? guest.relation[this.props.userId] : "";
    };

    private selectAllTexts = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "SMS" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
        this.requestNewCost();
    };
    private selectAllPostal = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "Postal Mail" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
        this.requestNewCost();
    };

    private send = async (paymentPayload: PaymentIntent) => {
        this.setState({ paymentPayload });
        this.props.setIsSendingInvite(true);
        const back = await this.invitePreview.current?.captureBack();
        const guestIds: string[] = [];
        for (const guest of this.props.guests) {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const inputRefs = this.guestReferences[guest._id!];
            if (isEmpty(inputRefs)) {
                continue;
            }
            const checkedMethods = inputRefs.filter((ref) => ref.checked).map((ref) => ref.name);
            if (isEmpty(checkedMethods)) {
                continue;
            }
            await this.sendGuestInvite(guest, back ?? "", checkedMethods, paymentPayload);
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            guestIds.push(guest._id!);
        }
        this.props.sendInviteConfirmation({ inviteId: this.state.invite?._id ?? "", guestIds: guestIds });
        this.props.setIsSendingInvite(false);
    };

    private sendGuestInvite = async (
        guest: Guest,
        back: string,
        methods: string[],
        paymentPayload: PaymentIntent,
    ): Promise<void> => {
        await new Promise((resolve) => {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            this.setState({ guestId: guest._id! }, () => {
                resolve(0);
            });
        });
        const front = await this.invitePreview.current?.captureFront();
        this.props.sendInvite({
            sendMethods: methods,
            front: front ?? "",
            back,
            inviteId: this.state.invite?._id ?? "",
            guest,
            paymentPayload,
        });
    };

    private getNumberOfInvitationsToSend = () => {
        let postCards = 0;
        let texts = 0;
        for (const guest of this.props.guests) {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const refs = this.guestReferences[guest._id!];
            const checked = refs.filter((ref) => ref.checked);
            if (isEmpty(checked)) {
                continue;
            }
            checked.forEach((ref) => {
                if (ref.name.toLowerCase() === "sms") {
                    texts++;
                } else {
                    postCards++;
                }
            });
        }
        return { postCards, texts };
    };

    private requestNewCost = async () => {
        const { postCards, texts } = this.getNumberOfInvitationsToSend();
        const clientSecret = await this.paymentService.requestCostAsync(postCards, texts);
        if (clientSecret && clientSecret.error) {
            toast.error(clientSecret.error);
            this.setState({
                clientSecret: undefined,
                cost: 0,
                postageCost: 0,
                smsCost: 0,
                numberOfPostage: 0,
                numberOfSms: 0,
            });
            return;
        }
        this.setState({
            clientSecret: clientSecret?.clientSecret ?? undefined,
            cost: clientSecret?.cost,
            postageCost: clientSecret?.postageCost,
            smsCost: clientSecret?.smsCost,
            numberOfSms: texts,
            numberOfPostage: postCards,
        });
    };
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const guestListActionCreator = container.resolve(GuestListActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const guestListSelector = container.resolve(GuestListSelector);
const inviteActionCreator = container.resolve(InviteActionCreator);
const inviteSelector = container.resolve(InviteSelector);

const mapStateToProps = (state: ApplicationReduxState, props: RouteComponentProps): IStateProps => {
    const locationState = props.location.state as { invite: Invite };

    const event = eventSelector.selectEventById(state, locationState.invite.event ?? "");
    const weddingCode = authenticationSelector.selectWeddingCode(state);
    return {
        event,
        weddingCode,
        guests: guestListSelector.selectGuestsByIds(state, event?.guests ?? []),
        userId: authenticationSelector.selectUserId(state),
        email: authenticationSelector.selectUserEmail(state),
        isSendingInvite: inviteSelector.selectIsSendingInvite(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
    getGuestLists: guestListActionCreator.fetchGuestLists,
    getGuests: guestListActionCreator.fetchGuests,
    sendInvite: inviteActionCreator.sendInvite,
    setIsSendingInvite: inviteActionCreator.setIsSendingInvite,
    sendInviteConfirmation: inviteActionCreator.sendInviteConfirmation,
};

export const PurchaseInvitationServicesScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(PurchaseInvitationServicesScreenComponent));
