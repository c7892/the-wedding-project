import React, { PureComponent, SyntheticEvent } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { InvitationPreview } from "./components";
import "../../styles/screens/invitations.css";
import { container } from "tsyringe";
import { FetchEventsAction, ApplicationReduxState, SaveInviteAction } from "../../store";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { EventSelector, AuthenticationSelector, InviteSelector } from "../../store/selectors";
import { Event, Invite } from "../../models";
import { connect } from "react-redux";
import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Dropzone from "react-dropzone";
import { ColorResult, SketchPicker } from "react-color";
import { InviteActionCreator } from "../../store/actions/InviteActionCreator";
import { extend, isEqual } from "lodash";
import { getBase64ImageDataString, toColorResult } from "../../utilities";
import { toast } from "react-toastify";
import { RouteKey } from "../../routing";
import { NoMultiMediaWysiwig } from "../../components";
import ReactQuill from "react-quill";

interface IOwnState {
    selectedEvent?: Event;
    frontImage?: File | string;
    backImage?: File | string;
    notes: string;
    notesPreview: string;
    bodyColor?: ColorResult;
    titleColor?: ColorResult;
    title2Color?: ColorResult;
    scanColor?: ColorResult;
    inviteToEdit?: Invite;
    title?: string;
}

interface IStateProps {
    events: Event[];
    weddingCode?: string;
    userId?: string;
    inviteBeingSaved?: Invite;
    isSavingInvite: boolean;
}

interface IDispatchProps {
    getEvents: (payload?: Date) => FetchEventsAction;
    saveInvite: (payload: Invite) => SaveInviteAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class EditInvitationScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly inviteTitle = React.createRef<HTMLInputElement>();
    private readonly title = React.createRef<HTMLInputElement>();
    private readonly selectEvent = React.createRef<HTMLSelectElement>();
    private readonly notes = React.createRef<ReactQuill>();

    public constructor(props: Props) {
        super(props);
        this.state = {
            selectedEvent: undefined,
            backImage: undefined,
            frontImage: undefined,
            notes: "",
            notesPreview: "",
            bodyColor: undefined,
            titleColor: undefined,
            title2Color: undefined,
            scanColor: undefined,
            inviteToEdit: undefined,
            title: undefined,
        };
    }

    public componentDidMount() {
        setTimeout(this.props.getEvents, 500);
        const locationState = this.props.location.state as { invite?: Invite } | undefined;
        const invite = locationState?.invite;
        if (!invite) {
            return;
        }
        this.setState({
            backImage: invite.backImage,
            frontImage: invite.frontImage,
            notes: invite.notes ?? "",
            notesPreview: invite.notes ?? "",
            inviteToEdit: invite,
            title: invite.title,
            title2Color: toColorResult(invite.title2Color),
            titleColor: toColorResult(invite.titleColor),
            bodyColor: toColorResult(invite.bodyColor),
            scanColor: toColorResult(invite.scanColor),
            selectedEvent: this.props.events.find((event) => event._id === invite.event),
        });
    }

    public componentDidUpdate(prevProps: Props) {
        if (!isEqual(prevProps.events, this.props.events) && this.state.inviteToEdit) {
            this.setState({
                selectedEvent: this.props.events.find((event) => event._id === this.state.inviteToEdit?.event),
            });
        }
        if (!prevProps.isSavingInvite && this.props.isSavingInvite) {
            toast.warning("Saving invitation, please wait.");
        }
        if (prevProps.isSavingInvite && !this.props.isSavingInvite) {
            if (this.props.inviteBeingSaved) {
                toast.success("Successfully saved invitation");
                this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.Invitations}`);
            } else {
                toast.error("Failed to saved invitation, please try again");
            }
        }
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h2>Edit Invite</h2>
                    </Col>
                </Row>
                <Row
                    style={{
                        marginBottom: 16,
                    }}>
                    <Col
                        style={{
                            borderRight: "1px solid gray",
                        }}>
                        <Form onSubmit={this.saveInvite}>
                            <Row>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Name of the invitation</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="The Engagement Party"
                                            ref={this.inviteTitle}
                                            defaultValue={this.state.inviteToEdit?.inviteTitle}
                                            required
                                            disabled={this.props.isSavingInvite}
                                        />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Title</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Save the date"
                                            ref={this.title}
                                            defaultValue={this.state.inviteToEdit?.title}
                                            value={this.state.title}
                                            onChange={() => this.setState({ title: this.title.current?.value })}
                                            required
                                            disabled={this.props.isSavingInvite}
                                        />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Event</Form.Label>
                                    <Form.Control
                                        as="select"
                                        ref={this.selectEvent}
                                        disabled={this.props.isSavingInvite}
                                        defaultValue={this.state.selectedEvent?._id}
                                        value={this.state.selectedEvent?._id}
                                        onChange={() => {
                                            this.setState({
                                                selectedEvent:
                                                    this.props.events[
                                                        (this.selectEvent.current?.selectedIndex ?? 1) - 1
                                                    ],
                                            });
                                        }}>
                                        <option selected value={undefined}>
                                            {" "}
                                            -- Select an event --{" "}
                                        </option>
                                        {this.props.events.map((event, index) => {
                                            return (
                                                <option key={index} value={event._id}>
                                                    {event.title}
                                                </option>
                                            );
                                        })}
                                    </Form.Control>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Notes</Form.Label>
                                    <NoMultiMediaWysiwig
                                        ref={this.notes}
                                        defaultValue={
                                            (this.state.inviteToEdit?.notes?.length ?? 0) === 0
                                                ? "Leave notes for back here"
                                                : this.state.inviteToEdit?.notes
                                        }
                                        readonly={this.props.isSavingInvite}
                                        shouldUpdateDefaultValue={true}
                                        onTextChange={(change) => {
                                            this.setState({ notesPreview: change });
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 12, marginBottom: 12 }}>
                                <Col>
                                    <Form.Label>Title 1 Color</Form.Label>
                                    <SketchPicker
                                        onChange={(color) => this.setState({ titleColor: color })}
                                        color={this.state.titleColor?.rgb}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>Scan Text Color</Form.Label>
                                    <SketchPicker
                                        onChange={(color) => this.setState({ scanColor: color })}
                                        color={this.state.scanColor?.rgb}
                                    />
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 12, marginBottom: 12 }}>
                                <Col>
                                    <Form.Label>Title 2 Color</Form.Label>
                                    <SketchPicker
                                        onChange={(color) => this.setState({ title2Color: color })}
                                        color={this.state.title2Color?.rgb}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>Back Body Color</Form.Label>
                                    <SketchPicker
                                        onChange={(color) => this.setState({ bodyColor: color })}
                                        color={this.state.bodyColor?.rgb}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col id="imageDrop">
                                    <Dropzone
                                        onDropAccepted={this.onFrontImageDropped}
                                        accept="image/*"
                                        disabled={this.props.isSavingInvite}
                                        multiple={false}>
                                        {({ getRootProps, getInputProps }) => (
                                            <section>
                                                <div id="centeredDiv" {...getRootProps()}>
                                                    <FontAwesomeIcon icon={faUpload} size={"10x"} />
                                                    <input {...getInputProps()} multiple={false} accept="image/*" />
                                                    <p>Drop image for front of invite.</p>
                                                </div>
                                            </section>
                                        )}
                                    </Dropzone>
                                </Col>
                                <Col id="imageDrop">
                                    <Dropzone
                                        onDropAccepted={this.onBackImageDropped}
                                        accept="image/*"
                                        disabled={this.props.isSavingInvite}
                                        multiple={false}>
                                        {({ getRootProps, getInputProps }) => (
                                            <section>
                                                <div id="centeredDiv" {...getRootProps()}>
                                                    <FontAwesomeIcon icon={faUpload} size={"10x"} />
                                                    <input {...getInputProps()} multiple={false} accept="image/*" />
                                                    <p>Drop image for back of invite.</p>
                                                </div>
                                            </section>
                                        )}
                                    </Dropzone>
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 16 }}>
                                <Col>
                                    <Button
                                        type="submit"
                                        onClick={this.saveInvite}
                                        disabled={this.props.isSavingInvite}>
                                        Save
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                    <Col>
                        <InvitationPreview
                            guestId={this.props.userId}
                            event={this.state.selectedEvent}
                            frontImage={this.state.frontImage}
                            backImage={this.state.backImage}
                            notes={this.state.notesPreview}
                            title={this.state.title}
                            titleColor={this.state.titleColor?.hex}
                            bodyColor={this.state.bodyColor?.hex}
                            title2Color={this.state.title2Color?.hex}
                            scanColor={this.state.scanColor?.hex}
                            weddingCode={this.props.weddingCode}
                        />
                    </Col>
                </Row>
            </Container>
        );
    }

    private onFrontImageDropped = (files: File[]) => {
        if (!files || files.length === 0) {
            return;
        }
        this.setState({ frontImage: files[0] });
    };
    private onBackImageDropped = (files: File[]) => {
        if (!files || files.length === 0) {
            return;
        }
        this.setState({ backImage: files[0] });
    };
    private saveInvite = async (e: SyntheticEvent) => {
        e.preventDefault();
        if (this.props.isSavingInvite) {
            return;
        }
        const invite = extend({}, this.state.inviteToEdit ?? {}, {
            notes: this.state.notesPreview,
            frontImage:
                typeof this.state.frontImage === "string"
                    ? this.state.frontImage
                    : await getBase64ImageDataString(this.state.frontImage),
            backImage:
                typeof this.state.backImage === "string"
                    ? this.state.backImage
                    : await getBase64ImageDataString(this.state.backImage),
            title: this.title.current?.value ?? "",
            inviteTitle: this.inviteTitle.current?.value ?? "",
            titleColor: this.state.titleColor?.hex,
            bodyColor: this.state.bodyColor?.hex,
            title2Color: this.state.title2Color?.hex,
            scanColor: this.state.scanColor?.hex,
            event:
                (this.state.selectedEvent
                    ? this.state.selectedEvent._id
                    : this.props.events[(this.selectEvent.current?.selectedIndex ?? 1) - 1]?._id) ?? "",
            dateCreated: this.state.inviteToEdit?.dateCreated ?? Date.now(),
            modifiedDttm: Date.now(),
            ownerWeddingCode: this.props.weddingCode ?? "",
        });
        this.props.saveInvite(invite);
    };
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const inviteActionCreator = container.resolve(InviteActionCreator);
const inviteSelector = container.resolve(InviteSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        events: eventSelector.selectUserEvents(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        userId: authenticationSelector.selectUserId(state),
        inviteBeingSaved: inviteSelector.selectInviteBeingSaved(state),
        isSavingInvite: inviteSelector.selectIsSavingInvite(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
    saveInvite: inviteActionCreator.saveInvite,
};

export const EditInvitationScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(EditInvitationScreenComponent));
