import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { RouteComponentProps, withRouter } from "react-router";
import ListGroup from "react-bootstrap/ListGroup";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ApplicationReduxState, DeleteInviteAction, FetchInvitesAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, InviteSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { RouteKey } from "../../routing";
import { Invite } from "../../models";
import { InviteActionCreator } from "../../store/actions/InviteActionCreator";
import ButtonGroup from "react-bootstrap/ButtonGroup";

interface IStateProps {
    weddingCode?: string;
    invites: Invite[];
}

interface IDispatchProps {
    fetchInvites: () => FetchInvitesAction;
    deleteInvite: (payload: string) => DeleteInviteAction;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class InvitationScreenComponent extends PureComponent<Props> {
    public componentDidMount() {
        setTimeout(this.props.fetchInvites, 500);
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Invitations</h1>
                    </Col>
                    <Col>
                        <Button
                            onClick={() => {
                                this.props.history.push(
                                    `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditInvitation}`,
                                );
                            }}>
                            <FontAwesomeIcon icon={faPlusCircle} size={"lg"} />
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ListGroup>
                            {this.props.invites.map((invite) => {
                                return (
                                    <ListGroup.Item key={invite._id}>
                                        {invite.inviteTitle}
                                        <ButtonGroup style={{ marginLeft: 16 }}>
                                            <Button
                                                variant="primary"
                                                onClick={() =>
                                                    this.props.history.push(
                                                        `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.ViewInvitation}`,
                                                        { invite },
                                                    )
                                                }>
                                                View
                                            </Button>
                                            <Button
                                                variant="warning"
                                                onClick={() =>
                                                    this.props.history.push(
                                                        `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditInvitation}`,
                                                        { invite },
                                                    )
                                                }>
                                                Edit
                                            </Button>
                                            <Button
                                                variant="danger"
                                                onClick={() => {
                                                    this.props.deleteInvite(invite._id ?? "");
                                                }}>
                                                Delete
                                            </Button>
                                        </ButtonGroup>
                                    </ListGroup.Item>
                                );
                            })}
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        );
    }
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const inviteSelector = container.resolve(InviteSelector);
const inviteActionCreator = container.resolve(InviteActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        weddingCode: authenticationSelector.selectWeddingCode(state),
        invites: inviteSelector.selectInvites(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchInvites: inviteActionCreator.fetchInvites,
    deleteInvite: inviteActionCreator.deleteInvite,
};

export const InvitationsScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(InvitationScreenComponent));
