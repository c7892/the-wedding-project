import React, { SyntheticEvent, useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";
import { PaymentIntent, StripeCardElementChangeEvent } from "@stripe/stripe-js";
import "../../styles/screens/purchase.css";
import { toast } from "react-toastify";

interface Props {
    clientSecret: string;
    totalCost: number;
    smsCost: number;
    numberOfSms: number;
    email: string;
    onPaymentSuccessful: (paymentPayload: PaymentIntent) => void;
}

export const PurchaseMessageForm = (props: Props) => {
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState<string | null | undefined>(null);
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [clientSecret, setClientSecret] = useState("");
    const stripe = useStripe();
    const elements = useElements();

    useEffect(() => {
        setClientSecret(props.clientSecret);
        if (error) {
            toast.error(error);
            setError(null);
        }
    }, [props, error]);

    const cardStyle = {
        style: {
            base: {
                color: "#32325d",
                fontFamily: "Arial, sans-serif",
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d",
                },
            },
            invalid: {
                fontFamily: "Arial, sans-serif",
                color: "#fa755a",
                iconColor: "#fa755a",
            },
        },
    };

    const handleChange = async (event: StripeCardElementChangeEvent) => {
        // Listen for changes in the CardElement
        // and display any errors as the customer types their card details
        setDisabled(event.empty);
        setError(event.error ? event.error.message : "");
    };

    const handleSubmit = async (ev: SyntheticEvent) => {
        ev.preventDefault();
        if (!stripe || !elements) {
            return;
        }
        const card = elements.getElement(CardElement);
        if (!card) {
            return;
        }
        setProcessing(true);

        const payload = await stripe.confirmCardPayment(clientSecret, {
            receipt_email: props.email,
            payment_method: {
                card,
            },
        });

        if (payload.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
        } else {
            setError(null);
            setProcessing(false);
            setSucceeded(true);
            props.onPaymentSuccessful(payload.paymentIntent);
        }
    };

    return (
        <Form id="payment-form" onSubmit={handleSubmit}>
            <Row>
                <Col>
                    <h5>Cost to send message</h5>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Number of items</th>
                                <th>Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>SMS Messages</td>
                                <td>{props.numberOfSms}</td>
                                <td>${(props.smsCost / 100).toFixed(2)}</td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td colSpan={3}>Grand total (including tax)</td>
                                <td>${(props.totalCost / 100).toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row>
                <Col id="cardContainer">
                    <CardElement id="card-element" options={cardStyle} onChange={handleChange} />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button
                        variant="success"
                        disabled={processing || disabled || succeeded}
                        id="submitPayment"
                        type="submit">
                        <span id="button-text">
                            {processing ? <div className="spinner" id="spinner"></div> : "Pay now"}
                        </span>
                    </Button>
                    <p className={succeeded ? "result-message" : "result-message hidden"}>Payment succeeded</p>
                </Col>
            </Row>
        </Form>
    );
};
