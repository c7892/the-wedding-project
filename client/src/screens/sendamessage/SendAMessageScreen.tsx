import React, { PureComponent, SyntheticEvent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { AuthenticationSelector, GuestListSelector } from "../../store/selectors";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Guest } from "../../models";
import { container } from "tsyringe";
import {
    FetchGuestListsAction,
    FetchGuestsAction,
    FetchEventsAction,
    EventActionCreator,
    GuestListActionCreator,
    ApplicationReduxState,
} from "../../store";
import { isEmpty, values } from "lodash";
import { Elements } from "@stripe/react-stripe-js";
import { CommunicationService, PaymentService } from "../../services";
import { IConfig } from "../../utilities";
import { loadStripe, PaymentIntent, Stripe } from "@stripe/stripe-js";
import { toast } from "react-toastify";
import { PurchaseMessageForm } from "./PurchaseMessageForm";

interface IOwnState {
    numberOfSms: number;
    clientSecret?: string;
    cost?: number;
    smsCost?: number;
    paymentPayload?: PaymentIntent;
    isSendingMessage: boolean;
}

interface IStateProps {
    guests: Guest[];
    weddingCode?: string;
    userId?: string;
    email?: string;
}

interface IDispatchProps {
    getGuestLists: () => FetchGuestListsAction;
    getGuests: () => FetchGuestsAction;
    getEvents: (payload?: Date) => FetchEventsAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class SendAMessage extends PureComponent<Props, IOwnState> {
    private readonly guestReferences: { [id: string]: HTMLInputElement[] } = {};
    private readonly messageRef = React.createRef<HTMLTextAreaElement>();
    private readonly paymentService = container.resolve<PaymentService>("PaymentService");
    private readonly communicationService = container.resolve<CommunicationService>("CommunicationService");
    private readonly stripePromise: Promise<Stripe | null>;

    public constructor(props: Props) {
        super(props);
        const config = container.resolve<IConfig>("Config");
        this.stripePromise = loadStripe(config.StripeKey);
        this.state = {
            numberOfSms: 0,
            clientSecret: undefined,
            smsCost: undefined,
            cost: undefined,
            paymentPayload: undefined,
            isSendingMessage: false,
        };
    }

    public componentDidMount() {
        setTimeout(() => {
            this.props.getEvents();
            this.props.getGuestLists();
            this.props.getGuests();
        }, 500);
    }

    public render() {
        return (
            <Container fluid style={{ marginBottom: 16 }}>
                <Row>
                    <Col>
                        <h1>Send a message to your guests</h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form onSubmit={this.send}>
                            <Row>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Message</Form.Label>
                                        <Form.Control
                                            as={"textarea"}
                                            ref={this.messageRef}
                                            placeholder="Type Message Here..."
                                            disabled={this.state.isSendingMessage}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Row>
                                        <Col>
                                            <h3>Guests</h3>
                                            <p>
                                                (De)Select each method you want to use to send your message to your
                                                guests.
                                            </p>
                                        </Col>
                                    </Row>
                                    <Row style={{ marginBottom: 8 }}>
                                        <Col>
                                            <Button variant="secondary" onClick={this.selectAllTexts}>
                                                Check All Texts
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button variant="secondary" onClick={this.selectAllEmails}>
                                                Check All Emails
                                            </Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            {this.props.guests.map(this.renderGuestRow)}
                                            <Row>
                                                <Col>
                                                    {isEmpty(this.props.guests) ? null : (
                                                        <Button
                                                            type="submit"
                                                            variant="success"
                                                            disabled={this.state.isSendingMessage}>
                                                            Send
                                                        </Button>
                                                    )}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                    {this.state.clientSecret && !this.state.isSendingMessage ? (
                        <Col sm={4} md={5}>
                            <Elements
                                options={{
                                    appearance: { theme: "stripe" },
                                    clientSecret: this.state.clientSecret,
                                }}
                                stripe={this.stripePromise}>
                                <PurchaseMessageForm
                                    clientSecret={this.state.clientSecret}
                                    totalCost={this.state.cost ?? 0}
                                    email={this.props.email ?? ""}
                                    numberOfSms={this.state.numberOfSms}
                                    onPaymentSuccessful={this.sendSmsMessages}
                                    smsCost={this.state.smsCost ?? 0}
                                />
                            </Elements>
                        </Col>
                    ) : null}
                </Row>
            </Container>
        );
    }

    /* eslint-disable @typescript-eslint/no-non-null-assertion */
    private renderGuestRow = (guest: Guest) => {
        if (isEmpty(this.guestReferences[guest._id!])) {
            this.guestReferences[guest._id!] = [];
        }
        return (
            <Form.Group as={Row} key={guest._id}>
                <Form.Label column sm={4}>
                    {guest.firstName}&nbsp;{guest.surname}&nbsp;
                    {this.getRelationShipToYou(guest) ? `(${this.getRelationShipToYou(guest)})` : ""}
                </Form.Label>
                <Col>
                    <Form.Check
                        inline
                        label="SMS"
                        name="SMS"
                        type={"checkbox"}
                        onChange={() => this.requestNewCost()}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex((r) => r.name === "SMS");
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                    <Form.Check
                        inline
                        disabled={!guest.email ?? isEmpty(guest.email)}
                        label="Email"
                        name="Email"
                        type={"checkbox"}
                        ref={(ref: HTMLInputElement | null) => {
                            if (ref) {
                                const index = this.guestReferences[guest._id!].findIndex((r) => r.name === "Email");
                                if (index >= 0) {
                                    this.guestReferences[guest._id!][index] = ref;
                                } else {
                                    this.guestReferences[guest._id!].push(ref);
                                }
                            }
                        }}
                    />
                </Col>
            </Form.Group>
        );
    };
    /* eslint-enable @typescript-eslint/no-non-null-assertion */
    private getRelationShipToYou = (guest: Guest) => {
        return guest.relation && this.props.userId ? guest.relation[this.props.userId] : "";
    };

    private selectAllTexts = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            this.requestNewCost();
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "SMS" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
        this.requestNewCost();
    };
    private selectAllEmails = () => {
        const allGuestRefs = values(this.guestReferences);
        if (isEmpty(allGuestRefs)) {
            return;
        }
        const textCheckBoxes = allGuestRefs.flat().filter((ref) => ref.name === "Email" && !ref.disabled);
        textCheckBoxes.forEach((ref) => {
            ref.checked = true;
        });
    };

    private send = async (e: SyntheticEvent) => {
        e.preventDefault();
        const message = this.messageRef.current?.value ?? "";
        if (message.length <= 0) {
            toast.error("Please provide a valid message to send out.");
            return;
        }
        this.setState({ isSendingMessage: true });
        toast.warning("Sending email messages first");
        const seekingPaidServices: string[] = [];
        const allGuests: string[] = [];
        for (const guest of this.props.guests) {
            allGuests.push(guest._id ?? "");
            const id = await this.sendGuestMessage(guest, message);
            if (id) {
                seekingPaidServices.push(id);
            }
        }
        this.setState({ isSendingMessage: false });
        if (!isEmpty(seekingPaidServices)) {
            this.requestNewCost();
            toast.success("Sent emails.");
            toast.warning("Please pay before trying to send SMS messages");
        }
    };

    private sendGuestMessage = async (guest: Guest, message: string): Promise<string | undefined> => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const refs = this.guestReferences[guest._id!];
        const checked = refs.filter((ref) => ref.checked);
        if (isEmpty(checked)) {
            return undefined;
        }
        const sendMethods = checked.map((ref) => ref.name);
        let wantsToSendViaPostOrSms = false;
        const index = sendMethods.indexOf("SMS");
        if (index >= 0) {
            wantsToSendViaPostOrSms = true;
            sendMethods.splice(index, 1);
        }
        if (sendMethods.length <= 0 && index < 0) {
            return undefined;
        } else if (sendMethods.length <= 0 && index >= 0) {
            return guest._id;
        }
        this.communicationService.sendMessage({
            message,
            sendMethods,
            guest,
        });
        if (wantsToSendViaPostOrSms) {
            return guest._id;
        }
        return undefined;
    };
    private getNumberOfInvitationsToSend = () => {
        let texts = 0;
        for (const guest of this.props.guests) {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const refs = this.guestReferences[guest._id!];
            const checked = refs.filter((ref) => ref.checked);
            if (isEmpty(checked)) {
                continue;
            }
            checked.forEach((ref) => {
                if (ref.name.toLowerCase() === "sms") {
                    texts++;
                }
            });
        }
        return texts;
    };
    private requestNewCost = async () => {
        const texts = this.getNumberOfInvitationsToSend();
        const clientSecret = await this.paymentService.requestCostAsync(0, texts);
        if (clientSecret && clientSecret.error) {
            toast.error(clientSecret.error);
            this.setState({
                clientSecret: undefined,
                cost: 0,
                smsCost: 0,
                numberOfSms: 0,
            });
            return;
        }
        this.setState({
            clientSecret: clientSecret?.clientSecret ?? undefined,
            cost: clientSecret?.cost,
            smsCost: clientSecret?.smsCost,
            numberOfSms: texts,
        });
    };
    private sendSmsMessages = async (paymentPayload: PaymentIntent) => {
        const message = this.messageRef.current?.value ?? "";
        if (message.length <= 0) {
            toast.error("Please provide a valid message to send out.");
            return;
        }
        this.setState({ isSendingMessage: true });
        toast.warning("Sending messages");
        const allGuests: string[] = [];
        for (const guest of this.props.guests) {
            allGuests.push(guest._id ?? "");
            const id = await this.sendGuestMessage(guest, message);
            if (id) {
                this.communicationService
                    .sendMessage({
                        message,
                        sendMethods: ["sms"],
                        guest,
                        paymentPayload,
                    })
                    .catch(() => {
                        toast.error(
                            `Failed to send message to ${guest.firstName} ${guest.surname}, please try again or contact support`,
                        );
                    });
            }
        }
        this.setState({ isSendingMessage: false });
        toast.success("Sent SMS messages.");
    };
}

const eventActionCreator = container.resolve(EventActionCreator);
const guestListActionCreator = container.resolve(GuestListActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const guestListSelector = container.resolve(GuestListSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    const weddingCode = authenticationSelector.selectWeddingCode(state);
    return {
        weddingCode,
        guests: guestListSelector.selectGuests(state),
        userId: authenticationSelector.selectUserId(state),
        email: authenticationSelector.selectUserEmail(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
    getGuestLists: guestListActionCreator.fetchGuestLists,
    getGuests: guestListActionCreator.fetchGuests,
};

export const SendAMessageScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(SendAMessage));
