import React, { PureComponent, SyntheticEvent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { RouteComponentProps, withRouter } from "react-router";
import { ApplicationReduxState, NotesActionCreator, SaveNotesAction, SetNotesBeingSavedAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, NotesSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { Notes } from "../../models";
import { Wysiwig } from "../../components/Wysiwig";
import ReactQuill from "react-quill";
import { QuillDeltaToHtmlConverter } from "quill-delta-to-html";
import { extend } from "lodash";
import { toast } from "react-toastify";

interface IStateProps {
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
    notesBeingSaved?: Notes;
    isSavingNotes?: boolean;
}

interface IDispatchProps {
    saveNotes: (payload: Notes) => SaveNotesAction;
    setNotesBeingSaved: (payload?: Notes) => SetNotesBeingSavedAction;
}

interface IOwnState {
    notesToEdit: Notes;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class UpdateNotesScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly titleRef = React.createRef<HTMLInputElement>();
    private readonly isPublishedRef = React.createRef<HTMLInputElement>();
    private readonly description = React.createRef<ReactQuill>();

    constructor(props: Props) {
        super(props);
        this.state = {
            notesToEdit: (props.location.state as { notes: Notes }).notes,
        };
    }

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSavingNotes && this.props.isSavingNotes) {
            toast.warning("Saving notes, please wait.");
            return;
        }
        if (prevProps.isSavingNotes && !this.props.isSavingNotes) {
            if (this.props.notesBeingSaved) {
                toast.success("Successfully saved notes");
                this.setState({ notesToEdit: this.props.notesBeingSaved });
            } else {
                toast.error("Failed to saved notes, please try again");
            }
        }
    }
    public componentWillUnmount() {
        this.props.setNotesBeingSaved(undefined);
    }

    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Edit Notes</h1>
                    </Col>
                    <hr />
                </Row>
                <Row style={{ margin: 8, marginBottom: 40 }}>
                    <Form onSubmit={this.submit} id="updateNotesForm">
                        <Form.Group as={Row}>
                            <Form.Label column sm={1}>
                                Title
                            </Form.Label>
                            <Col>
                                <Form.Control
                                    type="text"
                                    placeholder="Title to your notes (i.e. Local Attractions)"
                                    ref={this.titleRef}
                                    required
                                    defaultValue={this.state.notesToEdit.title}
                                    disabled={this.props.isSavingNotes}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Form.Check
                                inline
                                label="Publish your notes (i.e. show to your guests)"
                                ref={this.isPublishedRef}
                                defaultChecked={this.state.notesToEdit.isPublished}
                                disabled={this.props.isSavingNotes}
                            />
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Col>
                                <Form.Label>Details</Form.Label>
                                <Wysiwig
                                    ref={this.description}
                                    defaultValue={this.state.notesToEdit.details}
                                    readonly={this.props.isSavingNotes}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Col>
                                <Button type="submit" variant="success" disabled={this.props.isSavingNotes}>
                                    Save
                                </Button>
                            </Col>
                        </Form.Group>
                    </Form>
                </Row>
            </Container>
        );
    }

    private submit = (e: SyntheticEvent) => {
        e.preventDefault();
        if (this.props.isSavingNotes) {
            console.log("saving already");
            return;
        }
        const cfg = {
            encodeHtml: true,
            inlineStyles: true,
        };
        const converter = new QuillDeltaToHtmlConverter(
            this.description.current?.getEditor()?.getContents()?.ops ?? [],
            cfg,
        );
        const details = converter.convert();
        const title = this.titleRef.current?.value ?? this.state.notesToEdit.title;
        const isPublished = this.isPublishedRef.current
            ? this.isPublishedRef.current.checked
            : this.state.notesToEdit.isPublished;
        this.props.saveNotes(extend({}, this.state.notesToEdit, { details, title, isPublished }));
    };
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const notesSelector = container.resolve(NotesSelector);
const notesActionCreator = container.resolve(NotesActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        notesBeingSaved: notesSelector.selectNotesBeingSaved(state),
        isSavingNotes: notesSelector.selectIsSavingNotes(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    saveNotes: notesActionCreator.saveNotes,
    setNotesBeingSaved: notesActionCreator.setNotesBeingSaved,
};

export const UpdateNotesScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(UpdateNotesScreenComponent));
