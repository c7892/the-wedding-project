import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Badge from "react-bootstrap/Badge";
import Spinner from "react-bootstrap/Spinner";
import { RouteComponentProps, withRouter } from "react-router";
import { ApplicationReduxState, FetchNotesAction, NotesActionCreator } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, NotesSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { Notes } from "../../models";
import { isEqual } from "lodash";
import parse from "html-react-parser";

interface IStateProps {
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
    fetchedNotes?: Notes;
}

interface IDispatchProps {
    fetchNotes: (payload: string) => FetchNotesAction;
}

interface IOwnState {
    notes?: Notes;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class ViewNotesScreenComponent extends PureComponent<Props, IOwnState> {
    constructor(props: Props) {
        super(props);
        this.state = {
            notes: (props.location.state as { notes?: Notes })?.notes,
        };
    }

    public componentDidMount() {
        if (!this.state.notes) {
            const id = (this.props.match.params as { id: string }).id;
            // setTimeout(() => {
            this.props.fetchNotes(id);
            // }, 500);
        }
    }

    public componentDidUpdate(prevProps: Props) {
        if (!isEqual(prevProps.fetchedNotes, this.props.fetchedNotes) && this.props.fetchedNotes) {
            this.setState({ notes: this.props.fetchedNotes });
        }
    }

    public render() {
        return !this.state.notes ? (
            <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        ) : (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>
                            {this.state.notes.title}{" "}
                            {this.state.notes.isPublished ? (
                                <Badge className="bg-info" variant="info">
                                    Published
                                </Badge>
                            ) : (
                                <Badge className="bg-secondary" variant="secondary">
                                    Draft
                                </Badge>
                            )}
                        </h1>
                    </Col>
                    <hr />
                </Row>
                <Row>
                    <Col id="notesView">{parse(this.state.notes.details)}</Col>
                </Row>
            </Container>
        );
    }
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const notesSelector = container.resolve(NotesSelector);
const notesActionCreator = container.resolve(NotesActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        fetchedNotes: notesSelector.selectNotes(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchNotes: notesActionCreator.fetchNotes,
};

export const ViewNotesScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(ViewNotesScreenComponent));
