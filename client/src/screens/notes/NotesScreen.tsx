import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import { RouteComponentProps, withRouter } from "react-router";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ApplicationReduxState, DeleteNotesAction, FetchAllNotesAction, NotesActionCreator } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, NotesSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { Notes } from "../../models";
import { RouteKey } from "../../routing";

interface IStateProps {
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
    notes: Notes[];
}

interface IDispatchProps {
    fetchNotes: () => FetchAllNotesAction;
    deleteNotes: (payload: string) => DeleteNotesAction;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class NotesScreenComponent extends PureComponent<Props> {
    public componentDidMount() {
        setTimeout(() => {
            this.props.fetchNotes();
        }, 500);
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>
                            Notes&nbsp;&nbsp;&nbsp;&nbsp;
                            {!this.props.isGuest ? (
                                <Button
                                    onClick={() => {
                                        this.props.history.push(
                                            `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditNotes}`,
                                            {
                                                notes: {
                                                    title: "",
                                                    details: "",
                                                    isPublished: false,
                                                    ownerWeddingCode: this.props.weddingCode ?? "",
                                                    dateCreated: Date.now(),
                                                    modifiedDttm: Date.now(),
                                                },
                                            },
                                        );
                                    }}>
                                    <FontAwesomeIcon icon={faPlusCircle} size={"lg"} />
                                </Button>
                            ) : null}
                        </h1>
                    </Col>
                    <hr />
                </Row>
                <Row style={{ margin: 8, marginBottom: 40 }}>
                    <ListGroup>
                        {this.props.notes.map((notes) => {
                            return (
                                <ListGroupItem key={notes._id}>
                                    <Row>
                                        <Col>
                                            {notes.title}&nbsp;&nbsp;&nbsp;
                                            {notes.isPublished ? (
                                                <Badge className="bg-info" variant="info">
                                                    Published
                                                </Badge>
                                            ) : (
                                                <Badge className="bg-secondary" variant="secondary">
                                                    Draft
                                                </Badge>
                                            )}
                                        </Col>
                                        <Col>
                                            <ButtonGroup>
                                                <Button
                                                    variant="primary"
                                                    onClick={() => {
                                                        this.props.history.push(
                                                            `${
                                                                this.props.isGuest
                                                                    ? RouteKey.GuestBase
                                                                    : RouteKey.SpouseBase
                                                            }${this.props.weddingCode}${
                                                                this.props.isGuest ? `/${this.props.userId}` : ""
                                                            }${RouteKey.NotesBase}${notes._id}/view`,
                                                            { notes },
                                                        );
                                                    }}>
                                                    View
                                                </Button>
                                                {this.props.isGuest ? null : (
                                                    <>
                                                        <Button
                                                            variant="warning"
                                                            onClick={() => {
                                                                this.props.history.push(
                                                                    `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditNotes}`,
                                                                    {
                                                                        notes,
                                                                    },
                                                                );
                                                            }}>
                                                            Edit
                                                        </Button>
                                                        <Button
                                                            variant="danger"
                                                            onClick={() => this.props.deleteNotes(notes._id ?? "")}>
                                                            Delete
                                                        </Button>
                                                    </>
                                                )}
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            );
                        })}
                    </ListGroup>
                </Row>
            </Container>
        );
    }
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const notesSelector = container.resolve(NotesSelector);
const notesActionCreator = container.resolve(NotesActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        notes: notesSelector.selectAllNotes(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchNotes: notesActionCreator.fetchAllNotes,
    deleteNotes: notesActionCreator.deleteNotes,
};

export const NotesScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(NotesScreenComponent));
