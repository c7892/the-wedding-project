import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { RouteComponentProps, withRouter } from "react-router";
import { container } from "tsyringe";
import { IConfig } from "../../utilities";
import Button from "react-bootstrap/Button";
import { RouteKey } from "../../routing";
import "../../styles/screens/landingPage.css";

type Props = RouteComponentProps;

export class LandingPageComponent extends PureComponent<Props> {
    private readonly config = container.resolve<IConfig>("Config");
    public render() {
        return (
            <Container fluid id="landingPageContainer">
                <Container>
                    <Row>
                        <h1>{this.config.AppName}</h1>
                    </Row>
                    <Row
                        style={{
                            backgroundColor: "rgba(211,211,211,0.3)",
                            color: "black",
                            marginBottom: 10,
                            borderRadius: 25,
                        }}>
                        <Col>
                            <p>
                                The Wedding Project is passion project developed by TreJon House of&nbsp;
                                <a href="https://codinghouse.dev" target="_blank" rel="noreferrer">
                                    Coding House
                                </a>
                                &nbsp; The purpose of this project was and is to provide an alternative solution to
                                products such as{" "}
                                <a href="https://www.theknot.com" target="_blank" rel="noreferrer">
                                    The Knot
                                </a>
                                &nbsp; This project does not seek to replace any mainstream solution or provide the same
                                depth and breadth of functionality. My goal was to create a web app my spouse and I
                                could use that had only what we needed and wanted, very low level of overhead and we
                                share among our guests, also it was a way I could deeply contribute to our wedding. I am
                                making this project free* and open-source so it can be used freely by the community who
                                is looking for a lightweight solution that won&apos;t break the bank. As time goes on I
                                will try to maintain the project as best I can, so to the fellow OSS community, please
                                reach out if you&apos;d like to contribute.
                            </p>
                            <small>
                                * - While this application is free to use, some features (i.e. sending mail invites)
                                come at a cost to cover the expense of this service
                            </small>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button variant={"primary"} onClick={() => this.props.history.push(RouteKey.Login)}>
                                Enter
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ span: 4, offset: 8 }}>
                            <em className={"float-right"}>
                                Photo by{" "}
                                <a href="https://unsplash.com/@jeremywongweddings?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">
                                    Jeremy Wong Weddings
                                </a>{" "}
                                on{" "}
                                <a href="https://unsplash.com/s/photos/wedding?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">
                                    Unsplash
                                </a>
                            </em>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}

export const LandingPage = withRouter(LandingPageComponent);
