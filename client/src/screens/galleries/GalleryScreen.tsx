import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { RouteComponentProps, withRouter } from "react-router";
import {
    AddGalleryImageAction,
    ApplicationReduxState,
    DownloadGalleryAction,
    EventActionCreator,
    FetchEventsAction,
    FetchGalleryAction,
    GalleryActionCreator,
    RemoveGalleryImageAction,
} from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector, GallerySelector } from "../../store/selectors";
import { connect } from "react-redux";
import { Event, Gallery } from "../../models";
import { toast } from "react-toastify";
import { match } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { UpdateGalleryModal } from "./components/UpdateGalleryModal";
import { PhotoGallery } from "./components/PhotoGallery";
import Dropzone, { DropzoneRef } from "react-dropzone";
import { isEmpty } from "lodash";
import parse from "html-react-parser";

interface IStateProps {
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
    galleryActionMessage?: string;
    gallery?: Gallery;
    event?: Event;
    events: Event[];
    galleryDownloadLink?: string;
}

interface IDispatchProps {
    fetchEvents: (payload?: Date) => FetchEventsAction;
    fetchGallery: (payload: string) => FetchGalleryAction;
    addImageToGallery: (payload: { galleryId?: string; eventId?: string; image: File }[]) => AddGalleryImageAction;
    removeImageFromGallery: (payload: { galleryId: string; src: string }) => RemoveGalleryImageAction;
    downloadGallery: (payload: string) => DownloadGalleryAction;
}
interface IOwnState {
    showCreateGallery: boolean;
    canAdd: boolean;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps & RouteComponentProps;

export class GalleryScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly uploader = React.createRef<DropzoneRef>();
    public constructor(props: Props) {
        super(props);
        this.state = {
            showCreateGallery: false,
            canAdd: true,
        };
    }
    public componentDidMount() {
        if (!this.props.gallery) {
            const { galleryId } = this.props.match.params as { galleryId: string };
            this.props.fetchGallery(galleryId);
        }
        setTimeout(() => {
            this.props.fetchEvents(undefined);
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (prevProps.galleryActionMessage !== this.props.galleryActionMessage && this.props.galleryActionMessage) {
            toast(this.props.galleryActionMessage ?? "");
        }
        if (prevProps.galleryDownloadLink !== this.props.galleryDownloadLink && this.props.galleryDownloadLink) {
            window.open(this.props.galleryDownloadLink, "_blank");
        }
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>
                            {this.props.gallery?.title ?? "Loading..."}&nbsp;&nbsp;&nbsp;&nbsp;
                            <ButtonGroup style={{ marginLeft: 16 }}>
                                <Button
                                    variant="success"
                                    disabled={isEmpty(this.props.gallery?.images)}
                                    onClick={() => {
                                        this.props.downloadGallery(this.props.gallery?._id ?? "");
                                    }}>
                                    Download
                                </Button>
                                {this.props.gallery && !this.props.isGuest ? (
                                    <>
                                        <Button
                                            variant="warning"
                                            onClick={() => this.setState({ showCreateGallery: true })}>
                                            Edit&nbsp;
                                            <FontAwesomeIcon icon={faPencilAlt} />
                                        </Button>
                                        <UpdateGalleryModal
                                            galleryId={this.props.gallery?._id ?? ""}
                                            events={this.props.events}
                                            show={this.state.showCreateGallery}
                                            title={this.props.gallery?.title ?? ""}
                                            details={this.props.gallery?.details ?? ""}
                                            event={this.props.gallery?.event}
                                            onClose={() => {
                                                this.setState({ showCreateGallery: false });
                                            }}
                                        />
                                    </>
                                ) : null}
                            </ButtonGroup>
                        </h1>
                    </Col>
                    <Col sm={12} id="galleryDescription">
                        {parse(this.props.gallery?.details ?? "")}
                    </Col>
                </Row>
                <Row>
                    <hr />
                    <p>
                        <em>Drag and drop photos below to add to gallery or&nbsp;&nbsp;&nbsp;&nbsp;</em>
                        <Button variant="primary" onClick={() => this.uploader.current?.open()}>
                            Click to upload
                        </Button>
                    </p>
                    <Dropzone
                        ref={this.uploader}
                        onDropAccepted={this.onImageDropped}
                        accept="image/*"
                        multiple
                        noClick
                        noKeyboard>
                        {({ getRootProps, getInputProps }) => (
                            <Col id="photoGallCol" {...getRootProps()}>
                                <PhotoGallery
                                    photos={
                                        this.props.gallery?.images?.map((image) => ({
                                            src: image.src,
                                            width: 0.5,
                                            height: 0.5,
                                            createdBy: image.uploadedBy,
                                        })) ?? []
                                    }
                                    onClosed={() => this.setState({ canAdd: true })}
                                    onOpened={() => this.setState({ canAdd: false })}
                                    onDeleteImage={(src) => {
                                        this.props.removeImageFromGallery({
                                            galleryId: this.props.gallery?._id ?? "",
                                            src,
                                        });
                                    }}
                                    isGuest={this.props.isGuest}
                                    userId={this.props.userId}
                                />
                                <input {...getInputProps()} />
                            </Col>
                        )}
                    </Dropzone>
                </Row>
            </Container>
        );
    }
    private onImageDropped = async (files: File[]) => {
        if (!files || files.length === 0 || !this.state.canAdd) {
            return;
        }
        const imagesToAdd = files.map((file) => ({
            image: file,
            galleryId: this.props.gallery?._id,
            eventId: this.props.gallery?.event,
        }));
        this.props.addImageToGallery(imagesToAdd);
    };
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const gallerySelector = container.resolve(GallerySelector);
const eventSelector = container.resolve(EventSelector);
const galleryActionCreator = container.resolve(GalleryActionCreator);
const eventActionCreator = container.resolve(EventActionCreator);

const mapStateToProps = (state: ApplicationReduxState, props: RouteComponentProps): IStateProps => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const { galleryId } = (props as any as { computedMatch: match }).computedMatch.params as { galleryId: string };
    const gallery = gallerySelector.selectGallery(state, galleryId);
    const events = eventSelector.selectUserEvents(state);
    const event = events.find((eve) => eve._id === gallery?._id);
    return {
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        gallery,
        event,
        galleryActionMessage: gallerySelector.selectGalleryActionMessage(state),
        galleryDownloadLink: gallerySelector.selectGalleryDownloadLink(state),
        events,
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchGallery: galleryActionCreator.fetchGallery,
    addImageToGallery: galleryActionCreator.addImageToGallery,
    fetchEvents: eventActionCreator.fetchEvents,
    removeImageFromGallery: galleryActionCreator.removeImageFromGallery,
    downloadGallery: galleryActionCreator.downloadGallery,
};

export const GalleryScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(GalleryScreenComponent));
