import React, { SyntheticEvent, useEffect, useRef, useState } from "react";
import { Event, Gallery } from "../../../models";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { container } from "tsyringe";
import { useDispatch } from "react-redux";
import { GalleryActionCreator } from "../../../store";
import { map } from "lodash";
import { convertWysiwigTextToString, NoMultiMediaWysiwig } from "../../../components";
import ReactQuill from "react-quill";

interface IOwnProps {
    events: Event[];
    show: boolean;
    weddingCode: string;
    owners: string;
    onClose?: () => void;
}

export const CreateGalleryModal = ({ events, show, onClose, weddingCode, owners }: IOwnProps) => {
    const [shouldShow, setShouldShow] = useState(show);
    useEffect(() => {
        if (show !== shouldShow) {
            setShouldShow(show);
        }
    }, [shouldShow, show]);
    const title = useRef<HTMLInputElement>(null);
    const eventSelector = useRef<HTMLSelectElement>(null);
    const details = useRef<ReactQuill>(null);
    const dispatch = useDispatch();
    const galleryActionCreator = container.resolve(GalleryActionCreator);
    const submit = (e: SyntheticEvent) => {
        e.preventDefault();
        const index = (eventSelector.current?.selectedIndex ?? -1) - 1;
        let event: string | undefined = undefined;
        if (index >= 0) {
            event = events[index]._id;
        }
        const gallery: Gallery = {
            ownerWeddingCode: weddingCode,
            images: [],
            details: convertWysiwigTextToString(details.current?.getEditor()?.getContents()?.ops ?? []),
            title: title.current?.value ?? "",
            dateCreated: Date.now(),
            modifiedDttm: Date.now(),
            owners,
            event,
        };
        dispatch(galleryActionCreator.createGallery(gallery));
        setShouldShow(false);
        if (onClose) {
            onClose();
        }
    };
    return (
        <Modal
            show={shouldShow}
            onHide={() => {
                setShouldShow(false);
                if (onClose) {
                    onClose();
                }
            }}
            backdrop="static"
            keyboard={false}>
            <Modal.Header>
                <Modal.Title>Create Gallery</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={submit}>
                    <Form.Group>
                        <Form.Label>Gallery Title</Form.Label>
                        <Form.Control type="text" placeholder="title" ref={title} />
                        <Form.Label>Event (optional)</Form.Label>
                        <Form.Control as="select" ref={eventSelector}>
                            <option disabled selected value={undefined}>
                                {" "}
                                -- Optional --{" "}
                            </option>
                            {map(events, (event, index) => {
                                return (
                                    <option key={index} value={event._id}>
                                        {event.title}
                                    </option>
                                );
                            })}
                        </Form.Control>
                        <Form.Label>Details</Form.Label>
                        <NoMultiMediaWysiwig ref={details} defaultValue={""} readonly={false} />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={() => {
                        setShouldShow(false);
                        if (onClose) {
                            onClose();
                        }
                    }}>
                    Close
                </Button>
                <Button variant="primary" type="submit" onClick={submit}>
                    Create
                </Button>
            </Modal.Footer>
        </Modal>
    );
};
