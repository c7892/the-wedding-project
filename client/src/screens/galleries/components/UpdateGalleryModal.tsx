import React, { SyntheticEvent, useEffect, useRef, useState } from "react";
import { Event, Gallery } from "../../../models";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { container } from "tsyringe";
import { useDispatch } from "react-redux";
import { GalleryActionCreator } from "../../../store";
import { map } from "lodash";
import ReactQuill from "react-quill";
import { convertWysiwigTextToString, NoMultiMediaWysiwig } from "../../../components";

interface IOwnProps {
    events: Event[];
    show: boolean;
    title: string;
    details: string;
    event?: string;
    galleryId: string;
    onClose?: () => void;
}

export const UpdateGalleryModal = ({ events, show, galleryId, onClose, ...props }: IOwnProps) => {
    const [shouldShow, setShouldShow] = useState(show);
    useEffect(() => {
        if (show !== shouldShow) {
            setShouldShow(show);
        }
    }, [shouldShow, show]);
    const title = useRef<HTMLInputElement>(null);
    const eventSelector = useRef<HTMLSelectElement>(null);
    const details = useRef<ReactQuill>(null);
    const dispatch = useDispatch();
    const galleryActionCreator = container.resolve(GalleryActionCreator);
    const submit = (e: SyntheticEvent) => {
        e.preventDefault();
        const index = eventSelector.current?.selectedIndex ?? -1;
        let event: string | undefined = undefined;
        if (index > 0) {
            event = events[index]._id;
        }
        const gallery: Gallery = {
            _id: galleryId,
            ownerWeddingCode: "",
            images: [],
            details: convertWysiwigTextToString(details.current?.getEditor()?.getContents()?.ops ?? []),
            title: title.current?.value ?? "",
            dateCreated: Date.now(),
            modifiedDttm: Date.now(),
            owners: "",
            event,
        };
        dispatch(galleryActionCreator.updateGallery(gallery));
        setShouldShow(false);
        if (onClose) {
            onClose();
        }
    };
    return (
        <Modal
            show={shouldShow}
            onHide={() => {
                setShouldShow(false);
                if (onClose) {
                    onClose();
                }
            }}
            backdrop="static"
            keyboard={false}>
            <Modal.Header>
                <Modal.Title>Create Gallery</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={submit}>
                    <Form.Group>
                        <Form.Label>Gallery Title</Form.Label>
                        <Form.Control type="text" placeholder="title" defaultValue={props.title} ref={title} />
                        <Form.Label>Event (optional)</Form.Label>
                        <Form.Control as="select" ref={eventSelector} defaultValue={props.event}>
                            <option disabled selected value={undefined}>
                                {" "}
                                -- Optional --{" "}
                            </option>
                            {map(events, (event, index) => {
                                return (
                                    <option key={index} value={event._id}>
                                        {event.title}
                                    </option>
                                );
                            })}
                        </Form.Control>
                        <Form.Label>Details</Form.Label>
                        <NoMultiMediaWysiwig ref={details} defaultValue={props.details} readonly={false} />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={() => {
                        setShouldShow(false);
                        if (onClose) {
                            onClose();
                        }
                    }}>
                    Close
                </Button>
                <Button variant="primary" type="submit" onClick={submit}>
                    {galleryId ? " Update" : "Create"}
                </Button>
            </Modal.Footer>
        </Modal>
    );
};
