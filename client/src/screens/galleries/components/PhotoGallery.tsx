import React, { useState, useCallback } from "react";
import Gallery from "react-photo-gallery";
import Button from "react-bootstrap/Button";
import Carousel, { CommonProps, Modal, ModalGateway } from "react-images";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload, faTrash } from "@fortawesome/free-solid-svg-icons";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { download } from "../../../utilities";

interface Props {
    photos: {
        src: string;
        srcSet?: string | string[] | undefined;
        sizes?: string | string[] | undefined;
        width: number;
        height: number;
        alt?: string | undefined;
        key?: string | undefined;
        title?: string;
        createdBy: string;
    }[];
    onOpened?: () => void;
    onClosed?: () => void;
    onDeleteImage: (src: string) => void;
    isGuest: boolean;
    userId?: string;
}

export const PhotoGallery = ({ photos, onClosed, onOpened, onDeleteImage, isGuest, userId }: Props) => {
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);

    const openLightbox = useCallback(
        (event, { index }) => {
            setCurrentImage(index);
            setViewerIsOpen(true);
            if (onOpened) {
                onOpened();
            }
        },
        [onOpened],
    );

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
        if (onClosed) {
            onClosed();
        }
    };
    const DeleteButton = (props: CommonProps) => {
        let index = props.currentIndex ?? 0;
        const photo = photos[index];
        if (isGuest && photo.createdBy !== userId) {
            return null;
        }
        return (
            <Button
                variant="danger"
                onClick={() => {
                    if (!photo.src) {
                        return;
                    }
                    onDeleteImage(photo.src);
                    if (index >= photos.length - 1) {
                        index = 0;
                    } else {
                        index++;
                    }
                    setCurrentImage(index);
                }}>
                <FontAwesomeIcon icon={faTrash} />
            </Button>
        );
    };
    const DownloadBtn = (props: CommonProps) => {
        const index = props.currentIndex ?? 0;
        const photo = photos[index];
        return (
            <Button
                variant="success"
                onClick={() => {
                    if (!photo.src) {
                        return;
                    }
                    download(photo.src, "jpg", photo.createdBy);
                }}>
                <FontAwesomeIcon icon={faDownload} />
            </Button>
        );
    };
    const FooterCaption = (props: CommonProps) => {
        return (
            <ButtonGroup>
                <DownloadBtn {...props} />
                <DeleteButton {...props} />
            </ButtonGroup>
        );
    };
    return (
        <div>
            <Gallery photos={photos} onClick={openLightbox} />
            <ModalGateway>
                {viewerIsOpen ? (
                    <Modal onClose={closeLightbox} closeOnBackdropClick closeOnEsc allowFullscreen>
                        <Carousel
                            components={{
                                FooterCaption,
                            }}
                            currentIndex={currentImage}
                            views={photos.map((x) => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: x.title,
                                source: x.src,
                            }))}
                        />
                    </Modal>
                ) : null}
            </ModalGateway>
        </div>
    );
};
