import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { RouteComponentProps, withRouter } from "react-router";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";
import { faImage, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    ApplicationReduxState,
    DeleteGalleryAction,
    DownloadGalleryAction,
    EventActionCreator,
    FetchEventsAction,
    FetchGalleriesAction,
    GalleryActionCreator,
} from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector, GallerySelector } from "../../store/selectors";
import { connect } from "react-redux";
import { RouteKey } from "../../routing";
import { Event, Gallery } from "../../models";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { CreateGalleryModal } from "./components/CreateGalleryModal";
import { toast } from "react-toastify";
import { first, isEmpty } from "lodash";

interface IStateProps {
    isGuest: boolean;
    weddingCode?: string;
    userId?: string;
    galleryActionMessage?: string;
    galleryDownloadLink?: string;
    galleries: Gallery[];
    events: Event[];
}

interface IOwnState {
    showCreateGallery: boolean;
}

interface IDispatchProps {
    fetchEvents: (payload?: Date) => FetchEventsAction;
    fetchGalleries: () => FetchGalleriesAction;
    deleteGallery: (payload: string) => DeleteGalleryAction;
    downloadGallery: (payload: string) => DownloadGalleryAction;
}
type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class GalleriesScreenComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            showCreateGallery: false,
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            this.props.fetchGalleries();
            this.props.fetchEvents(undefined);
        }, 500);
    }
    public componentDidUpdate(prevProps: Props) {
        if (prevProps.galleryActionMessage !== this.props.galleryActionMessage && this.props.galleryActionMessage) {
            toast(this.props.galleryActionMessage ?? "");
        }
        if (prevProps.galleryDownloadLink !== this.props.galleryDownloadLink && this.props.galleryDownloadLink) {
            window.open(this.props.galleryDownloadLink, "_blank");
        }
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>
                            Galleries&nbsp;&nbsp;&nbsp;&nbsp;
                            {!this.props.isGuest ? (
                                <Button
                                    onClick={() => {
                                        this.setState({ showCreateGallery: true });
                                    }}>
                                    <FontAwesomeIcon icon={faPlusCircle} size={"lg"} />
                                </Button>
                            ) : null}
                        </h1>
                    </Col>
                    <CreateGalleryModal
                        events={this.props.events}
                        show={this.state.showCreateGallery}
                        weddingCode={this.props.weddingCode ?? ""}
                        onClose={() => this.setState({ showCreateGallery: false })}
                        owners=""
                    />
                    <hr />
                </Row>
                <Row style={{ margin: 8, marginBottom: 40 }}>
                    <CardColumns id="galleryCardContainer">
                        {this.props.galleries.map((gallery) => {
                            const thumbnail = first(gallery.images);
                            return (
                                <Card
                                    key={gallery._id}
                                    style={{
                                        margin: 5,
                                        maxWidth: this.props.galleries.length % 3 === 0 ? "32%" : "48%",
                                    }}>
                                    {thumbnail ? (
                                        <Card.Img variant="top" src={thumbnail.src} />
                                    ) : (
                                        <FontAwesomeIcon
                                            className="card-img-top"
                                            icon={faImage}
                                            size={"10x"}
                                            style={{
                                                alignSelf: "center",
                                            }}
                                        />
                                    )}
                                    <Card.Body>
                                        <Container fluid>
                                            <Row>
                                                <Col>
                                                    <Card.Title>{gallery.title}</Card.Title>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <ButtonGroup>
                                                        <Button
                                                            variant="success"
                                                            disabled={isEmpty(gallery.images)}
                                                            onClick={() => {
                                                                this.props.downloadGallery(gallery._id ?? "");
                                                            }}>
                                                            Download
                                                        </Button>
                                                        <Button
                                                            variant="secondary"
                                                            onClick={() =>
                                                                this.props.history.push(
                                                                    `${
                                                                        this.props.isGuest
                                                                            ? RouteKey.GuestBase
                                                                            : RouteKey.SpouseBase
                                                                    }${this.props.weddingCode}${
                                                                        this.props.isGuest
                                                                            ? `/${this.props.userId}`
                                                                            : ""
                                                                    }${RouteKey.Gallery}/${gallery._id}`,
                                                                    { gallery: gallery },
                                                                )
                                                            }>
                                                            View
                                                        </Button>
                                                        {!this.props.isGuest ? (
                                                            <Button
                                                                variant="danger"
                                                                onClick={() => {
                                                                    this.props.deleteGallery(gallery._id ?? "");
                                                                }}>
                                                                Delete
                                                            </Button>
                                                        ) : null}
                                                    </ButtonGroup>
                                                </Col>
                                            </Row>
                                        </Container>
                                    </Card.Body>
                                </Card>
                            );
                        })}
                    </CardColumns>
                </Row>
            </Container>
        );
    }
}
const authenticationSelector = container.resolve(AuthenticationSelector);
const gallerySelector = container.resolve(GallerySelector);
const eventSelector = container.resolve(EventSelector);
const galleryActionCreator = container.resolve(GalleryActionCreator);
const eventActionCreator = container.resolve(EventActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        galleries: gallerySelector.selectGalleries(state),
        events: eventSelector.selectUserEvents(state),
        galleryActionMessage: gallerySelector.selectGalleryActionMessage(state),
        galleryDownloadLink: gallerySelector.selectGalleryDownloadLink(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchGalleries: galleryActionCreator.fetchGalleries,
    deleteGallery: galleryActionCreator.deleteGallery,
    downloadGallery: galleryActionCreator.downloadGallery,
    fetchEvents: eventActionCreator.fetchEvents,
};

export const GalleriesScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(GalleriesScreenComponent));
