import React, { SyntheticEvent, useEffect, useRef, useState } from "react";
import { Event } from "../../../models";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { container } from "tsyringe";
import { useDispatch } from "react-redux";
import { EventActionCreator } from "../../../store";

interface IOwnProps {
    event: Event;
    show: boolean;
    onClose?: () => void;
}

export const RsvpModal = ({ event, show, onClose }: IOwnProps) => {
    const [shouldShow, setShouldShow] = useState(show);
    useEffect(() => {
        if (show !== shouldShow) {
            setShouldShow(show);
        }
    }, [shouldShow, show]);
    const guests = useRef<HTMLInputElement>(null);
    const dispatch = useDispatch();
    const eventActionCreator = container.resolve(EventActionCreator);
    const submit = (e: SyntheticEvent) => {
        e.preventDefault();
        const guestCount = isNaN(guests.current?.valueAsNumber ?? NaN) ? 0 : guests.current?.valueAsNumber ?? 0;
        dispatch(eventActionCreator.rsvp({ eventId: event._id ?? "", guests: guestCount }));
        setShouldShow(false);
        if (onClose) {
            onClose();
        }
    };
    return (
        <Modal
            show={shouldShow}
            onHide={() => {
                setShouldShow(false);
                if (onClose) {
                    onClose();
                }
            }}
            backdrop="static"
            keyboard={false}>
            <Modal.Header>
                <Modal.Title>RSVP to {event.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={submit}>
                    <Form.Group>
                        <Form.Label>How many guests do you plan to bring</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="0"
                            ref={guests}
                            min={0}
                            step={1}
                            max={event.maxAllowedGuestsOfGuests}
                        />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={() => {
                        setShouldShow(false);
                        if (onClose) {
                            onClose();
                        }
                    }}>
                    Close
                </Button>
                <Button variant="primary" type="submit" onClick={submit}>
                    RSVP
                </Button>
            </Modal.Footer>
        </Modal>
    );
};
