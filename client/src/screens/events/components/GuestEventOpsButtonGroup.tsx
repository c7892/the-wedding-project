import React, { useRef, useState } from "react";
import { Event } from "../../../models";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import Overlay from "react-bootstrap/Overlay";
import Tooltip from "react-bootstrap/Tooltip";
import moment from "moment-timezone";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector } from "../../../store/selectors";
import { useDispatch, useSelector } from "react-redux";
import { ApplicationReduxState, EventActionCreator } from "../../../store";
import { RsvpModal } from "./RsvpModal";
import { useHistory } from "react-router-dom";
import { RouteKey } from "../../../routing";

interface IOwnProps {
    event: Event;
}

export const GuestEventOpsButtonGroup = ({ event }: IOwnProps) => {
    const eventSelector = container.resolve(EventSelector);
    const authSelector = container.resolve(AuthenticationSelector);
    const userId = useSelector(authSelector.selectUserId);
    const weddingCode = useSelector(authSelector.selectWeddingCode);
    const isRsvpd = useSelector((state: ApplicationReduxState) =>
        eventSelector.selectIsRsvpdForEvent(state, { eventId: event._id ?? "", userId: userId ?? "" }),
    );
    const isGuest = useSelector(authSelector.selectIsGuest);
    const history = useHistory();
    const rsvpRef = useRef<HTMLButtonElement>(null);
    const dispatch = useDispatch();
    const eventActionCreator = container.resolve(EventActionCreator);
    const [showDisabledRsvpToolTip, setShowDisabledRsvpToolTip] = useState(false);
    const [showRsvpModal, setShowRsvpModal] = useState(false);
    return (
        <>
            <ButtonGroup>
                <Button
                    onClick={() => {
                        history.push(
                            `${isGuest ? RouteKey.GuestBase : RouteKey.SpouseBase}${weddingCode}${
                                isGuest ? `/${userId}` : ""
                            }${RouteKey.ViewEvent}`,
                            {
                                event,
                            },
                        );
                    }}>
                    View
                </Button>
                {isGuest ? (
                    moment().isAfter(moment(event.rsvpByDate)) ? (
                        <>
                            <Button
                                ref={rsvpRef}
                                variant="warning"
                                onMouseEnter={() => setShowDisabledRsvpToolTip(true)}
                                onMouseLeave={() => setShowDisabledRsvpToolTip(false)}>
                                {isRsvpd ? "Un-RSVP" : "RSVP"}
                            </Button>
                            <Overlay target={rsvpRef.current} placement={"bottom"} show={showDisabledRsvpToolTip}>
                                <Tooltip id="tooltip-bottom">The date to RSVP has already passed</Tooltip>
                            </Overlay>
                        </>
                    ) : (
                        <Button
                            variant="warning"
                            onClick={() => {
                                if (!isRsvpd) {
                                    setShowRsvpModal(true);
                                } else {
                                    dispatch(eventActionCreator.unrsvp(event._id ?? ""));
                                }
                            }}>
                            {isRsvpd ? "Un-RSVP" : "RSVP"}
                        </Button>
                    )
                ) : (
                    <>
                        <Button
                            variant="warning"
                            onClick={() => {
                                history.push(`${RouteKey.SpouseBase}${weddingCode}${RouteKey.EditEvent}`, {
                                    event,
                                });
                            }}>
                            Edit
                        </Button>
                        <Button
                            variant="danger"
                            onClick={() => {
                                dispatch(eventActionCreator.deleteEvent(event._id ?? ""));
                                history.push(`${RouteKey.SpouseBase}${weddingCode}${RouteKey.Events}`);
                            }}>
                            Delete
                        </Button>
                    </>
                )}
            </ButtonGroup>
            <RsvpModal event={event} show={showRsvpModal} onClose={() => setShowRsvpModal(false)} />
        </>
    );
};
