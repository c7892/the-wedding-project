import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { container } from "tsyringe";
import { Guest, RSVPEntry } from "../../../models";
import { ApplicationReduxState, FetchGuestsAction, GuestListActionCreator } from "../../../store";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import { GuestListSelector } from "../../../store/selectors";
import { DragDropContext, Droppable, Draggable, DropResult } from "react-beautiful-dnd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { isEmpty, isEqual } from "lodash";

interface IOwnProps {
    rsvpList: RSVPEntry[];
    invitedGuests: string[];
}

interface IStateProps {
    guests: Guest[];
}

interface IDispatchProps {
    getGuests: () => FetchGuestsAction;
}

interface IOwnState {
    rsvpdGuests: Guest[];
}

type Props = IStateProps & IDispatchProps & IOwnProps;

export class RsvpListComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            rsvpdGuests: [],
        };
    }

    public componentDidMount() {
        setTimeout(this.props.getGuests, 500);
        if (isEmpty(this.props.guests)) {
            return;
        }
        const rsvp = this.props.rsvpList.map((item) => item.hostingGuest);
        const rsvpdGuests = this.props.guests.filter((guest) => rsvp.includes(guest._id ?? ""));
        this.setState({ rsvpdGuests });
    }

    public componentDidUpdate(prevProps: Props) {
        if (isEqual(prevProps.guests, this.props.guests)) {
            return;
        }
        const rsvp = this.props.rsvpList.map((item) => item.hostingGuest);
        const rsvpdGuests = this.props.guests.filter((guest) => rsvp.includes(guest._id ?? ""));
        this.setState({ rsvpdGuests });
    }

    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h3>RSVP List</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h5>Un-RSVP&apos;d</h5>
                    </Col>
                    <Col>
                        <h5>RSVP&apos;d</h5>
                    </Col>
                </Row>
                <Row>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId="unrsvp">
                            {(provided) => (
                                <Col className="unrsvp" {...provided.droppableProps} ref={provided.innerRef}>
                                    {this.getUnsvpList().map((item, index) => this.getGuestDraggable(item, index))}
                                    {provided.placeholder}
                                </Col>
                            )}
                        </Droppable>
                        <Droppable droppableId="rsvp">
                            {(provided) => (
                                <Col className="rsvp" {...provided.droppableProps} ref={provided.innerRef}>
                                    {this.state.rsvpdGuests.map((item, index) => this.getGuestDraggable(item, index))}
                                    {provided.placeholder}
                                </Col>
                            )}
                        </Droppable>
                    </DragDropContext>
                </Row>
            </Container>
        );
    }
    private getGuestDraggable = (item: Guest, index: number) => {
        return (
            <Draggable draggableId={item._id ?? ""} index={index} key={item._id}>
                {(provided) => (
                    <Row
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        id="guestRsvpItem">
                        <Col key={item._id}>
                            <Row>
                                <Col>
                                    {item.profilePic !== undefined ? (
                                        <Image className="roundProfilePic" src={item.profilePic} roundedCircle />
                                    ) : (
                                        <FontAwesomeIcon icon={faUser} size={"2x"} />
                                    )}
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <p>
                                        {item.firstName}&nbsp;{item.surname}
                                    </p>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <p>{item.mobile}</p>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                )}
            </Draggable>
        );
    };
    private getUnsvpList = () => {
        const rsvpd = this.state.rsvpdGuests.map((guest) => guest._id ?? "");
        return this.props.guests.filter(
            (guest) => !rsvpd.includes(guest._id ?? "") && this.props.invitedGuests.includes(guest._id ?? ""),
        );
    };
    private onDragEnd = (result: DropResult) => {
        const { draggableId, destination } = result;
        if (!destination) {
            return;
        }
        const guest = this.props.guests.find((g) => g._id === draggableId);
        if (!guest) {
            return;
        }
        if (destination.droppableId === "rsvp") {
            const rsvps = [guest];
            this.setState({ rsvpdGuests: rsvps.concat(this.state.rsvpdGuests) });
        } else {
            const rsvps = this.state.rsvpdGuests;
            const index = rsvps.findIndex((guest) => guest._id === draggableId);
            if (index < 0) {
                return;
            }
            rsvps.splice(index, 1);
            this.setState({ rsvpdGuests: rsvps.concat([]) });
        }
    };

    public getRsvpEntries = (): RSVPEntry[] => {
        let existing = this.props.rsvpList;
        const existingIds = existing.map((item) => item.hostingGuest);
        const rsvpdIds = this.state.rsvpdGuests.map((guest) => guest._id ?? "");
        const newRsvps = this.state.rsvpdGuests.filter((guest) => !existingIds.includes(guest._id ?? ""));
        const newEntries: RSVPEntry[] = newRsvps.map((guest) => ({
            hostingGuest: guest._id ?? "",
            totalBringingWith: 0,
        }));
        existing = this.props.rsvpList.filter((guest) => rsvpdIds.includes(guest.hostingGuest ?? ""));
        return existing.concat(newEntries);
    };
}

const guestListSelector = container.resolve(GuestListSelector);
const guestListActionCreator = container.resolve(GuestListActionCreator);
// const authenticationSelector = container.resolve(AuthenticationSelector);
const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        guests: guestListSelector.selectGuests(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getGuests: guestListActionCreator.fetchGuests,
};

export const RsvpList = connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })(
    RsvpListComponent,
);
