import React from "react";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarPlus } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector } from "../../../store/selectors";
import { RouteKey } from "../../../routing";
import { ApplicationReduxState } from "../../../store";
import moment from "moment-timezone";
import { Event } from "../../../models";
import { EventActionCreator } from "../../../store/actions/EventActionCreator";
import { GuestEventOpsButtonGroup } from "./GuestEventOpsButtonGroup";

interface IOwnProps {
    date: Date;
}

export const DayOfEvents = (props: IOwnProps) => {
    const history = useHistory();
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const eventSelector = container.resolve(EventSelector);
    const eventActionCreator = container.resolve(EventActionCreator);
    const weddingCode = useSelector(authenticationSelector.selectWeddingCode);
    const isGuest = useSelector(authenticationSelector.selectIsGuest);
    const events = useSelector((state: ApplicationReduxState) => eventSelector.selectEventsForDate(state, props.date));
    const dispatch = useDispatch();
    const navigate = (event?: Event) => {
        history.push(`${RouteKey.SpouseBase}${weddingCode}${RouteKey.EditEvent}`, { event });
    };
    const viewNavigate = (event?: Event) => {
        history.push(`${RouteKey.SpouseBase}${weddingCode}${RouteKey.ViewEvent}`, { event });
    };
    return (
        <>
            <Row>
                <Col>
                    <h2>{moment(props.date).format("dddd, MMMM Do YYYY")}</h2>
                </Col>
            </Row>
            <Row>
                <Col>
                    <ListGroup style={{ marginBottom: 16 }}>
                        {events.map((event) => {
                            return (
                                <ListGroup.Item key={event._id}>
                                    <Row>
                                        <Col md={9}>
                                            <Row>
                                                <Col>{event.title}</Col>
                                            </Row>
                                            {event.location ? (
                                                <Row>
                                                    <Col>
                                                        <a
                                                            href={`https://maps.google.com/?q=${event.location}`}
                                                            target="_blank"
                                                            rel="noopener noreferrer">
                                                            {event.location}
                                                        </a>
                                                    </Col>
                                                </Row>
                                            ) : null}
                                            <Row>
                                                <Col>
                                                    {moment
                                                        .tz(event.start, event.timezone)
                                                        .format("ddd, MMM Do YYYY, h:mm a z")}
                                                    &nbsp;-&nbsp;
                                                    {moment
                                                        .tz(event.end, event.timezone)
                                                        .format("ddd, MMM Do YYYY, h:mm a z")}
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            {isGuest ? (
                                                <GuestEventOpsButtonGroup event={event} />
                                            ) : (
                                                <ButtonGroup>
                                                    <Button variant="secondary" onClick={() => viewNavigate(event)}>
                                                        View
                                                    </Button>
                                                    <Button variant="warning" onClick={() => navigate(event)}>
                                                        Edit
                                                    </Button>
                                                    <Button
                                                        variant="danger"
                                                        onClick={() => {
                                                            dispatch(eventActionCreator.deleteEvent(event._id ?? ""));
                                                        }}>
                                                        Delete
                                                    </Button>
                                                </ButtonGroup>
                                            )}
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                            );
                        })}
                    </ListGroup>
                </Col>
            </Row>
            {isGuest ? null : (
                <Row style={{ marginBottom: 16 }}>
                    <Col>
                        <Button style={{ width: "100%" }} onClick={() => navigate()}>
                            Create event&nbsp;
                            <FontAwesomeIcon icon={faCalendarPlus} />
                        </Button>
                    </Col>
                </Row>
            )}
        </>
    );
};
