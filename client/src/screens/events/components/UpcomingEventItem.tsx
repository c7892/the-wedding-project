import React from "react";
import { Event } from "../../../models";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import moment from "moment-timezone";
import { GuestEventOpsButtonGroup } from "./GuestEventOpsButtonGroup";

interface IOwnProps {
    event: Event;
}

export const UpcomingEventItem = ({ event }: IOwnProps) => {
    return (
        <ListGroupItem key={event._id}>
            <Row>
                <Col sm={9}>
                    <Row>
                        <Col>{event.title}</Col>
                    </Row>
                    <Row>
                        <Col>
                            {moment.tz(event.start, event.timezone).format("ddd, MMM Do YYYY, h:mm a z")}
                            &nbsp;-&nbsp;
                            {moment.tz(event.end, event.timezone).format("ddd, MMM Do YYYY, h:mm a z")}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <a
                                href={`https://maps.google.com/?q=${event.location}`}
                                target="_blank"
                                rel="noopener noreferrer">
                                {event.location}
                            </a>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <p>RSVP by {moment(event.rsvpByDate).format("ddd, MMM Do YYYY")}</p>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <GuestEventOpsButtonGroup event={event} />
                </Col>
            </Row>
        </ListGroupItem>
    );
};
