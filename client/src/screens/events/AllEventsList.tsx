import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { sortBy } from "lodash";
import { Event } from "../../models";
import { ApplicationReduxState, FetchAllEventsAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector } from "../../store/selectors";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { connect } from "react-redux";
import { UpcomingEventItem } from "./components";

interface IStateProps {
    events: Event[];
    weddingCode?: string;
}

interface IDispatchProps {
    getEvents: () => FetchAllEventsAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class AllEventsListComponent extends PureComponent<Props> {
    public componentDidMount() {
        setTimeout(this.props.getEvents, 500);
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>All Events</h1>
                        <hr />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ListGroup id="allEvents">
                            {sortBy(this.props.events, ["start", "end"]).map((event) => {
                                return <UpcomingEventItem event={event} key={event._id} />;
                            })}
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        events: eventSelector.selectAllEvents(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchAllEvents,
};

export const AllEventsList = connect(mapStateToProps, mapDispatchToProps)(withRouter(AllEventsListComponent));
