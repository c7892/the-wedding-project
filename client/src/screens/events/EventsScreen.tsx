import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import { RouteComponentProps, withRouter } from "react-router-dom";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { DayOfEvents } from "./components";
import { isEqual } from "lodash";
import { Event } from "../../models";
import { ApplicationReduxState, FetchEventsAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector } from "../../store/selectors";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { connect } from "react-redux";
import { RouteKey } from "../../routing";

interface IOwnState {
    selectedDate?: Date;
}

interface IStateProps {
    events: Event[];
    weddingCode?: string;
    userId?: string;
    isGuest: boolean;
}

interface IDispatchProps {
    getEvents: (payload: Date) => FetchEventsAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

const localizer = momentLocalizer(moment);
export class EventsScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly eventRow = React.createRef<HTMLDivElement>();
    public constructor(props: Props) {
        super(props);
        this.state = {
            selectedDate: undefined,
        };
    }
    public componentDidMount() {
        setTimeout(() => this.props.getEvents(new Date()), 500);
    }
    public componentDidUpdate(prevProps: Props, prevState: IOwnState) {
        if (isEqual(prevState, this.state) || !this.state.selectedDate) {
            return;
        }
        this.eventRow.current?.scrollIntoView();
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Events</h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Calendar
                            events={this.props.events}
                            localizer={localizer}
                            startAccessor="start"
                            endAccessor="end"
                            style={{ height: 500 }}
                            defaultDate={new Date()}
                            views={["month", "week", "day"]}
                            popup
                            selectable
                            onNavigate={(date) => this.props.getEvents(date)}
                            onSelectSlot={(info) => this.setState({ selectedDate: new Date(info.start) })}
                            onSelectEvent={(event: Event) => {
                                if (this.props.isGuest) {
                                    this.props.history.push(
                                        `${RouteKey.GuestBase}${this.props.weddingCode}/${this.props.userId}${RouteKey.ViewEvent}`,
                                        { event },
                                    );
                                } else {
                                    this.props.history.push(
                                        `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.ViewEvent}`,
                                        {
                                            event,
                                        },
                                    );
                                }
                            }}
                        />
                    </Col>
                </Row>
                <Row style={this.state.selectedDate ? { marginTop: 64 } : undefined} ref={this.eventRow}>
                    <Col>{this.state.selectedDate ? <DayOfEvents date={this.state.selectedDate} /> : null}</Col>
                </Row>
            </Container>
        );
    }
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        events: eventSelector.selectUserEvents(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
        userId: authenticationSelector.selectUserId(state),
        isGuest: authenticationSelector.selectIsGuest(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
};

export const EventsScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(EventsScreenComponent));
