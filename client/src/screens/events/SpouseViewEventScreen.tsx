import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import moment from "moment-timezone";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { container } from "tsyringe";
import { Event, Invite } from "../../models";
import {
    ApplicationReduxState,
    DeleteEventAction,
    EventActionCreator,
    InviteActionCreator,
    SaveEventAction,
    SaveInviteAction,
    UnRsvpAction,
} from "../../store";
import { AuthenticationSelector, InviteSelector } from "../../store/selectors";
import parse from "html-react-parser";
import { RouteKey } from "../../routing";
import { RsvpList, RsvpListComponent } from "./components/RsvpList";
import "../../styles/screens/guestList.css";
import { extend } from "lodash";
import { toast } from "react-toastify";

interface IOwnState {
    event: Event;
}

interface IDispatchProps {
    unrsvp: (payload: string) => UnRsvpAction;
    deleteEvent: (payload: string) => DeleteEventAction;
    saveEvent: (payload: Event) => SaveEventAction;
    saveInvite: (payload: Invite) => SaveInviteAction;
}

interface IStateProps {
    userId?: string;
    weddingCode?: string;
    inviteBeingSaved?: Invite;
    isSavingInvite: boolean;
}

type Props = IStateProps & IDispatchProps & RouteComponentProps;

export class SpouseViewEventScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly rsvpList = React.createRef<RsvpListComponent>();
    public constructor(props: Props) {
        super(props);
        const locationState = props.location.state as { event: Event };
        this.state = {
            event: locationState.event,
        };
    }
    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSavingInvite && this.props.isSavingInvite) {
            toast.warning("Saving invitation, please wait.");
        }
        if (prevProps.isSavingInvite && !this.props.isSavingInvite) {
            if (this.props.inviteBeingSaved) {
                toast.success("Successfully saved invitation");
                this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditInvitation}`, {
                    invite: this.props.inviteBeingSaved,
                });
            } else {
                toast.error("Failed to saved invitation, please try again");
            }
        }
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>{this.state.event.title}</h1>
                    </Col>
                    <Col>
                        <ButtonGroup>
                            <Button variant="info" onClick={this.createInvitation}>
                                Create Invite
                            </Button>
                            <Button
                                variant="warning"
                                onClick={() => {
                                    this.props.history.push(
                                        `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.EditEvent}`,
                                        {
                                            event: this.state.event,
                                        },
                                    );
                                }}>
                                Edit
                            </Button>
                            <Button
                                variant="danger"
                                onClick={() => {
                                    this.props.deleteEvent(this.state.event._id ?? "");
                                    this.props.history.push(
                                        `${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.Events}`,
                                    );
                                }}>
                                Delete
                            </Button>
                        </ButtonGroup>
                    </Col>
                    <hr />
                </Row>
                <Row>
                    <Col xs={12} sm={8} md={6} style={{ borderRight: "1px solid black" }}>
                        <Row>
                            <Col>
                                <h3>Details</h3>
                            </Col>
                        </Row>
                        <Row>
                            <Col id="eventDetailsViewer">{parse(this.state.event.description)}</Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={4} md={6}>
                        <Col>
                            <p>
                                Starts:{" "}
                                {moment
                                    .tz(this.state.event.start, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY, hh:mm A z")}
                            </p>
                            <p>
                                End:{" "}
                                {moment
                                    .tz(this.state.event.end, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY, hh:mm A z")}
                            </p>
                            {this.state.event.location ? (
                                <p>
                                    At:&nbsp;
                                    <a
                                        href={`https://maps.google.com/?q=${this.state.event.location}`}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        {this.state.event.location}
                                    </a>
                                </p>
                            ) : null}
                            <p>
                                RSVP by:{" "}
                                {moment
                                    .tz(this.state.event.rsvpByDate, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY")}
                            </p>
                            {this.state.event.rsvpContact &&
                            (this.state.event.rsvpContact.email || this.state.event.rsvpContact.phone) ? (
                                <>
                                    <p>Additional RSVP contacts:</p>
                                    {this.state.event.rsvpContact.phone ? (
                                        <p>
                                            Call:&nbsp;
                                            <a href={`tel:${this.state.event.rsvpContact.phone}`}>
                                                {this.state.event.rsvpContact.phone}
                                            </a>
                                        </p>
                                    ) : null}
                                    {this.state.event.rsvpContact.email ? (
                                        <p>
                                            Email:&nbsp;
                                            <a href={`mailto:${this.state.event.rsvpContact.email}`}>
                                                {this.state.event.rsvpContact.email}
                                            </a>
                                        </p>
                                    ) : null}
                                </>
                            ) : null}
                            {this.state.event.parking !== undefined ? (
                                <p>{this.state.event.parking ? "Parking is available" : "Parking is not available"}</p>
                            ) : null}
                            {this.state.event.attire !== undefined ? (
                                <p>{`Attire: ${this.state.event.attire}`}</p>
                            ) : null}
                            {this.state.event.costToYou !== undefined ? (
                                <p>{`Cost to spouses: $${this.state.event.costToYou}`}</p>
                            ) : null}
                            {this.state.event.downPaymentAmount !== undefined ? (
                                <p>{`Down payment amount: $${this.state.event.downPaymentAmount}`}</p>
                            ) : null}
                            {this.state.event.arePaymentsComplete !== undefined ? (
                                <p>{`Payments are ${
                                    this.state.event.arePaymentsComplete ? "complete" : "not complete"
                                }`}</p>
                            ) : null}
                            {this.state.event.costToGuest !== undefined ? (
                                <p>{`Cost to guests: $${this.state.event.costToGuest}`}</p>
                            ) : null}
                            {this.state.event.maxAllowedGuestsOfGuests > 0 ? (
                                <p>{`Guests can bring upto ${this.state.event.maxAllowedGuestsOfGuests} additional guest(s) with them`}</p>
                            ) : (
                                <p>Your guests cannot bring any additional guests</p>
                            )}
                        </Col>
                    </Col>
                </Row>
                <Row style={{ marginTop: 16, marginBottom: 16 }}>
                    <Col>
                        <RsvpList
                            ref={this.rsvpList}
                            rsvpList={this.state.event.rsvpList}
                            invitedGuests={this.state.event.guests}
                        />
                    </Col>
                </Row>
                <Row style={{ marginTop: 8, marginBottom: 16 }}>
                    <Col>
                        <Button variant="secondary" className="float-right" onClick={this.saveRsvpList}>
                            Save RSVP List
                        </Button>
                    </Col>
                </Row>
            </Container>
        );
    }

    private createInvitation = () => {
        if (this.props.isSavingInvite) {
            return;
        }
        const invite: Invite = {
            inviteTitle: `Autogenerated invite for ${this.state.event.title}`,
            title: "",
            event: this.state.event._id ?? "",
            dateCreated: Date.now(),
            modifiedDttm: Date.now(),
            ownerWeddingCode: this.props.weddingCode ?? "",
        };
        this.props.saveInvite(invite);
    };

    private saveRsvpList = () => {
        const entries = this.rsvpList.current?.getRsvpEntries();
        if (entries === undefined) {
            return;
        }
        toast.info("Saving RSVP list");
        const event = extend({}, this.state.event, { rsvpList: entries });
        this.props.saveEvent(event);
        this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.ViewEvent}`, {
            event,
        });
    };
}

// const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const inviteActionCreator = container.resolve(InviteActionCreator);
const inviteSelector = container.resolve(InviteSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    const userId = authenticationSelector.selectUserId(state);
    return {
        userId,
        weddingCode: authenticationSelector.selectWeddingCode(state),
        inviteBeingSaved: inviteSelector.selectInviteBeingSaved(state),
        isSavingInvite: inviteSelector.selectIsSavingInvite(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    unrsvp: eventActionCreator.unrsvp,
    deleteEvent: eventActionCreator.deleteEvent,
    saveEvent: eventActionCreator.saveEvent,
    saveInvite: inviteActionCreator.saveInvite,
};

export const SpouseViewEventScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(SpouseViewEventScreenComponent));
