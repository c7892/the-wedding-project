import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Overlay from "react-bootstrap/Overlay";
import Tooltip from "react-bootstrap/Tooltip";
import moment from "moment-timezone";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { container } from "tsyringe";
import { Event } from "../../models";
import { ApplicationReduxState, EventActionCreator, UnRsvpAction } from "../../store";
import { EventSelector, AuthenticationSelector } from "../../store/selectors";
import { RsvpModal } from "./components";
import parse from "html-react-parser";

interface IOwnState {
    event: Event;
    showRsvpModal: boolean;
    showDisabledTooltip: boolean;
}

interface IDispatchProps {
    unrsvp: (payload: string) => UnRsvpAction;
}

interface IStateProps {
    userId?: string;
    isRsvpd: boolean;
}

type Props = IStateProps & IDispatchProps & RouteComponentProps;

export class GuestViewEventScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly rsvpRef = React.createRef<HTMLButtonElement>();
    public constructor(props: Props) {
        super(props);
        const locationState = props.location.state as { event: Event };
        this.state = {
            showDisabledTooltip: false,
            showRsvpModal: false,
            event: locationState.event,
        };
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>{this.state.event.title}</h1>
                    </Col>
                    <Col>
                        {moment().isAfter(moment(this.state.event.rsvpByDate)) ? (
                            <>
                                <Button
                                    ref={this.rsvpRef}
                                    variant="warning"
                                    onMouseEnter={() => this.setState({ showDisabledTooltip: true })}
                                    onMouseLeave={() => this.setState({ showDisabledTooltip: false })}>
                                    {this.props.isRsvpd ? "Un-RSVP" : "RSVP"}
                                </Button>
                                <Overlay
                                    target={this.rsvpRef.current}
                                    placement={"bottom"}
                                    show={this.state.showDisabledTooltip}>
                                    <Tooltip id="tooltip-bottom">The date to RSVP has already passed</Tooltip>
                                </Overlay>
                            </>
                        ) : (
                            <Button
                                variant="warning"
                                onClick={() => {
                                    if (!this.props.isRsvpd) {
                                        this.setState({ showRsvpModal: true });
                                    } else {
                                        this.props.unrsvp(this.state.event._id ?? "");
                                    }
                                }}>
                                {this.props.isRsvpd ? "Un-RSVP" : "RSVP"}
                            </Button>
                        )}
                    </Col>
                    <hr />
                </Row>
                <Row>
                    <Col sm={8} style={{ borderRight: "1px solid black" }}>
                        <Row>
                            <Col>
                                <h3>Details</h3>
                            </Col>
                        </Row>
                        <Row>
                            <Col id="eventDetailsViewer">{parse(this.state.event.description)}</Col>
                        </Row>
                    </Col>
                    <Col>
                        <Col>
                            <p>
                                Starts:{" "}
                                {moment
                                    .tz(this.state.event.start, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY, hh:mm A z")}
                            </p>
                            <p>
                                End:{" "}
                                {moment
                                    .tz(this.state.event.end, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY, hh:mm A z")}
                            </p>
                            {this.state.event.location ? (
                                <p>
                                    At:&nbsp;
                                    <a
                                        href={`https://maps.google.com/?q=${this.state.event.location}`}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        {this.state.event.location}
                                    </a>
                                </p>
                            ) : null}
                            <p>
                                RSVP by:{" "}
                                {moment
                                    .tz(this.state.event.rsvpByDate, this.state.event.timezone)
                                    .format("ddd, MMM Do YYYY")}
                            </p>
                            {this.state.event.rsvpContact &&
                            (this.state.event.rsvpContact.email || this.state.event.rsvpContact.phone) ? (
                                <>
                                    <p>Additional RSVP contacts:</p>
                                    {this.state.event.rsvpContact.phone ? (
                                        <p>
                                            Call:&nbsp;
                                            <a href={`tel:${this.state.event.rsvpContact.phone}`}>
                                                {this.state.event.rsvpContact.phone}
                                            </a>
                                        </p>
                                    ) : null}
                                    {this.state.event.rsvpContact.email ? (
                                        <p>
                                            Email:&nbsp;
                                            <a href={`mailto:${this.state.event.rsvpContact.email}`}>
                                                {this.state.event.rsvpContact.email}
                                            </a>
                                        </p>
                                    ) : null}
                                </>
                            ) : null}
                            {this.state.event.parking !== undefined ? (
                                <p>{this.state.event.parking ? "Parking is available" : "Parking is not available"}</p>
                            ) : null}
                            {this.state.event.attire !== undefined ? (
                                <p>{`Attire: ${this.state.event.attire}`}</p>
                            ) : null}
                            {this.state.event.costToGuest !== undefined ? (
                                <p>{`Cost to you: $${this.state.event.costToGuest}`}</p>
                            ) : null}
                            {this.state.event.maxAllowedGuestsOfGuests > 0 ? (
                                <p>{`You can bring upto ${this.state.event.maxAllowedGuestsOfGuests} guest(s) with you`}</p>
                            ) : (
                                <p>You cannot bring any additional guests</p>
                            )}
                            <p>{this.props.isRsvpd ? "You are RSVP'd" : "You have yet to RSVP"}</p>
                        </Col>
                    </Col>
                </Row>
                <RsvpModal
                    event={this.state.event}
                    show={this.state.showRsvpModal}
                    onClose={() => this.setState({ showRsvpModal: false })}
                />
            </Container>
        );
    }
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState, props: RouteComponentProps): IStateProps => {
    const locationState = props.location.state as { event: Event };
    const userId = authenticationSelector.selectUserId(state);
    return {
        isRsvpd: eventSelector.selectIsRsvpdForEvent(state, {
            eventId: locationState.event._id ?? "",
            userId: userId ?? "",
        }),
        userId,
    };
};

const mapDispatchToProps: IDispatchProps = {
    unrsvp: eventActionCreator.unrsvp,
};

export const GuestViewEventScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(GuestViewEventScreenComponent));
