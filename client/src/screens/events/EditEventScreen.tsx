import React, { PureComponent, SyntheticEvent } from "react";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import moment from "moment-timezone";
import InputGroup from "react-bootstrap/InputGroup";
import { extend, map, values } from "lodash";
import Form from "react-bootstrap/Form";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Event, EventPriority } from "../../models";
import ReactQuill from "react-quill";
import { AddressValidator, Wysiwig } from "../../components";
import { GuestListSelectorComponent, GuestListSelectorReactComponent } from "../guestlist";
import { ApplicationReduxState, SaveEventAction, SetEventBeingSavedAction } from "../../store";
import { container } from "tsyringe";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { AuthenticationSelector, EventSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { RouteKey } from "../../routing";
import { QuillDeltaToHtmlConverter } from "quill-delta-to-html";
import "../../styles/screens/events.css";

interface IOwnState {
    event?: Event;
    start: string;
    end: string;
    rsvpBy: string;
}
interface IStateProps {
    isSavingEvent: boolean;
    eventBeingSaved?: Event;
    weddingCode?: string;
}

interface IDispatchProps {
    save: (payload: Event) => SaveEventAction;
    setEventBeingSaving: (payload?: Event) => SetEventBeingSavedAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class EditEventScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly title = React.createRef<HTMLInputElement>();
    private readonly description = React.createRef<ReactQuill>();
    private readonly address = React.createRef<AddressValidator>();
    private readonly start = React.createRef<HTMLInputElement>();
    private readonly end = React.createRef<HTMLInputElement>();
    private readonly allDay = React.createRef<HTMLInputElement>();
    private readonly isWedding = React.createRef<HTMLInputElement>();
    private readonly attire = React.createRef<HTMLInputElement>();
    private readonly parking = React.createRef<HTMLInputElement>();
    private readonly priority = React.createRef<HTMLSelectElement>();
    private readonly timezone = React.createRef<HTMLSelectElement>();
    private readonly costToGuest = React.createRef<HTMLInputElement>();
    private readonly costToSpouses = React.createRef<HTMLInputElement>();
    private readonly downPayment = React.createRef<HTMLInputElement>();
    private readonly arePaymentsComplete = React.createRef<HTMLInputElement>();
    private readonly guests = React.createRef<GuestListSelectorReactComponent>();
    private readonly rsvpBy = React.createRef<HTMLInputElement>();
    private readonly maxNumberOfGuests = React.createRef<HTMLInputElement>();
    private readonly phone = React.createRef<HTMLInputElement>();
    private readonly email = React.createRef<HTMLInputElement>();
    private telPattern = "[0-9]{3}-[0-9]{3}-[0-9]{4}";

    public constructor(props: Props) {
        super(props);
        this.state = {
            event: undefined,
            start: "",
            end: "",
            rsvpBy: "",
        };
    }

    public componentDidMount() {
        const locationState: { event?: Event } | undefined = this.props.location.state as { event?: Event };
        if (!locationState || !locationState.event) {
            return;
        }
        if (locationState.event.priority) {
            const priorities = Object.keys(EventPriority);
            const index = priorities.indexOf(locationState.event.priority);
            if (index >= 0 && this.priority.current) {
                this.priority.current.selectedIndex = index + 1;
            }
        }
        this.setState({
            event: locationState.event,
            end: moment(locationState.event?.end).format("yyyy-MM-DDThh:mm"),
            start: moment(locationState.event?.start).format("yyyy-MM-DDThh:mm"),
            rsvpBy: moment(locationState.event?.rsvpByDate).format("yyyy-MM-DD"),
        });
    }

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSavingEvent && this.props.isSavingEvent) {
            toast.warning("Saving event, please wait.");
        }
        if (prevProps.isSavingEvent && !this.props.isSavingEvent) {
            if (this.props.eventBeingSaved) {
                toast.success("Successfully saved event");
                this.props.history.push(`${RouteKey.SpouseBase}${this.props.weddingCode}${RouteKey.Events}`);
            } else {
                toast.error("Failed to saved event, please try again");
            }
        }
    }

    public componentWillUnmount() {
        this.props.setEventBeingSaving(undefined);
    }

    public render() {
        return (
            <Container fluid style={{ marginBottom: 16 }}>
                <Row>
                    <Col>
                        <h2>Edit event</h2>
                        <hr />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form onSubmit={this.onSubmitEvent} id="editEventForm">
                            <Row>
                                <Col xs={9}>
                                    <Form.Group>
                                        <Form.Label>Name of the event</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="The Engagement Party"
                                            ref={this.title}
                                            defaultValue={this.state.event?.title}
                                            required
                                            disabled={this.props.isSavingEvent}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label="Is this the wedding date?"
                                        ref={this.isWedding}
                                        defaultChecked={this.state.event?.isWedding}
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Description</Form.Label>
                                    <Wysiwig
                                        ref={this.description}
                                        defaultValue={this.state.event?.description}
                                        readonly={this.props.isSavingEvent}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Start</Form.Label>
                                    <Form.Control
                                        type="datetime-local"
                                        ref={this.start}
                                        value={this.state.start}
                                        onChange={(e) => {
                                            this.setState({
                                                start: e.target.value,
                                            });
                                        }}
                                        required
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>End</Form.Label>
                                    <Form.Control
                                        type="datetime-local"
                                        ref={this.end}
                                        value={this.state.end}
                                        onChange={(e) => {
                                            this.setState({
                                                end: e.target.value,
                                            });
                                        }}
                                        required
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>Timezone</Form.Label>
                                    <Form.Control
                                        as="select"
                                        defaultValue={this.state.event?.timezone ?? moment.tz.guess(true)}
                                        disabled={this.props.isSavingEvent}
                                        ref={this.timezone}>
                                        {map(moment.tz.names().sort(), (timezone, index) => {
                                            return (
                                                <option key={index} value={timezone}>
                                                    {timezone}
                                                </option>
                                            );
                                        })}
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label="All day?"
                                        ref={this.allDay}
                                        defaultChecked={this.state.event?.allDay}
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <AddressValidator
                                        ref={this.address}
                                        label={"Event location"}
                                        disabled={this.props.isSavingEvent}
                                        defaultValue={this.state.event?.location}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Attire</Form.Label>
                                    <Form.Control
                                        type="text"
                                        ref={this.attire}
                                        defaultValue={this.state.event?.attire}
                                        disabled={this.props.isSavingEvent}
                                        placeholder="Optional"
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>Event Priority</Form.Label>
                                    <Form.Control
                                        as="select"
                                        defaultValue={this.state.event?.priority}
                                        disabled={this.props.isSavingEvent}
                                        ref={this.priority}>
                                        <option disabled selected value={undefined}>
                                            {" "}
                                            -- Optional --{" "}
                                        </option>
                                        {map(EventPriority, (priority, index) => {
                                            return (
                                                <option key={index} value={priority}>
                                                    {priority}
                                                </option>
                                            );
                                        })}
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label="Is there parking available?"
                                        ref={this.parking}
                                        disabled={this.props.isSavingEvent}
                                        defaultChecked={this.state.event?.parking}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Cost to guests</Form.Label>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>$</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                type={"number"}
                                                placeholder="Optional"
                                                ref={this.costToGuest}
                                                disabled={this.props.isSavingEvent}
                                                value={this.state.event?.costToGuest}
                                                min={0}
                                                step={0.01}
                                                required
                                            />
                                        </InputGroup>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Cost to you and spouse</Form.Label>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>$</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                type={"number"}
                                                placeholder="Optional"
                                                ref={this.costToSpouses}
                                                value={this.state.event?.costToYou}
                                                disabled={this.props.isSavingEvent}
                                                min={0}
                                                step={0.01}
                                                required
                                            />
                                        </InputGroup>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group>
                                        <Form.Label>Down payment amount</Form.Label>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>$</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                type={"number"}
                                                placeholder="Optional"
                                                ref={this.downPayment}
                                                value={this.state.event?.downPaymentAmount}
                                                disabled={this.props.isSavingEvent}
                                                min={0}
                                                step={0.01}
                                                required
                                            />
                                        </InputGroup>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label="Are the payments complete?"
                                        ref={this.arePaymentsComplete}
                                        disabled={this.props.isSavingEvent}
                                        defaultChecked={this.state.event?.arePaymentsComplete}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={2}>
                                    <Form.Label>RSVP By</Form.Label>
                                    <Form.Control
                                        type="date"
                                        ref={this.rsvpBy}
                                        value={this.state.rsvpBy}
                                        onChange={(e) => {
                                            this.setState({
                                                rsvpBy: e.target.value,
                                            });
                                        }}
                                        required
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                                <Col sm={4}>
                                    <Form.Label>Max number of people, Guests can bring</Form.Label>
                                    <Form.Control
                                        type="number"
                                        ref={this.maxNumberOfGuests}
                                        min={0}
                                        value={this.state.event?.maxAllowedGuestsOfGuests}
                                        required
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>RSVP Phone Number</Form.Label>
                                    <Form.Control
                                        type="phone"
                                        ref={this.phone}
                                        placeholder={"Optional"}
                                        value={this.state.event?.rsvpContact?.phone}
                                        pattern={this.telPattern}
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>RSVP Phone Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        ref={this.email}
                                        placeholder={"Optional"}
                                        value={this.state.event?.rsvpContact?.email}
                                        disabled={this.props.isSavingEvent}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <GuestListSelectorComponent
                                    selectedGuestLists={this.state.event?.guestLists}
                                    selectedGuests={this.state.event?.guests}
                                    ref={this.guests}
                                />
                            </Row>
                            <Row>
                                <Col>
                                    <Button
                                        variant={"success"}
                                        type="submit"
                                        onClick={this.onSubmitEvent}
                                        disabled={this.props.isSavingEvent}>
                                        Submit
                                    </Button>
                                </Col>
                                <Col>
                                    <Button
                                        variant={"warning"}
                                        onClick={() => this.props.history.goBack()}
                                        disabled={this.props.isSavingEvent}
                                        className="float-right">
                                        Cancel
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }

    private onSubmitEvent = (e: SyntheticEvent) => {
        e.preventDefault();
        if (this.props.eventBeingSaved) {
            return;
        }
        const cfg = {
            encodeHtml: true,
            inlineStyles: true,
        };
        const converter = new QuillDeltaToHtmlConverter(
            this.description.current?.getEditor()?.getContents()?.ops ?? [],
            cfg,
        );
        const timezone = this.timezone.current?.selectedOptions[0].value ?? moment.tz.guess(true);
        const guestLists = this.guests.current?.getSelectedGuestLists() ?? [];
        const guests = this.guests.current?.getSelectedGuests() ?? [];
        const locationState: { event?: Event } | undefined = this.props.location.state as { event?: Event };
        const maxAllowedGuestsOfGuests = isNaN(this.maxNumberOfGuests.current?.valueAsNumber ?? NaN)
            ? 0
            : this.maxNumberOfGuests.current?.valueAsNumber ?? 0;
        const costToGuest = isNaN(this.costToGuest.current?.valueAsNumber ?? NaN)
            ? undefined
            : this.costToGuest.current?.valueAsNumber;
        const costToYou = isNaN(this.costToSpouses.current?.valueAsNumber ?? NaN)
            ? undefined
            : this.costToSpouses.current?.valueAsNumber;
        const downPaymentAmount = isNaN(this.downPayment.current?.valueAsNumber ?? NaN)
            ? undefined
            : this.downPayment.current?.valueAsNumber;
        const eventUpdates: Event = {
            title: this.title.current?.value ?? "",
            start: moment.tz(this.start.current?.value ?? new Date(), timezone).toDate(),
            end: moment.tz(this.end.current?.value ?? new Date(), timezone).toDate(),
            rsvpByDate: moment(this.rsvpBy.current?.value ?? new Date()).toDate(),
            timezone,
            allDay: this.allDay.current?.checked,
            description: converter.convert(),
            guestLists,
            guests,
            rsvpList: locationState?.event?.rsvpList ?? [],
            parking: this.parking.current?.checked,
            reminders: [],
            location: this.address.current?.getAddress(),
            costToGuest: costToGuest !== undefined ? (costToGuest < 0 ? 0 : costToGuest) : undefined,
            costToYou: costToYou !== undefined ? (costToYou < 0 ? 0 : costToYou) : undefined,
            downPaymentAmount:
                downPaymentAmount !== undefined ? (downPaymentAmount < 0 ? 0 : downPaymentAmount) : undefined,
            maxAllowedGuestsOfGuests: maxAllowedGuestsOfGuests < 0 ? 0 : maxAllowedGuestsOfGuests,
            arePaymentsComplete: this.arePaymentsComplete.current?.checked,
            attire: this.attire.current?.value,
            priority:
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                this.priority.current!.selectedIndex > 0
                    ? // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                      values(EventPriority)[this.priority.current!.selectedIndex - 1]
                    : undefined,
            rsvpContact: {
                email: this.email.current?.value,
                phone: this.phone.current?.value,
            },
            isWedding: this.isWedding.current?.checked ?? false,
        };
        const eventToSave = extend({}, this.state.event ?? {}, eventUpdates);
        this.props.save(eventToSave);
    };
}
const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSavingEvent: eventSelector.selectIsSavingEvent(state),
        eventBeingSaved: eventSelector.selectEventBeingSaved(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    save: eventActionCreator.saveEvent,
    setEventBeingSaving: eventActionCreator.setEventBeingSaving,
};

export const EditEventScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(EditEventScreenComponent));
