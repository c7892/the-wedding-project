import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import { RouteComponentProps, withRouter } from "react-router-dom";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { isEmpty, isEqual, sortBy } from "lodash";
import { Event } from "../../models";
import { ApplicationReduxState, FetchEventsAction } from "../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, EventSelector } from "../../store/selectors";
import { EventActionCreator } from "../../store/actions/EventActionCreator";
import { connect } from "react-redux";
import { UpcomingEventItem } from "./components";

interface IOwnState {
    selectedDate?: Date;
}

interface IStateProps {
    events: Event[];
    weddingCode?: string;
}

interface IDispatchProps {
    getEvents: () => FetchEventsAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class UpcomingEventsScreenComponent extends PureComponent<Props, IOwnState> {
    private readonly eventRow = React.createRef<HTMLDivElement>();
    public constructor(props: Props) {
        super(props);
        this.state = {
            selectedDate: undefined,
        };
    }
    public componentDidMount() {
        setTimeout(this.props.getEvents, 500);
    }
    public componentDidUpdate(prevProps: Props, prevState: IOwnState) {
        if (isEqual(prevState, this.state) || !this.state.selectedDate) {
            return;
        }
        this.eventRow.current?.scrollIntoView();
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Upcoming Events</h1>
                        <hr />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {isEmpty(this.props.events) ? (
                            <h5 className="text-warning">No upcoming events</h5>
                        ) : (
                            <ListGroup>
                                {sortBy(this.props.events, ["start", "end"]).map((event) => {
                                    return <UpcomingEventItem event={event} key={event._id} />;
                                })}
                            </ListGroup>
                        )}
                    </Col>
                </Row>
            </Container>
        );
    }
}

const eventSelector = container.resolve(EventSelector);
const eventActionCreator = container.resolve(EventActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        events: eventSelector.selectUpcomingEvents(state),
        weddingCode: authenticationSelector.selectWeddingCode(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getEvents: eventActionCreator.fetchEvents,
};

export const UpcomingEventsScreen = connect(
    mapStateToProps,
    mapDispatchToProps,
)(withRouter(UpcomingEventsScreenComponent));
