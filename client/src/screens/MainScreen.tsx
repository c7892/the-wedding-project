import React from "react";
import { Switch } from "react-router";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { guestPrivateRoutes, spousePrivateRoutes } from "../routing";
import { PrivateRoute } from "../components";
import { HomeScreen } from "./home/HomeScreen";
import { SideNavigationBar } from "../components";
import { useSelector } from "react-redux";
import { container } from "tsyringe";
import { AuthenticationSelector } from "../store/selectors";

// type Props = RouteComponentProps;

export const MainScreen = () => {
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const isGuest = useSelector(authenticationSelector.selectIsGuest);
    const routes = isGuest ? guestPrivateRoutes : spousePrivateRoutes;
    return (
        <Container fluid>
            <Row>
                <Col md={2}>
                    <SideNavigationBar />
                </Col>
                <Col id="mainColContainer">
                    <Switch>
                        {routes.map((route, index) => (
                            <PrivateRoute key={index} {...route} />
                        ))}
                        <PrivateRoute component={HomeScreen} />
                    </Switch>
                </Col>
            </Row>
        </Container>
    );
};

// export const MainScreen = withRouter(MainScreenComponent);
