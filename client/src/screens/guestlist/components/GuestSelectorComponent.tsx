import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import ListGroup from "react-bootstrap/ListGroup";
import { isEmpty, isEqual, sortBy, values } from "lodash";
import Form from "react-bootstrap/Form";
import { connect } from "react-redux";
import { container } from "tsyringe";
import { Guest, GuestList } from "../../../models";
import {
    FetchGuestListsAction,
    FetchGuestsAction,
    GuestListActionCreator,
    ApplicationReduxState,
} from "../../../store";
import { AuthenticationSelector, GuestListSelector } from "../../../store/selectors";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faSearch } from "@fortawesome/free-solid-svg-icons";

interface IOwnState {
    selected: string[];
    filteredGuests: Guest[];
    filteredGuestList: GuestList[];
}

interface IStateProps {
    guests: Guest[];
    guestLists: GuestList[];
    userId?: string;
}

interface IDispatchProps {
    getGuestLists: () => FetchGuestListsAction;
    getGuests: () => FetchGuestsAction;
}

interface IOwnProps {
    selectedGuests?: string[];
    selectedGuestLists?: string[];
}

type Props = IStateProps & IDispatchProps & IOwnProps;

export class GuestListSelectorReactComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            selected: [],
            filteredGuestList: [],
            filteredGuests: [],
        };
    }

    public componentDidMount() {
        this.setState({
            filteredGuests: this.props.guests,
            filteredGuestList: this.props.guestLists,
            selected: ([] as string[])
                .concat(this.props.selectedGuests ?? [])
                .concat(this.props.selectedGuestLists ?? []),
        });
        setTimeout(() => {
            this.props.getGuestLists();
            this.props.getGuests();
        }, 500);
    }

    public componentDidUpdate(prevProps: Props) {
        if (isEqual(prevProps, this.props)) {
            return;
        }
        this.setState({
            filteredGuests: this.props.guests,
            filteredGuestList: this.props.guestLists,
            selected: this.state.selected
                .concat(this.props.selectedGuests ?? [])
                .concat(this.props.selectedGuestLists ?? []),
        });
    }

    public render() {
        return (
            <Container id="guestListSelector">
                <Row>
                    <Col>
                        <Form.Label>Guests</Form.Label>
                    </Col>
                </Row>
                <Row style={{ marginBottom: 12 }}>
                    <Col>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <FontAwesomeIcon icon={faSearch} size="2x" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    type={"text"}
                                    placeholder="Search"
                                    onChange={(e) => this.search(e?.target?.value)}
                                />
                                <InputGroup.Append>
                                    <InputGroup.Text>
                                        ({this.getSelectedGuestCount()})&nbsp;
                                        <FontAwesomeIcon
                                            id={this.isAllGuestsSelected() ? "icon-success" : "icon-primary"}
                                            onClick={() => {
                                                if (!this.isAllGuestsSelected()) {
                                                    this.setState({ selected: [] }, () => {
                                                        this.props.guests.forEach((guest) => this.selectGuest(guest));
                                                    });
                                                    return;
                                                }
                                                this.setState({ selected: [] });
                                            }}
                                            icon={faCheckCircle}
                                            size="2x"
                                        />
                                    </InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ListGroup>
                            {sortBy(this.state.filteredGuests, ["surname"]).map((guest) => {
                                return (
                                    <ListGroup.Item key={guest._id ?? ""}>
                                        <Row>
                                            <Col md={9}>
                                                {guest.firstName}&nbsp;{guest.surname}
                                                <br />
                                                <em>{this.getRelationShipToYou(guest)}</em>
                                                <br />
                                                <em>{guest.mobile}</em>
                                            </Col>
                                            <Col>
                                                <FontAwesomeIcon
                                                    id={
                                                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                                        this.state.selected.indexOf(guest._id!) >= 0
                                                            ? "icon-success"
                                                            : "icon-primary"
                                                    }
                                                    icon={faCheckCircle}
                                                    size="2x"
                                                    onClick={() => this.selectGuest(guest)}
                                                />
                                            </Col>
                                        </Row>
                                    </ListGroup.Item>
                                );
                            })}
                            {this.state.filteredGuestList.map((guestList) => {
                                return (
                                    <ListGroup.Item key={guestList._id}>
                                        <Row>
                                            <Col md={9}>
                                                <Row>
                                                    <Col>{guestList.name}</Col>
                                                </Row>
                                                {guestList.description ? <em>{guestList.description}</em> : null}
                                            </Col>
                                            <Col>
                                                <FontAwesomeIcon
                                                    id={
                                                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                                        this.state.selected.indexOf(guestList._id!) >= 0
                                                            ? "icon-success"
                                                            : "icon-primary"
                                                    }
                                                    icon={faCheckCircle}
                                                    size="2x"
                                                    onClick={() => this.selectGuestList(guestList)}
                                                />
                                            </Col>
                                        </Row>
                                    </ListGroup.Item>
                                );
                            })}
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        );
    }

    private isAllGuestsSelected = (): boolean => {
        const { selected } = this.state;
        const { guests } = this.props;
        if (isEmpty(selected)) {
            return false;
        }
        const search = guests.map((guest) => {
            return selected.includes(guest._id ?? "");
        });
        return !search.includes(false);
    };
    private getSelectedGuestCount = (): number => {
        const { selected } = this.state;
        const { guests } = this.props;
        let count = 0;
        if (isEmpty(selected)) {
            return count;
        }
        guests.forEach((guest) => {
            if (selected.includes(guest._id ?? "")) {
                count++;
            }
        });
        return count;
    };

    private selectGuest = (guest: Guest) => {
        const selected = this.state.selected;
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const index = selected.indexOf(guest._id!);
        if (index >= 0) {
            selected.splice(index, 1);
        } else {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            selected.push(guest._id!);
        }
        this.setState({
            selected: ([] as string[]).concat(selected),
        });
    };
    private selectGuestList = (guestList: GuestList) => {
        const selected = this.state.selected;
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const index = selected.indexOf(guestList._id!);
        if (index >= 0) {
            selected.splice(index, 1);
            for (const guestId of guestList.guests) {
                const index = selected.indexOf(guestId);
                if (index < 0) {
                    continue;
                }
                selected.splice(index, 1);
            }
        } else {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            selected.push(guestList._id!);
            for (const guestId of guestList.guests) {
                if (!selected.includes(guestId)) {
                    selected.push(guestId);
                }
            }
        }
        this.setState({
            selected: ([] as string[]).concat(selected),
        });
    };
    private search = (value?: string) => {
        if (!value) {
            this.setState({
                filteredGuests: this.props.guests,
                filteredGuestList: this.props.guestLists,
            });
            return;
        }
        const filter = value.toLowerCase();
        const filteredGuests = this.props.guests.filter((guest) => {
            const { firstName, surname, relation } = guest;
            const relationships = values(relation ?? { _spouse: "" }).map((v) => v.toLowerCase());
            if (
                firstName.toLowerCase().includes(filter) ||
                surname.toLowerCase().includes(filter) ||
                relationships.includes(filter)
            ) {
                return true;
            }
            return false;
        });
        const filteredGuestList = this.props.guestLists.filter((guestList) => {
            const { name, description } = guestList;
            if (name.toLowerCase().includes(filter) || (description ?? "").toLowerCase().includes(filter)) {
                return true;
            }
            return false;
        });
        this.setState({
            filteredGuests,
            filteredGuestList,
        });
    };
    private getRelationShipToYou = (guest: Guest) => {
        return guest.relation && this.props.userId ? guest.relation[this.props.userId] : "";
    };
    public getSelectedGuests = () => {
        const guests = this.props.guests
            .filter((guest) => this.state.selected.includes(guest._id ?? ""))
            .map((guest) => guest._id ?? "");
        return guests;
    };
    public getSelectedGuestLists = () => {
        const guestLists = this.props.guestLists
            .filter((guestList) => this.state.selected.includes(guestList._id ?? ""))
            .map((guestList) => guestList._id ?? "");
        return guestLists;
    };
}

const guestListSelector = container.resolve(GuestListSelector);
const guestListActionCreator = container.resolve(GuestListActionCreator);
const authenticationSelector = container.resolve(AuthenticationSelector);
const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        guestLists: guestListSelector.selectGuestLists(state),
        guests: guestListSelector.selectGuests(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getGuestLists: guestListActionCreator.fetchGuestLists,
    getGuests: guestListActionCreator.fetchGuests,
};

export const GuestListSelectorComponent = connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })(
    GuestListSelectorReactComponent,
);
