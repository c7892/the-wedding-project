import React, { PureComponent } from "react";
import { Guest, GuestList } from "../../../models";
import ListGroup from "react-bootstrap/ListGroup";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faPencilAlt, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

interface IOwnProps {
    guestList: GuestList;
    onEdit: (guestList: GuestList) => void;
    onDelete: (guestList: GuestList) => void;
    onEditGuestInfo: (guest: Guest) => void;
}

interface IOwnState {
    isExpanded: boolean;
}
type Props = IOwnProps;

export class GuestListComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            isExpanded: false,
        };
    }
    public render() {
        return (
            <Row id="guestListRow">
                <Row>
                    <Col>
                        <Row>
                            <Col md={3}>
                                <Row>
                                    <Col>{this.props.guestList.name}</Col>
                                </Row>
                                {this.props.guestList.description ? <em>{this.props.guestList.description}</em> : null}
                            </Col>
                            <Col md={5}>
                                <Button variant="warning" onClick={() => this.props.onEdit(this.props.guestList)}>
                                    <FontAwesomeIcon icon={faPencilAlt} />
                                    &nbsp;Edit
                                </Button>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <Button variant="danger" onClick={() => this.props.onDelete(this.props.guestList)}>
                                    <FontAwesomeIcon icon={faTrashAlt} />
                                    &nbsp;Delete Guest List
                                </Button>
                            </Col>
                            <Col md={{ offset: 2, span: 2 }}>
                                <FontAwesomeIcon
                                    size={"2x"}
                                    className="float-right"
                                    id="icon-primary"
                                    icon={this.state.isExpanded ? faChevronUp : faChevronDown}
                                    onClick={() => this.setState({ isExpanded: !this.state.isExpanded })}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row style={{ marginTop: 15 }}>
                    <Col>
                        {this.state.isExpanded && this.props.guestList.guestObjs ? (
                            <ListGroup id="guestListRowGuests" className=".alt-scroll">
                                {this.props.guestList.guestObjs.map((guest) => {
                                    return (
                                        <ListGroup.Item key={guest._id ?? ""}>
                                            <Row>
                                                <Col md={3}>
                                                    <Row>
                                                        <Col>
                                                            {guest.firstName}&nbsp;{guest.surname}
                                                        </Col>
                                                    </Row>
                                                    <em>{guest.mobile}</em>
                                                </Col>
                                                <Col>
                                                    <Button
                                                        variant="warning"
                                                        onClick={() => this.props.onEditGuestInfo(guest)}>
                                                        <FontAwesomeIcon icon={faPencilAlt} />
                                                        &nbsp;Edit Guest information
                                                    </Button>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <Button
                                                        variant="danger"
                                                        onClick={() => this.props.onDelete(this.props.guestList)}>
                                                        <FontAwesomeIcon icon={faTrashAlt} />
                                                        &nbsp;Remove Guest From List
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </ListGroup.Item>
                                    );
                                })}
                            </ListGroup>
                        ) : null}
                    </Col>
                </Row>
            </Row>
        );
    }
}
