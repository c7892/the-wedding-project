import React, { PureComponent, SyntheticEvent } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTrash } from "@fortawesome/free-solid-svg-icons";
import { AddressValidator } from "../../../components";
import { Guest } from "../../../models";
import {
    ApplicationReduxState,
    DeleteGuestAction,
    GuestListActionCreator,
    UpdateGuestInfoAction,
} from "../../../store";
import { container } from "tsyringe";
import { connect } from "react-redux";
import { AuthenticationSelector, GuestListSelector } from "../../../store/selectors";
import { extend } from "lodash";
import { toast } from "react-toastify";

interface IOwnProps {
    onCancelled: () => void;
    guest?: Guest;
}

interface IStateProps {
    isSavingGuestList: boolean;
    guestListError?: string;
    userId?: string;
}

interface IDispatchProps {
    saveGuest: (payload: Guest) => UpdateGuestInfoAction;
    deleteGuest: (payload: Guest) => DeleteGuestAction;
}

type Props = IOwnProps & IStateProps & IDispatchProps;

export class EditGuestRowInformationComponent extends PureComponent<Props> {
    private readonly firstName = React.createRef<HTMLInputElement>();
    private readonly lastName = React.createRef<HTMLInputElement>();
    private readonly email = React.createRef<HTMLInputElement>();
    private readonly phone = React.createRef<HTMLInputElement>();
    private readonly relationToYou = React.createRef<HTMLInputElement>();
    private readonly relationToSpouse = React.createRef<HTMLInputElement>();
    private readonly address = React.createRef<AddressValidator>();
    private telPattern = "[0-9]{3}-[0-9]{3}-[0-9]{4}";

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSavingGuestList && this.props.isSavingGuestList) {
            toast.info("Saving guest info");
            return;
        }
        if (prevProps.isSavingGuestList && !this.props.isSavingGuestList) {
            if (!this.props.guestListError) {
                toast.success("Saved guest info successfully");
                this.props.onCancelled();
            } else {
                toast.error(this.props.guestListError);
            }
        }
    }
    public render() {
        return (
            <Form onSubmit={this.onFormSubmitted}>
                <Row>
                    <Col>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>First name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Joe"
                                        ref={this.firstName}
                                        defaultValue={this.props.guest?.firstName}
                                        required
                                        disabled={this.props.isSavingGuestList}
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Last name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Gardner"
                                        ref={this.lastName}
                                        defaultValue={this.props.guest?.surname}
                                        required
                                        disabled={this.props.isSavingGuestList}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Optional"
                                        defaultValue={this.props.guest?.email}
                                        ref={this.email}
                                        disabled={this.props.isSavingGuestList}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Mobile</Form.Label>
                                    <Form.Control
                                        type="tel"
                                        placeholder="Required"
                                        ref={this.phone}
                                        defaultValue={this.props.guest?.mobile}
                                        pattern={this.telPattern}
                                        required
                                        disabled={this.props.isSavingGuestList}
                                    />
                                    <Form.Text className="text-muted">ex. 123-456-7890</Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <AddressValidator
                                    ref={this.address}
                                    defaultValue={this.props.guest?.address}
                                    disabled={this.props.isSavingGuestList}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Relationship to you</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Brother"
                                        ref={this.relationToYou}
                                        defaultValue={this.getRelationShipToYou()}
                                        required
                                        disabled={this.props.isSavingGuestList}
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Relationship to spouse</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Brother in law"
                                        ref={this.relationToSpouse}
                                        defaultValue={this.getRelationShipToSpouse()}
                                        required
                                        disabled={this.props.isSavingGuestList}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row style={{ marginTop: 15 }}>
                    <Col>
                        <ButtonGroup>
                            <Button
                                variant="success"
                                type="submit"
                                onClick={this.onFormSubmitted}
                                disabled={this.props.isSavingGuestList}>
                                Submit&nbsp;
                                <FontAwesomeIcon icon={faCheck} />
                            </Button>
                            <Button
                                variant="warning"
                                onClick={this.props.onCancelled}
                                disabled={this.props.isSavingGuestList}>
                                Cancel
                            </Button>
                            <Button variant="danger" onClick={this.deleteGuest} disabled={this.props.isSavingGuestList}>
                                Delete Guest&nbsp;
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            </Form>
        );
    }

    private getRelationShipToYou = () => {
        return this.props.guest?.relation && this.props.userId ? this.props.guest?.relation[this.props.userId] : "";
    };
    private getRelationShipToSpouse = () => {
        if (!this.props.guest?.relation) {
            return "";
        }
        const keys = Object.keys(this.props.guest.relation);
        const spouseId = keys.find((key) => key !== this.props.userId);
        if (!spouseId) {
            return "";
        }
        return this.props.guest.relation[spouseId];
    };

    private onFormSubmitted = (e: SyntheticEvent) => {
        e.preventDefault();
        if (this.props.isSavingGuestList) {
            return;
        }
        const guestInfo = {
            firstName: this.firstName.current?.value ?? "",
            surname: this.lastName.current?.value ?? "",
            mobile: this.phone.current?.value ?? "",
            email: this.email.current?.value ?? "",
            address: this.address.current?.getAddress() ?? "",
            relation: {
                [this.props.userId ?? ""]: this.relationToYou.current?.value ?? "",
                _spouse: this.relationToSpouse.current?.value ?? "",
            },
        };
        const guest = extend({}, this.props.guest ?? {}, guestInfo);
        this.props.saveGuest(guest as Guest);
    };

    private deleteGuest = () => {
        if (!this.props.guest) {
            this.props.onCancelled();
            return;
        }
        this.props.deleteGuest(this.props.guest);
    };
}

const guestListSelector = container.resolve(GuestListSelector);
const authenticationSelector = container.resolve(AuthenticationSelector);
const guestListActionCreator = container.resolve(GuestListActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSavingGuestList: guestListSelector.selectIsSavingGuestList(state),
        guestListError: guestListSelector.selectGuestListError(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    saveGuest: guestListActionCreator.updateGuestInfo,
    deleteGuest: guestListActionCreator.deleteGuest,
};

export const EditGuestRowInformation = connect(mapStateToProps, mapDispatchToProps)(EditGuestRowInformationComponent);
