import React, { PureComponent, SyntheticEvent } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { GuestRow } from "./GuestRow";
import { extend, filter, values } from "lodash";
import { Guest, GuestList } from "../../../models";
import { ApplicationReduxState, GuestListActionCreator, SaveGuestListAction } from "../../../store";
import { container } from "tsyringe";
import { AuthenticationSelector, GuestListSelector } from "../../../store/selectors";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import ListGroup from "react-bootstrap/ListGroup";
interface IOwnProps {
    onCancelled: () => void;
    guestList?: GuestList;
}

interface IOwnState {
    guests: { [key: number]: JSX.Element };
    safeGuestsToAdd: Guest[];
}

interface IStateProps {
    isSavingGuestList: boolean;
    guestListError?: string;
    existingGuests: Guest[];
    userId?: string;
}

interface IDispatchProps {
    saveGuestList: (payload: { list: GuestList; guests?: Guest[] }) => SaveGuestListAction;
}

type Props = IOwnProps & IDispatchProps & IStateProps;

export class CreateGuestListFormComponent extends PureComponent<Props, IOwnState> {
    private readonly listNameRef = React.createRef<HTMLInputElement>();
    private readonly descriptionRef = React.createRef<HTMLInputElement>();
    private readonly guestRefs: { [key: number]: GuestRow | null } = {};
    private guests: string[] = [];
    public constructor(props: Props) {
        super(props);
        this.state = {
            guests: {},
            safeGuestsToAdd: props.existingGuests ?? [],
        };
    }

    public componentDidMount() {
        if (!this.props.guestList || !this.props.guestList.guestObjs) {
            return;
        }
        for (const guest of this.props.guestList.guestObjs) {
            this.guests.push(guest._id ?? "");
            this.addGuest(guest);
        }
    }

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSavingGuestList && this.props.isSavingGuestList) {
            toast.info("Saving guest list");
            return;
        }
        if (prevProps.isSavingGuestList && !this.props.isSavingGuestList) {
            if (!this.props.guestListError) {
                toast.success("Saved list successfully");
                this.props.onCancelled();
            } else {
                toast.error(this.props.guestListError);
            }
        }
    }
    public render() {
        return (
            <>
                <Row>
                    <Col>
                        <Form onSubmit={this.onFormSubmitted} id="guestListForm">
                            <Row>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Label>List name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Class of 95'"
                                            ref={this.listNameRef}
                                            required
                                            defaultValue={this.props.guestList?.name}
                                            disabled={this.props.isSavingGuestList}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md={6}>
                                    <Form.Group>
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="My spouse's highschool class of 1995 (optional)"
                                            ref={this.descriptionRef}
                                            required
                                            defaultValue={this.props.guestList?.description}
                                            disabled={this.props.isSavingGuestList}
                                        />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <h4>Guest(s)</h4>
                                    <hr />
                                </Col>
                            </Row>
                            <Row className="alt-scroll" id={"guestCardsRow"}>
                                {values(this.state.guests).map((guest, index) => {
                                    return (
                                        <>
                                            {index > 0 ? <hr key={index} /> : null}
                                            {guest}
                                        </>
                                    );
                                })}
                            </Row>
                            <Row>
                                <Col>
                                    <Button variant="info" style={{ width: "100%" }} onClick={() => this.addGuest()}>
                                        Add guest&nbsp;
                                        <FontAwesomeIcon icon={faPlusCircle} />
                                    </Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Button
                                        onClick={this.props.onCancelled}
                                        variant="warning"
                                        disabled={this.props.isSavingGuestList}>
                                        Cancel
                                    </Button>
                                </Col>
                                <Col>
                                    <Button
                                        type="submit"
                                        className="float-right"
                                        disabled={this.props.isSavingGuestList}>
                                        Save list
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
                {this.state.safeGuestsToAdd.length === this.props.existingGuests.length ? (
                    <Row style={{ marginTop: 25 }}>
                        <Col>
                            <p>Your other guests</p>
                        </Col>
                    </Row>
                ) : null}
                <Row>
                    <Col>
                        <ListGroup id="guestListRowGuests" className=".alt-scroll">
                            {this.state.safeGuestsToAdd.map((guest) => {
                                return (
                                    <ListGroup.Item key={guest._id ?? ""}>
                                        <Row>
                                            <Col md={3}>
                                                <Row>
                                                    <Col>
                                                        {guest.firstName}&nbsp;{guest.surname}
                                                    </Col>
                                                </Row>
                                                <em>{guest.mobile}</em>
                                            </Col>
                                            <Col>
                                                <Button
                                                    className="float-right"
                                                    variant="success"
                                                    onClick={() => this.addGuest(guest)}>
                                                    Add Guest To List&nbsp;
                                                    <FontAwesomeIcon icon={faPlusCircle} />
                                                </Button>
                                            </Col>
                                        </Row>
                                    </ListGroup.Item>
                                );
                            })}
                        </ListGroup>
                    </Col>
                </Row>
            </>
        );
    }
    private onFormSubmitted = (e: SyntheticEvent) => {
        e.preventDefault();
        const listName = this.listNameRef.current?.value ?? "";
        const description = this.descriptionRef.current?.value;
        const guestInfo = values(this.guestRefs).map((guest) => guest?.getGuestInfo());
        const guests: Guest[] = [];
        const list: GuestList = {
            name: listName,
            description,
            dateCreated: Date.now(),
            modifiedDttm: Date.now(),
            ownerWeddingCode: "",
            guests: this.guests,
            _id: this.props.guestList?._id,
        };
        for (const info of guestInfo) {
            if (!info) {
                continue;
            }
            guests.push({
                _id: info._id,
                firstName: info.firstName ?? "",
                surname: info.lastName ?? "",
                mobile: info.mobile ?? "",
                uniqueWeddingCode: "",
                dateCreated: Date.now(),
                modifiedDttm: Date.now(),
                events: [],
                relation: info.relation,
                email: info.email,
                address: info.address,
            });
        }
        this.props.saveGuestList({ list, guests: guests });
    };
    private addGuest = (guest?: Guest) => {
        if (guest) {
            const safeGuestsToAdd = this.state.safeGuestsToAdd.filter((safeGuest) => safeGuest._id !== guest._id);
            this.setState({ safeGuestsToAdd });
        }
        const guests = this.state.guests;
        const key = Date.now();
        this.setState({
            guests: extend({}, guests, {
                [key]: (
                    <GuestRow
                        key={key}
                        onRemove={() => this.removeGuest(key)}
                        disabled={this.props.isSavingGuestList}
                        ref={(ref) => {
                            this.guestRefs[key] = ref;
                        }}
                        userId={this.props.userId}
                        guest={guest}
                    />
                ),
            }),
        });
    };
    private removeGuest = (key: number) => {
        const guests = this.state.guests;
        const guestRefProp = this.guestRefs[key]?.getGuest();
        if (guestRefProp) {
            this.guests = filter(this.guests, (guest) => guest !== guestRefProp._id);
            const safeGuestsToAdd = filter(
                this.props.existingGuests,
                (guest) => !this.guests.includes(guest._id?.toString() ?? ""),
            );
            this.setState({ safeGuestsToAdd });
        }
        delete guests[key];
        delete this.guestRefs[key];
        this.setState({ guests: extend({}, guests, {}) });
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const guestListSelector = container.resolve(GuestListSelector);
const guestListActionCreator = container.resolve(GuestListActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSavingGuestList: guestListSelector.selectIsSavingGuestList(state),
        guestListError: guestListSelector.selectGuestListError(state),
        existingGuests: guestListSelector.selectGuests(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    saveGuestList: guestListActionCreator.saveGuestList,
};

export const CreateGuestListForm = connect(mapStateToProps, mapDispatchToProps)(CreateGuestListFormComponent);
