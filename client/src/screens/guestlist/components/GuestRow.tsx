import React, { PureComponent } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { AddressValidator } from "../../../components";
import { Guest } from "../../../models";

interface IOwnProps {
    onRemove: () => void;
    disabled?: boolean;
    guest?: Guest;
    userId?: string;
}

type Props = IOwnProps;

export class GuestRow extends PureComponent<Props> {
    private readonly firstName = React.createRef<HTMLInputElement>();
    private readonly lastName = React.createRef<HTMLInputElement>();
    private readonly email = React.createRef<HTMLInputElement>();
    private readonly phone = React.createRef<HTMLInputElement>();
    private readonly address = React.createRef<AddressValidator>();
    private readonly relationToYou = React.createRef<HTMLInputElement>();
    private readonly relationToSpouse = React.createRef<HTMLInputElement>();
    private telPattern = "[0-9]{3}-[0-9]{3}-[0-9]{4}";
    public render() {
        return (
            <Row>
                <Col md={9}>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>First name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Joe"
                                    ref={this.firstName}
                                    defaultValue={this.props.guest?.firstName}
                                    required
                                    disabled={this.props.disabled}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Last name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Gardner"
                                    ref={this.lastName}
                                    defaultValue={this.props.guest?.surname}
                                    required
                                    disabled={this.props.disabled}
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Optional"
                                    defaultValue={this.props.guest?.email}
                                    ref={this.email}
                                    disabled={this.props.disabled}
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Mobile</Form.Label>
                                <Form.Control
                                    type="tel"
                                    placeholder="Required"
                                    ref={this.phone}
                                    defaultValue={this.props.guest?.mobile}
                                    pattern={this.telPattern}
                                    required
                                    disabled={this.props.disabled}
                                />
                                <Form.Text className="text-muted">ex. 123-456-7890</Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <AddressValidator
                                ref={this.address}
                                defaultValue={this.props.guest?.address}
                                disabled={this.props.disabled}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Relationship to you</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Brother"
                                    ref={this.relationToYou}
                                    defaultValue={this.getRelationShipToYou()}
                                    required
                                    disabled={this.props.disabled}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Relationship to spouse</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Brother in law"
                                    ref={this.relationToSpouse}
                                    defaultValue={this.getRelationShipToSpouse()}
                                    required
                                    disabled={this.props.disabled}
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Button
                        variant="danger"
                        onClick={this.props.onRemove}
                        className="float-right"
                        id="removeGuestButton"
                        disabled={this.props.disabled}>
                        <FontAwesomeIcon icon={faTrash} />
                    </Button>
                </Col>
            </Row>
        );
    }
    private getRelationShipToYou = () => {
        return this.props.guest?.relation && this.props.userId ? this.props.guest?.relation[this.props.userId] : "";
    };
    private getRelationShipToSpouse = () => {
        if (!this.props.guest?.relation) {
            return "";
        }
        const keys = Object.keys(this.props.guest.relation);
        const spouseId = keys.find((key) => key !== this.props.userId);
        if (!spouseId) {
            return "";
        }
        return this.props.guest.relation[spouseId];
    };

    public getGuestInfo = () => ({
        _id: this.props.guest?._id,
        firstName: this.firstName.current?.value,
        lastName: this.lastName.current?.value,
        mobile: this.phone.current?.value,
        email: this.email.current?.value,
        address: this.address.current?.getAddress(),
        relation: {
            [this.props.userId ?? ""]: this.relationToYou.current?.value ?? "",
            _spouse: this.relationToSpouse.current?.value ?? "",
        },
    });

    public getGuest = () => this.props.guest;
}
