import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt, faPlusCircle, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { CreateGuestListForm } from "./components";
import "../../styles/screens/guestList.css";
import { Guest, GuestList } from "../../models";
import {
    ApplicationReduxState,
    DeleteGuestAction,
    DeleteGuestListAction,
    FetchGuestListsAction,
    FetchGuestsAction,
    GuestListActionCreator,
} from "../../store";
import { container } from "tsyringe";
import { GuestListSelector } from "../../store/selectors";
import { connect } from "react-redux";
import { GuestListComponent } from "./components/GuestListComponent";
import ListGroup from "react-bootstrap/ListGroup";
import { EditGuestRowInformation } from "./components/EditGuestInformationCard";

interface IOwnState {
    shouldRenderGuestListForm: boolean;
    editGuestInfoComp?: JSX.Element;
    guestListToEdit?: GuestList;
}

interface IStateProps {
    guests: Guest[];
    guestLists: GuestList[];
}

interface IDispatchProps {
    getGuestLists: () => FetchGuestListsAction;
    getGuests: () => FetchGuestsAction;
    deleteGuest: (payload: Guest) => DeleteGuestAction;
    deleteGuestList: (payload: GuestList) => DeleteGuestListAction;
}

type Props = IStateProps & IDispatchProps;

export class GuestListScreenComponent extends PureComponent<Props, IOwnState> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            shouldRenderGuestListForm: false,
            editGuestInfoComp: undefined,
            guestListToEdit: undefined,
        };
    }
    public componentDidMount() {
        setTimeout(() => {
            this.props.getGuestLists();
            this.props.getGuests();
        }, 500);
    }
    public render() {
        return (
            <Container fluid>
                {this.state.editGuestInfoComp ? null : (
                    <>
                        <Row>
                            <Col>
                                <h1>Your Guest List(s) &nbsp;&nbsp;&nbsp;&nbsp;</h1>
                            </Col>
                            <Col>
                                <Button
                                    onClick={() =>
                                        this.setState({
                                            shouldRenderGuestListForm: true,
                                            guestListToEdit: undefined,
                                        })
                                    }>
                                    <FontAwesomeIcon icon={faPlusCircle} size={"lg"} />
                                </Button>
                            </Col>
                            <hr />
                        </Row>
                        {this.state.shouldRenderGuestListForm ? (
                            <CreateGuestListForm
                                onCancelled={this.cancelledGuestListCreation}
                                guestList={this.state.guestListToEdit}
                            />
                        ) : (
                            <Row id="guestListsContainer">
                                <Col>
                                    {this.props.guestLists.map((list) => (
                                        <GuestListComponent
                                            key={list._id ?? ""}
                                            onDelete={this.onDeleteList}
                                            guestList={list}
                                            onEdit={this.onEditList}
                                            onEditGuestInfo={this.onEditGuestInfo}
                                        />
                                    ))}
                                </Col>
                            </Row>
                        )}
                    </>
                )}
                {this.state.shouldRenderGuestListForm ? null : (
                    <>
                        <Row>
                            <Col>
                                <h1>Your Guest(s) &nbsp;&nbsp;&nbsp;&nbsp;</h1>
                            </Col>
                            <Col>
                                <Button
                                    onClick={() => {
                                        this.setState({
                                            shouldRenderGuestListForm: false,
                                            guestListToEdit: undefined,
                                        });
                                        this.onEditGuestInfo();
                                    }}>
                                    <FontAwesomeIcon icon={faPlusCircle} size={"lg"} />
                                </Button>
                            </Col>
                            <hr />
                        </Row>
                        {this.state.editGuestInfoComp ? (
                            this.state.editGuestInfoComp
                        ) : (
                            <Row>
                                <Col>
                                    <ListGroup id="guestListRowGuests" className="alt-scroll">
                                        {this.props.guests.map((guest) => {
                                            return (
                                                <ListGroup.Item key={guest._id ?? ""}>
                                                    <Row>
                                                        <Col md={3}>
                                                            <Row>
                                                                <Col>
                                                                    {guest.firstName}&nbsp;{guest.surname}
                                                                </Col>
                                                            </Row>
                                                            <em>{guest.mobile}</em>
                                                        </Col>
                                                        <Col>
                                                            <Button
                                                                variant="warning"
                                                                onClick={() => this.onEditGuestInfo(guest)}>
                                                                <FontAwesomeIcon icon={faPencilAlt} />
                                                                &nbsp;Edit Guest information
                                                            </Button>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <Button
                                                                variant="danger"
                                                                onClick={() => this.props.deleteGuest(guest)}>
                                                                <FontAwesomeIcon icon={faTrashAlt} />
                                                                &nbsp;Delete Guest Info
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                            );
                                        })}
                                    </ListGroup>
                                </Col>
                            </Row>
                        )}
                    </>
                )}
            </Container>
        );
    }
    private cancelledGuestListCreation = () => {
        this.setState({ shouldRenderGuestListForm: false, guestListToEdit: undefined });
    };
    private onEditList = (guestList: GuestList) => {
        this.setState({
            shouldRenderGuestListForm: true,
            guestListToEdit: guestList,
        });
    };
    private onEditGuestInfo = (guest?: Guest) => {
        this.setState({
            editGuestInfoComp: (
                <Row>
                    <Col>
                        <EditGuestRowInformation guest={guest} onCancelled={this.onCancelEditGuestInfo} />
                    </Col>
                </Row>
            ),
        });
    };
    private onDeleteList = (guestList: GuestList) => {
        this.setState({
            guestListToEdit: undefined,
        });
        this.props.deleteGuestList(guestList);
    };
    private onCancelEditGuestInfo = () => this.setState({ editGuestInfoComp: undefined });
}

const guestListSelector = container.resolve(GuestListSelector);
const guestListActionCreator = container.resolve(GuestListActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        guestLists: guestListSelector.selectGuestLists(state),
        guests: guestListSelector.selectGuests(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    getGuestLists: guestListActionCreator.fetchGuestLists,
    getGuests: guestListActionCreator.fetchGuests,
    deleteGuest: guestListActionCreator.deleteGuest,
    deleteGuestList: guestListActionCreator.deleteGuestList,
};

export const GuestListScreen = connect(mapStateToProps, mapDispatchToProps)(GuestListScreenComponent);
