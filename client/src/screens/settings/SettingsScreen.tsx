import React, { PureComponent } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { container } from "tsyringe";
import { ApplicationReduxState, AuthenticationActionCreator, FetchUserAction } from "../../store";
import { AuthenticationSelector } from "../../store/selectors";
import { Guest, User } from "../../models";
import { ChangePassword, EditUserInformation } from "./components";
import { DeleteAccountComponent } from "./components/DeleteAccount";

interface IStateProps {
    user?: Guest | User;
    isGuest: boolean;
}

interface IDispatchProps {
    fetchUser: () => FetchUserAction;
}

type Props = RouteComponentProps & IStateProps & IDispatchProps;

export class SettingsScreenComponent extends PureComponent<Props> {
    public componentDidMount() {
        setTimeout(this.props.fetchUser, 500);
    }
    public render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Settings</h1>
                    </Col>
                    <hr />
                </Row>
                <Row style={{ marginBottom: 50 }}>
                    <Col>
                        <EditUserInformation isGuest={this.props.isGuest} user={this.props.user} />
                    </Col>
                </Row>
                {this.props.isGuest ? null : (
                    <Row style={{ marginBottom: 50 }}>
                        <Col>
                            <ChangePassword isGuest={this.props.isGuest} user={this.props.user} />
                        </Col>
                    </Row>
                )}
                <Row style={{ marginBottom: 50 }}>
                    <Col>
                        <DeleteAccountComponent />
                    </Col>
                </Row>
            </Container>
        );
    }
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        user: authenticationSelector.selectUser(state),
        isGuest: authenticationSelector.selectIsGuest(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    fetchUser: authenticationActionCreator.fetchUser,
};

export const SettingsScreen = connect(mapStateToProps, mapDispatchToProps)(withRouter(SettingsScreenComponent));
