import React, { PureComponent, SyntheticEvent } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faUserAlt } from "@fortawesome/free-solid-svg-icons";
import { AddressValidator } from "../../../components";
import { Guest, User } from "../../../models";
import { ApplicationReduxState, AuthenticationActionCreator, UpdateUserAction } from "../../../store";
import { container } from "tsyringe";
import { connect } from "react-redux";
import { AuthenticationSelector } from "../../../store/selectors";
import { toast } from "react-toastify";
import Dropzone from "react-dropzone";
import { getBase64ImageDataString } from "../../../utilities";

interface IOwnProps {
    user?: Guest | User;
    isGuest: boolean;
}

interface IOwnState {
    profilePic?: File;
    profilePicSrc?: string;
}

interface IStateProps {
    userId?: string;
    savingError?: string;
    isSaving: boolean;
}

interface IDispatchProps {
    update: (payload: { updates: User | Guest; image?: File }) => UpdateUserAction;
}

type Props = IOwnProps & IStateProps & IDispatchProps;

export class EditUserInformationComponent extends PureComponent<Props, IOwnState> {
    private readonly firstName = React.createRef<HTMLInputElement>();
    private readonly lastName = React.createRef<HTMLInputElement>();
    private readonly email = React.createRef<HTMLInputElement>();
    private readonly newFamilyName = React.createRef<HTMLInputElement>();
    private readonly phone = React.createRef<HTMLInputElement>();
    private readonly address = React.createRef<AddressValidator>();
    private telPattern = "[0-9]{3}-[0-9]{3}-[0-9]{4}";

    public constructor(props: Props) {
        super(props);
        this.state = {
            profilePic: undefined,
            profilePicSrc: undefined,
        };
    }

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSaving && this.props.isSaving) {
            toast.info("Updating please wait.");
            return;
        }
        if (prevProps.isSaving && !this.props.isSaving) {
            if (!this.props.savingError) {
                toast.success("Updated info successfully");
            } else {
                toast.error(this.props.savingError);
            }
        }
    }
    public render() {
        return (
            <>
                <Row>
                    <Dropzone onDropAccepted={this.onImageDropped} accept="image/*" multiple={false}>
                        {({ getRootProps, getInputProps }) => (
                            <Col {...getRootProps()} style={{ textAlign: "center", marginBottom: 10 }}>
                                {this.state.profilePic || (this.props.user && this.props.user.profilePic) ? (
                                    this.state.profilePic && this.state.profilePicSrc ? (
                                        <Image src={this.state.profilePicSrc} style={{ maxHeight: 500 }} />
                                    ) : (
                                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                        <Image src={this.props.user!.profilePic} style={{ maxHeight: 500 }} />
                                    )
                                ) : (
                                    <FontAwesomeIcon icon={faUserAlt} size={"4x"} />
                                )}
                                <input {...getInputProps()} />
                                <br />
                                <span>
                                    <em>Click here or drag image to change profile image</em>
                                </span>
                            </Col>
                        )}
                    </Dropzone>
                </Row>
                <Row>
                    <Col>
                        <Form onSubmit={this.onFormSubmitted}>
                            <Row>
                                <Col>
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>First name</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Joe"
                                                    ref={this.firstName}
                                                    defaultValue={this.props.user?.firstName}
                                                    required
                                                    disabled={this.props.isSaving}
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Last name</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Gardner"
                                                    ref={this.lastName}
                                                    defaultValue={this.props.user?.surname}
                                                    required
                                                    disabled={this.props.isSaving}
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    {this.props.isGuest ? null : (
                                        <Row>
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label>New Family Name</Form.Label>
                                                    <Form.Control
                                                        type="text"
                                                        placeholder={!this.props.isGuest ? "Optional" : "Required"}
                                                        required={!this.props.isGuest}
                                                        defaultValue={(this.props.user as User)?.newFamilyName}
                                                        ref={this.newFamilyName}
                                                        disabled={this.props.isSaving}
                                                    />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    )}
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control
                                                    type="email"
                                                    placeholder={this.props.isGuest ? "Optional" : "Required"}
                                                    required={!this.props.isGuest}
                                                    defaultValue={this.props.user?.email}
                                                    ref={this.email}
                                                    disabled={this.props.isSaving}
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Mobile</Form.Label>
                                                <Form.Control
                                                    type="tel"
                                                    placeholder={!this.props.isGuest ? "Optional" : "Required"}
                                                    required={this.props.isGuest}
                                                    ref={this.phone}
                                                    defaultValue={this.props.user?.mobile}
                                                    pattern={this.telPattern}
                                                    disabled={this.props.isSaving}
                                                />
                                                <Form.Text className="text-muted">ex. 123-456-7890</Form.Text>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <AddressValidator
                                                ref={this.address}
                                                defaultValue={this.props.user?.address}
                                                disabled={this.props.isSaving}
                                                required={!this.props.isGuest}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 15 }}>
                                <Col>
                                    <Button
                                        variant="success"
                                        type="submit"
                                        onClick={this.onFormSubmitted}
                                        disabled={this.props.isSaving}>
                                        Update Info&nbsp;
                                        <FontAwesomeIcon icon={faCheck} />
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </>
        );
    }

    private onImageDropped = async (file: File[]) => {
        if (!file || file.length === 0) {
            return;
        }
        this.setState({ profilePic: file[0] }, () => {
            getBase64ImageDataString(file[0]).then((src) => {
                this.setState({ profilePicSrc: src });
            });
        });
    };
    private onFormSubmitted = (e: SyntheticEvent) => {
        e.preventDefault();
        /*eslint-disable @typescript-eslint/no-non-null-assertion*/
        const updates: Guest | User = {
            firstName: this.firstName.current?.value ?? this.props.user!.firstName,
            surname: this.lastName.current?.value ?? this.props.user!.surname,
            mobile: this.phone.current?.value ?? this.props.user?.mobile,
            email: this.email.current?.value ?? this.props.user?.email ?? "",
            address: this.address.current?.getAddress() ?? "",
            newFamilyName: this.newFamilyName.current?.value ?? (this.props.user as User)?.newFamilyName,
            profilePic: this.props.user?.profilePic,
        };
        /*eslint-enable @typescript-eslint/no-non-null-assertion*/
        this.props.update({ updates, image: this.state.profilePic });
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSaving: authenticationSelector.selectIsSavingUser(state),
        savingError: authenticationSelector.selectIsSavingUserError(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    update: authenticationActionCreator.updateUser,
};

export const EditUserInformation = connect(mapStateToProps, mapDispatchToProps)(EditUserInformationComponent);
