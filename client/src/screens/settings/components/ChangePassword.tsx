import React, { PureComponent, SyntheticEvent } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { SecureInput } from "../../../components";
import { Guest, User } from "../../../models";
import { ApplicationReduxState, AuthenticationActionCreator, ChangePasswordAction } from "../../../store";
import { container } from "tsyringe";
import { connect } from "react-redux";
import { AuthenticationSelector } from "../../../store/selectors";
import { toast } from "react-toastify";

interface IOwnProps {
    user?: Guest | User;
    isGuest: boolean;
}

interface IStateProps {
    userId?: string;
    savingError?: string;
    isSaving: boolean;
}

interface IDispatchProps {
    changePassword: (payload: { oldPassword: string; newPassword: string }) => ChangePasswordAction;
}

type Props = IOwnProps & IStateProps & IDispatchProps;

export class ChangePasswordComponent extends PureComponent<Props> {
    private readonly oldPasswordRef = React.createRef<HTMLInputElement>();
    private readonly newPasswordRef = React.createRef<HTMLInputElement>();

    public componentDidUpdate(prevProps: Props) {
        if (!prevProps.isSaving && this.props.isSaving) {
            toast.info("Updating please wait.");
            return;
        }
        if (prevProps.isSaving && !this.props.isSaving) {
            if (!this.props.savingError) {
                toast.success("Updated info successfully");
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                this.oldPasswordRef.current!.value = "";
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                this.newPasswordRef.current!.value = "";
            } else {
                toast.error(this.props.savingError);
            }
        }
    }
    public render() {
        return (
            <>
                <Row>
                    <h2>Change Password</h2>
                    <hr />
                </Row>
                <Row>
                    <Col>
                        <Form onSubmit={this.onFormSubmitted}>
                            <Row>
                                <SecureInput
                                    renderAs={Col}
                                    fieldTitle={"Current Password"}
                                    ref={this.oldPasswordRef}
                                    minimumLength={8}
                                    minimumLengthErrorText={"Password must be at least 8 characters"}
                                    weakErrorText={""}
                                    disabled={this.props.isSaving}
                                />
                                <SecureInput
                                    renderAs={Col}
                                    fieldTitle={"New Password"}
                                    ref={this.newPasswordRef}
                                    minimumLength={8}
                                    minimumLengthErrorText={"Password must be at least 8 characters"}
                                    weakErrorText={"Password is too weak, please supply stronger password"}
                                    disabled={this.props.isSaving}
                                />
                            </Row>
                            <Row style={{ marginTop: 15 }}>
                                <Col>
                                    <Button
                                        variant="warning"
                                        type="submit"
                                        onClick={this.onFormSubmitted}
                                        disabled={this.props.isSaving}>
                                        Change Password&nbsp;
                                        <FontAwesomeIcon icon={faCheck} />
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </>
        );
    }

    private onFormSubmitted = (e: SyntheticEvent) => {
        e.preventDefault();
        const oldPassword = this.oldPasswordRef.current?.value;
        const newPassword = this.newPasswordRef.current?.value;
        if (!oldPassword || oldPassword.length < 8 || !newPassword || newPassword.length < 8) {
            toast.error("Please provide a stronger password");
            return;
        }
        this.props.changePassword({ oldPassword, newPassword });
    };
}

const authenticationSelector = container.resolve(AuthenticationSelector);
const authenticationActionCreator = container.resolve(AuthenticationActionCreator);

const mapStateToProps = (state: ApplicationReduxState): IStateProps => {
    return {
        isSaving: authenticationSelector.selectIsSavingUser(state),
        savingError: authenticationSelector.selectIsSavingUserError(state),
        userId: authenticationSelector.selectUserId(state),
    };
};

const mapDispatchToProps: IDispatchProps = {
    changePassword: authenticationActionCreator.changePassword,
};

export const ChangePassword = connect(mapStateToProps, mapDispatchToProps)(ChangePasswordComponent);
