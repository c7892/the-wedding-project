import React, { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { container } from "tsyringe";
import { AuthenticationSelector } from "../../../store/selectors";
import { useDispatch, useSelector } from "react-redux";
import { AuthenticationActionCreator } from "../../../store";
import { toast } from "react-toastify";

export const DeleteAccountComponent = () => {
    const [showModal, setShowModal] = useState(false);
    const authenticationSelector = container.resolve(AuthenticationSelector);
    const authenticationActionCreator = container.resolve(AuthenticationActionCreator);
    const isGuest = useSelector(authenticationSelector.selectIsGuest);
    const errorMessage = useSelector(authenticationSelector.selectIsSavingUserError);
    const dispatch = useDispatch();
    useEffect(() => {
        if (errorMessage) {
            toast.error(errorMessage);
        }
    }, [errorMessage]);
    return (
        <>
            <h4>Danger Zone!!</h4>
            <hr />
            <Button
                className="float-left"
                variant="danger"
                onClick={() => {
                    setShowModal(true);
                }}>
                Delete Account
            </Button>
            <Modal
                show={showModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                onHide={() => {
                    setShowModal(false);
                }}>
                <Modal.Header closeButton>
                    <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Are you sure you want to delete your account?</h4>
                    <p>
                        This is an irreversible action and cannot be undone once completed, and you will lose access to
                        all resources. As a {isGuest ? "guest" : "spouse"}{" "}
                        {isGuest
                            ? "you will be un-rsvp'd from each event you took part in, the wedding couple will be notified of your departure to help their continued planning efforts."
                            : "all your (and spouse) data (events, guest lists, invites, etc.) will be deleted, your guests will also be notified of their accounts being deleted as well."}
                        &nbsp;Thank you for using our services, and we hope we could make this part of life&apos;s
                        journey easier.
                    </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="danger"
                        onClick={() => {
                            dispatch(authenticationActionCreator.deleteAccount());
                            setShowModal(false);
                        }}>
                        Continue to deleting account
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};
