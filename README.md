
# The Wedding Project

The Wedding Project is passion project developed by TreJon House of [Coding House](https://codinghouse.dev/) The purpose of this project was and is to provide an alternative solution to products such as  [The Knot](https://www.theknot.com/) This project does not seek to replace any mainstream solution or provide the same depth and breadth of functionality. My goal was to create a web app my spouse and I could use that had only what we needed and wanted, very low level of overhead and we share among our guests, also it was a way I could deeply contribute to our wedding. I am making this project free* and open-source so it can be used freely by the community who is looking for a lightweight solution that won't break the bank. As time goes on I will try to maintain the project as best I can, so to the fellow OSS community, please reach out if you'd like to contribute.

\* - While this application is free to use, some features (i.e. sending mail invites) come at a cost to cover the expense of this service


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Setup
The project is built on a few technological assumptions:

 - Uses Azure for secret management (you can provide your own configuration provider if not using azure)
 - Uses CloudinaryAPI for media CDN
 - LOB for mailing post card invitations
 - Stripe for payment processing
 - NodeMailer for sending email and sms/mms

 With the exception of payment processing most if not all the techs above can be hot swapped for other technologies since the application uses [Tsyringe](https://github.com/microsoft/tsyringe) and you can inject your own services.

### The API

 1. run `npm i`
 2. Setup your configs (see env/dev.json and Configuration.ts for examples)
 3. run `npm run dev` or whatever environment you wish to run

### The Frontend

 1. run `npm i`
 2. Setup your configs (see env/dev.json)
 3. run `npm start`

Both applications can be built via `npm run build` and the api is also setup to serve the react app as well if you place the built react bundle in the same directory as the  api.

## Contributing
I am currently open to any and all help with contributing to this project. I currently use Taiga to manage this project, so anyone willing to help out, I would in return set them up in Taiga. But if you're looking to contribute I can be reached at [trejon.house@codinghouse.dev](trejon.house@codinghouse.dev)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT LICENSE

## Project status
I am currently maintaining this repo until my own marriage takes place in a little under a year. Then afterwards I plan to only fix major issues, unless I get a lot of noise to make major investments into the project.
