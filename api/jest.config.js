// eslint-disable-next-line no-undef
module.exports = {
    roots: ["<rootDir>/src"],
    transform: {
        "^.+\\.tsx?$": "ts-jest",
        "^.+\\.js$": "babel-jest",
    },
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|tsx?)$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    collectCoverage: true,
    collectCoverageFrom: [
        "**/*.{ts,tsx}",
        "!**/__tests__",
        "!src/jest-setup-file.ts",
        "!**/node_modules/**"
    ],
    coverageDirectory: "./jest-coverage",
    setupFilesAfterEnv: ["./src/jest-setup-file.ts"],
    moduleDirectories: ["node_modules"],
};
