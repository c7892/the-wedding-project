import moment from "moment";
import { LeanDocument } from "mongoose";
import { inject, singleton } from "tsyringe";
import { IEventModel, IEventSchema, IUserModel } from "../dal";
import { Event } from "../types";
import { parse } from "node-html-parser";
import { isEmpty } from "lodash";
import { AssetService } from "./AssetService";

@singleton()
export class EventsService {
    public constructor(
        @inject("IEventModel") private readonly eventModel: IEventModel,
        @inject("IUserModel") private readonly userModel: IUserModel,
        private readonly assetService: AssetService,
    ) {}

    public saveEventAsync = async (
        uniqueWeddingCode: string,
        eventToSave: Event,
    ): Promise<Event | undefined | LeanDocument<IEventSchema>> => {
        try {
            eventToSave = await this.replaceImageDataWithCDNAsync(eventToSave);
            if ((eventToSave as IEventSchema)._id) {
                await this.findAndDeleteEventImages((eventToSave as IEventSchema)._id);
                const saved = await this.eventModel
                    .findOneAndUpdate(
                        { _id: (eventToSave as IEventSchema)._id, ownerWeddingCode: uniqueWeddingCode },
                        { ...eventToSave },
                    )
                    .lean();
                return saved ? saved : undefined;
            }
            const event = new this.eventModel(eventToSave);
            event.ownerWeddingCode = uniqueWeddingCode;
            event.dateCreated = Date.now();
            return await event.save();
        } catch (ex) {
            console.log(ex);
            return undefined;
        }
    };
    private findAndDeleteEventImages = async (id: string): Promise<void> => {
        try {
            const event = await this.eventModel.findById(id).lean();
            if (!event) {
                return;
            }
            const description = event.description;
            const root = parse(description);
            const imgTags = root.querySelectorAll("img");
            if (isEmpty(imgTags)) {
                return;
            }
            for (const img of imgTags) {
                const src = img.getAttribute("src");
                if (!src || !src.includes("cloudinary")) {
                    continue;
                }
                await this.assetService.deleteImageFromURLAsync(src);
            }
        } catch (ex) {
            // do nothing
        }
    };

    public deleteEvent = async (id: string): Promise<Event | undefined | LeanDocument<IEventSchema>> => {
        try {
            await this.findAndDeleteEventImages(id);
            return await this.eventModel.findByIdAndDelete(id).lean();
        } catch (ex) {
            return undefined;
        }
    };

    public fetchEventsAsync = async (uniqueWeddingCode: string, date?: string): Promise<Event[]> => {
        try {
            if (!date) {
                return await this.eventModel
                    .find({ ownerWeddingCode: uniqueWeddingCode })
                    .sort({ start: "ascending" })
                    .lean();
            }
            const momentDate = moment(date);
            const beginning = momentDate.startOf("month").toDate();
            const end = momentDate.add(1, "month").toDate();
            return await this.eventModel
                .find({
                    $and: [{ ownerWeddingCode: uniqueWeddingCode }, { start: { $gte: beginning, $lt: end } }],
                })
                .sort({ start: "ascending" })
                .lean();
        } catch (ex) {
            return [];
        }
    };
    public fetchAllEventsForUserAndWedding = async (uniqueWeddingCode: string, userId: string): Promise<Event[]> => {
        try {
            const user = await this.userModel.findById(userId).lean();
            const isOwner = user !== undefined && user !== null;
            return isOwner
                ? await this.eventModel
                      .find({
                          ownerWeddingCode: uniqueWeddingCode,
                      })
                      .sort({ start: "asc" })
                      .lean()
                : await this.eventModel
                      .find({
                          $and: [
                              {
                                  ownerWeddingCode: uniqueWeddingCode,
                              },
                              {
                                  guests: { $in: [userId] },
                              },
                          ],
                      })
                      .sort({ start: "asc" })
                      .lean();
        } catch (ex) {
            return [];
        }
    };

    public fetchWeddingDay = async (uniqueWeddingCode: string): Promise<Event | undefined> => {
        try {
            return await this.eventModel
                .findOne({
                    $and: [{ ownerWeddingCode: uniqueWeddingCode }, { isWedding: true }],
                })
                .lean();
        } catch (ex) {
            return undefined;
        }
    };
    public fetchEventAsync = async (id: string, guestId: string): Promise<Event | undefined> => {
        try {
            return await this.eventModel
                .findOne({
                    $and: [{ _id: id }, { guests: { $in: [guestId] } }],
                })
                .lean();
        } catch (ex) {
            return undefined;
        }
    };

    private replaceImageDataWithCDNAsync = async (event: Event): Promise<Event> => {
        const description = event.description;
        const root = parse(description);
        const imgTags = root.querySelectorAll("img");
        if (isEmpty(imgTags)) {
            return event;
        }
        for (const img of imgTags) {
            const src = img.getAttribute("src");
            if (!src || !src.includes("base64")) {
                continue;
            }
            const newSrc = await this.assetService.saveImageAsync(src);
            img.setAttribute("src", newSrc ?? src);
            root.exchangeChild(img, img);
        }
        event.description = root.toString();
        return event;
    };
}
