import { inject, singleton } from "tsyringe";
import { IEventModel } from "../dal";
import { Event } from "../types";

@singleton()
export class GuestService {
    public constructor(@inject("IEventModel") private readonly eventModel: IEventModel) {}

    public getEvents = async (guestId: string): Promise<Event[]> => {
        try {
            return this.eventModel.find({ guests: { $in: [guestId] } }).lean();
        } catch (ex) {
            return [];
        }
    };

    public rsvp = async (
        eventId: string,
        guestId: string,
        numberOfGuests: number,
    ): Promise<Event | undefined | null> => {
        try {
            const event = await this.eventModel
                .findOne({ $and: [{ _id: eventId }, { guests: { $in: [guestId] } }] })
                .lean();
            if (!event) {
                return undefined;
            }
            const index = event.rsvpList.findIndex((entry) => entry.hostingGuest === guestId);
            const rsvpList = event.rsvpList;
            if (index >= 0) {
                rsvpList.splice(index, 1);
            }
            rsvpList.push({ hostingGuest: guestId, totalBringingWith: numberOfGuests });
            return await this.eventModel.findByIdAndUpdate(eventId, { rsvpList }).lean();
        } catch (ex) {
            return undefined;
        }
    };
    public unrsvp = async (eventId: string, guestId: string): Promise<Event | undefined | null> => {
        try {
            const event = await this.eventModel
                .findOne({ $and: [{ _id: eventId }, { guests: { $in: [guestId] } }] })
                .lean();
            if (!event) {
                return undefined;
            }
            const index = event.rsvpList.findIndex((entry) => entry.hostingGuest === guestId);
            const rsvpList = event.rsvpList;
            if (index >= 0) {
                rsvpList.splice(index, 1);
            }
            return await this.eventModel.findByIdAndUpdate(eventId, { rsvpList }).lean();
        } catch (ex) {
            return undefined;
        }
    };
}
