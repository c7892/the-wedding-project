import path from "path";
import { readFileSync } from "fs";
import { singleton } from "tsyringe";
import glob from "glob";

@singleton()
export class FileHandler {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}

    public readFileAsync = async (fileName: string): Promise<string | undefined> => {
        return new Promise((resolve) => {
            this.getDirectories("./", (err, results) => {
                if (err) {
                    resolve(undefined);
                }
                for (let i = 0; i < results.length; i++) {
                    const match = results[i];
                    if (!match.includes(fileName)) {
                        continue;
                    }
                    try {
                        const filePath = path.resolve(match);
                        const fileContents = readFileSync(filePath);
                        resolve(fileContents.toString("utf-8"));
                    } catch (err) {
                        resolve(undefined);
                    }
                    return;
                }
            });
        });
    };
    private getDirectories = (src: string, callback: (err: Error | null, matches: string[]) => void) => {
        glob(src + "/**/*", callback);
    };
}
