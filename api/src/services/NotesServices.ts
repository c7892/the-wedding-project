import { LeanDocument } from "mongoose";
import { inject, singleton } from "tsyringe";
import { INotesSchema, INotesModel, IUserModel } from "../dal";
import { Notes } from "../types";
import { parse } from "node-html-parser";
import { isEmpty } from "lodash";
import { AssetService } from "./AssetService";

@singleton()
export class NotesService {
    public constructor(
        @inject("INotesModel") private readonly notesModel: INotesModel,
        @inject("IUserModel") private readonly userModel: IUserModel,
        private readonly assetService: AssetService,
    ) {}

    public saveNotesAsync = async (
        uniqueWeddingCode: string,
        notesToSave: Notes,
    ): Promise<Notes | undefined | LeanDocument<INotesSchema>> => {
        try {
            notesToSave = await this.replaceImageDataWithCDNAsync(notesToSave);
            if ((notesToSave as INotesSchema)._id) {
                await this.findAndDeleteNotesImages((notesToSave as INotesSchema)._id);
                const saved = await this.notesModel
                    .findOneAndUpdate(
                        { _id: (notesToSave as INotesSchema)._id, ownerWeddingCode: uniqueWeddingCode },
                        { ...notesToSave },
                    )
                    .lean();
                return saved ? saved : undefined;
            }
            const notes = new this.notesModel(notesToSave);
            notes.ownerWeddingCode = uniqueWeddingCode;
            notes.dateCreated = Date.now();
            return await notes.save();
        } catch (ex) {
            console.log(ex);
            return undefined;
        }
    };
    private findAndDeleteNotesImages = async (id: string): Promise<void> => {
        try {
            const notes = await this.notesModel.findById(id).lean();
            if (!notes) {
                return;
            }
            const description = notes.details;
            const root = parse(description);
            const imgTags = root.querySelectorAll("img");
            if (isEmpty(imgTags)) {
                return;
            }
            for (const img of imgTags) {
                const src = img.getAttribute("src");
                if (!src || !src.includes("cloudinary")) {
                    continue;
                }
                await this.assetService.deleteImageFromURLAsync(src);
            }
        } catch (ex) {
            // do nothing
        }
    };

    public deleteNotes = async (id: string): Promise<Notes | undefined | LeanDocument<INotesSchema>> => {
        try {
            await this.findAndDeleteNotesImages(id);
            return await this.notesModel.findByIdAndDelete(id).lean();
        } catch (ex) {
            return undefined;
        }
    };

    public fetchNotesAsync = async (id: string): Promise<Notes | undefined> => {
        try {
            return await this.notesModel.findById(id).lean();
        } catch (ex) {
            return undefined;
        }
    };
    public fetchAllNotesAsync = async (uniqueWeddingCode: string, isPublished?: boolean): Promise<Notes[]> => {
        try {
            return isPublished !== undefined
                ? await this.notesModel
                      .find({
                          $and: [{ ownerWeddingCode: uniqueWeddingCode }, { isPublished }],
                      })
                      .lean()
                : await this.notesModel.find({ ownerWeddingCode: uniqueWeddingCode }).lean();
        } catch (ex) {
            return [];
        }
    };

    private replaceImageDataWithCDNAsync = async (event: Notes): Promise<Notes> => {
        const description = event.details;
        const root = parse(description);
        const imgTags = root.querySelectorAll("img");
        if (isEmpty(imgTags)) {
            return event;
        }
        for (const img of imgTags) {
            const src = img.getAttribute("src");
            if (!src || !src.includes("base64")) {
                continue;
            }
            const newSrc = await this.assetService.saveImageAsync(src);
            img.setAttribute("src", newSrc ?? src);
            root.exchangeChild(img, img);
        }
        event.details = root.toString();
        return event;
    };
}
