import { inject, singleton } from "tsyringe";
import { IRegistryModel } from "../dal";
import { Registry } from "../types";
import { parse } from "node-html-parser";
import { isEmpty } from "lodash";
import { AssetService } from "./AssetService";

@singleton()
export class RegistryService {
    public constructor(
        @inject("IRegistryModel") private readonly registryModel: IRegistryModel,
        private readonly assetService: AssetService,
    ) {}

    public getRegistryAsync = async (weddingCode: string): Promise<Registry> => {
        try {
            let registry = await this.registryModel.findOne({ ownerWeddingCode: weddingCode });
            if (!registry) {
                registry = new this.registryModel();
                registry.miscLinks = [];
                registry.items = [];
                registry.note = "A note to your guests...";
                registry.ownerWeddingCode = weddingCode;
                registry.dateCreated = Date.now();
                registry.modifiedDttm = Date.now();
                registry = await registry.save();
            }
            return registry;
        } catch (ex) {
            return {
                note: "",
                miscLinks: [],
                items: [],
                ownerWeddingCode: weddingCode,
                dateCreated: Date.now(),
                modifiedDttm: Date.now(),
            };
        }
    };

    public updateRegistry = async (weddingCode: string, updates: Registry): Promise<Registry> => {
        try {
            const cdnSafeNote = await this.replaceImageDataWithCDNAsync(updates);
            const registry = await this.registryModel.findOneAndUpdate(
                { ownerWeddingCode: weddingCode },
                { ...cdnSafeNote, modifiedDttm: Date.now() },
                { upsert: true, new: true },
            );
            if (!registry) {
                throw new Error();
            }
            return registry;
        } catch (ex) {
            return {
                note: "",
                miscLinks: [],
                items: [],
                ownerWeddingCode: weddingCode,
                dateCreated: Date.now(),
                modifiedDttm: Date.now(),
            };
        }
    };
    private replaceImageDataWithCDNAsync = async (registry: Registry): Promise<Registry> => {
        const description = registry.note;
        const root = parse(description);
        const imgTags = root.querySelectorAll("img");
        if (isEmpty(imgTags)) {
            return registry;
        }
        for (const img of imgTags) {
            const src = img.getAttribute("src");
            if (!src || !src.includes("base64")) {
                continue;
            }
            const newSrc = await this.assetService.saveImageAsync(src);
            img.setAttribute("src", newSrc ?? src);
            root.exchangeChild(img, img);
        }
        registry.note = root.toString();
        return registry;
    };
}
