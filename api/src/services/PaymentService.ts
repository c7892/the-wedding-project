import { first } from "lodash";
import Stripe from "stripe";
import { inject, singleton } from "tsyringe";
import { IUserModel } from "../dal";
import { Address, AddressValidator, IConfig } from "../utilities";
import { SalesTax } from "../utilities";

@singleton()
export class PaymentService {
    private readonly paymentProcessor: Stripe;
    public constructor(
        @inject("Config") private readonly config: IConfig,
        @inject("IUserModel") private readonly userModel: IUserModel,
        private readonly addressValidator: AddressValidator,
        private readonly salesTax: SalesTax,
    ) {
        this.paymentProcessor = new Stripe(config.Secrets.StripeKey, {
            apiVersion: "2020-08-27",
        });
    }

    private calculateOrderAmount = async (items: number, costPer: number, address: Address) => {
        if (items === 0) {
            return 0;
        }
        this.salesTax.setTaxOriginCountry("US");
        const costWOTax = items * costPer;
        const withTax = await this.salesTax.getAmountWithSalesTax("US", address.stateAbbr.toUpperCase(), costWOTax);
        const cost = withTax.total + 0.3 / (1 - 0.029);
        if (cost < 0) {
            return 0;
        }
        return parseInt((cost * 100).toFixed(2));
    };
    private calculateSmsOrderAmount = async (items: number, address: Address) =>
        this.calculateOrderAmount(items, this.config.SmsCost, address);

    private calculatePostageOrderAmount = async (items: number, address: Address) =>
        this.calculateOrderAmount(items, this.config.PostageCost, address);

    public createPaymentIntent = async (
        userId: string,
        postCards: number,
        texts: number,
    ): Promise<{
        clientSecret: string | null;
        cost: number;
        error?: string;
        smsCost: number;
        postageCost: number;
    } | null> => {
        if (postCards <= 0 && texts <= 0) {
            return null;
        }
        const user = await this.userModel.findById(userId);
        let address: Address | undefined;
        if (!user) {
            return {
                clientSecret: "",
                cost: 0,
                error: "Failed to locate your account, please try again later",
                postageCost: 0,
                smsCost: 0,
            };
        }
        let customer: Stripe.Customer | Stripe.DeletedCustomer | undefined;
        if (user.customerCode) {
            customer = await this.paymentProcessor.customers.retrieve(user.customerCode);
        } else {
            if (user.address) {
                const addresses = await this.addressValidator.validate(user.address);
                address = first((addresses?.exact ?? []).concat(addresses?.inexact ?? []));
            } else {
                return {
                    clientSecret: "",
                    cost: 0,
                    error: "Please submit your address in the settings page before continuing",
                    postageCost: 0,
                    smsCost: 0,
                };
            }
            customer = await this.paymentProcessor.customers.create({
                email: user.email,
                name: `${user.firstName} ${user.surname}`,
                phone: user.mobile,
                address: address
                    ? {
                          city: address.city,
                          country: address.country,
                          line1: address.street,
                          line2: address.subpremise,
                          postal_code: address.postalCode,
                          state: address.state,
                      }
                    : undefined,
            });
            if (customer) {
                this.userModel.findByIdAndUpdate(userId, { $set: { customerCode: customer.id } }).exec();
            }
        }
        if (!address && user.address) {
            const addresses = await this.addressValidator.validate(user.address);
            address = first((addresses?.exact ?? []).concat(addresses?.inexact ?? []));
        }
        if (!address) {
            return {
                clientSecret: "",
                cost: 0,
                error: "Please submit your address in the settings page before continuing",
                postageCost: 0,
                smsCost: 0,
            };
        }
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const postageCost = await this.calculatePostageOrderAmount(postCards, address!);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const smsCost = await this.calculateSmsOrderAmount(texts, address!);
        const cost = postageCost + smsCost;
        try {
            const paymentIntent = await this.paymentProcessor.paymentIntents.create({
                customer: customer?.id,
                amount: cost,
                currency: "usd",
                setup_future_usage: "off_session",
                payment_method_types: ["card"],
            });
            return { clientSecret: paymentIntent.client_secret, cost, smsCost, postageCost };
        } catch (ex) {
            return {
                clientSecret: "",
                cost: 0,
                error: "Failed to generate request, please try again",
                postageCost: 0,
                smsCost: 0,
            };
        }
    };

    public verifyPaymentPayload = async (payload: Stripe.PaymentIntent): Promise<boolean> => {
        try {
            const retrievedPI = await this.paymentProcessor.paymentIntents.retrieve(payload.id);
            if (!retrievedPI) {
                return false;
            }
            const { customer, status } = retrievedPI;
            if (!customer) {
                return false;
            }
            const spouse = await this.userModel.findOne({ customerCode: customer.toString() }).lean();
            if (!spouse) {
                return false;
            }
            if (status !== "processing" && status !== "succeeded") {
                return false;
            }
            return true;
        } catch (ex) {
            return false;
        }
    };
}
