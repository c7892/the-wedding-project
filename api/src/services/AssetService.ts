import { inject, singleton } from "tsyringe";
import { IConfig } from "../utilities";
import { v2 as cloudinary } from "cloudinary";

@singleton()
export class AssetService {
    private readonly cloundinaryUploader = cloudinary;
    public constructor(@inject("Config") private readonly config: IConfig) {
        this.cloundinaryUploader.config({
            cloud_name: config.Secrets.CloudinaryName,
            api_key: config.Secrets.CloudinaryKey,
            api_secret: config.Secrets.CloudinarySecret,
        });
    }

    public saveImageAsync = async (imageData: string, tags?: string[]) => {
        try {
            const cloudinaryUrl = await this.cloundinaryUploader.uploader.upload(imageData, { tags });
            return cloudinaryUrl?.secure_url;
        } catch (ex) {
            return undefined;
        }
    };

    public deleteImageFromURLAsync = async (url: string) => {
        try {
            const index = url.lastIndexOf("/") + 1;
            const id = url.substring(index, url.length - 4);
            await this.deleteImageByIdAsync(id);
        } catch (ex) {
            return undefined;
        }
    };
    public deleteImageByIdAsync = async (id: string) => {
        try {
            await this.cloundinaryUploader.uploader.destroy(id);
        } catch (ex) {
            // do nothing
        }
    };
    public zipImagesAsync = async (tags: string | string[]): Promise<string> => {
        const response = await this.cloundinaryUploader.uploader.create_zip({ tags });
        return response.secure_url;
    };
}
