import { unlink, writeFileSync } from "fs";
import { LeanDocument } from "mongoose";
import sharp from "sharp";
import { inject, singleton } from "tsyringe";
import { String } from "typescript-string-operations";
import { IEventModel, IGuestModel } from "../dal";
import { IInviteModel, IInviteSchema } from "../dal/models/InviteModel";
import { Guest, Invite } from "../types";
import { getFileLocationOnDisk, IConfig } from "../utilities";
import { AssetService } from "./AssetService";
import { CommunicationService } from "./CommunicationService";
import { FileHandler } from "./FileHandler";
import { Guid } from "guid-typescript";
import Stripe from "stripe";
import { PaymentService } from "./PaymentService";

@singleton()
export class InviteService {
    public constructor(
        @inject("IInviteModel") private readonly inviteModel: IInviteModel,
        private readonly assetService: AssetService,
        @inject("Config") private readonly config: IConfig,
        @inject("IEventModel") private readonly eventModel: IEventModel,
        private readonly fileHandler: FileHandler,
        private readonly communicationService: CommunicationService,
        @inject("IGuestModel") private readonly guestModel: IGuestModel,
        private readonly paymentService: PaymentService,
    ) {}

    public saveInviteAsync = async (
        uniqueWeddingCode: string,
        inviteToSave: Invite,
    ): Promise<Event | undefined | LeanDocument<IInviteSchema>> => {
        try {
            inviteToSave = await this.replaceImagesWithCdnAsync(inviteToSave);
            if ((inviteToSave as IInviteSchema)._id) {
                await this.findAndDeleteInviteImages((inviteToSave as IInviteSchema)._id);
                const saved = await this.inviteModel
                    .findOneAndUpdate(
                        { _id: (inviteToSave as IInviteSchema)._id, ownerWeddingCode: uniqueWeddingCode },
                        { ...inviteToSave },
                    )
                    .lean();
                return saved ? saved : undefined;
            }
            const invite = new this.inviteModel(inviteToSave);
            invite.ownerWeddingCode = uniqueWeddingCode;
            invite.dateCreated = Date.now();
            return await invite.save();
        } catch (ex) {
            console.log(ex);
            return undefined;
        }
    };

    public deleteInvite = async (id: string): Promise<Event | undefined | LeanDocument<IInviteSchema>> => {
        try {
            await this.findAndDeleteInviteImages(id);
            return await this.inviteModel.findByIdAndDelete(id).lean();
        } catch (ex) {
            return undefined;
        }
    };

    public fetchInvitesAsync = async (uniqueWeddingCode: string): Promise<Invite[]> => {
        try {
            return await this.inviteModel.find({ ownerWeddingCode: uniqueWeddingCode }).lean();
        } catch (ex) {
            return [];
        }
    };

    private findAndDeleteInviteImages = async (id: string): Promise<void> => {
        try {
            const invite = await this.inviteModel.findById(id).lean();
            if (!invite) {
                return;
            }
            if (invite.frontImage) {
                await this.assetService.deleteImageFromURLAsync(invite.frontImage);
            }
            if (invite.backImage) {
                await this.assetService.deleteImageFromURLAsync(invite.backImage);
            }
        } catch (ex) {
            // do nothing
        }
    };

    private replaceImagesWithCdnAsync = async (invite: Invite): Promise<Invite> => {
        if (invite.backImage && !invite.backImage.includes("https://")) {
            invite.backImage = await this.assetService.saveImageAsync(invite.backImage);
        }
        if (invite.frontImage && !invite.frontImage.includes("https://")) {
            invite.frontImage = await this.assetService.saveImageAsync(invite.frontImage);
        }
        return invite;
    };

    public sendConfirmationReceipt = async (name: string, email: string, inviteId: string, guestIds: string[]) => {
        const invite = await this.inviteModel.findById(inviteId).lean();
        const guests = await this.guestModel
            .find({ _id: { $in: guestIds } })
            .sort({ surname: "asc" })
            .lean();
        const lis = guests.map((guest) => {
            return `<li>${guest.firstName} ${guest.surname}</li>`;
        });
        const emailBody = await this.fileHandler.readFileAsync("spouse.invitations.sent.html");
        const formattedEmail = String.Format(emailBody ?? "", name, invite?.inviteTitle ?? "", lis.join(""));
        this.communicationService.sendEmail({
            html: formattedEmail,
            recipients: email,
            subject: "Invitations Confirmation",
        });
    };

    public sendAsync = async (
        front: string,
        back: string,
        inviteId: string,
        guest: Guest,
        sendMethods: string[],
        paymentPayload?: Stripe.PaymentIntent,
    ): Promise<boolean> => {
        const loginRoot = `${this.config.ClientEndpoint}login`;
        const guestName = `${guest.firstName} ${guest.surname}`;
        const inviteObj = await this.inviteModel.findById(inviteId).lean();
        const emailBody = await this.fileHandler.readFileAsync("guest.invitation.html");
        const formattedEmail = String.Format(
            emailBody ?? "",
            guestName,
            guest.firstName,
            inviteObj?.title,
            front,
            back,
            loginRoot,
        );
        let smsFront: string | undefined;
        let smsBack: string | undefined;
        let paymentVerified = false;
        for (const method of sendMethods) {
            if (method.toLowerCase() === "email") {
                this.communicationService.sendEmail({
                    html: formattedEmail,
                    recipients: guest.email ?? "",
                    subject: guestName + " - " + inviteObj?.title,
                });
            }
            if (!paymentPayload) {
                continue;
            }
            if (!paymentVerified) {
                paymentVerified = await this.paymentService.verifyPaymentPayload(paymentPayload);
                if (!paymentVerified) {
                    continue;
                }
            }
            if (method.toLowerCase() === "sms") {
                if (!smsFront) {
                    smsFront = await this.assetService.saveImageAsync(front, [inviteId]);
                }
                if (!smsBack) {
                    smsBack = await this.assetService.saveImageAsync(back, [inviteId]);
                }
                this.communicationService.sendMMS(
                    inviteObj?.title ??
                        `You're Invited to ${inviteObj?.title}, login at ${loginRoot} for more details and to RSVP.`,
                    [guest.mobile],
                    inviteObj?.title ?? "You're Invited",
                    [smsFront ?? "", smsBack ?? ""],
                );
            }
            if (method.toLowerCase() === "postal mail" && guest.address) {
                try {
                    const dir = Guid.create().toString();
                    const frontBase64Data = front.replace(/^data:image\/png;base64,/, "");
                    const frontLocation = getFileLocationOnDisk(dir, "./front.png");
                    const resizedFrontLocation = getFileLocationOnDisk(dir, "./resizedFront.jpg");
                    writeFileSync(frontLocation, frontBase64Data, "base64");
                    await sharp(frontLocation)
                        .resize(3375, 1875)
                        .jpeg({ quality: 100, mozjpeg: true })
                        .toFile(resizedFrontLocation);
                    const backBase64Data = back.replace(/^data:image\/png;base64,/, "");
                    const backLocation = getFileLocationOnDisk(dir, "./back.png");
                    const resizedBackLocation = getFileLocationOnDisk(dir, "./resizedBack.jpg");
                    writeFileSync(backLocation, backBase64Data, "base64");
                    await sharp(backLocation)
                        .resize(3375, 1875)
                        .jpeg({ quality: 100, mozjpeg: true })
                        .toFile(resizedBackLocation);
                    const frontPath = await this.assetService.saveImageAsync(resizedFrontLocation);
                    const backPath = await this.assetService.saveImageAsync(resizedBackLocation);
                    unlink(frontLocation, () => {
                        // do nothing
                    });
                    unlink(backLocation, () => {
                        // do nothing
                    });
                    if (!frontPath || !backPath) {
                        unlink(resizedFrontLocation, () => {
                            this.assetService.deleteImageFromURLAsync(frontPath ?? "");
                        });
                        unlink(resizedBackLocation, () => {
                            this.assetService.deleteImageFromURLAsync(backPath ?? "");
                        });
                        continue;
                    }
                    this.communicationService
                        .sendPostcard(
                            `${inviteObj?.inviteTitle} Postcard`,
                            frontPath,
                            backPath,
                            guest.address,
                            guestName,
                        )
                        .then(() => {
                            unlink(resizedFrontLocation, () => {
                                this.assetService.deleteImageFromURLAsync(frontPath);
                            });
                            unlink(resizedBackLocation, () => {
                                this.assetService.deleteImageFromURLAsync(backPath);
                            });
                        })
                        .catch((ex) => {
                            console.log(ex);
                        });
                } catch (ex) {
                    console.log(ex);
                }
            }
        }
        setTimeout(() => {
            if (smsFront) {
                this.assetService.deleteImageFromURLAsync(smsFront);
            }
            if (smsBack) {
                this.assetService.deleteImageFromURLAsync(smsBack);
            }
        }, 5000);
        return true;
    };
}
