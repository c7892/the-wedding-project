import { createTransport } from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { CARRIERS } from "./Carriers";
import { SMTP_SERVICES } from "./Smtp";

const assembleRecipients = (phoneNumber: string, region?: string) => {
    let allRecipients;
    if (!region) {
        region = "us";
    }
    switch (region.trim().toLowerCase()) {
        case "eu":
            allRecipients = assembler(phoneNumber, CARRIERS.EU);
            break;
        case "can":
            allRecipients = assembler(phoneNumber, CARRIERS.CAN);
            break;
        case "mex":
            allRecipients = assembler(phoneNumber, CARRIERS.MEX);
            break;
        case "as":
            allRecipients = assembler(phoneNumber, CARRIERS.AS);
            break;
        case "aus":
            allRecipients = assembler(phoneNumber, CARRIERS.AUS);
            break;
        case "afr":
            allRecipients = assembler(phoneNumber, CARRIERS.AFR);
            break;
        case "sa":
            allRecipients = assembler(phoneNumber, CARRIERS.SA);
            break;
        default:
            allRecipients = assembler(phoneNumber, CARRIERS.USA);
            break;
    }
    return allRecipients;
};

const assembler = (phoneNumber: string, region: { [key: string]: string }) => {
    let recipient = "";
    const carriers = Object.keys(region);
    const recipients: string[] = [];
    carriers.forEach((carrier) => {
        const gateway: string | undefined = region[carrier];
        if (gateway) {
            const rec = gateway.replace(/number/g, phoneNumber);
            recipient += rec + ",";
            recipients.push(rec);
        }
    });
    return { recipient: recipient.substr(0, recipient.length - 1), recipients: recipients };
};

const findSmtpServerice = (host: string) => {
    switch (host.trim().toLowerCase()) {
        case "yahoo":
            return SMTP_SERVICES.yahoo;
        case "outlook":
            return SMTP_SERVICES.outlook;
        case "aol":
            return SMTP_SERVICES.aol;
        case "icloud":
            return SMTP_SERVICES.icloud;
        case "hotmail":
            return SMTP_SERVICES.hotmail;
        case "live":
            return SMTP_SERVICES.live;
        case "namecheap":
            return SMTP_SERVICES.namecheap;
        default:
            return SMTP_SERVICES.google;
    }
};

export interface SmsOptions {
    phoneNumber: string;
    region?: string;
    host: string;
    message: string;
    email: string;
    password: string;
    subject?: string;
    attachments?: Mail.Attachment[];
}
export const sendSMSMessage = (options: SmsOptions) => {
    const recipients = assembleRecipients(options.phoneNumber, options.region);
    const smtpService = findSmtpServerice(options.host);
    const SMTP_TRANSPORT = {
        host: smtpService.server,
        port: smtpService.port,
        auth: {
            user: options.email,
            pass: options.password,
        },
        secureConnection: "false",
        tls: {
            ciphers: "SSLv3",
        },
    };
    const transporter = createTransport(SMTP_TRANSPORT);
    recipients.recipients.forEach(async (to) => {
        const mailOptions = {
            to,
            subject: options.subject,
            text: options.message,
            from: options.email,
        };
        try {
            transporter
                .sendMail(mailOptions)
                .then(() => {
                    // do nothing
                })
                .catch((err) => {
                    console.log("failed", err);
                });
        } catch (ex) {
            console.log("Failed to send mail", ex);
        }
    });
};

export const sendMMSMessage = (options: SmsOptions) => {
    const recipients = assembleRecipients(options.phoneNumber, options.region);
    const smtpService = findSmtpServerice(options.host);
    const SMTP_TRANSPORT = {
        host: smtpService.server,
        port: smtpService.port,
        auth: {
            user: options.email,
            pass: options.password,
        },
        secureConnection: "false",
        tls: {
            ciphers: "SSLv3",
        },
    };
    const transporter = createTransport(SMTP_TRANSPORT);
    recipients.recipients.forEach(async (to) => {
        const mailOptions = {
            to,
            subject: options.subject,
            text: options.message,
            from: options.email,
            attachments: options.attachments,
        };
        try {
            transporter
                .sendMail(mailOptions)
                .then(() => {
                    // do nothing
                })
                .catch((err) => {
                    console.log("failed", err);
                });
        } catch (ex) {
            console.log("Failed to send mail", ex);
        }
    });
};
