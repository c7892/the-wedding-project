import { first, isEmpty } from "lodash";
import { createTransport } from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { inject, singleton } from "tsyringe";
import { AddressValidator, IConfig } from "../utilities";
import twilio from "twilio";
import Stripe from "stripe";
import { Guest } from "../types";
import { FileHandler } from "./FileHandler";
import { PaymentService } from "./PaymentService";
import { String } from "typescript-string-operations";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const html_to_pdf = require("html-pdf-node");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const inlineBase64 = require("nodemailer-plugin-inline-base64");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const lob = require("lob");

export interface EmailOptions {
    recipients: string | string[];
    html: string;
    subject?: string;
}

@singleton()
export class CommunicationService {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private readonly Lob: any;
    private readonly smsClient: twilio.Twilio;
    public constructor(
        @inject("Config") private readonly config: IConfig,
        private readonly addressValidator: AddressValidator,
        private readonly paymentService: PaymentService,
        private readonly fileHandler: FileHandler,
    ) {
        this.Lob = lob(this.config.Secrets.LobApiKey);
        this.smsClient = twilio(this.config.Secrets.TwilioSID, this.config.Secrets.TwilioToken);
    }

    public sendEmail = async (options: EmailOptions) => {
        const SMTP_TRANSPORT = {
            host: this.config.EmailHost.server,
            port: this.config.EmailHost.port,
            auth: {
                user: this.config.Secrets.Email,
                pass: this.config.Secrets.Password,
            },
            secureConnection: "false",
            tls: {
                ciphers: "SSLv3",
            },
        };
        const transporter = createTransport(SMTP_TRANSPORT);
        const opts = { format: "A4", args: ["--no-sandbox", "--disable-setuid-sandbox", "--use-gl=egl"] };

        const file = { content: options.html };
        const buffer = await html_to_pdf.generatePdf(file, opts);
        const mailOptions: Mail.Options = {
            to: options.recipients,
            subject: options.subject,
            html: options.html,
            from: this.config.Secrets.Email,
            attachments: [
                {
                    // binary buffer as an attachment
                    filename: `${options.subject ?? "The Wedding Project"}.pdf`,
                    content: buffer,
                },
            ],
        };
        try {
            transporter.use("compile", inlineBase64({ cidPrefix: "ch_" }));
            await transporter.sendMail(mailOptions);
        } catch (ex) {
            console.log("Failed to send mail", ex);
        }
    };

    public sendSms = async (message: string, numbers: string[], subject?: string) => {
        numbers = numbers.filter((number) => number.length > 0);
        if (isEmpty(numbers)) {
            return;
        }
        try {
            for (const number of numbers) {
                let safeNumber = number.replace(/-/g, "").trim();
                if (!safeNumber.startsWith("+1")) {
                    safeNumber = `+1${safeNumber}`;
                }
                this.smsClient.messages
                    .create({
                        from: this.config.Secrets.TwilioPhone,
                        to: safeNumber,
                        body: subject ? `${subject}:\n ${message}` : message,
                    })
                    .catch((ex) => {
                        console.log(ex);
                    });
            }
        } catch (ex) {
            console.log("failed to send sms, ", ex);
        }
    };

    public sendMMS = async (message: string, numbers: string[], subject?: string, attachments?: string[]) => {
        numbers = numbers.filter((number) => number.length > 0);
        if (isEmpty(numbers)) {
            return;
        }
        try {
            for (const number of numbers) {
                let safeNumber = number.replace(/-/g, "").trim();
                if (!safeNumber.startsWith("+1")) {
                    safeNumber = `+1${safeNumber}`;
                }
                this.smsClient.messages
                    .create({
                        from: this.config.Secrets.TwilioPhone,
                        to: safeNumber,
                        body: subject ? `${subject}:\n ${message}` : message,
                        mediaUrl: attachments,
                    })
                    .catch((ex) => {
                        console.log(ex);
                    });
            }
        } catch (ex) {
            console.log("failed to send sms, ", ex);
        }
    };

    public sendPostcard = async (description: string, front: string, back: string, address: string, name: string) => {
        try {
            const addy = await this.addressValidator.validate(address);
            if (!addy) {
                return undefined;
            }
            const postcardAddress = first((addy.exact ?? []).concat(addy.inexact ?? []));
            if (!postcardAddress) {
                return undefined;
            }
            return await this.Lob.postcards.create({
                description,
                to: {
                    name,
                    address_line1: `${postcardAddress.streetNumber} ${postcardAddress.street}`.toUpperCase(),
                    address_line2: postcardAddress.subpremise
                        ? `#${postcardAddress.subpremise}`.toUpperCase()
                        : undefined,
                    address_city: postcardAddress.city.toUpperCase(),
                    address_state: postcardAddress.stateAbbr.toUpperCase(),
                    address_zip: postcardAddress.postalCode.toUpperCase(),
                },
                mail_type: "usps_standard",
                size: "6x11",
                front,
                back,
            });
        } catch (ex) {
            console.log("failed to send post card", ex);
            return undefined;
        }
    };

    public sendMessageToGuests = async (
        message: string,
        guest: Guest,
        sendMethods: string[],
        name: string,
        email: string,
        paymentPayload?: Stripe.PaymentIntent,
    ): Promise<boolean> => {
        const guestName = `${guest.firstName} ${guest.surname}`;
        const emailBody = await this.fileHandler.readFileAsync("guest.message.html");
        const formattedEmail = String.Format(emailBody ?? "", guestName, message);
        let paymentVerified = false;
        for (const method of sendMethods) {
            if (method.toLowerCase() === "email") {
                this.sendEmail({
                    html: formattedEmail,
                    recipients: guest.email ?? "",
                    subject: "Message from the wedding couple",
                });
            }
            if (!paymentPayload) {
                continue;
            }
            if (!paymentVerified) {
                paymentVerified = await this.paymentService.verifyPaymentPayload(paymentPayload);
                if (!paymentVerified) {
                    continue;
                }
            }
            if (method.toLowerCase() === "sms") {
                this.sendSms(message, [guest.mobile], "Message from the wedding couple");
                const emailBody = await this.fileHandler.readFileAsync("spouse.message.sent.html");
                const recieptEmail = String.Format(emailBody ?? "", name, guestName);
                this.sendEmail({
                    html: recieptEmail,
                    recipients: email,
                    subject: "Send Message Confirmation",
                });
            }
        }
        return true;
    };
}
