import { inject, injectable } from "tsyringe";
import {
    IEventModel,
    IEventSchema,
    IGalleryModel,
    IGuestModel,
    IUserModel,
    IGuestListModel,
    IImageModel,
} from "../dal";
import { Guest, SpouseRegistrationInformation, User } from "../types";
import { IConfig } from "../utilities";
import { CommunicationService } from "./CommunicationService";
import { FileHandler } from "./FileHandler";
import { String } from "typescript-string-operations";
import { generate } from "randomstring";
import { isEmpty } from "lodash";
import { AssetService } from "./";
import { unlink } from "fs";
import { parse } from "node-html-parser";
import { LeanDocument } from "mongoose";
import { IInviteModel } from "../dal/models/InviteModel";

@injectable()
export class AuthenticationService {
    public constructor(
        @inject("IUserModel") private readonly userModel: IUserModel,
        @inject("IGuestModel") private readonly guestModel: IGuestModel,
        @inject("Config") private readonly config: IConfig,
        private readonly communicationService: CommunicationService,
        private readonly fileHandler: FileHandler,
        private readonly assetService: AssetService,
        @inject("IGalleryModel") private readonly galleryModel: IGalleryModel,
        @inject("IEventModel") private readonly eventModel: IEventModel,
        @inject("IGuestListModel") private readonly guestListModel: IGuestListModel,
        @inject("IInviteModel") private readonly inviteModel: IInviteModel,
        @inject("IImageModel") private readonly imageModel: IImageModel,
    ) {}

    public registerAsync = async (spouses: SpouseRegistrationInformation[]): Promise<boolean | string> => {
        try {
            if (spouses.length !== 2) {
                return `Requires 2 people to register for ${this.config.AppName}`;
            }
            const uniqueWeddingCode = await this.generateWeddingCodeAsync();
            for (let i = 0; i < spouses.length; i++) {
                const spouse = spouses[i];
                let spouseAccount = await this.userModel.findOne({
                    email: spouse.email,
                });
                if (!spouseAccount) {
                    spouseAccount = new this.userModel();
                    spouseAccount.dateCreated = Date.now();
                }
                spouseAccount.email = spouse.email;
                spouseAccount.firstName = spouse.firstName;
                spouseAccount.surname = spouse.surname;
                spouseAccount.newFamilyName = spouse.newFamilyName;
                spouseAccount.setPassword(spouse.password);
                spouseAccount.uniqueWeddingCode = uniqueWeddingCode;
                spouseAccount.modifiedDttm = Date.now();
                await spouseAccount.save();
            }
            this.sendRegistrationEmailAsync(
                spouses.map((spouse) => spouse.firstName),
                spouses.map((spouse) => spouse.email),
            );
            return true;
        } catch (ex) {
            return ex?.message ?? "Failed";
        }
    };

    public getJwtTokenAsync = async (userId: string): Promise<string> => {
        try {
            const user = await this.userModel.findById(userId);
            if (user) {
                return user.generateJwt();
            }
            const guest = await this.guestModel.findById(userId);
            if (guest) {
                return guest.generateJwt();
            }
            return "";
        } catch (error) {
            return "";
        }
    };

    public getUserInfo = async (userId: string): Promise<User | Guest | undefined> => {
        try {
            const user = await this.userModel.findById(userId).select("-salt -hash").lean();
            if (user) {
                return user;
            }
            const guest = await this.guestModel.findById(userId).lean();
            if (guest) {
                return guest;
            }
            return undefined;
        } catch (error) {
            return undefined;
        }
    };
    public getSpouses = async (weddingCode: string): Promise<User[]> => {
        try {
            const user = await this.userModel.find({ uniqueWeddingCode: weddingCode }).select("-salt -hash").lean();
            if (user) {
                return user;
            }
            return [];
        } catch (error) {
            return [];
        }
    };

    public updateUser = async (
        userId: string,
        user: User | Guest,
        image?: Express.Multer.File,
    ): Promise<User | Guest | undefined> => {
        try {
            let profilePic = user.profilePic;
            if (image) {
                if (user.profilePic) {
                    this.assetService.deleteImageFromURLAsync(user.profilePic);
                }
                profilePic = await this.assetService.saveImageAsync(image.path);
                unlink(image.path, () => {
                    // do nothing
                });
            }
            const updatedUser = await this.userModel
                .findByIdAndUpdate(
                    userId,
                    {
                        $set: {
                            firstName: user.firstName,
                            surname: user.surname,
                            email: user.email,
                            mobile: user.mobile,
                            address: user.address,
                            newFamilyName: (user as User).newFamilyName,
                            modifiedDttm: Date.now(),
                            profilePic,
                        },
                    },
                    { new: true },
                )
                .select("-salt -hash")
                .lean();
            if (updatedUser) {
                return updatedUser;
            }
            const guest = await this.guestModel
                .findByIdAndUpdate(
                    userId,
                    {
                        $set: {
                            firstName: user.firstName,
                            surname: user.surname,
                            email: user.email,
                            mobile: user.mobile,
                            address: user.address,
                            modifiedDttm: Date.now(),
                            profilePic,
                        },
                    },
                    { new: true },
                )
                .lean();
            if (guest) {
                return guest;
            }
            return undefined;
        } catch (error) {
            return undefined;
        }
    };
    public changePassword = async (
        userId: string,
        oldPassword: string,
        newPassword: string,
    ): Promise<User | undefined> => {
        try {
            const updatedUser = await this.userModel.findById(userId);
            if (!updatedUser || !updatedUser.validPassword(oldPassword)) {
                return undefined;
            }
            updatedUser.setPassword(newPassword);
            await updatedUser.save();
            if (updatedUser) {
                return updatedUser;
            }
            return undefined;
        } catch (error) {
            return undefined;
        }
    };
    public deleteUser = async (userId: string): Promise<boolean> => {
        try {
            const user = await this.userModel.findById(userId).lean();
            if (!user) {
                return false;
            }
            const spouse = await this.userModel
                .findOne({
                    $and: [{ uniqueWeddingCode: user.uniqueWeddingCode }, { $ne: { _id: user._id } }],
                })
                .lean();
            const guests = await this.guestModel.find({ uniqueWeddingCode: user.uniqueWeddingCode }).lean();
            const galleries = await this.galleryModel.find({ ownerWeddingCode: user.uniqueWeddingCode }).lean();
            galleries.forEach((gallery) => {
                gallery.images.forEach(async (image) => {
                    const deletedImage = await this.imageModel.findByIdAndDelete(image);
                    if (deletedImage) {
                        this.assetService.deleteImageFromURLAsync(deletedImage.src);
                    }
                });
            });
            const events = await this.eventModel.find({ ownerWeddingCode: user.uniqueWeddingCode }).lean();
            events.forEach((event) => {
                this.findAndDeleteEventImages(event);
            });
            this.galleryModel.deleteMany({ ownerWeddingCode: user.uniqueWeddingCode }).exec();
            this.guestModel.deleteMany({ uniqueWeddingCode: user.uniqueWeddingCode }).exec();
            this.guestListModel.deleteMany({ ownerWeddingCode: user.uniqueWeddingCode }).exec();
            this.inviteModel.deleteMany({ ownerWeddingCode: user.uniqueWeddingCode }).exec();
            this.eventModel.deleteMany({ ownerWeddingCode: user.uniqueWeddingCode }).exec();
            guests.forEach((guest) => {
                if (guest.profilePic) {
                    this.assetService.deleteImageFromURLAsync(guest.profilePic);
                }
                if (guest.email) {
                    this.sendGuestDeletionEmail(guest.firstName, this.config.GuestDeleteAccountForm, guest.email);
                }
            });
            if (spouse) {
                if (spouse.profilePic) {
                    this.assetService.deleteImageFromURLAsync(spouse.profilePic);
                }
                this.sendAccountDeletionEmail(spouse.firstName, this.config.SpouseDeleteAccountForm, spouse.email);
                await this.userModel.findByIdAndDelete(spouse._id);
            }
            if (user.profilePic) {
                this.assetService.deleteImageFromURLAsync(user.profilePic);
            }
            this.sendAccountDeletionEmail(user.firstName, this.config.SpouseDeleteAccountForm, user.email);
            await this.userModel.findByIdAndDelete(user._id);
            return true;
        } catch (ex) {
            console.log(ex);
            return false;
        }
    };
    public deleteGuest = async (userId: string): Promise<boolean> => {
        try {
            const guest = await this.guestModel.findById(userId).lean();
            if (!guest) {
                return false;
            }
            await this.eventModel.updateMany(
                {
                    ownerWeddingCode: guest.uniqueWeddingCode,
                },
                { $pull: { guests: `${guest._id}`, rsvpList: { hostingGuest: `${guest._id}` } } },
            );
            await this.guestListModel.updateMany({ guests: guest._id }, { $pull: { guests: guest._id } });
            const spouses = await this.userModel.find({ uniqueWeddingCode: guest.uniqueWeddingCode }).lean();
            spouses.forEach((spouse) => {
                this.sendSpouseGuestDeletionEmail(spouse.firstName, guest.firstName, guest.surname, spouse.email);
            });
            if (guest.email) {
                this.sendGuestDeletionEmail(guest.firstName, this.config.GuestDeleteAccountForm, guest.email);
            }
            if (guest.profilePic) {
                this.assetService.deleteImageFromURLAsync(guest.profilePic);
            }
            await this.guestModel.findByIdAndDelete(guest._id);
            return true;
        } catch (ex) {
            console.log(ex);
            return false;
        }
    };

    private sendSpouseGuestDeletionEmail = async (
        firstName: string,
        guestFirstName: string,
        guestLastName: string,
        email: string,
    ) => {
        const emailBody = await this.fileHandler.readFileAsync("spouse.guest.delete.account.html");
        if (!emailBody) {
            return;
        }
        const formattedEmail = String.Format(emailBody, firstName, guestFirstName, guestLastName);
        this.communicationService.sendEmail({
            html: formattedEmail,
            recipients: email,
            subject: this.config.AppName + "Guest Account Deletion",
        });
    };
    private sendAccountDeletionEmail = async (firstName: string, formUrl: string, email: string) => {
        const emailBody = await this.fileHandler.readFileAsync("spouse.delete.account.html");
        if (!emailBody) {
            return;
        }
        const formattedEmail = String.Format(emailBody, firstName, formUrl);
        this.communicationService.sendEmail({
            html: formattedEmail,
            recipients: email,
            subject: this.config.AppName + " Account Deletion",
        });
    };

    private sendGuestDeletionEmail = async (firstName: string, formUrl: string, email: string) => {
        const emailBody = await this.fileHandler.readFileAsync("guest.delete.account.html");
        if (!emailBody) {
            return;
        }
        const formattedEmail = String.Format(emailBody, firstName, formUrl);
        this.communicationService.sendEmail({
            html: formattedEmail,
            recipients: email,
            subject: this.config.AppName + " Account Deletion",
        });
    };

    private sendRegistrationEmailAsync = async (firstNames: string[], emails: string[]) => {
        const emailBody = await this.fileHandler.readFileAsync("spouse.registration.html");
        if (!emailBody) {
            return;
        }
        const formattedEmail = String.Format(
            emailBody,
            this.config.AppName,
            firstNames[0],
            firstNames[1],
            this.config.ClientLoginEndpoint,
        );
        this.communicationService.sendEmail({
            html: formattedEmail,
            recipients: emails,
            subject: this.config.AppName + " Registration",
        });
    };

    private generateWeddingCodeAsync = async (): Promise<string> => {
        try {
            let uniqueCode: string | null = null;
            do {
                uniqueCode = generate({
                    length: 6,
                    readable: true,
                    charset: "alphanumeric",
                    capitalization: "uppercase",
                });
                const existingCode = await this.userModel.find({ uniqueWeddingCode: uniqueCode }).lean();
                if (existingCode && !isEmpty(existingCode)) {
                    uniqueCode = null;
                }
            } while (uniqueCode === null);
            return uniqueCode;
        } catch (err) {
            return generate({
                length: 6,
                readable: true,
                charset: "alphanumeric",
                capitalization: "uppercase",
            });
        }
    };

    private findAndDeleteEventImages = async (event: LeanDocument<IEventSchema>): Promise<void> => {
        try {
            const description = event.description;
            const root = parse(description);
            const imgTags = root.querySelectorAll("img");
            if (isEmpty(imgTags)) {
                return;
            }
            for (const img of imgTags) {
                const src = img.getAttribute("src");
                if (!src || !src.includes("cloudinary")) {
                    continue;
                }
                await this.assetService.deleteImageFromURLAsync(src);
            }
        } catch (ex) {
            // do nothing
        }
    };
}
