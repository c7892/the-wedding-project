import { injectable } from "tsyringe";
import Meta from "html-metadata-parser";

@injectable()
export class MetaService {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    constructor() {}

    public fetchUrlMetadata = async (url: string) => {
        try {
            const result = await Meta(url);
            return result;
        } catch (err) {
            return null;
        }
    };
}
