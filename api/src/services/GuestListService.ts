import { flatten, isEmpty, uniq } from "lodash";
import { inject, injectable } from "tsyringe";
import { IGuestListModel, IGuestListSchema, IGuestModel, IGuestSchema, IUserModel } from "../dal";
import { Guest, GuestList, User } from "../types";

@injectable()
export class GuestListService {
    public constructor(
        @inject("IGuestListModel") private readonly guestListModel: IGuestListModel,
        @inject("IGuestModel") private readonly guestModel: IGuestModel,
        @inject("IUserModel") private readonly userModel: IUserModel,
    ) {}

    public saveGuestListAsync = async (spouse: User, list: GuestList, guests: Guest[]) => {
        const guestIds: string[] = [];
        const spouses = await this.userModel.find({ uniqueWeddingCode: spouse.uniqueWeddingCode }).lean();
        for (let i = 0; i < guests.length; i++) {
            const guest = guests[i];
            let guestRecord: IGuestSchema | null = null;
            if (guest._id) {
                guestRecord = await this.guestModel.findById(guest._id);
            }
            try {
                if (!guestRecord) {
                    guestRecord = new this.guestModel();
                    guestRecord.dateCreated = Date.now();
                }
                const relation = guest.relation;
                if (relation && relation._spouse) {
                    for (const spouse of spouses) {
                        if (relation[spouse._id]) {
                            continue;
                        }
                        relation[spouse._id] = relation._spouse;
                    }
                }
                guest.relation = relation;
                guestRecord.firstName = guest.firstName.trim();
                guestRecord.surname = guest.surname.trim();
                guestRecord.address = guest.address?.trim();
                guestRecord.email = guest.email?.trim();
                guestRecord.mobile = guest.mobile.replace(/-/g, "").trim();
                guestRecord.uniqueWeddingCode = spouse.uniqueWeddingCode;
                guestRecord.modifiedDttm = Date.now();
                guestRecord = await guestRecord.save();
                guestIds.push(guestRecord._id ?? "");
            } catch (ex) {
                // do nothing
            }
        }
        let guestListRecord: IGuestListSchema | null = null;
        try {
            if (list._id) {
                guestListRecord = await this.guestListModel.findById(list._id);
            }
            if (!guestListRecord) {
                guestListRecord = new this.guestListModel();
                guestListRecord.dateCreated = Date.now();
            }
            guestListRecord.name = list.name.trim();
            guestListRecord.description = list.description?.trim();
            if (!guestListRecord.ownerWeddingCode || guestListRecord.ownerWeddingCode !== spouse.uniqueWeddingCode) {
                guestListRecord.ownerWeddingCode = spouse.uniqueWeddingCode;
            }
            const existingGuests = guestListRecord.guests ?? [];
            if (isEmpty(guestIds) && isEmpty(list.guests)) {
                guestListRecord.guests = [];
            } else {
                guestListRecord.guests = uniq(existingGuests.concat(guestIds));
            }
            guestListRecord = await guestListRecord.save();
        } catch (ex) {
            // nothing
        }
        return guestListRecord;
    };

    public getListsAsync = async (spouse: User, id: string | undefined = undefined) => {
        try {
            const lists = await this.guestListModel
                .find(id ? { _id: id } : { ownerWeddingCode: spouse.uniqueWeddingCode })
                .lean();
            const guests = uniq(flatten(lists.map((list) => list.guests)));
            const guestsObjs = await this.guestModel.find({ _id: { $in: guests } }).lean();
            const listWithGuestObjs: GuestList[] = [];
            lists.forEach((list) => {
                const listGuests = list.guests.map((guest) => guest.toString());
                const guestsInList = guestsObjs.filter((guest) => listGuests.includes(guest._id.toString()));
                listWithGuestObjs.push({
                    ...list,
                    guestObjs: guestsInList,
                });
            });
            return listWithGuestObjs;
        } catch (ex) {
            return [];
        }
    };
    public getGuests = async (spouse: User) => {
        try {
            const guests = await this.guestModel.find({ uniqueWeddingCode: spouse.uniqueWeddingCode }).lean();
            return guests;
        } catch (ex) {
            return [];
        }
    };
    public deleteGuestList = async (guestListId: string) => {
        try {
            await this.guestListModel.findByIdAndDelete(guestListId);
            return true;
        } catch (ex) {
            return false;
        }
    };
    public removeGuestFromList = async (guestListId: string, guestId: string) => {
        try {
            await this.guestListModel.findByIdAndUpdate(guestListId, { $pull: { guests: guestId } });
            return true;
        } catch (ex) {
            return false;
        }
    };
    public deleteGuest = async (guestId: string) => {
        try {
            const guest = await this.guestModel.findByIdAndDelete(guestId);
            if (!guest) {
                return true;
            }
            await this.guestListModel.updateMany({ guests: guestId }, { $pull: { guests: guestId } });
            return true;
        } catch (ex) {
            return false;
        }
    };
    public updateGuest = async (uniqueWeddingCode: string, guest: Guest) => {
        try {
            const relation = guest.relation;
            if (relation && relation._spouse) {
                const spouses = await this.userModel.find({ uniqueWeddingCode }).lean();
                for (const spouse of spouses) {
                    if (relation[spouse._id]) {
                        continue;
                    }
                    relation[spouse._id] = relation._spouse;
                }
            }
            guest.relation = relation;
            const existingGuest = await this.guestModel
                .findOne({
                    $and: [
                        {
                            uniqueWeddingCode,
                        },
                        { mobile: guest.mobile.replace(/-/g, "").trim() },
                    ],
                })
                .lean();
            if (existingGuest) {
                guest._id = existingGuest._id;
            }
            if (!guest._id) {
                const guestRecord = new this.guestModel();
                guestRecord.dateCreated = Date.now();
                guestRecord.firstName = guest.firstName.trim();
                guestRecord.surname = guest.surname.trim();
                guestRecord.address = guest.address?.trim();
                guestRecord.email = guest.email?.trim();
                guestRecord.mobile = guest.mobile.replace(/-/g, "").trim();
                guestRecord.relation = relation;
                guestRecord.uniqueWeddingCode = uniqueWeddingCode;
                guestRecord.modifiedDttm = Date.now();
                await guestRecord.save();
                return true;
            }
            await this.guestModel.findByIdAndUpdate(guest._id, { ...guest, modifiedDttm: Date.now() });
            return true;
        } catch (ex) {
            return false;
        }
    };
}
