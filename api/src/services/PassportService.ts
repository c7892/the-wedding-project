import { CallbackError } from "mongoose";
import passport from "passport";
import { Strategy } from "passport-local";
import { container } from "tsyringe";
import { IGuestModel, IGuestSchema, IUserModel, IUserSchema } from "../dal";

export const setupPassportStrategy = (): void => {
    const UserModel = container.resolve<IUserModel>("IUserModel");
    const GuestModel = container.resolve<IGuestModel>("IGuestModel");
    passport.use(
        new Strategy(
            {
                usernameField: "email",
                passwordField: "password",
            },
            function (email, password, done) {
                if (password.length >= 0 && password.length <= 6) {
                    GuestModel.findOne(
                        {
                            $or: [
                                {
                                    email,
                                },
                                { mobile: email.replace(/-/g, "").trim() },
                            ],
                        },
                        (error: CallbackError, user: IGuestSchema | null) => {
                            if (error) {
                                return done(error);
                            }
                            if (!user) {
                                return done(null, false, {
                                    message: "The given credentials are not valid",
                                });
                            }
                            if (user.uniqueWeddingCode !== password.toUpperCase()) {
                                return done(null, false, {
                                    message: "The given credentials are not valid",
                                });
                            }
                            // If credentials are correct, return the user object
                            return done(null, user);
                        },
                    );
                } else {
                    UserModel.findOne({ email }, (error: CallbackError, user: IUserSchema | null) => {
                        if (error) {
                            return done(error);
                        }
                        if (!user) {
                            return done(null, false, {
                                message: "The given credentials are not valid",
                            });
                        }
                        if (!user.validPassword(password)) {
                            return done(null, false, {
                                message: "The given credentials are not valid",
                            });
                        }
                        // If credentials are correct, return the user object
                        return done(null, user);
                    });
                }
            },
        ),
    );

    passport.serializeUser(function (user, done) {
        done(null, (user as IUserSchema)._id);
    });

    passport.deserializeUser(async (id, done) => {
        try {
            const user = await UserModel.findById(id).lean();
            if (user) {
                return done(null, user);
            }
            const guest = await GuestModel.findById(id).lean();
            if (guest) {
                return done(null, guest);
            }
            done(new Error(), null);
        } catch (ex) {
            done(ex, null);
        }
    });
};
