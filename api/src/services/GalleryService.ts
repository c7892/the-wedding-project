import { unlink } from "fs";
import { isEmpty } from "lodash";
import moment from "moment";
import { LeanDocument } from "mongoose";
import { inject, singleton } from "tsyringe";
import { AssetService } from ".";
import { IGalleryModel, IGallerySchema, IImageModel, IUserModel, IUserSchema } from "../dal";
import { Gallery } from "../types";
import { scheduleJob } from "node-schedule";
import { CompressionHelper } from "../utilities";
import { readFile, writeFile } from "fs/promises";
import mime from "mime";

@singleton()
export class GalleryService {
    public constructor(
        @inject("IGalleryModel") private readonly galleryModel: IGalleryModel,
        @inject("IUserModel") private readonly userModel: IUserModel,
        @inject("IImageModel") private readonly imageModel: IImageModel,
        private readonly assetService: AssetService,
        private readonly compressionHelper: CompressionHelper,
    ) {}

    public createGalleryAsync = async (
        uniqueWeddingCode: string,
        galleryToSave: Gallery,
    ): Promise<Gallery | undefined> => {
        try {
            const owners: string[] = (await this.userModel.find({ uniqueWeddingCode: uniqueWeddingCode }).lean()).map(
                (user: LeanDocument<IUserSchema>) => user._id ?? "",
            );
            const gallery = new this.galleryModel();
            gallery.title = galleryToSave.title;
            gallery.event = galleryToSave.event;
            gallery.details = galleryToSave.details;
            gallery.ownerWeddingCode = uniqueWeddingCode;
            gallery.dateCreated = Date.now();
            gallery.modifiedDttm = Date.now();
            gallery.images = [];
            gallery.owners = owners.join(",");
            return await gallery.save();
        } catch (ex) {
            return undefined;
        }
    };

    public updateGalleryAsync = async (galleryToSave: Gallery): Promise<Gallery | undefined | null> => {
        try {
            return await this.galleryModel
                .findByIdAndUpdate(
                    (galleryToSave as IGallerySchema)._id,
                    { modifiedDttm: Date.now(), event: galleryToSave.event, title: galleryToSave.title },
                    { new: true },
                )
                .populate("images")
                .lean();
        } catch (ex) {
            console.log(ex);
            return undefined;
        }
    };

    public getAllGalleries = async (id: string): Promise<Gallery[]> => {
        try {
            return await this.galleryModel.find({ ownerWeddingCode: id }).populate("images").lean();
        } catch (ex) {
            return [];
        }
    };

    public fetchGallery = async (id: string): Promise<Gallery | undefined> => {
        try {
            return await this.galleryModel.findById(id).populate("images").lean();
        } catch (ex) {
            return undefined;
        }
    };

    public downloadGallery = async (id: string): Promise<string | undefined> => {
        try {
            const gallery = await this.galleryModel.findById(id).lean();
            if (!gallery) {
                return undefined;
            }
            const zipUrl = await this.assetService.zipImagesAsync(`${gallery._id}`);
            if (zipUrl) {
                const executionTime = moment().add(1, "hour").toDate();
                scheduleJob(executionTime, () => {
                    this.assetService.deleteImageFromURLAsync(zipUrl);
                });
            }
            return zipUrl;
        } catch (ex) {
            return undefined;
        }
    };

    public addImageToGallery = async (
        userId: string,
        retry = true,
        imageData?: Express.Multer.File,
        galleryId?: string,
        eventId?: string,
    ): Promise<Gallery | undefined | null> => {
        try {
            if (!imageData) {
                return null;
            }
            let gallery = await this.galleryModel.findById(galleryId).lean();
            if (!gallery && eventId) {
                gallery = await this.galleryModel.findOne({ event: eventId }).lean();
            }
            if (!gallery) {
                return undefined;
            }
            const file = await readFile(imageData.path);
            let compressedImage = await this.compressionHelper.compressImage(
                file,
                retry ? 0.6 : 0.2,
                retry ? 0.8 : 0.4,
                retry ? 70 : 40,
                retry ? 1920 : 720,
            );
            let imageUrl: string | undefined = "";
            if (compressedImage) {
                const name = imageData.path.substring(0, imageData.path.lastIndexOf("."));
                await writeFile(`${name}(1).${mime.extension(imageData.mimetype)}`, compressedImage);
                compressedImage = null;
                imageUrl = await this.assetService.saveImageAsync(`${name}(1).${mime.extension(imageData.mimetype)}`, [
                    `${gallery._id}`,
                ]);
                unlink(`${name}.${mime.extension(imageData.mimetype)}`, () => {
                    // do nothing
                });
            }
            if (!imageUrl && retry) {
                return await this.addImageToGallery(userId, false, imageData, galleryId, eventId);
            }
            unlink(imageData.path, () => {
                // do nothing
            });
            const image = await new this.imageModel({
                src: imageUrl ?? "",
                uploadedBy: userId,
            }).save();
            return await this.galleryModel
                .findByIdAndUpdate(gallery._id, { $push: { images: image._id } }, { new: true })
                .populate("images")
                .lean(true);
        } catch (ex) {
            return undefined;
        }
    };

    public deleteGalleryAsync = async (id: string): Promise<void> => {
        try {
            const gallery = await this.galleryModel.findByIdAndDelete(id).lean();
            gallery?.images?.forEach(async (image) => {
                const deletedImage = await this.imageModel.findByIdAndDelete(image);
                if (deletedImage) {
                    this.assetService.deleteImageFromURLAsync(deletedImage.src);
                }
            });
        } catch (ex) {
            return;
        }
    };

    public removeImage = async (
        userId: string,
        src: string,
        galleryId: string,
    ): Promise<Gallery | undefined | null> => {
        try {
            const gallery = await this.galleryModel.findById(galleryId).lean();
            const images = await this.imageModel.find({ src }).lean();
            if (isEmpty(images) || !gallery) {
                return undefined;
            }
            for (const image of images) {
                if (image.uploadedBy !== userId && !gallery.owners.includes(userId)) {
                    continue;
                }
                this.imageModel.findByIdAndDelete(image._id);
                await this.assetService.deleteImageFromURLAsync(image.src);
                return await this.galleryModel.findByIdAndUpdate(
                    gallery._id,
                    { $pull: { images: { src: image.src } } },
                    { new: true },
                );
            }
        } catch (ex) {
            return undefined;
        }
    };
}
