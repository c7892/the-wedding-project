export interface IConfig {
    AppName: string;
    DatabaseName: string;
    Port: number;
    IsDebug: boolean;
    SwaggerOutputLocation: string;
    ApiBasePath: string;
    EmailHost: { server: string; port: number };
    ClientLoginEndpoint: string;
    JwtTokenName: string;
    ClientEndpoint: string;
    SMTP: string;
    SpouseDeleteAccountForm: string;
    GuestDeleteAccountForm: string;
    PostageCost: number;
    SmsCost: number;
    KeyvaultName: string;
    AzureClientId: string;
    AzureTenantId: string;
    AzureClientSecret: string;
    Secrets: {
        CloudinarySecret: string;
        PassportKey: string;
        Password: string;
        CloudinaryKey: string;
        CommunicationAccessKey: string;
        CloudinaryName: string;
        ConnectionString: string;
        CommunicationPhoneNumber: string;
        Email: string;
        LobApiKey: string;
        GoogleGeoEncodeApiKey: string;
        StripeKey: string;
        TwilioSID: string;
        TwilioToken: string;
        TwilioPhone: string;
    };
}

export const Secrets = [
    "CloudinarySecret",
    "PassportKey",
    "Password",
    "CloudinaryKey",
    "CloudinaryName",
    "ConnectionString",
    "Email",
    "LobApiKey",
    "GoogleGeoEncodeApiKey",
    "StripeKey",
    "TwilioSID",
    "TwilioToken",
    "TwilioPhone",
];
