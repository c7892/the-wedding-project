import { IConfig } from "./Config";

export interface IConfigurationProvider {
    getConfig: () => Promise<IConfig>;
}
