import { singleton } from "tsyringe";
import { SecretClient } from "@azure/keyvault-secrets";
import { ClientSecretCredential } from "@azure/identity";
import { IConfig, Secrets } from "./Config";
import { IConfigurationProvider } from "./IConfigurationProvider";
import { merge } from "lodash";

@singleton()
export class AzureConfigurationProvider implements IConfigurationProvider {
    private readonly baseConfig: IConfig = require("../../env.json");
    private fullSecureConfig: IConfig | undefined;
    private readonly keyvaultSecretClient: SecretClient;

    public constructor() {
        const vaultUrl = `https://${this.baseConfig.KeyvaultName}.vault.azure.net`;
        const credentials = new ClientSecretCredential(
            this.baseConfig.AzureTenantId,
            this.baseConfig.AzureClientId,
            this.baseConfig.AzureClientSecret,
        );
        this.keyvaultSecretClient = new SecretClient(vaultUrl, credentials);
    }

    public getConfig = async (): Promise<IConfig> => {
        if (this.fullSecureConfig) {
            return this.fullSecureConfig;
        }
        this.fullSecureConfig = this.baseConfig;
        for (const secret of Secrets) {
            try {
                const value = await this.keyvaultSecretClient.getSecret(secret);
                this.fullSecureConfig.Secrets = merge(this.fullSecureConfig.Secrets, {
                    [secret]: value.value,
                });
            } catch (ex) {
                this.fullSecureConfig.Secrets = merge(this.fullSecureConfig.Secrets, {
                    [secret]: "",
                });
            }
        }
        return this.fullSecureConfig;
    };
}
