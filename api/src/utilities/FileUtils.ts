import { existsSync, mkdirSync } from "fs";
import os from "os";

export const getFileLocationOnDisk = (directory: string, name: string) => {
    const dir = os.platform() === "win32" ? os.tmpdir() : os.homedir();
    const filSep = os.platform() === "win32" ? "\\" : "/";
    if (!existsSync(`${dir}${filSep}${directory}`)) {
        mkdirSync(`${dir}${filSep}${directory}`);
    }
    return `${dir}${filSep}${directory}${filSep}${name}`;
};
