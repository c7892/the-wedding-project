import { has } from "lodash";
import request from "request";
import { injectable } from "tsyringe";
import { RegionCountries } from "./RegionCountries";
import { TaxRate, TaxRates, Us } from "./TaxRates";
/* eslint-disable @typescript-eslint/no-var-requires */
const check_fraud_eu_vat = require("validate-vat");
const validate_eu_vat = require("jsvat");
const validate_us_vat = require("ein-validator");

const tax_rates: TaxRates = require("./sales_tax_rates.json");
const region_countries: RegionCountries = require("./region_countries.json");

// eslint-enable @typescript-eslint/no-var-requires
const regex_whitespace = /\s/g;
const regex_eu_vat = /^[A-Z]{2}(.+)$/;
const regex_gb_vat = /^GB([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})$/;
const regex_ca_vat = /^[0-9]{9}$/;

const validate_gb_vat_url = "https://api.service.hmrc.gov.uk/organisations/vat/check-vat-number/lookup";
const validate_gb_vat_options = {
    timeout: 20000,
};

const tax_default_object = {
    type: "none",
    rate: 0.0,
    states: {},
};

@injectable()
export class SalesTax {
    private taxOriginCountry: string | null = null;
    private useRegionalTax = true;
    private enabledTaxNumberValidation = true;
    private enabledTaxNumberFraudCheck = false;

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public constructor() {}
    /**
     * public hasSalesTax
     * @public
     * @param  {string}  countryCode
     * @return {boolean} Whether country has sales tax
     */
    public hasSalesTax = (countryCode: string) => {
        countryCode = (countryCode || "").toUpperCase();

        return ((this.readCurrentTaxRates(countryCode as keyof TaxRates) || {}).rate || 0.0) > 0 ? true : false;
    };

    /**
     * public hasStateSalesTax
     * @public
     * @param  {string}  countryCode
     * @param  {string}  stateCode
     * @return {boolean} Whether country state has sales tax
     */
    public hasStateSalesTax = (countryCode: string, stateCode: string) => {
        countryCode = (countryCode || "").toUpperCase();
        stateCode = (stateCode || "").toUpperCase();

        const stateTax = (((this.readCurrentTaxRates(countryCode as keyof TaxRates) || {}) as Us).states || {})[
            stateCode
        ];

        return ((stateTax || {}).rate || 0.0) > 0 ? true : false;
    };

    /**
     * public getSalesTax
     * @public
     * @param  {string} countryCode
     * @param  {string} [stateCode]
     * @param  {string} [taxNumber]
     * @return {object} Promise object (returns the sales tax from 0 to 1)
     */
    public getSalesTax = (countryCode?: string, stateCode?: string, taxNumber?: string | null) => {
        countryCode = (countryCode || "").toUpperCase();
        stateCode = (stateCode || "").toUpperCase();
        taxNumber = taxNumber || null;

        // Acquire target tax area
        const targetArea = this.getTargetArea(countryCode);

        // Acquire sales tax for country, or default (if no known sales tax)
        // Notice: if regional tax is ignored, force national tax \
        //   (eg. EU w/o VAT MOSS)
        let countryTax: TaxRate, stateTax: TaxRate;

        if (targetArea === "regional" && this.useRegionalTax === false && this.taxOriginCountry !== null) {
            countryTax = this.readCurrentTaxRates(this.taxOriginCountry as keyof TaxRates) || tax_default_object;

            stateTax = tax_default_object;
        } else {
            countryTax = this.readCurrentTaxRates(countryCode as keyof TaxRates) || tax_default_object;

            stateTax = ((countryTax as Us).states || {})[stateCode] || tax_default_object;
        }

        if (countryTax.rate > 0 || stateTax.rate > 0) {
            return this.getTaxExchangeStatus(countryCode, stateCode, taxNumber).then((exchangeStatus) => {
                return Promise.resolve(this.buildSalesTaxContext(countryTax, stateTax, exchangeStatus));
            });
        }

        return Promise.resolve(this.buildSalesTaxContext(countryTax, stateTax));
    };

    /**
     * public getAmountWithSalesTax
     * @public
     * @param  {string} countryCode
     * @param  {string} [stateCode]
     * @param  {number} [amount]
     * @param  {string} [taxNumber]
     * @return {object} Promise object (returns the total tax amount)
     */
    public getAmountWithSalesTax = (
        countryCode: string,
        stateCode: string,
        amount: number,
        taxNumber?: string | null,
    ) => {
        countryCode = (countryCode || "").toUpperCase();
        stateCode = (stateCode || "").toUpperCase();
        amount = amount || 0.0;
        taxNumber = taxNumber || null;

        // Acquire sales tax, then process amount.
        return this.getSalesTax(countryCode, stateCode, taxNumber).then(function (tax) {
            // Generate amount details (list of all sub-amounts from each sub-tax \
            //   rate)
            const amountDetails = [];

            for (let i = 0; i < tax.details.length; i++) {
                amountDetails.push({
                    type: tax.details[i].type,
                    rate: tax.details[i].rate,
                    amount: tax.details[i].rate * amount,
                });
            }

            // Return total amount with sales tax
            return Promise.resolve({
                type: tax.type,
                rate: tax.rate,
                price: amount,
                total: (1.0 + tax.rate) * amount,
                area: tax.area,
                exchange: tax.exchange,
                charge: tax.charge,
                details: amountDetails,
            });
        });
    };

    /**
     * public validateTaxNumber
     * @public
     * @param  {string} countryCode
     * @param  {string} taxNumber
     * @return {object} Promise object (returns a boolean for validity)
     */
    public validateTaxNumber = (countryCode?: string, taxNumber?: string) => {
        countryCode = (countryCode || "").toUpperCase();

        if (this.enabledTaxNumberValidation === true) {
            // Normalize tax number (eg. remove spaces)
            const cleanTaxNumber = (taxNumber || "").replace(regex_whitespace, "");

            if (cleanTaxNumber) {
                // United States
                if (countryCode === "US") {
                    // Validate US EIN
                    return Promise.resolve(validate_us_vat.isValid(cleanTaxNumber) && true);
                }

                // Canada
                if (countryCode === "CA") {
                    // Validate CA BN
                    return Promise.resolve(regex_ca_vat.test(cleanTaxNumber) && true);
                }

                // United Kingdom
                if (countryCode === "GB") {
                    return new Promise((resolve, reject) => {
                        // Validate GB VAT number
                        const splitMatch = cleanTaxNumber.match(regex_gb_vat) ?? [];
                        const isValid = splitMatch && splitMatch[1] ? true : false;

                        // Check number for fraud? (online check)
                        if (isValid === true && this.enabledTaxNumberFraudCheck === true) {
                            // Query UK HMRC validation API
                            request.get(
                                validate_gb_vat_url + "/" + splitMatch[1],
                                validate_gb_vat_options,

                                function (error, response) {
                                    if (error) {
                                        return reject(error);
                                    }

                                    return resolve(
                                        response.statusCode >= 200 && response.statusCode <= 299 ? true : false,
                                    );
                                },
                            );
                        } else {
                            return resolve(isValid);
                        }
                    });
                }

                // European Union member states (sourced from dynamic list)
                if ((region_countries.EU || []).indexOf(countryCode) !== -1) {
                    return new Promise((resolve, reject) => {
                        // Validate EU VAT number
                        const validationInfo = validate_eu_vat.checkVAT(cleanTaxNumber);
                        let isValid = validationInfo.isValid && true;

                        // No country match?
                        if (isValid === true && ((validationInfo.country || {}).isoCode || {}).short !== countryCode) {
                            isValid = false;
                        }

                        // Check number for fraud? (online check)
                        if (isValid === true && this.enabledTaxNumberFraudCheck === true) {
                            // Split VAT number (n extract actual VAT number)
                            const splitMatch = cleanTaxNumber.match(regex_eu_vat);

                            // Check fraud on EU VAT number?
                            if (splitMatch && splitMatch[1]) {
                                check_fraud_eu_vat(
                                    countryCode,
                                    splitMatch[1],

                                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                                    function (error: any, fraudInfo: any) {
                                        if (error) {
                                            return reject(error);
                                        }

                                        // Return whether valid or not
                                        return resolve(fraudInfo.valid && true);
                                    },
                                );
                            } else {
                                return resolve(false);
                            }
                        } else {
                            return resolve(isValid);
                        }
                    });
                }
            }

            // Consider as invalid tax number (tax number country not recognized, \
            //   or no tax number provided)
            return Promise.resolve(false);
        }

        // Consider all tax numbers as valid
        return Promise.resolve(true);
    };

    /**
     * public getTaxExchangeStatus
     * @public
     * @param  {string} countryCode
     * @param  {string} [stateCode]
     * @param  {string} [taxNumber]
     * @return {object} Promise object (returns an exchange status object)
     */
    public getTaxExchangeStatus = (countryCode?: string, stateCode?: string | null, taxNumber?: string | null) => {
        countryCode = (countryCode || "").toUpperCase();
        stateCode = (stateCode || "").toUpperCase() || null;
        taxNumber = taxNumber || null;

        const targetArea = this.getTargetArea(countryCode);

        // Country or state (if any) has any sales tax?
        if (this.hasTotalSalesTax(countryCode, stateCode) === true) {
            // Check for tax-exempt status? (if tax number is provided)
            if (taxNumber) {
                return this.validateTaxNumber(countryCode, taxNumber).then(function (isValid) {
                    // Consider valid numbers as tax-exempt (overrides exempt status if \
                    //   area is national)
                    if (isValid === true) {
                        return Promise.resolve({
                            exchange: "business",
                            area: targetArea,
                            exempt: targetArea !== "national" && true,
                        });
                    }

                    return Promise.resolve({
                        exchange: "consumer",
                        area: targetArea,
                        exempt: false,
                    });
                });
            }

            // Consider as non tax-exempt
            return Promise.resolve({
                exchange: "consumer",
                area: targetArea,
                exempt: false,
            });
        }

        // Consider as tax-exempt (country has no sales tax)
        return Promise.resolve({
            exchange: "consumer",
            area: targetArea,
            exempt: true,
        });
    };

    /**
     * public setTaxOriginCountry
     * @public
     * @param  {string} countryCode
     * @return {undefined}
     */
    public setTaxOriginCountry = (countryCode?: string, useRegionalTax: boolean = this.useRegionalTax) => {
        this.taxOriginCountry = (countryCode || "").toUpperCase() || null;
        this.useRegionalTax = useRegionalTax;
    };

    /**
     * public toggleEnabledTaxNumberValidation
     * @public
     * @param  {boolean} enabled
     * @return {undefined}
     */
    public toggleEnabledTaxNumberValidation = (enabled: boolean) => {
        this.enabledTaxNumberValidation = enabled && true;
    };

    /**
     * public toggleEnabledTaxNumberFraudCheck
     * @public
     * @param  {boolean} enabled
     * @return {undefined}
     */
    public toggleEnabledTaxNumberFraudCheck = (enabled: boolean) => {
        this.enabledTaxNumberFraudCheck = enabled && true;
    };

    /**
     * private getTargetArea
     * @private
     * @param  {string} countryCode
     * @return {string} Target area
     */
    private getTargetArea = (countryCode: string): string => {
        // Default to worldwide
        let targetArea = "worldwide";

        if (this.taxOriginCountry !== null) {
            if (this.taxOriginCountry === countryCode) {
                // Same country (national)
                targetArea = "national";
            } else {
                // Same economic community? (regional)
                for (const region in region_countries) {
                    const regionCountries = region_countries[region as keyof RegionCountries];

                    if (
                        regionCountries.indexOf(this.taxOriginCountry) !== -1 &&
                        regionCountries.indexOf(countryCode) !== -1
                    ) {
                        targetArea = "regional";

                        break;
                    }
                }
            }
        }

        return targetArea;
    };

    /**
     * private buildSalesTaxContext
     * @private
     * @param  {object} countryTax
     * @param  {object} stateTax
     * @param  {object} [exchangeStatus]
     * @return {object} Sales tax context object
     */
    private buildSalesTaxContext = (
        countryTax: TaxRate,
        stateTax: TaxRate,
        exchangeStatus?: {
            exchange: string;
            area: string;
            exempt: boolean;
        },
    ) => {
        // Acquire exchange + exempt + area
        const taxExchange = (exchangeStatus || {}).exchange || "consumer";
        const taxArea = (exchangeStatus || {}).area || "worldwide";
        const isExempt = (exchangeStatus || {}).exempt || false;
        const fullRate = countryTax.rate + stateTax.rate;

        // Generate tax type (multiple sales tax may apply, eg. country + state)
        let type = countryTax.type;

        if (stateTax.rate > 0) {
            if (countryTax.rate > 0) {
                type = type + "+" + stateTax.type;
            } else {
                type = stateTax.type;
            }
        }

        // Build charge object
        const taxCharge: {
            direct: boolean;
            reverse: boolean;
        } = { direct: true, reverse: true };

        if (type !== "none") {
            taxCharge.direct = !isExempt;
            taxCharge.reverse = isExempt && fullRate > 0 && true;
        } else {
            taxCharge.direct = false;
            taxCharge.reverse = false;
        }

        // Build details object (list of all sub-taxes that make up the total rate)
        const taxDetails = [];

        if (isExempt !== true) {
            if (countryTax.rate > 0) {
                taxDetails.push({
                    type: countryTax.type,
                    rate: countryTax.rate,
                });
            }
            if (stateTax.rate > 0) {
                taxDetails.push({
                    type: stateTax.type,
                    rate: stateTax.rate,
                });
            }
        }

        // Build sales tax context
        return {
            type: type,
            rate: isExempt === true ? 0.0 : fullRate,
            area: taxArea,
            exchange: taxExchange,
            charge: taxCharge,
            details: taxDetails,
        };
    };

    /**
     * private readCurrentTaxRates
     * @private
     * @param  {object} countryCode
     * @return {object} Current tax rates
     */
    private readCurrentTaxRates = (countryCode: keyof TaxRates): TaxRate => {
        const countryTaxRates = tax_rates[countryCode];

        if (
            has(countryTaxRates || {}, "before") &&
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            typeof (countryTaxRates as any as { before: any }).before === "object"
        ) {
            const nowDate = this.getCurrentDate();

            let lowestBeforeDateString = null;
            let lowestBeforeDate = null;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const before = (countryTaxRates as any as { before: any }).before;
            for (const beforeDateString in before) {
                // Parse date string to an actual date object
                // Notice: the date string uses an UTC+00:00 timezone, which gets parsed \
                //   into the runtime-local timezone, and compared against current date.
                const beforeDate = new Date(beforeDateString);

                // Date is invalid? Throw an error, as we need to be clear that the \
                //   tax rates object is invalid.
                if (isNaN(beforeDate.getTime()) === true) {
                    throw new Error("Invalid 'before' date string: " + beforeDateString);
                }

                // Are we still in the past relative to this country sales tax \
                //   rates? Then, pickup this tax rate (until we get past-date, and \
                //   then we can use the latest/master tax rate)
                if (nowDate < beforeDate) {
                    // New lowest before date? (or none previously-defined)
                    // Notice: pick the lowest found before date, as multiple 'before' \
                    //   dates can be defined as to schedule multiple tax rate changes in \
                    //   the future. Thus, we do not want to pick the first 'before' match.
                    if (lowestBeforeDate === null || beforeDate < lowestBeforeDate) {
                        lowestBeforeDate = beforeDate;
                        lowestBeforeDateString = beforeDateString;
                    }
                }
            }

            if (lowestBeforeDateString !== null) {
                return before[lowestBeforeDateString];
            }
        }

        return countryTaxRates;
    };

    /**
     * private hasTotalSalesTax
     * @private
     * @param  {string}  countryCode
     * @param  {string}  stateCode
     * @return {boolean} Whether country and state added result in a tax or not
     */
    private hasTotalSalesTax = (countryCode?: string, stateCode?: string | null) => {
        countryCode = (countryCode || "").toUpperCase();
        stateCode = (stateCode || "").toUpperCase();

        const taxRates = this.readCurrentTaxRates(countryCode as keyof TaxRates) || {};

        const countryTax = taxRates.rate || 0.0;
        let stateTax = 0.0;

        if (stateCode) {
            stateTax = (((taxRates as Us).states || {})[stateCode] || {}).rate || 0.0;
        }

        return countryTax + stateTax > 0 ? true : false;
    };

    /**
     * private getCurrentDate
     * @private
     * @return {object} Current date
     */
    private getCurrentDate = () => {
        // Return current date
        // Notice: this function is useless as-is, though it comes handy when \
        //   unit-testing the library, as it lets us override current date with \
        //   an hardcoded date, eg. to test the automated sales tax change feature.
        return new Date();
    };
}
