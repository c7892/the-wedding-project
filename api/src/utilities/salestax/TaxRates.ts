import { ValueOf } from "../../types";

export interface TaxRates {
    AL: AE;
    AR: AE;
    AM: AE;
    AU: AE;
    AT: AE;
    AZ: AE;
    BS: AE;
    BH: AE;
    BB: AE;
    BY: AE;
    BE: AE;
    BO: AE;
    BW: AE;
    BR: AE;
    BG: AE;
    CA: CA;
    CL: AE;
    CN: AE;
    CO: AE;
    CR: AE;
    HR: AE;
    CW: AE;
    CY: AE;
    CZ: AE;
    DK: AE;
    DO: AE;
    DZ: AE;
    EC: AE;
    EG: AE;
    SV: AE;
    EE: AE;
    FI: AE;
    FR: AE;
    GE: AE;
    DE: De;
    GH: AE;
    GR: AE;
    GT: AE;
    HN: AE;
    HU: AE;
    IS: AE;
    IN: AE;
    ID: AE;
    IE: De;
    IM: AE;
    IL: AE;
    IT: AE;
    JP: AE;
    JE: AE;
    JO: AE;
    KZ: AE;
    KE: AE;
    KR: AE;
    XK: AE;
    KW: AE;
    LI: AE;
    LV: AE;
    LB: AE;
    LT: AE;
    LU: AE;
    MC: AE;
    MK: AE;
    MG: AE;
    MY: AE;
    MV: AE;
    MT: AE;
    MU: AE;
    MX: AE;
    MD: AE;
    MN: AE;
    MA: AE;
    MM: AE;
    NA: AE;
    NL: AE;
    NZ: AE;
    NI: AE;
    NG: AE;
    NO: AE;
    OM: AE;
    PK: AE;
    PA: AE;
    PG: AE;
    PY: AE;
    PE: AE;
    PH: AE;
    PL: AE;
    PT: AE;
    PR: AE;
    QA: AE;
    RO: AE;
    RU: AE;
    RW: AE;
    LC: AE;
    SA: AE;
    RS: AE;
    SC: AE;
    SG: AE;
    SK: AE;
    SI: AE;
    ZA: AE;
    ES: Es;
    SR: AE;
    SE: AE;
    CH: AE;
    TW: AE;
    TZ: AE;
    TH: AE;
    TT: AE;
    TN: AE;
    TR: AE;
    UG: AE;
    AE: AE;
    AO: AE;
    GB: AE;
    US: Us;
    UY: AE;
    VE: AE;
    VN: AE;
    ZM: AE;
    ZW: AE;
}
export type TaxRate = ValueOf<TaxRates>;

export interface AE {
    type: Type;
    rate: number;
}

export enum Type {
    Gst = "gst",
    Hst = "hst",
    Pst = "pst",
    Qst = "qst",
    Rst = "rst",
    Vat = "vat",
}

export interface CA {
    type: Type;
    rate: number;
    states: CAStates;
}

export interface CAStates {
    BC: AE;
    MB: AE;
    NB: AE;
    NL: AE;
    NS: AE;
    ON: AE;
    PE: AE;
    QC: AE;
    SK: AE;
}

export interface De {
    type: Type;
    rate: number;
    before: { [key: string]: AE };
}

export interface Es {
    type: Type;
    rate: number;
    states: ESStates;
}

export interface ESStates {
    GC: AE;
    TF: AE;
    CE: AE;
    ML: AE;
}

export interface Us {
    type: string;
    rate: number;
    states: { [key: string]: AE };
}
