import { inject, singleton } from "tsyringe";
import JPEG from "jpeg-js";
import Jimp from "jimp";
import imagemin from "imagemin";
import imageminJpegtran from "imagemin-jpegtran";
import imageminPngquant from "imagemin-pngquant";
import { IConfig } from "./config";

/**
 * @exports CompressionHelper
 * @class
 * A helper class that handles file compression
 */
@singleton()
export class CompressionHelper {
    /**
     * Creates a new compression helper instance
     * @constructor
     * @param {IConfig} configuration - The configuration to use for compression
     * @see {@link Configuration}
     */
    public constructor(@inject("Config") private readonly config: IConfig) {
        Jimp.decoders["image/jpeg"] = (data: Buffer) => JPEG.decode(data, { maxMemoryUsageInMB: 1024 });
    }

    /**
     * Resizes the given image buffer to configured dimensions, and compresses the image to specified quality. Returns the compressed image if successful.
     * @param {Buffer} fileBuffer - The image file buffer to compress
     * @param {number} min - The minimum reduction quality %
     * @param {number} max - The maximum reduction quality
     * @returns {Promise<Buffer | null>}
     */
    public compressImage = async (
        fileBuffer: Buffer,
        min = 0.6,
        max = 0.8,
        quality = 70,
        resizeWidth = 1920,
    ): Promise<Buffer | null> => {
        try {
            const resizedImage = await this.jimpResize(fileBuffer, quality, resizeWidth);
            if (!resizedImage) {
                return null;
            }
            return await imagemin.buffer(resizedImage, {
                plugins: [
                    imageminJpegtran(),
                    imageminPngquant({
                        quality: [min, max],
                    }),
                ],
            });
        } catch (err) {
            console.log(err);
            if (!err || err.exitCode !== 99) {
                return null;
            }
            return await this.jimpCompress(fileBuffer, quality);
        }
    };
    private jimpResize = async (fileBuffer: Buffer, quality = 70, resizeWidth = 1920): Promise<Buffer | null> => {
        let jimpReadImage = await Jimp.read(fileBuffer);
        jimpReadImage = jimpReadImage.resize(resizeWidth, Jimp.AUTO).quality(quality);
        return await jimpReadImage.getBufferAsync(jimpReadImage.getMIME());
    };
    private jimpCompress = async (fileBuffer: Buffer, quality = 70): Promise<Buffer | null> => {
        try {
            let jimpReadImage = await Jimp.read(fileBuffer);
            jimpReadImage = jimpReadImage.quality(quality);
            return await jimpReadImage.getBufferAsync(jimpReadImage.getMIME());
        } catch (err) {
            return null;
        }
    };
}
