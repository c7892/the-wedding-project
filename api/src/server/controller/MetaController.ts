import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { MetaService } from "../../services/MetaService";

@injectable()
export class MetaController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly metaService: MetaService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.get("/meta", this.fetchMeta);
    }

    private fetchMeta = async (
        req: Request<unknown, unknown, unknown, { url: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const metadata = await this.metaService.fetchUrlMetadata(req.query.url);
        res.status(metadata ? 200 : 400).json({ metadata });
        next();
    };
}
