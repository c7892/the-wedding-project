import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { InviteService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { IUserSchema } from "../../dal";
import { Guest, Invite } from "../../types";
import Stripe from "stripe";

@injectable()
export class InviteController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly inviteService: InviteService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.post("/invite", this.jwtAuthRequestHandler, this.saveInviteAsync);
        router.get("/invite", this.jwtAuthRequestHandler, this.fetchInvitesAsync);
        router.post("/invite/send", this.jwtAuthRequestHandler, this.sendAsync);
        router.post("/invite/send/confirmation", this.jwtAuthRequestHandler, this.sendConfirmationAsync);
        router.delete("/invite/:id", this.jwtAuthRequestHandler, this.deleteInviteAsync);
    }

    private saveInviteAsync = async (
        req: Request<unknown, unknown, { invite: Invite }>,
        res: Response,
        next: NextFunction,
    ) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const invite = await this.inviteService.saveInviteAsync(weddingCode, req.body.invite);
        res.status(invite ? 200 : 400).json(invite);
        next();
    };

    private fetchInvitesAsync = async (req: Request, res: Response, next: NextFunction) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const invites = await this.inviteService.fetchInvitesAsync(weddingCode);
        res.status(200).json(invites);
        next();
    };

    private deleteInviteAsync = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        const invite = await this.inviteService.deleteInvite(req.params.id);
        res.status(invite ? 200 : 400).json(invite);
        next();
    };
    private sendAsync = async (
        req: Request<
            unknown,
            unknown,
            {
                front: string;
                back: string;
                inviteId: string;
                guest: Guest;
                sendMethods: string[];
                paymentPayload?: Stripe.PaymentIntent;
            }
        >,
        res: Response,
        next: NextFunction,
    ) => {
        const sent = await this.inviteService.sendAsync(
            req.body.front,
            req.body.back,
            req.body.inviteId,
            req.body.guest,
            req.body.sendMethods,
            req.body.paymentPayload,
        );
        res.sendStatus(sent ? 200 : 400);
        next();
    };
    private sendConfirmationAsync = async (
        req: Request<
            unknown,
            unknown,
            {
                inviteId: string;
                guestIds: string[];
            }
        >,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as IUserSchema;
        this.inviteService.sendConfirmationReceipt(user.firstName, user.email, req.body.inviteId, req.body.guestIds);
        res.sendStatus(200);
        next();
    };
}
