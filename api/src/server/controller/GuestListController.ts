import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { IController } from "./IController";
import { GuestListService } from "../../services/GuestListService";
import { IConfig } from "../../utilities";
import { Guest, GuestList, User } from "../../types";

@injectable()
export class GuestListController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly guestListService: GuestListService) {
        super(config);
    }
    public defineRoutes(router: Router): void {
        router.post("/guestlist", this.jwtAuthRequestHandler, this.saveGuestList);
        router.get("/guestlist", this.jwtAuthRequestHandler, this.getGuestLists);
        router.delete("/guestlist/:id", this.jwtAuthRequestHandler, this.deleteList);
        router.get("/guestlist/guests", this.jwtAuthRequestHandler, this.getGuests);
        router.put("/guestlist/:id/removeguest", this.jwtAuthRequestHandler, this.removeGuestFromList);
        router.delete("/guestlist/guest/:id", this.jwtAuthRequestHandler, this.deleteGuest);
        router.put("/guestlist/guest", this.jwtAuthRequestHandler, this.updateGuest);
    }
    private saveGuestList = async (
        req: Request<unknown, unknown, { list: GuestList; guests?: Guest[] }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as User;
        const list = await this.guestListService.saveGuestListAsync(user, req.body.list, req.body.guests ?? []);
        if (!list) {
            res.status(400).send();
        } else {
            res.status(200).json(list);
        }
        next();
    };
    private getGuestLists = async (
        req: Request<{ id?: string }, unknown, { list: GuestList; guests?: Guest[] }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as User;
        const list = await this.guestListService.getListsAsync(user, req.params.id);
        res.status(200).json(list);
        next();
    };
    private deleteList = async (
        req: Request<{ id: string }, unknown, { list: GuestList; guests?: Guest[] }>,
        res: Response,
        next: NextFunction,
    ) => {
        const didDelete = await this.guestListService.deleteGuestList(req.params.id);
        res.sendStatus(didDelete ? 200 : 400);
        next();
    };
    private getGuests = async (req: Request, res: Response, next: NextFunction) => {
        const user = req.user as User;
        const list = await this.guestListService.getGuests(user);
        res.status(200).json(list);
        next();
    };
    private removeGuestFromList = async (
        req: Request<{ id: string }, unknown, { guestId: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const didRemove = await this.guestListService.removeGuestFromList(req.params.id, req.body.guestId);
        res.sendStatus(didRemove ? 200 : 400);
        next();
    };
    private deleteGuest = async (req: Request<{ id: string }, unknown, unknown>, res: Response, next: NextFunction) => {
        const didRemove = await this.guestListService.deleteGuest(req.params.id);
        res.sendStatus(didRemove ? 200 : 400);
        next();
    };
    private updateGuest = async (
        req: Request<unknown, unknown, { guest: Guest }>,
        res: Response,
        next: NextFunction,
    ) => {
        const didUpdate = await this.guestListService.updateGuest((req.user as User).uniqueWeddingCode, req.body.guest);
        res.sendStatus(didUpdate ? 200 : 400);
        next();
    };
}
