import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { GalleryService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { Gallery, Guest, User } from "../../types";
import multer, { StorageEngine, Multer } from "multer";

@injectable()
export class GalleryController extends IController {
    private readonly multerStorage: StorageEngine;
    private readonly multerInstance: Multer;
    public constructor(@inject("Config") config: IConfig, private readonly galleryService: GalleryService) {
        super(config);
        this.multerStorage = this.createDiskStorage();
        this.multerInstance = multer({ storage: this.multerStorage });
    }
    public defineRoutes(router: Router): void {
        router.get("/gallery/all", this.jwtAuthRequestHandler, this.getAllGalleries);
        router.get("/gallery/:id", this.jwtAuthRequestHandler, this.getGallery);
        router.get("/gallery/download/:id", this.jwtAuthRequestHandler, this.downloadGallery);
        router.delete("/gallery/:id", this.jwtAuthRequestHandler, this.deleteGallery);
        router.post("/gallery", this.jwtAuthRequestHandler, this.createGallery);
        router.put("/gallery", this.jwtAuthRequestHandler, this.updateGallery);
        router.put("/gallery/image/:id", this.jwtAuthRequestHandler, this.removeImage);
        router.put("/gallery/image", [this.jwtAuthRequestHandler, this.multerInstance.single("image")], this.addImage);
    }

    private getAllGalleries = async (req: Request, res: Response, next: NextFunction) => {
        const user = req.user as Guest;
        const list = await this.galleryService.getAllGalleries(user.uniqueWeddingCode);
        res.status(200).json(list);
        next();
    };
    private createGallery = async (
        req: Request<unknown, unknown, { gallery: Gallery }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as User;
        const gallery = await this.galleryService.createGalleryAsync(user.uniqueWeddingCode, req.body.gallery);
        if (gallery) {
            res.status(200).json(gallery);
        } else {
            res.sendStatus(400);
        }
        next();
    };
    private removeImage = async (
        req: Request<{ id: string }, unknown, { src: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as Guest;
        const gallery = await this.galleryService.removeImage(user._id ?? "", req.body.src, req.params.id);
        if (gallery) {
            res.status(200).json(gallery);
        } else {
            res.sendStatus(400);
        }
        next();
    };
    private addImage = async (
        req: Request<unknown, unknown, { galleryId: string; eventId: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as Guest;
        const gallery = await this.galleryService.addImageToGallery(
            user._id ?? "",
            true,
            req.file,
            req.body.galleryId,
            req.body.eventId,
        );
        if (gallery) {
            res.status(200).json(gallery);
        } else {
            res.sendStatus(400);
        }
        next();
    };
    private updateGallery = async (
        req: Request<unknown, unknown, { gallery: Gallery }>,
        res: Response,
        next: NextFunction,
    ) => {
        const gallery = await this.galleryService.updateGalleryAsync(req.body.gallery);
        if (gallery) {
            res.status(200).json(gallery);
        } else {
            res.sendStatus(400);
        }
        next();
    };
    private getGallery = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        const list = await this.galleryService.fetchGallery(req.params.id);
        if (list) {
            res.status(200).json(list);
        } else {
            res.status(404).send();
        }
        next();
    };
    private downloadGallery = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        const list = await this.galleryService.downloadGallery(req.params.id);
        if (list) {
            res.status(200).json(list);
        } else {
            res.status(404).send();
        }
        next();
    };
    private deleteGallery = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        await this.galleryService.deleteGalleryAsync(req.params.id);
        res.status(200).send();
        next();
    };
}
