import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { Registry, User } from "../../types";
import { RegistryService } from "../../services/RegistryService";

@injectable()
export class RegistryController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly registryService: RegistryService) {
        super(config);
    }
    public defineRoutes(router: Router): void {
        router.get("/registry", this.jwtAuthRequestHandler, this.getRegistry);
        router.put("/registry", this.jwtAuthRequestHandler, this.updateRegistry);
    }

    private getRegistry = async (req: Request, res: Response, next: NextFunction) => {
        const user = req.user as User;
        const list = await this.registryService.getRegistryAsync(user.uniqueWeddingCode);
        res.status(200).json(list);
        next();
    };
    private updateRegistry = async (
        req: Request<unknown, unknown, { updates: Registry }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as User;
        const list = await this.registryService.updateRegistry(user.uniqueWeddingCode, req.body.updates);
        res.status(200).json(list);
        next();
    };
}
