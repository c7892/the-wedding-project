import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { GuestService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { Guest } from "../../types";

@injectable()
export class GuestController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly guestService: GuestService) {
        super(config);
    }
    public defineRoutes(router: Router): void {
        router.get("/guest/events", this.jwtAuthRequestHandler, this.getEvents);
        router.post("/guest/events/rsvp/:eventId", this.jwtAuthRequestHandler, this.rsvp);
        router.post("/guest/events/unrsvp/:eventId", this.jwtAuthRequestHandler, this.unrsvp);
    }
    private getEvents = async (req: Request, res: Response, next: NextFunction) => {
        const user = req.user as Guest;
        const list = await this.guestService.getEvents(user._id ?? "");
        res.status(200).json(list);
        next();
    };
    private rsvp = async (
        req: Request<{ eventId: string }, unknown, { guests: number }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as Guest;
        const rsvp = await this.guestService.rsvp(req.params.eventId, user._id ?? "", req.body.guests);
        res.status(rsvp ? 200 : 400).json(rsvp);
        next();
    };
    private unrsvp = async (req: Request<{ eventId: string }>, res: Response, next: NextFunction) => {
        const user = req.user as Guest;
        const rsvp = await this.guestService.unrsvp(req.params.eventId, user._id ?? "");
        res.status(rsvp ? 200 : 400).json(rsvp);
        next();
    };
}
