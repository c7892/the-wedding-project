import { container } from "tsyringe";
import { AuthenticationController } from "./AuthenticationController";
import { CommunicationController } from "./CommunicationController";
import { EventsController } from "./EventsController";
import { GalleryController } from "./GalleryController";
import { GuestController } from "./GuestController";
import { GuestListController } from "./GuestListController";
import { InviteController } from "./InviteController";
import { MetaController } from "./MetaController";
import { NotesController } from "./NotesController";
import { PaymentController } from "./PaymentController";
import { RegistryController } from "./RegistryController";

export * from "./AuthenticationController";
export * from "./IController";

export const controllers = () => ({
    authentication: container.resolve(AuthenticationController),
    guestList: container.resolve(GuestListController),
    events: container.resolve(EventsController),
    invite: container.resolve(InviteController),
    guest: container.resolve(GuestController),
    gallery: container.resolve(GalleryController),
    registry: container.resolve(RegistryController),
    payment: container.resolve(PaymentController),
    notes: container.resolve(NotesController),
    meta: container.resolve(MetaController),
    communication: container.resolve(CommunicationController),
});
