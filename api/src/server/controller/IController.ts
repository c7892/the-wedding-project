import { Router } from "express";
import * as jwt from "express-jwt";
import { existsSync, mkdirSync } from "fs";
import mime from "mime";
import os from "os";
import multer, { StorageEngine } from "multer";
import { Guest } from "../../types";
import { IConfig } from "../../utilities";

export abstract class IController {
    protected jwtAuthRequestHandler: jwt.RequestHandler;
    constructor(protected readonly config: IConfig) {
        this.jwtAuthRequestHandler = jwt.default({
            secret: config.Secrets.PassportKey,
            algorithms: ["HS256"],
        });
    }

    /**
     * Sets up the routes for the controller
     * @param {Router} router - The default express router
     */
    public abstract defineRoutes(router: Router): void;
    protected createDiskStorage = (): StorageEngine => {
        return multer.diskStorage({
            destination: function (req, file, cb) {
                const requesterUid = (req.user as Guest)._id;
                const dir = os.platform() === "win32" ? os.tmpdir() : os.homedir();
                const filSep = os.platform() === "win32" ? "\\" : "/";
                if (!existsSync(`${dir}${filSep}${requesterUid}`)) {
                    mkdirSync(`${dir}${filSep}${requesterUid}`);
                }
                cb(null, `${dir}${filSep}${requesterUid}`);
            },
            filename: function (req, file, cb) {
                const ext = mime.extension(file.mimetype);
                const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
                cb(null, file.fieldname + "-" + uniqueSuffix + "." + ext);
            },
        });
    };
}
