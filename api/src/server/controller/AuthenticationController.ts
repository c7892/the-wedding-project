import { NextFunction, Router, Request, Response } from "express";
import multer, { StorageEngine, Multer } from "multer";
import passport from "passport";
import { inject, injectable } from "tsyringe";
import { IUserSchema } from "../../dal";
import { AuthenticationService } from "../../services/AuthenticationService";
import { Guest, User } from "../../types";
import { IConfig } from "../../utilities";
import { IController } from "./IController";

@injectable()
export class AuthenticationController extends IController {
    private readonly multerStorage: StorageEngine;
    private readonly multerInstance: Multer;
    public constructor(
        @inject("Config") config: IConfig,
        private readonly authenticationService: AuthenticationService,
    ) {
        super(config);
        this.multerStorage = this.createDiskStorage();
        this.multerInstance = multer({ storage: this.multerStorage });
    }
    public defineRoutes(router: Router): void {
        router.post("/authentication/register", this.register);
        router.delete("/authentication/user", this.jwtAuthRequestHandler, this.deleteUser);
        router.delete("/authentication/guest", this.jwtAuthRequestHandler, this.deleteGuest);
        router.post("/authentication/refreshtoken", this.jwtAuthRequestHandler, this.loginAsync);
        router.get("/authentication/user", this.jwtAuthRequestHandler, this.getUser);
        router.get("/authentication/spouse", this.jwtAuthRequestHandler, this.getSpouse);
        router.put(
            "/authentication/user",
            [this.jwtAuthRequestHandler, this.multerInstance.single("image")],
            this.updateUser,
        );
        router.put("/authentication/password", this.jwtAuthRequestHandler, this.changePassword);
        router.post(
            "/authentication/login",
            passport.authenticate("local", {
                successRedirect: undefined,
                failureRedirect: undefined,
                failureFlash: false,
                session: false,
            }),
            this.loginAsync,
        );
    }

    private register = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const body = req.body;
            const didRegister = await this.authenticationService.registerAsync(body);
            if (didRegister === true) {
                res.sendStatus(200);
            } else {
                res.status(400).json({ message: didRegister });
            }
        } catch (ex) {
            res.sendStatus(400).json({ message: ex.message });
        }
        next();
    };
    private loginAsync = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.user) {
            next();
            return;
        }
        const token = await this.authenticationService.getJwtTokenAsync((req.user as IUserSchema)._id);
        if (!token) {
            next();
            return;
        }
        res.status(200)
            .cookie(`${this.config.JwtTokenName}-jwt`, token, {
                secure: !this.config.IsDebug,
                maxAge: 60 * 60 * 1000,
                signed: true,
            })
            .send(token);
        next();
    };
    private deleteUser = async (req: Request, res: Response, next: NextFunction) => {
        const deleted = await this.authenticationService.deleteUser((req.user as IUserSchema)._id);
        res.status(200).send(deleted);
        next();
    };
    private deleteGuest = async (req: Request, res: Response, next: NextFunction) => {
        const deleted = await this.authenticationService.deleteGuest((req.user as IUserSchema)._id);
        res.status(200).send(deleted);
        next();
    };
    private getUser = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.user) {
            res.sendStatus(400);
            next();
            return;
        }
        const user = await this.authenticationService.getUserInfo((req.user as IUserSchema)._id);
        if (!user) {
            res.sendStatus(404);
            next();
            return;
        }
        res.status(200).json(user);
        next();
    };
    private getSpouse = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.user) {
            res.sendStatus(400);
            next();
            return;
        }
        const user = await this.authenticationService.getSpouses((req.user as IUserSchema).uniqueWeddingCode);
        if (!user) {
            res.sendStatus(404);
            next();
            return;
        }
        res.status(200).json(user);
        next();
    };
    private updateUser = async (
        req: Request<unknown, unknown, { updates: User | Guest }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = await this.authenticationService.updateUser(
            (req.user as IUserSchema)._id,
            req.body as unknown as User | Guest,
            req.file,
        );
        if (!user) {
            res.sendStatus(400);
            next();
            return;
        }
        res.status(200).json(user);
        next();
    };
    private changePassword = async (
        req: Request<unknown, unknown, { oldPassword: string; newPassword: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const user = await this.authenticationService.changePassword(
            (req.user as IUserSchema)._id,
            req.body.oldPassword,
            req.body.newPassword,
        );
        if (!user) {
            res.sendStatus(400);
            next();
            return;
        }
        res.status(200).json(user);
        next();
    };
}
