import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { NotesService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { IUserSchema } from "../../dal";
import { Notes } from "../../types";

@injectable()
export class NotesController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly notesService: NotesService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.post("/notes", this.jwtAuthRequestHandler, this.saveNotesAsync);
        router.get("/notes", this.jwtAuthRequestHandler, this.fetchAllNotesAsync);
        router.get("/notes/:id", this.jwtAuthRequestHandler, this.fetchNotesAsync);
        router.delete("/notes/:id", this.jwtAuthRequestHandler, this.deleteNotesAsync);
    }

    private saveNotesAsync = async (
        req: Request<unknown, unknown, { notes: Notes }>,
        res: Response,
        next: NextFunction,
    ) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const notes = await this.notesService.saveNotesAsync(weddingCode, req.body.notes);
        res.status(notes ? 200 : 400).json(notes);
        next();
    };
    private fetchAllNotesAsync = async (
        req: Request<unknown, unknown, unknown, { isPublished?: boolean }>,
        res: Response,
        next: NextFunction,
    ) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const notes = await this.notesService.fetchAllNotesAsync(weddingCode, req.query.isPublished);
        res.status(200).json(notes);
        next();
    };
    private fetchNotesAsync = async (
        req: Request<{ id: string }, unknown, unknown, unknown>,
        res: Response,
        next: NextFunction,
    ) => {
        const notes = await this.notesService.fetchNotesAsync(req.params.id);
        res.status(200).json(notes);
        next();
    };
    private deleteNotesAsync = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        const notes = await this.notesService.deleteNotes(req.params.id);
        res.status(notes ? 200 : 400).json(notes);
        next();
    };
}
