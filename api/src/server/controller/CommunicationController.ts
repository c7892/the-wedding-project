import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { CommunicationService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { Guest } from "../../types";
import Stripe from "stripe";
import { IUserSchema } from "../../dal";

@injectable()
export class CommunicationController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly communicationService: CommunicationService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.post("/message", this.jwtAuthRequestHandler, this.sendAsync);
    }
    private sendAsync = async (
        req: Request<
            unknown,
            unknown,
            {
                message: string;
                guest: Guest;
                sendMethods: string[];
                paymentPayload?: Stripe.PaymentIntent;
            }
        >,
        res: Response,
        next: NextFunction,
    ) => {
        const user = req.user as IUserSchema;
        const sent = await this.communicationService.sendMessageToGuests(
            req.body.message,
            req.body.guest,
            req.body.sendMethods,
            user.firstName,
            user.email,
            req.body.paymentPayload,
        );
        res.sendStatus(sent ? 200 : 400);
        next();
    };
}
