import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { EventsService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { IUserSchema } from "../../dal";
import { Event } from "../../types";

@injectable()
export class EventsController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly eventsService: EventsService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.post("/events", this.jwtAuthRequestHandler, this.saveEventAsync);
        router.get("/events", this.jwtAuthRequestHandler, this.fetchEventsAsync);
        router.get("/events/wedding", this.jwtAuthRequestHandler, this.fetchWeddingDay);
        router.get("/events/all", this.jwtAuthRequestHandler, this.fetchAllEventsAsync);
        router.delete("/events/:id", this.jwtAuthRequestHandler, this.deleteEventAsync);
        router.get("/events/ar", this.fetchArEventAsync);
    }

    private saveEventAsync = async (
        req: Request<unknown, unknown, { event: Event }>,
        res: Response,
        next: NextFunction,
    ) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const event = await this.eventsService.saveEventAsync(weddingCode, req.body.event);
        res.status(event ? 200 : 400).json(event);
        next();
    };
    private fetchEventsAsync = async (
        req: Request<unknown, unknown, unknown, { date?: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const events = await this.eventsService.fetchEventsAsync(weddingCode, req.query.date);
        res.status(200).json(events);
        next();
    };
    private fetchAllEventsAsync = async (req: Request, res: Response, next: NextFunction) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const userId = (req.user as IUserSchema)._id;
        const events = await this.eventsService.fetchAllEventsForUserAndWedding(weddingCode, userId);
        res.status(200).json(events);
        next();
    };
    private fetchWeddingDay = async (req: Request, res: Response, next: NextFunction) => {
        const weddingCode = (req.user as IUserSchema).uniqueWeddingCode;
        const events = await this.eventsService.fetchWeddingDay(weddingCode);
        res.status(200).json(events);
        next();
    };
    private fetchArEventAsync = async (
        req: Request<unknown, unknown, unknown, { guest: string; id: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const event = await this.eventsService.fetchEventAsync(req.query.id, req.query.guest);
        res.status(event ? 200 : 400).json(event);
        next();
    };
    private deleteEventAsync = async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
        const event = await this.eventsService.deleteEvent(req.params.id);
        res.status(event ? 200 : 400).json(event);
        next();
    };
}
