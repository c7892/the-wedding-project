import { inject, injectable } from "tsyringe";
import { NextFunction, Router, Request, Response } from "express";
import { PaymentService } from "../../services";
import { IController } from "./IController";
import { IConfig } from "../../utilities";
import { IUserSchema } from "../../dal";

@injectable()
export class PaymentController extends IController {
    public constructor(@inject("Config") config: IConfig, private readonly paymentService: PaymentService) {
        super(config);
    }

    public defineRoutes(router: Router): void {
        router.get("/payment/request_cost", this.jwtAuthRequestHandler, this.requestCost);
    }

    private requestCost = async (
        req: Request<unknown, unknown, unknown, { postCards: string; texts: string }>,
        res: Response,
        next: NextFunction,
    ) => {
        const id = (req.user as IUserSchema)._id;
        const clientSecret = await this.paymentService.createPaymentIntent(
            id,
            parseInt(`${req.query.postCards}`),
            parseInt(`${req.query.texts}`),
        );
        res.status(clientSecret ? 200 : 400).json(clientSecret);
        next();
    };
}
