import { NextFunction, Request, Response } from "express";
export const ping = (req: Request, res: Response, next: NextFunction): void => {
    if (req.path === "/api/ping") {
        res.sendStatus(200);
        return;
    }
    next();
};
