import { Request, Response, NextFunction } from "express";
import { resolve } from "path";
import { existsSync } from "fs";

export const validClientPaths = ["/", "/spouse", "/guest", "/login", "/invitation/ar"];

export const sendSite = async (req: Request, res: Response, next: NextFunction) => {
    if (req.path.toLowerCase().includes("privacypolicy")) {
        next();
        return;
    }
    if (req.path.includes("api/")) {
        next();
        return;
    }
    if (validClientPaths.indexOf(req.path) >= 0) {
        const file = resolve("./build/index.html");
        res.status(200).sendFile(file);
        return;
    }
    let sent = false;
    const pathParts = req.path.split("/").filter((part) => part.length > 0);
    validClientPaths.forEach((path) => {
        if (sent) {
            return;
        }
        if (path === "/" && pathParts.length === 0) {
            res.status(200).sendFile(resolve("./build/index.html"));
            sent = true;
            return;
        }
        if (path === "/" + pathParts[0]) {
            res.status(200).sendFile(resolve("./build/index.html"));
            sent = true;
            return;
        }
        if (path === `/${pathParts[0]}/${pathParts[1]}`) {
            res.status(200).sendFile(resolve("./build/index.html"));
            sent = true;
            return;
        }
    });
    if (sent) {
        return;
    }
    const file = resolve(`./build/${req.path}`);
    if (existsSync(file)) {
        res.status(200).sendFile(file);
        return;
    }
    next();
};
