import express, { Router, Express } from "express";
import { Server as HttpServer } from "http";
import methodOverride from "method-override";
import morgan from "morgan";
import helmet from "helmet";
import compression from "compression";
import { Server as HttpsServer } from "https";
import path from "path";
import { inject, singleton } from "tsyringe";
import * as useragent from "express-useragent";
import { handleRequests, handleResponses, OpenAPIV2 } from "express-oas-generator";
import { IConfig } from "../utilities";
import { ping, sendSite } from "./middleware";
import cors from "cors";
import { set } from "lodash";
import session from "express-session";
import cookieParser from "cookie-parser";
import passport from "passport";

@singleton()
export class Server {
    private readonly router: Router;
    private readonly appServer: Express;
    private listener: HttpServer | HttpsServer | null = null;
    private httpListener: HttpServer | null = null;

    constructor(@inject("Config") private readonly config: IConfig) {
        this.appServer = express();
        this.router = express.Router();
        this.setupMiddleWares();
    }

    public start(port: number, callback?: () => void) {
        this.appServer.use("/api", this.router);
        if (this.config.IsDebug) {
            handleRequests();
        }
        this.listener = this.appServer.listen(port, () => {
            console.log("listening on port: " + port);
            if (callback) {
                callback();
            }
        });
    }

    public getRouter = () => this.router;

    public shutdown() {
        this.listener?.close();
        this.httpListener?.close();
    }

    private setupMiddleWares() {
        if (this.config.IsDebug) {
            handleResponses(this.appServer, {
                predefinedSpec: (spec: OpenAPIV2.Document) => {
                    set(spec, "info.title", this.config.AppName);
                    set(spec, "basePath", this.config.ApiBasePath);
                    return spec;
                },
                specOutputPath: path.resolve(this.config.SwaggerOutputLocation),
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            } as any);
        }
        this.appServer.use(morgan("dev"));
        this.appServer.use(express.json({ limit: "500mb" }));
        this.appServer.use(
            session({
                secret: this.config.Secrets.PassportKey,
                cookie: {
                    maxAge: 60 * 60 * 1000,
                    secure: !this.config.IsDebug,
                },
                saveUninitialized: false,
                resave: false,
                unset: "destroy",
            }),
        );
        this.appServer.use(cookieParser(this.config.Secrets.PassportKey));
        this.appServer.use(express.urlencoded({ extended: false }));
        this.appServer.use(passport.initialize());
        this.appServer.use(passport.session());
        this.appServer.use(helmet());
        this.appServer.use(methodOverride("X-HTTP-Method-Override"));
        this.appServer.use(compression());
        this.appServer.use(useragent.express());
        this.appServer.use(ping);
        this.appServer.use(sendSite);
        this.appServer.use(
            cors({
                origin: [
                    "http://localhost:3000",
                    "http://localhost:3001",
                    "http://127.0.0.1:3000",
                    "http://192.168.86.224:3000",
                    "http://192.168.86.41:3000",
                    "http://192.168.87.24:3000",
                    "http://192.168.87.24:5000",
                    "http://192.168.86.41:5000",
                    "http://localhost:5000",
                    "https://wedding.codinghouse.dev",
                ],
                preflightContinue: true,
                credentials: true,
                exposedHeaders: ["set-cookie"],
            }),
        );
        this.appServer.options(
            cors({
                origin: [
                    "http://localhost:3000",
                    "http://localhost:3001",
                    "http://127.0.0.1:3000",
                    "http://192.168.86.224:3000",
                    "http://192.168.86.41:3000",
                    "http://192.168.86.41:5000",
                    "http://192.168.87.24:3000",
                    "http://192.168.87.24:5000",
                    "http://localhost:5000",
                    "https://wedding.codinghouse.dev",
                ],
                preflightContinue: true,
                credentials: true,
                exposedHeaders: ["set-cookie"],
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            }) as any,
        );
        this.appServer.use(express.static(path.join(__dirname, "build")));
    }
}
