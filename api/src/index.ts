import "reflect-metadata";
import { Server } from "./server";
import { forIn } from "lodash";
import { container } from "tsyringe";
import { AzureConfigurationProvider, IConfig } from "./utilities";
import { DatabaseManager, registerModels } from "./dal";
import { setupPassportStrategy } from "./services";
import { controllers } from "./server/controller";

const configProvider = container.resolve(AzureConfigurationProvider);
configProvider
    .getConfig()
    .then((config) => {
        container.register<IConfig>("Config", { useValue: config });
        const server = container.resolve(Server);
        const db = container.resolve(DatabaseManager);
        db.connect();
        db.on("connected", () => {
            server.shutdown();
            console.log("connected to mongo, starting server...");
            registerModels();
            setupPassportStrategy();
            console.log("defining routes");
            const router = server.getRouter();
            forIn(controllers(), (controller) => {
                controller.defineRoutes(router);
            });
            server.start(config.Port);
            console.log("server started.");
        });

        //for nodemon restarts
        process.once("SIGUSR2", function () {
            server.shutdown();
            // db.closeConnection();
            process.kill(process.pid, "SIGUSR2");
        });
    })
    .catch(console.log);

//for app termination
process.once("SIGINT", function () {
    process.exit(0);
});
