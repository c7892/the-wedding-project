export enum Models {
    IUserModel = "User",
    IGuestModel = "Guest",
    IGuestListModel = "GuestList",
    IEvent = "Event",
    IInvite = "Invite",
    IGallery = "Gallery",
    IRegistry = "Registry",
    IImage = "Image",
    INotes = "Notes",
}
