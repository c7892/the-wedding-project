import { sign } from "jsonwebtoken";
import mongoose, { Schema, Document, Model } from "mongoose";
import { Models } from "./Models";
import { IConfig } from "../../utilities";
import { container } from "tsyringe";

export interface IGuestSchema extends Document {
    firstName: string;
    surname: string;
    email?: string;
    mobile: string;
    profilePic?: string;
    address?: string;
    relation?: {
        [id: string]: string;
        _spouse: string;
    };
    uniqueWeddingCode: string;
    events: string[];
    dateCreated: number;
    modifiedDttm: number;
    generateJwt: () => string;
}

const GuestSchema = new Schema<IGuestSchema, IGuestModel>({
    firstName: {
        type: String,
        required: true,
        unique: false,
    },
    surname: {
        type: String,
        required: true,
        unique: false,
    },
    mobile: {
        type: String,
        required: true,
        unique: false,
    },
    email: {
        type: String,
        required: false,
        unique: false,
    },
    profilePic: {
        type: String,
        required: false,
        unique: false,
    },
    address: {
        type: String,
        required: false,
        unique: false,
    },
    uniqueWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    relation: {
        type: Object,
        required: false,
        unique: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    events: {
        type: [],
        required: true,
        default: [],
    },
});
GuestSchema.pre<IGuestSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

GuestSchema.methods.generateJwt = function () {
    const config = container.resolve<IConfig>("Config");
    const expiration = new Date();
    expiration.setTime(expiration.getTime() + 60 * 60 * 1000);
    const user = this as IGuestSchema;
    const signingKey = config.Secrets.PassportKey;
    return sign(
        {
            _id: user._id,
            email: user.email,
            firstName: user.firstName,
            surname: user.surname,
            profilePic: user.profilePic,
            uniqueWeddingCode: user.uniqueWeddingCode,
            exp: parseInt(`${expiration.getTime()}`),
        },
        signingKey,
    );
};
export type IGuestModel = Model<IGuestSchema>;
const GuestModel = mongoose.model<IGuestSchema, IGuestModel>(Models.IGuestModel, GuestSchema, Models.IGuestModel);
export default GuestModel;
