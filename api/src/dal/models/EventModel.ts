import mongoose, { Schema, Document, Model } from "mongoose";
import { Models } from "./Models";
import { Event as WeddingEvent } from "../../types";

export interface IEventSchema extends Document, WeddingEvent {}

const EventSchema = new Schema<IEventSchema, IEventModel>({
    title: {
        type: String,
        required: true,
        unique: false,
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    description: {
        type: String,
        required: true,
        unique: false,
    },
    start: {
        type: Date,
        required: true,
    },
    end: {
        type: Date,
        required: true,
    },
    timezone: {
        type: String,
        required: true,
    },
    allDay: {
        type: Boolean,
        required: false,
    },
    parking: {
        type: Boolean,
        required: false,
    },
    arePaymentsComplete: {
        type: Boolean,
        required: false,
    },
    location: {
        type: String,
        required: false,
    },
    priority: {
        type: String,
        required: false,
    },
    attire: {
        type: String,
        required: false,
    },
    rsvpList: {
        type: [],
        required: false,
        default: [],
    },
    reminders: {
        type: [],
        required: false,
        default: [],
    },
    guests: {
        type: [],
        required: false,
        default: [],
    },
    guestLists: {
        type: [],
        required: false,
        default: [],
    },
    costToYou: {
        type: Number,
        required: false,
    },
    costToGuest: {
        type: Number,
        required: false,
    },
    downPaymentAmount: {
        type: Number,
        required: false,
    },
    maxAllowedGuestsOfGuests: {
        type: Number,
        required: true,
        default: 0,
    },
    rsvpByDate: {
        type: Date,
        required: true,
        default: new Date(),
    },
    rsvpContact: {
        type: Object,
        required: false,
    },
    isWedding: {
        type: Boolean,
        required: true,
        default: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
});
EventSchema.pre<IEventSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});
export type IEventModel = Model<IEventSchema>;
const EventModel = mongoose.model<IEventSchema, IEventModel>(Models.IEvent, EventSchema, Models.IEvent);
export default EventModel;
