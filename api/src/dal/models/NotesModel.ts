import mongoose, { Schema, Document, Model } from "mongoose";
import { Notes } from "../../types";
import { Models } from "./Models";

export interface INotesSchema extends Document, Notes {}

const NotesSchema = new Schema<INotesSchema, INotesModel>({
    title: {
        type: String,
        required: true,
        unique: false,
    },
    details: {
        type: String,
        required: true,
        default: "",
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    isPublished: {
        type: Boolean,
        required: true,
        default: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
});
NotesSchema.pre<INotesSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

export type INotesModel = Model<INotesSchema>;
const NotesModel = mongoose.model<INotesSchema, INotesModel>(Models.INotes, NotesSchema, Models.INotes);
export default NotesModel;
