import mongoose, { Schema, Document, Model } from "mongoose";
import { Registry } from "../../types";
import { Models } from "./Models";

export interface IRegistrySchema extends Document, Registry {}

const RegistrySchema = new Schema<IRegistrySchema, IRegistryModel>({
    miscLinks: {
        type: [],
        required: true,
        default: [],
    },
    items: {
        type: [],
        required: true,
        default: [],
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    note: {
        type: String,
        required: true,
        default: "",
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
});
RegistrySchema.pre<IRegistrySchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

export type IRegistryModel = Model<IRegistrySchema>;
const RegistryModel = mongoose.model<IRegistrySchema, IRegistryModel>(
    Models.IRegistry,
    RegistrySchema,
    Models.IRegistry,
);
export default RegistryModel;
