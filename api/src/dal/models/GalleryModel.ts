import mongoose, { Schema, Document, Model } from "mongoose";
import { Gallery } from "../../types";
import { Models } from "./Models";

export interface IGallerySchema extends Document, Gallery {}

const GallerySchema = new Schema<IGallerySchema, IGalleryModel>({
    title: {
        type: String,
        required: true,
        unique: false,
    },
    event: {
        type: String,
        required: false,
        unique: false,
    },
    details: {
        type: String,
        required: true,
        default: "",
    },
    images: {
        type: [Schema.Types.ObjectId],
        required: true,
        default: [],
        ref: Models.IImage,
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    owners: {
        type: String,
        required: true,
        unique: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
});
GallerySchema.pre<IGallerySchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

export type IGalleryModel = Model<IGallerySchema>;
const GalleryModel = mongoose.model<IGallerySchema, IGalleryModel>(Models.IGallery, GallerySchema, Models.IGallery);
export default GalleryModel;
