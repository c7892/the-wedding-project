import mongoose, { Schema, Document, Model } from "mongoose";
import { Models } from "./Models";

export interface IGuestListSchema extends Document {
    name: string;
    ownerWeddingCode: string;
    description?: string;
    guests: string[];
    dateCreated: number;
    modifiedDttm: number;
}

const GuestListSchema = new Schema<IGuestListSchema, IGuestListModel>({
    name: {
        type: String,
        required: true,
        unique: false,
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    description: {
        type: String,
        required: false,
        unique: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    guests: {
        type: [],
        required: true,
        default: [],
    },
});
GuestListSchema.pre<IGuestListSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});
export type IGuestListModel = Model<IGuestListSchema>;
const GuestListModel = mongoose.model<IGuestListSchema, IGuestListModel>(
    Models.IGuestListModel,
    GuestListSchema,
    Models.IGuestListModel,
);
export default GuestListModel;
