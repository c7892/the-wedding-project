import mongoose, { Schema, Document, Model } from "mongoose";
import { Image } from "../../types";
import { Models } from "./Models";

export interface IImageSchema extends Document, Image {}

const ImageSchema = new Schema<IImageSchema, IImageModel>({
    src: {
        type: String,
        required: true,
        unique: false,
    },
    uploadedBy: {
        type: String,
        required: true,
        unique: false,
    },
});

export type IImageModel = Model<IImageSchema>;
const ImageModel = mongoose.model<IImageSchema, IImageModel>(Models.IImage, ImageSchema, Models.IImage);
export default ImageModel;
