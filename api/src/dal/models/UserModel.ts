import mongoose, { Schema, Document, Model } from "mongoose";
import { Models } from "./Models";
import { sign } from "jsonwebtoken";
import { randomBytes, pbkdf2Sync } from "crypto";
import { IConfig } from "../../utilities";
import { container } from "tsyringe";

export interface IUserSchema extends Document {
    firstName: string;
    surname: string;
    email: string;
    newFamilyName: string;
    profilePic?: string;
    dateCreated: number;
    modifiedDttm: number;
    hash?: string;
    salt?: string;
    mobile?: string;
    address?: string;
    uniqueWeddingCode: string;
    customerCode?: string;
    setPassword: (password: string) => void;
    validPassword: (password: string) => boolean;
    generateJwt: () => string;
}

const UserSchema = new Schema<IUserSchema, IUserModel>({
    firstName: {
        type: String,
        required: true,
        unique: false,
    },
    surname: {
        type: String,
        required: true,
        unique: false,
    },
    newFamilyName: {
        type: String,
        required: true,
        unique: false,
    },
    customerCode: {
        type: String,
        required: false,
        unique: false,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: false,
        unique: false,
    },
    mobile: {
        type: String,
        required: false,
        unique: false,
    },
    profilePic: {
        type: String,
        required: false,
        unique: false,
    },
    uniqueWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    hash: {
        type: String,
        required: false,
        unique: false,
    },
    salt: {
        type: String,
        required: false,
        unique: false,
    },
});
UserSchema.pre<IUserSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

UserSchema.methods.setPassword = function (password: string) {
    if (!password || password.length === 0) {
        throw Error("Password cannot be empty");
    }
    (this as IUserSchema).salt = randomBytes(16).toString("hex");
    (this as IUserSchema).hash = pbkdf2Sync(password, (this as IUserSchema).salt ?? "", 1000, 64, "SHA1").toString(
        "hex",
    );
};

UserSchema.methods.validPassword = function (password: string) {
    if (!(this as IUserSchema).salt) {
        return false;
    }
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const hash = pbkdf2Sync(password, (this as IUserSchema).salt!, 1000, 64, "SHA1").toString("hex");
    return (this as IUserSchema).hash === hash;
};

UserSchema.methods.generateJwt = function () {
    const config = container.resolve<IConfig>("Config");
    const expiration = new Date();
    expiration.setTime(expiration.getTime() + 60 * 60 * 1000);
    const user = this as IUserSchema;
    const signingKey = config.Secrets.PassportKey;
    return sign(
        {
            _id: user._id,
            email: user.email,
            firstName: user.firstName,
            surname: user.surname,
            newFamilyName: user.newFamilyName,
            profilePic: user.profilePic,
            uniqueWeddingCode: user.uniqueWeddingCode,
            exp: parseInt(`${expiration.getTime()}`),
        },
        signingKey,
    );
};
export type IUserModel = Model<IUserSchema>;
const UserModel = mongoose.model<IUserSchema, IUserModel>(Models.IUserModel, UserSchema, Models.IUserModel);
export default UserModel;
