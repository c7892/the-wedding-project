import mongoose, { Schema, Document, Model } from "mongoose";
import { Invite } from "../../types";
import { Models } from "./Models";

export interface IInviteSchema extends Document, Invite {}

const InviteSchema = new Schema<IInviteSchema, IInviteModel>({
    title: {
        type: String,
        required: true,
        unique: false,
    },
    inviteTitle: {
        type: String,
        required: true,
        unique: false,
    },
    event: {
        type: String,
        required: false,
        default: "",
        unique: false,
    },
    notes: {
        type: String,
        required: false,
        unique: false,
    },
    bodyColor: {
        type: String,
        required: false,
        unique: false,
    },
    scanColor: {
        type: String,
        required: false,
        unique: false,
    },
    titleColor: {
        type: String,
        required: false,
        unique: false,
    },
    title2Color: {
        type: String,
        required: false,
        unique: false,
    },
    frontImage: {
        type: String,
        required: false,
        unique: false,
    },
    backImage: {
        type: String,
        required: false,
        unique: false,
    },
    ownerWeddingCode: {
        type: String,
        required: true,
        unique: false,
    },
    dateCreated: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
    modifiedDttm: {
        type: Number,
        required: true,
        default: new Date().getTime(),
    },
});
InviteSchema.pre<IInviteSchema>("save", function (next) {
    this.modifiedDttm = new Date().getTime();
    next();
});

export type IInviteModel = Model<IInviteSchema>;
const InviteModel = mongoose.model<IInviteSchema, IInviteModel>(Models.IInvite, InviteSchema, Models.IInvite);
export default InviteModel;
