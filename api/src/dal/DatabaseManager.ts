import mongoose from "mongoose";
import { inject, singleton } from "tsyringe";
import { IConfig } from "../utilities";

@singleton()
export class DatabaseManager {
    private readonly config: IConfig;

    constructor(@inject("Config") config: IConfig) {
        this.config = config;
    }

    public connect() {
        mongoose.connect(this.config.Secrets.ConnectionString);
    }

    public closeConnection() {
        mongoose.connection.close();
    }

    public on(event: string, callback: () => void) {
        mongoose.connection.on(event, callback);
    }
}
