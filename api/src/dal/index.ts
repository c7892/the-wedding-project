import { container } from "tsyringe";
import EventModel from "./models/EventModel";
import GalleryModel from "./models/GalleryModel";
import GuestListModel from "./models/GuestListModel";
import GuestModel from "./models/GuestModel";
import ImageModel from "./models/ImageModel";
import InviteModel from "./models/InviteModel";
import NotesModel from "./models/NotesModel";
import RegistryModel from "./models/RegistryModel";
import UserModel from "./models/UserModel";

export * from "./models";
export * from "./DatabaseManager";

export const registerModels = (): void => {
    container.register("IUserModel", {
        useValue: UserModel,
    });
    container.register("IGuestModel", {
        useValue: GuestModel,
    });
    container.register("IGuestListModel", {
        useValue: GuestListModel,
    });
    container.register("IEventModel", {
        useValue: EventModel,
    });
    container.register("IInviteModel", {
        useValue: InviteModel,
    });
    container.register("IGalleryModel", {
        useValue: GalleryModel,
    });
    container.register("IRegistryModel", {
        useValue: RegistryModel,
    });
    container.register("IImageModel", {
        useValue: ImageModel,
    });
    container.register("INotesModel", {
        useValue: NotesModel,
    });
};
