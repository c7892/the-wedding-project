export interface User {
    firstName: string;
    surname: string;
    email: string;
    mobile?: string;
    address?: string;
    newFamilyName: string;
    profilePic?: string;
    uniqueWeddingCode: string;
    customerCode?: string;
}

export interface SpouseRegistrationInformation extends User {
    password: string;
}
