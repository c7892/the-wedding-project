import { Schema } from "mongoose";

export interface Gallery {
    title: string;
    event?: string;
    details: string;
    images: Schema.Types.ObjectId[];
    ownerWeddingCode: string;
    owners: string;
    dateCreated: number;
    modifiedDttm: number;
}

export interface Image {
    src: string;
    dateCreated: number;
    uploadedBy: string;
}
