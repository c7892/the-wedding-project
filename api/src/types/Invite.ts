export interface Invite {
    title: string;
    inviteTitle: string;
    notes?: string;
    event?: string;
    bodyColor?: string;
    titleColor?: string;
    title2Color?: string;
    scanColor?: string;
    frontImage?: string;
    backImage?: string;
    ownerWeddingCode: string;
    dateCreated: number;
    modifiedDttm: number;
}
