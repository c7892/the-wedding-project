export interface RegistryItem {
    name: string;
    link: string;
    purchasedBy?: string[];
    quantityPurchased: number;
    quantityWanted: number;
}

export interface Registry {
    note: string;
    miscLinks: string[];
    items: RegistryItem[];
    ownerWeddingCode: string;
    dateCreated: number;
    modifiedDttm: number;
}
