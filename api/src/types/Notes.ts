export interface Notes {
    title: string;
    details: string;
    isPublished: boolean;
    ownerWeddingCode: string;
    dateCreated: number;
    modifiedDttm: number;
}
