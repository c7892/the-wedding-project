export enum EventPriority {
    HIGH = "HIGH",
    LOW = "LOW",
    MEDIUM = "MEDIUM",
}

export interface RSVPEntry {
    hostingGuest: string;
    totalBringingWith: number;
}

export interface Event {
    ownerWeddingCode: string;
    start: Date | string;
    end: Date | string;
    timezone: string;
    allDay?: boolean;
    title: string;
    description: string;
    location?: string;
    priority?: EventPriority;
    guests: string[];
    guestLists: string[];
    attire?: string;
    rsvpList: RSVPEntry[];
    maxAllowedGuestsOfGuests: number;
    parking?: boolean;
    costToGuest?: number;
    costToYou?: number;
    downPaymentAmount?: number;
    arePaymentsComplete?: boolean;
    reminders: Date[];
    rsvpByDate: Date | string;
    rsvpContact?: {
        phone?: string;
        email?: string;
    };
    isWedding: boolean;
    dateCreated: number;
    modifiedDttm: number;
}
